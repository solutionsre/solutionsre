import React from "react";
import Conversiones from "../components/Content/Tipologias/Conversiones";
import NavigationPrimary from "../components/NavigationPrimary/NavigationPrimary";

const ConversionesRoute = (props) => {
  return (
    <section className="global-container">
      <NavigationPrimary app={props.match.params.aplicativo} />
      <Conversiones
        app={props.match.params.aplicativo}
        id={props.match.params.id}
      />
    </section>
  );
};

export default ConversionesRoute;
