import React from "react";
import Dashboard from "../components/Content/Dashboard";
import NavigationPrimary from "../components/NavigationPrimary/NavigationPrimary";

const DashboardRoute = (props) => {
  return (
    <section className="global-container">

      <NavigationPrimary app={props.match.params.aplicativo} />
      <Dashboard
        app={props.match.params.aplicativo}
        id={props.match.params.id}
      />
    </section>
  );
};

export default DashboardRoute;
