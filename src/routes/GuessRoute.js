import React, { Component } from "react";
import { Route, Redirect } from "react-router-dom";

class GuessRoute extends Component {
  render() {
    const defaultId = JSON.parse(sessionStorage.getItem("defaultId"));
    if (
      sessionStorage.getItem("isAuth") &&
      sessionStorage.getItem("defaultApp")
    ) {
      return (
        <Redirect
          to={
            "/" +
            JSON.parse(sessionStorage.getItem("defaultApp")).descripcion +
            "/Dashboard/" +
            defaultId
          }
        />
      );
    } else {
      return (
        <Route
          push={true}
          path={this.props.path}
          component={this.props.component}
          children={this.props.children}
        />
      );
    }
  }
}

export default GuessRoute;
