import React from 'react';
import NavigationPrimary from '../components/NavigationPrimary/NavigationPrimary';
import Navegaciones from '../components/Content/Tipologias/Navegaciones';

const NavegacionesRoute = (props) => {
  return (
    <section className="global-container">
      <NavigationPrimary app={props.match.params.aplicativo} />
      <Navegaciones app={props.match.params.aplicativo} id={props.match.params.id} />
    </section>
  )
}

export default NavegacionesRoute;