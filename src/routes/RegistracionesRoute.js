import React from 'react';
import Registraciones from '../components/Content/Tipologias/Registraciones';
import NavigationPrimary from '../components/NavigationPrimary/NavigationPrimary';

const RegistracionesRoute = (props) => {
    
    return (
        <section className="global-container">
            <NavigationPrimary app={props.match.params.aplicativo}/>
            <Registraciones app={props.match.params.aplicativo} id={props.match.params.id} />
        </section>
    )
}

export default RegistracionesRoute;