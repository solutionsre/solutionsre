import React from 'react';
import Mantenimientos from '../components/Content/Tipologias/Mantenimientos';
import NavigationPrimary from '../components/NavigationPrimary/NavigationPrimary';

const MantenimientosRoute = (props) => {
  return (
    <section className="global-container">
      <NavigationPrimary app={props.match.params.aplicativo} />
      <Mantenimientos app={props.match.params.aplicativo} id={props.match.params.id} />
    </section>
  )
}

export default MantenimientosRoute;