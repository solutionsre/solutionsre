import React from "react";
import Reportes from "../components/Content/Tipologias/Reportes";
import NavigationPrimary from "../components/NavigationPrimary/NavigationPrimary";

const ReportesRoute = (props) => {
  return (
    <section className="global-container">
      <NavigationPrimary app={props.match.params.aplicativo} />
      <Reportes
        app={props.match.params.aplicativo}
        id={props.match.params.id}
      />
    </section>
  );
};

export default ReportesRoute;
