import React from 'react';
import Seguridad from '../components/Content/Tipologias/Seguridad';
import NavigationPrimary from '../components/NavigationPrimary/NavigationPrimary';

const RegistracionesRoute = (props) => {
    
    return (
        <section className="global-container">
            <NavigationPrimary app={props.match.params.aplicativo}/>
            <Seguridad app={props.match.params.aplicativo} id={props.match.params.id} />
        </section>
    )
}

export default RegistracionesRoute;