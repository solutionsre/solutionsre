import React from 'react';
import NavigationPrimary from '../components/NavigationPrimary/NavigationPrimary';
import Definiciones from '../components/Content/Tipologias/Definiciones';

const DefinicionesRoute = (props) => {
  return (
    <section className="global-container">
      <NavigationPrimary app={props.match.params.aplicativo} />
      <Definiciones app={props.match.params.aplicativo} id={props.match.params.id} />
    </section>
  )
}

export default DefinicionesRoute;