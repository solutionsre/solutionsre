import React, { Component } from 'react';
import { Route, Redirect } from 'react-router-dom';

class PrivateRoute extends Component {
    
    render() {

        if (sessionStorage.getItem('isAuth')) {
            if (JSON.parse(sessionStorage.getItem('user')).cambiarclave === "S") {
                return (
                    <Redirect to="/cambiarContraseña"/>
                )
            } else {
                return (
                    <Route path={this.props.path} component={this.props.component} children={this.props.children} />
                )
            }
        } else {
            return (
                <Redirect to="/login" />
            )
        }
    }
}

export default PrivateRoute;