import React from 'react';
import Procesos from '../components/Content/Tipologias/Procesos';
import NavigationPrimary from '../components/NavigationPrimary/NavigationPrimary';

const ProcesosRoute = (props) => {
  return (
    <section className="global-container">
      <NavigationPrimary app={props.match.params.aplicativo} />
      <Procesos app={props.match.params.aplicativo} id={props.match.params.id} />
    </section>
  )
}

export default ProcesosRoute;