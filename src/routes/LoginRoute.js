import React, { Component } from 'react';
import LoginComponent from '../components/Login/LoginComponent';
import GuessRoute from './GuessRoute';

class LoginRoute extends Component{
    
    render(){
        return(
            <GuessRoute>
                <LoginComponent/>
            </GuessRoute>
        )
    }
}

export default LoginRoute;