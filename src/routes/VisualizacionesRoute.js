import React from 'react';
import NavigationPrimary from '../components/NavigationPrimary/NavigationPrimary';
import Visualizaciones from '../components/Content/Tipologias/Visualizaciones';

const VisualizacionesRoute = (props) => {
  return (
    <section className="global-container">
      <NavigationPrimary app={props.match.params.aplicativo} />
      <Visualizaciones app={props.match.params.aplicativo} id={props.match.params.id} />
    </section>
  )
}

export default VisualizacionesRoute;