import React, { useState } from "react";
import { Route, Redirect, Switch, HashRouter } from "react-router-dom";
import store from "../redux/store";

import LoginRoute from "./LoginRoute";
import ChangePasswordRoute from "./ChangePasswordRoute";
import PrivateRoute from "./PrivateRoute";
import DashboardRoute from "./DashboardRoute";
import DefinicionesRoute from "./DefinicionesRoute";
import MantenimientosRoute from "./MantenimientosRoute";
import InterfacesRoute from "./InterfacesRoute";
import RegistracionesRoute from "./RegistracionesRoute";
import ReportesRoute from "./ReportesRoute";
import SeguridadRoute from "./SeguridadRoute";
import VisualizacionesRoute from "./VisualizacionesRoute";
import ConversionesRoute from "./ConversionesRoute";
import ProcesosRoute from "./ProcesosRoute";

export default function Routes() {
  const [serverUrl, setserverUrl] = useState(null);

  if (document.getElementById("OmuniWebChat")!= undefined){
    if (sessionStorage.getItem("ticketera") !== undefined && sessionStorage.getItem("ticketera") === "SI") {
      document.getElementById('OmuniWebChat').style.visibility = "visible";
    }else{
      document.getElementById('OmuniWebChat').style.visibility = "hidden";
    }  
  }

  fetch("./url.json")
    .then((res) => res.json())
    .then(async (data) => {
      await store.dispatch({
        type: "SERVER_URL",
        serverUrl: data["SERVER_URL"],
      });
      setserverUrl(data["SERVER_URL"]);
    })
    .catch((err) => console.log(err, " error"));

  return serverUrl ? (
    <HashRouter>
      <Switch>
        <Route path="/login" component={LoginRoute} />
        <Route path="/cambiarContraseña" component={ChangePasswordRoute} />
        <PrivateRoute path="/:aplicativo" component={MenuRoutes} />
        <Redirect from="/" to="/login" />
      </Switch>
    </HashRouter>
  ) : null;
}

const MenuRoutes = (props) => {
  const path = props.match.path;
  const app = props.match.params.aplicativo;
  const defaultApp = JSON.parse(
    sessionStorage.getItem("defaultApp"),
  ).descripcion;
  const defaultId = JSON.parse(sessionStorage.getItem("defaultId"));

  return (
    <div>
      {validateUrl(app) ? (
        <Switch>
          <PrivateRoute
            path={`${path}/Dashboard/:id`}
            component={DashboardRoute}
            replace
          />
          <PrivateRoute
            path={`${path}/Definiciones/:id`}
            component={DefinicionesRoute}
            replace
          />
          {/* <PrivateRoute path={`${path}/Interfaces/:id`} component={} replace/> */}
          <PrivateRoute
            path={`${path}/Mantenimientos/:id`}
            component={MantenimientosRoute}
            replace
          />
          <PrivateRoute
            path={`${path}/Navegaciones/:id`}
            component={VisualizacionesRoute}
            replace
          />
          {/* <PrivateRoute path={`${path}/Procesos/:id`} component={} replace/>  */}
          <PrivateRoute
            path={`${path}/Registraciones/:id`}
            component={RegistracionesRoute}
            replace
          />
          <PrivateRoute
            path={`${path}/Interfaces/:id`}
            component={InterfacesRoute}
            replace
          />
          <PrivateRoute
            path={`${path}/Reportes/:id`}
            component={ReportesRoute}
            replace
          />
          <PrivateRoute
            path={`${path}/Seguridad/:id`}
            component={SeguridadRoute}
            replace
          />
          <PrivateRoute
            path={`${path}/Conversiones/:id`}
            component={ConversionesRoute}
            replace
          />
          <PrivateRoute
            path={`${path}/Procesos/:id`}
            component={ProcesosRoute}
            replace
          />
          <Redirect from={path} to={`${path}/Dashboard/` + defaultId} />
        </Switch>
      ) : (
        <Redirect to={"/" + defaultApp + "/Dashboard/" + defaultId} />
      )}
    </div>
  );
};

function validateUrl(app) {
  var validate = false;
  const apps = JSON.parse(sessionStorage.getItem("userMenu")).principal[0]
    .aplicativos;
  const appsList = apps.map((app) => app.descripcion);

  if (app) {
    if (appsList.includes(app)) {
      validate = true;
      const currentApp = apps.find((a) => a.descripcion === app);
      sessionStorage.setItem("currentApp", JSON.stringify(currentApp));
    }

    if (app === "Seguridad") {
      validate = true;
    }
  }

  return validate;
}
