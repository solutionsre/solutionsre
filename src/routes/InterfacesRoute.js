import React from 'react';
import Interfaces from '../components/Content/Tipologias/Interfaces';
import NavigationPrimary from '../components/NavigationPrimary/NavigationPrimary';

const InterfacesRoute = (props) => {
  return (
    <section className="global-container">
      <NavigationPrimary app={props.match.params.aplicativo} />
      <Interfaces app={props.match.params.aplicativo} id={props.match.params.id} />
    </section>
  )
}

export default InterfacesRoute;