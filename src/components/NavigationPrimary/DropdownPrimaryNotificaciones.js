import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';

class DropdownPrimaryNotificaciones extends Component {
    render() {
        return (
            <div className="dropdown-primary">
                
                <div className="header">
                    <div className="d-flex justify-content-between align-items-center">
                        <h4>{this.props.title}</h4>
                        <div className="close">
                            <i className="fi fi-rs-cross"></i>
                        </div>
                    </div>
                </div>

                <div className="divider"></div>

                <ul className="notificaciones-menu">
                    <li className="notificaciones-menu-li">
                        <NavLink to={"/dashboard"}>
                            <span className="contenido">
                                <span className="titulo">
                                    Lorem ipsum
                                </span>
                                <span className="descripcion">
                                    Lorem ipsum dolor sit amet
                                </span>
                            </span>
                            <button className="close">
                                <i className="fi fi-rs-cross"></i> 
                            </button>
                        </NavLink>
                    </li>
                    <div className="divider"></div>
                    <li className="notificaciones-menu-li">
                        <NavLink to={"/dashboard"}>
                                <span className="contenido">
                                <span className="titulo">
                                    Lorem ipsum
                                </span>
                                <span className="descripcion">
                                    Lorem ipsum dolor sit amet
                                </span>
                            </span>
                            <button className="close">
                                <i className="fi fi-rs-cross"></i> 
                            </button>
                        </NavLink>
                    </li>
                    <div className="divider"></div>
                    <li className="notificaciones-menu-li">
                        <NavLink to={"/dashboard"}>
                            <span className="contenido">
                                <span className="titulo">
                                    Lorem ipsum
                                </span>
                                <span className="descripcion">
                                    Lorem ipsum dolor sit amet
                                </span>
                            </span>
                            <button className="close">
                                <i className="fi fi-rs-cross"></i> 
                            </button>
                        </NavLink>
                    </li>
                    <div className="divider"></div>
                    <li className="notificaciones-menu-li">
                        <NavLink to={"/dashboard"}>
                            <span className="contenido">
                                <span className="titulo">
                                    Lorem ipsum
                                </span>
                                <span className="descripcion">
                                    Lorem ipsum dolor sit amet
                                </span>
                            </span>
                            <button className="close">
                                <i className="fi fi-rs-cross"></i> 
                            </button>
                        </NavLink>
                    </li>
                    <div className="divider"></div>
                    <li className="notificaciones-menu-li">
                        <NavLink to={"/dashboard"}>
                            <span className="contenido">
                                <span className="titulo">
                                    Lorem ipsum
                                </span>
                                <span className="descripcion">
                                    Lorem ipsum dolor sit amet
                                </span>
                            </span>
                            <button className="close">
                                <i className="fi fi-rs-cross"></i> 
                            </button>
                        </NavLink>
                    </li>
                </ul>


            </div>
        );
    }
}

export default DropdownPrimaryNotificaciones;