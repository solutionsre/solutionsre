// Generic
import React, { Fragment } from 'react';

// Components
import DropdownPrimary from './DropdownPrimary';

// Images

// Css


const CurrentNavigation = (props) => {
    var menu = null
    var app = props.app

    if (props.app && props.app !== "Seguridad"){
        const apps = JSON.parse(sessionStorage.getItem('userMenu')).principal[0].aplicativos
        const app = apps.find(app => app.descripcion === props.app)
        if (app){
            menu = app.menu
        }
    } else {
        const currentApp = JSON.parse(sessionStorage.getItem('currentApp'))
        const defaultApp = JSON.parse(sessionStorage.getItem('defaultApp'))

        if (currentApp !== null) {
            menu = currentApp.menu
        } else {
            menu = null
        }
        
        app = defaultApp.descripcion
    }

    return (
        <Fragment>
            { menu ? 
                menu.map(item => {
                    if (item.habilitado === "1")
                        return <Fragment key={item.id}>
                            <li key={item.id} className="menu-li">
                                <span className="menu-li-a">
                                    <i className={item.icono}></i>
                                    {item.texto}
                                </span>
                                <DropdownPrimary
                                    title={item.texto} data={item.menu} app={app}
                                />
                            </li>
                            <div className="divider"></div>
                        </Fragment>

                    return <Fragment key={item.id}>
                        <li key={item.id} className="menu-li deshabilitado">
                            <span className="menu-li-a">
                                <i className={item.icono}></i>
                                {item.texto}
                            </span>
                        </li>
                        <div className="divider"></div>
                    </Fragment>

                })
            : null }
        </Fragment>
    )
}

export default CurrentNavigation;