import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import profiledemo from '../../assets/images/profile-demo.png';


class DropdownPrimaryProfile extends Component {
    render() {
        var imgprofile =  'url(' + profiledemo + ')'
        
        document.documentElement.style.setProperty('--background-image', imgprofile)
        return (
            <div className="dropdown-primary">
                
                <div className="header">
                    <div className="d-flex justify-content-between align-items-center">
                        <h4>{this.props.title}</h4>
                        <div className="close">
                            <i className="fi fi-rs-cross"></i>
                        </div>
                    </div>
                </div>

                <div className="divider"></div>

                <div className="box-profile">
                    <div className="img-profile-big imagenDeFondo" /*style={imgprofile}*/>
                    </div>
                    <h3>Nombre y apellido</h3>
                    <h4>email@dominio.com</h4>
                </div>

                <div className="divider"></div>

                <h3>Opciones</h3>

                <ul className="primary-menu">
                    <li className="primary-menu-li">
                        <Link to={"/dashboard"}>
                            Option menu
                        </Link>
                    </li>
                    <li className="primary-menu-li">
                        <Link to={"/dashboard"}>
                            Option menu
                        </Link>
                    </li>
                    <li className="primary-menu-li">
                        <Link to={"/dashboard"}>
                            Option menu
                        </Link>
                    </li>
                    <li className="primary-menu-li">
                        <Link to={"/dashboard"}>
                            Option menu
                        </Link>
                    </li>
                </ul>


            </div>
        );
    }
}

export default DropdownPrimaryProfile;