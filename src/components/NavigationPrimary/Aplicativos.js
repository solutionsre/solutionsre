// Generic
import React from 'react';

// Components
import DropdownPrimaryProductos from './DropdownPrimaryProductos';

// Images

// Css

const Aplicativos = () => {

    return (
        <li className="menu-li">
            <span className="menu-li-a">
                <i className="fi fi-rs-apps"></i>
                Aplicativos
            </span>
            <DropdownPrimaryProductos
                title={"Aplicativos"}
            />
        </li>
    );
}

export default Aplicativos;