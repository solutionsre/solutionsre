// Generic
import React from "react";
import { NavLink } from "react-router-dom";

// Components
import Aplicativos from "./Aplicativos";
import CurrentNavigation from "./CurrentNavigation";

// Images
import logo from "../../assets/images/favicon.png";

// Css
import "./NavigationPrimary.css";

const NavigationPrimary = (props) => {
  const defaultApp = JSON.parse(
    sessionStorage.getItem("defaultApp"),
  ).descripcion;
  const defaultId = JSON.parse(sessionStorage.getItem("defaultId"));
  const informacion = JSON.parse(sessionStorage.getItem("applicationInfo"));
  return (
    <div
      className="navigation-primary menu-open"
      id="navigationPrimary"
    >
      {/* Logo */}
      {/* <div className="flogo">
        <NavLink
          to={"/" + defaultApp + "/Dashboard/" + defaultId}
          className="img-brand"
        >
          <img src={logo} alt="" />
        </NavLink>
      </div> */}

      {/* Logo */}
      <nav className="navigation-primary-nav">
        <button id="scroll-up">
          <i className="fas fa-sort-up"></i>
        </button>
        <ul className="menu" id="scroll">
          <Aplicativos />
          <div className="divider"></div>
          <CurrentNavigation app={props.app} />
        </ul>
      </nav>
      <div className="application-info">
        <p>Ver: {informacion.version}</p>
        <p>Rev: {informacion.revision}</p>
      </div>
    </div>    
  );
};

export default NavigationPrimary;
