// Generic
import React, { Component } from "react";
import { Link } from "react-router-dom";

// Components

// Images
import flowmanager from "../../assets/images/flowmanager.png";
import layout from "../../assets/images/layout.png";
import monitoring from "../../assets/images/monitoring.png";
import ecommerce from "../../assets/images/ecommerce.png";
import consultoria from "../../assets/images/consultoria.png";
import bi from "../../assets/images/bi.png";
import solutions from "../../assets/images/solutions.png";
import portal from "../../assets/images/portal.png";
import ecustomer from "../../assets/images/ecustomer.png";
import ingresos from "../../assets/images/ingresos.png";

var images = {
  flowmanager: flowmanager,
  layout: layout,
  monitoring: monitoring,
  ecommerce: ecommerce,
  consultoria: consultoria,
  bi: bi,
  solutions: solutions,
  portal: portal,
  ecustomer: ecustomer,
  ingresos: ingresos,
};

// Css

class DropdownPrimaryProductos extends Component {
  render() {
    const apps = JSON.parse(sessionStorage.getItem("userMenu")).principal[0]
      .aplicativos;
    const currentApp = JSON.parse(sessionStorage.getItem("currentApp"));

    return (
      <div className="dropdown-primary">
        <div className="header">
          <div className="d-flex justify-content-between align-items-center">
            <h4>{this.props.title}</h4>
          </div>
        </div>

        <div className="divider"></div>

        <ul className="products-menu">
          {apps
            ? apps.map((app) => {
                return (
                  <li key={app.id} className="products-menu-li">
                    <Link to={"/" + app.descripcion + "/Dashboard/" + app.id}>
                      <span className="icon">
                        <img src={images[app.imagen]} alt={app.descripcion} />
                      </span>
                      {app.descripcion}
                      {app.id === currentApp.id && (
                        <div className="span new">
                          <p>En uso</p>
                        </div>
                      )}
                    </Link>
                  </li>
                );
              })
            : null}
        </ul>
      </div>
    );
  }
}

export default DropdownPrimaryProductos;
