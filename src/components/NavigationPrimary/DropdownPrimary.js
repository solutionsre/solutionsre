import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Tooltip from 'react-simple-tooltip';
import Favoritos from '../GlobalComponents/Favoritos';
import $ from 'jquery';

class DropdownPrimary extends Component {
    constructor(props) {
        super(props);
        this.state = {
            filter: "",
            data: this.props.data,
            update: []
        }

        this.handleClickCloseMenu = this.handleClickCloseMenu.bind(this)
    }

    handleChange = event => {
        this.setState({ filter: event.target.value });
    };

    // handleDropdown(){
    //     $('.dropdown-primary').css("display", "none!important");
    // }

    handleClickCloseMenu(){
        $('#btnMenu').toggleClass('active')
        $('#navigationPrimary').toggleClass('menu-open')
    }

    render() {
        var favoritos = [];
        var favoritosMenu = [];

        const { filter, data } = this.state;
        const lowercasedFilter = filter.toLowerCase();
        const filteredData = data.filter(item => {
            return Object.keys(item).some(key => {
                    if (item[key].charAt) {
                        return item[key].toLowerCase().includes(lowercasedFilter)
                    }else{
                        return "";
                    }
                }
            );

        });

        return (
            <div className="dropdown-primary">

                <div className="header">
                    <div className="d-flex justify-content-between align-items-center">
                        <h4>{this.props.title}</h4>
                        {/* <div className="close" onClick={this.handleDropdown}>
                            <i className="fi fi-rs-cross"></i>
                        </div> */}
                    </div>
                </div>

                <div className="divider"></div>

                <div className="search-group">
                    <input value={filter} onChange={this.handleChange} className="input-search" type="text" placeholder="Buscar"></input>
                    <span className="button-search">
                        <i className="fi fi-rs-search"></i>
                    </span>
                </div>


                {
                    this.props.data.forEach(item => {
                        favoritos.push(item.favorito)
                    })
                }
                {
                    favoritos.includes("1") &&
                    <div>
                        <div className="divider"></div>
                        <h3>Favoritos</h3>
                    </div>
                }

                <ul className="primary-menu">
                    {
                        filteredData.map((item) => {
                            if (item.favorito === "1")
                                return <li className="primary-menu-li favorite" key={item.id}>
                                    <Favoritos id={item.id} operacion="ELIMINAR"></Favoritos>
                                    <Link onClick={() => this.handleClickCloseMenu()} to={"/" + this.props.app + "/" + item.tipologia + "/" + item.id}>
                                        <span>
                                            <i className={`${item.icono} mr-2`}></i>
                                            {item.texto}
                                        </span>
                                    </Link>
                                </li>
                            return false
                        })
                    }
                </ul>

                {
                    this.props.data.forEach(item => {
                        favoritosMenu.push(item.favorito)
                    })
                }

                {
                    favoritosMenu.includes("0") &&
                    <div>
                        <div className="divider"></div>
                        <h3>Menú</h3>
                    </div>
                }

                <ul className="primary-menu">
                    {
                        filteredData.map((item) => {
                            if (item.favorito !== "1" && item.habilitado === "1")
                                return <li className="primary-menu-li flex" key={item.id}>
                                    <Link onClick={() => this.handleClickCloseMenu()} to={"/" + this.props.app + "/" + item.tipologia + "/" + item.id}>
                                        <span>
                                            <i className={`${item.icono} mr-2`}></i>
                                            {item.texto}
                                        </span>
                                    </Link>
                                    <span className="icons">
                                        {/* <Tooltip content={item.observaciones}>
                                            <i className="fi fi-rs-interrogation mr-2"></i>
                                        </Tooltip> */}
                                        <Favoritos className="ml-2" id={item.id} operacion="AGREGAR"></Favoritos>
                                    </span>
                                </li>
                            return false
                        })
                    }
                </ul>

            </div >
        );
    }
}

export default DropdownPrimary;