import React, { Component } from "react";
import Input from "../GlobalComponents/Input";
import "./RightOverlay.css";
import store from "../../redux/store";
import _ from "lodash";
import URLS from "../../urls";
import $ from "jquery";
import { getElement } from "wijmo/wijmo";

class RightOverlay extends Component {
  constructor(props) {
    super(props);
    this.state = {
      resetAdjunto: false,
      data: [],
      files: {},
      tipologiaInfo: [],
      buttonDisable: false,
      validation: "needs-validation",
      state: 0,
      errorsData: [],
      newHerencia: [],
    };
    this.setFileValor = this.setFileValor.bind(this);
  }

  componentDidUpdate(prevProps) {
    if (prevProps.overlayShow !== this.props.overlayShow) {
      this.setState({
        overlayShow: this.props.overlayShow,
      });
    }
    if (prevProps.data !== this.props.data) {
      if (this.props.data) {
        this.setState({
          data: this.props.data,
        });
      }
    }
    if (prevProps.state !== this.props.state) {
      this.setState({
        state: this.props.state,
      });
    }
    if (prevProps.handleState !== this.props.handleState) {
      this.setState({
        buttonDisable: false,
      });
    }
  }

  componentDidMount() {
    if (this.props.data) {
      this.setState({
        data: this.props.data,
      });
    }
  }

  async handleChange(e, condiciones, sineventos) {
    if (e.target) {
      var { value, name, type } = e.target;
      var target = e.target;
    } else if (e.showDropDownButton) {
      var value = e.selectedValue;
      var name = e._tbx.name;
      var type = e._tbx.type;
      var target = e._tbx;
    } else {
      var value = e.value;
      var name = e._tbx.name;
      var type = e._tbx.type;
      var target = e._tbx;
    }
    if (target === undefined) {
      if (e.type === "file") {
        if (e.files.length !== 0) {
          const file = e.files[0];

          const fileToBase64 = (file) =>
            new Promise((resolve, reject) => {
              const reader = new FileReader();
              reader.readAsDataURL(file);

              reader.onload = () =>
                resolve(reader.result.replace(/^data:.+;base64,/, ""));

              reader.onerror = (error) => reject(error);
            });

          const files = this.state.files;
          files[name] = await fileToBase64(file);
          this.setState({
            fileValor: {
              // "interno": e.name,
              interno: e.getAttribute("interno"),
              archivo: e.getAttribute("custom-name"),
              adjunto: await fileToBase64(file),
            },
          });
        } else {
          this.setState({
            fileValor: {
              // "interno": e.name,
              interno: e.getAttribute("interno"),
              archivo: "",
              adjunto: "",
            },
          });
        }
      } else {
        this.handleReset();
      }
    } else {
      if (target && type === "file") {
        if (target.files && target.files[0]) {
          const file = target.files[0];

          value = _.get(file, `name`, "");

          const fileToBase64 = (file) =>
            new Promise((resolve, reject) => {
              const reader = new FileReader();
              reader.readAsDataURL(file);

              reader.onload = () =>
                resolve(reader.result.replace(/^data:.+;base64,/, ""));

              reader.onerror = (error) => reject(error);
            });

          const files = this.state.files;
          files[name] = await fileToBase64(file);
          const internoAdjuntos = this.props.editando
            ? this.state.data[24]
            : "";

          this.setState({
            fileValor: {
              // "interno": e.target.name,
              /*   interno: internoAdjuntos,
              archivo: file.name, */
              interno: target.getAttribute("interno"),
              archivo: target.getAttribute("custom-name"),
              adjunto: await fileToBase64(file),
            },
          });
        }
      }

      if (condiciones && condiciones.length > 0) {
        this.setState(({ data }) => ({
          data: {
            ...data,
            [name]: value,
            condiciones: condiciones,
          },
        }));
      } else {
        this.setState(({ data }) => ({
          data: {
            ...data,
            [name]: value,
          },
        }));
      }
    }
  }

  handleCloseErroModal() {
    this.props.handleState(0);
    this.setState({
      buttonDisable: false,
      validation: "needs-validation",
      errorsData: [],
    });
  }

  handleCloseThis(render) {
    this.props.overlayShowHide();
    this.props.handleState(0);
    this.setState({
      data: this.props.data,
      buttonDisable: false,
      validation: "needs-validation",
      errorsData: [],
    });
    if (render) {
      store.dispatch({
        type: "RE_RENDER",
        rerender: true,
      });
    }
  }

  handleKeyDown(e) {
    if (e.key === "Enter") {
      this.handleSubmit(e);
    }
  }

  /* Validacion */

  handleValidation() {
    var validacion = true;
    const errorsData = [];

    if (this.state.data) {
      this.props.atributos.map((art) => {
        if (this.state.data[art.id] != undefined) {
          if (art.tipologia === "TEXTO") {
            if (art.tipo === "CARACTER" && art.estilo === "MAYUSCULAS") {
              var input = $("label[id = label" + art.id + "]");
              input.removeClass("is-invalid");
              const newState = this.state.data;

              newState[art.id] = newState[art.id].toUpperCase();
              this.setState({
                data: newState,
              });
            }
            if (art.tipo === "NUMERICO") {
              var input = $("label[id = label" + art.id + "]");
              input.removeClass("is-invalid");
              if (
                (art.maximo &&
                  parseFloat(this.state.data[art.id]) >
                    parseFloat(art.maximo)) ||
                (art.minimo &&
                  parseFloat(this.state.data[art.id]) < parseFloat(art.minimo))
              ) {
                const newState = this.state.data;
                newState[art.id] = "";
                validacion = false;
                errorsData.push(
                  "El numero ingresado en " +
                    art.descripcion +
                    " supera los limites"
                );
              }
            }
          } else if (art.tipologia === "ADJUNTO") {
            if (
              art.obligatorio === "S" &&
              this.state.fileValor &&
              this.state.fileValor.adjunto === ""
            ) {
              var input = $("span[id = label-adjunto]");
              input.addClass("label-adjunto-invalido");
              validacion = false;
            } else {
              var input = $("span[id = label-adjunto]");
              input.removeClass("label-adjunto-invalido");
            }
          } else if (art.tipologia === "FECHA") {
            var input = $("label[id = label" + art.id + "]");
            input.removeClass("is-invalid");
          }
        } else {
          if (art.obligatorio === "S") {
            if (art.tipo === "ADJUNTO") {
              var input = $("span[id = label-adjunto]");
              input.addClass("label-adjunto-invalido");
            } else {
              var input = $("label[id = label" + art.id + "]");
              input.addClass("is-invalid");
            }
            validacion = false;
          }
        }

        return validacion;
      });
    } else {
      validacion = false;
    }

    this.setState({
      errorsData: errorsData,
    });

    return validacion;
  }

  setFileValor = (e) => {
    this.setState({
      fileValor: e,
    });
  };

  /* Submit */

  async handleSubmit() {
    this.setState({
      validation: "needs-validation was-validated",
    });

    if (this.handleValidation()) {
      this.setState({
        buttonDisable: true,
      });
      var arrayTemp = [];

      //console.log(this.props.atributos)
      //console.log(this.state.data)

      const atributos = Object.entries(this.props.atributos).map((art) => {
        var value = "";

        const cargados = Object.entries(this.state.data).map((car) => {
          if (art[1].id === car[0]) {
            if (art[1].tipologia === "ADJUNTO") {
              if (this.state.fileValor) {
                value = {
                  interno: this.state.fileValor.interno,
                  archivo: this.state.fileValor.archivo,
                  adjunto: this.state.fileValor.adjunto,
                };
              } else {
                value = {
                  interno: car[1],
                  archivo: "",
                  adjunto: "",
                };
              }
            } else if (art[1].calculado == "") {
              value = car[1];
              //console.log("id: "+art[1].id+" calculado: " +value)
            } else {
              value = $("input[name = " + art[1].id + "]").val();
              //console.log("defecto id: "+art[1].id+" value: "+value)
            }
          }
        });

        if (value === null || value === "") {
          value = "";
        }
        //console.log("id: "+art[1].id+" viendo value: "+value)
        // Filtro para campo Cantidad Operaciones

        if (
          art[1].id == 4 &&
          $("input[name = " + art[1].id + "]").val() == ""
        ) {
          value = "";
        }
        // Filtro para Importe de ventas
        if (
          art[1].id == 36 &&
          $("input[name = " + art[1].id + "]").val() == ""
        ) {
          value = "";
        }

        arrayTemp.push({
          id: art[1].id,
          valor: value,
        });
      });

      // console.log(arrayTemp)

      var arrayTemporal = [];

      //console.log(this.props.atributos)

      for (let i = 0; i < this.props.atributos.length; i++) {
        if (
          this.props.atributos[i].visible === "S" ||
          this.props.atributos[i].clave === "PRINCIPAL" ||
          this.props.atributos[i].clave === "UNICA"
        ) {
          //console.log(this.props.atributos[i].id)
          var encontrado = false;
          for (let z = 0; z < arrayTemp.length; z++) {
            if (arrayTemp[z].id === this.props.atributos[i].id) {
              arrayTemporal.push({
                id: arrayTemp[z].id,
                valor: arrayTemp[z].valor,
              });
              encontrado = true;
            }
          }
          if (!encontrado) {
            arrayTemporal.push({
              id: this.props.atributos[i].id,
              valor: "",
            });
          }
        }
      }
      //console.log(arrayTemporal);
      await this.props.handleSubmit(arrayTemporal);
    }
  }
  /* Render */

  render() {
    switch (this.state.state) {
      case 400:
        return this.renderErrorModal();
      case 200:
        return this.renderSuccessModal();
      case 201:
        return this.renderSuccessModal();
      default:
        return this.renderInputModal();
    }
  }

  /* Error Modal */

  renderErrorModal() {
    return (
      this.props.overlayShow && (
        <div
          className={`right-content bg-white shadow-lg ${this.props.size} w-${this.props.edicion}`}
        >
          <div className="right-header">
            <div className="row d-flex justify-content-between align-items-center">
              <div className="titleContainer">
                <h6>Error</h6>
              </div>
              <div className="buttonContainer">
                <button
                  className="btn btn-close"
                  onClick={() => this.handleCloseThis(false)}
                >
                  <i className="fi fi-rs-cross"></i>
                </button>
              </div>
            </div>
          </div>
          <div className="right-body bg-white">
            <div className="row mt-5">
              <div className="error col-lg-12 d-flex flex-column justify-content-center align-items-center">
                <div className="iconContainer">
                  <i className="fi fi-rs-cross"></i>
                </div>
                <p>Ocurrió un error</p>
                {this.props.errorMessages
                  ? this.props.errorMessages.map((error, i) => {
                      return (
                        <div
                          className="alert alert-danger"
                          role="alert"
                          key={i}
                        >
                          <div
                            className="col-lg-12 wordBreak"
                          >
                            {error.mensaje}
                          </div>
                        </div>
                      );
                    })
                  : null}
              </div>
            </div>
          </div>
          <div className="right-footer d-flex justify-content-center align-items-stretch">
            <button
              disabled={this.state.buttonDisable}
              className="btn btn-primary"
              onClick={() => this.handleCloseErroModal()}
            >
              Volver
            </button>
          </div>
        </div>
      )
    );
  }

  /* Success Modal */

  renderSuccessModal() {
    return (
      this.props.overlayShow && (
        <div
          className={`right-content bg-white shadow-lg ${this.props.size} w-${this.props.edicion}`}
        >
          <div className="right-header">
            <div className="row d-flex justify-content-between align-items-center">
              <div className="titleContainer">
                <h6>Actualizado con éxito</h6>
              </div>
              <div className="buttonContainer">
                <button
                  className="btn btn-close"
                  onClick={() => this.handleCloseThis(true)}
                >
                  <i className="fi fi-rs-cross"></i>
                </button>
              </div>
            </div>
          </div>
          <div className="right-body bg-white">
            <div className="row mt-5">
              <div className="success col-lg-12 d-flex flex-column justify-content-center align-items-center">
                <div className="iconContainer">
                  <i className="fi fi-rs-social-network"></i>
                </div>
                <p>Se actualizó con éxito!</p>
              </div>
            </div>
          </div>
          <div className="right-footer d-flex justify-content-center align-items-stretch">
            <button
              disabled={this.state.buttonDisable}
              className="btn btn-primary"
              onClick={() => this.handleCloseThis(true)}
            >
              Cerrar
            </button>
          </div>
        </div>
      )
    );
  }

  handleReset() {
    this.setState((prevState) => ({
      resetAdjunto: !prevState.resetAdjunto,
      value: "",
    }));
  }

  handleVisible(visible, id) {
    let newVisible = visible;

    const condiciones = this.state.data.condiciones;

    if (condiciones && condiciones.length > 0) {
      condiciones.map((condicion) => {
        if (
          condicion.des_atributo === id &&
          condicion.des_propiedad === "VISIBLE"
        ) {
          const valor = this.state.data[condicion.eva_atributo];

          switch (condicion.eva_operador) {
            case "=":
              if (valor === condicion.eva_valor) {
                newVisible = condicion.des_valor;
              }
              break;
            case ">":
              if (valor > condicion.eva_valor) {
                newVisible = condicion.des_valor;
              }
              break;
            case "<":
              if (valor < condicion.eva_valor) {
                newVisible = condicion.des_valor;
              }
              break;
            case "<>":
              if (valor !== condicion.eva_valor) {
                newVisible = condicion.des_valor;
              }
              break;
          }
        }
      });
    }

    return newVisible;
  }

  handleEditable(editable, id) {
    let newEditable = editable;
    const condiciones = this.state.data.condiciones;

    if (condiciones && condiciones.length > 0) {
      condiciones.map((condicion) => {
        if (
          condicion.des_atributo === id &&
          condicion.des_propiedad === "EDITABLE"
        ) {
          const valor2 = this.state.data[condicion.eva_atributo];
          switch (condicion.eva_operador) {
            case "=":
              if (valor2 === condicion.eva_valor) {
                newEditable = condicion.des_valor;
              }
              break;
            case ">":
              if (valor2 > condicion.eva_valor) {
                newEditable = condicion.des_valor;
              }
              break;
            case "<":
              if (valor2 < condicion.eva_valor) {
                newEditable = condicion.des_valor;
              }
              break;
            case "<>":
              if (valor2 !== condicion.eva_valor) {
                newEditable = condicion.des_valor;
              }
              break;
          }
        }
      });
    }
    return newEditable;
  }

  handleObligatorio(obligatorio, id, tipo) {
    let newObligatorio = obligatorio;
    const condiciones = this.state.data.condiciones;

    if (tipo != "ADJUNTO") {
      if (condiciones && condiciones.length > 0) {
        condiciones.map((condicion) => {
          if (
            condicion.des_atributo === id &&
            condicion.des_propiedad === "OBLIGATORIO"
          ) {
            const valor3 = this.state.data[condicion.eva_atributo];
            switch (condicion.eva_operador) {
              case "=":
                if (valor3 === condicion.eva_valor) {
                  newObligatorio = condicion.des_valor;
                }
                break;
              case ">":
                if (valor3 > condicion.eva_valor) {
                  newObligatorio = condicion.des_valor;
                }
                break;
              case "<":
                if (valor3 < condicion.eva_valor) {
                  newObligatorio = condicion.des_valor;
                }
                break;
              case "<>":
                if (valor3 !== condicion.eva_valor) {
                  newObligatorio = condicion.des_valor;
                }
                break;
            }
          }
        });
      }
    }

    return newObligatorio;
  }

  /* Render Input Modal */

  renderInputModal() {
    return this.props.overlayShow ? (
      <div className="right-overlay" key={this.props.atributos[0].id}>
        <div
          className={`right-content bg-white shadow-lg ${this.props.size} w-${this.props.edicion}`}
        >
          <div className="right-header">
            <div className="row d-flex justify-content-between align-items-center">
              <div className="titleContainer">
                {this.props.data && this.props.menu !== "mapas" ? (
                  this.props.editar === true ? (
                    <h6>Editar</h6>
                  ) : (
                    <h6>Agregar</h6>
                  )
                ) : (
                  <h6>Añadir</h6>
                )}
              </div>
              <div className="buttonContainer">
                <button
                  className="btn btn-close"
                  onClick={() => this.handleCloseThis(false)}
                >
                  <i className="fi fi-rs-cross"></i>
                </button>
              </div>
            </div>
          </div>

          <div className="right-body bg-white">
            <div className="row">
              <div className="col-lg-12 mt-3">
                <form
                  className={this.state.validation}
                  noValidate
                  autoComplete="off"
                  onKeyDown={(e) => this.handleKeyDown(e)}
                >
                  {this.props.atributos.map((art) => {
                    /* console.log(art); */
                    /*
                    if (art.tipo === "NUMERICO"){
                      if (this.state.data[art.id] == undefined){
                        const newState = this.state.data;
                        newState[art.id] = 0;
                        this.setState({
                          data: newState,
                        });  
                      }
                    }  
                    */

                    return (
                      <Input
                        reset={(e) => this.handleReset(e)}
                        condiciones={art.condicionales}
                        alineacion={art.alineacion}
                        ancho={art.ancho}
                        atr_valor={art.atr_valor}
                        atr_nombre={art.atr_nombre}
                        decimales={art.decimales}
                        descripcion={art.descripcion}
                        editable={this.handleEditable(art.editable, art.id)}
                        estilo={art.estilo}
                        extensions={art.extensiones}
                        id={art.id}
                        key={art.id}
                        longitud={art.longitud}
                        mascara={art.mascara}
                        maximo={art.maximo}
                        minimo={art.minimo}
                        nombre={art.nombre}
                        obligatorio={this.handleObligatorio(
                          art.obligatorio,
                          art.id,
                          art.tipo
                        )}
                        observaciones={art.observaciones}
                        onChange={(e) =>
                          this.handleChange(e, art.condicionales)
                        }
                        orden={art.orden}
                        origen={art.origen}
                        prefijo={art.prefijo}
                        sufijo={art.sufijo}
                        tipo={art.tipo}
                        dia={this.props.dia}
                        tipologia={art.tipologia}
                        value={this.state.data[art.id]}
                        herencia={this.state.newHerencia}
                        visible={this.handleVisible(art.visible, art.id)}
                        onChangeSeleccion={(e) => this.handleChange(e)}
                        objeto={this.props.objeto}
                        opcion={this.props.opcion}
                        atributos={
                          art.visualizacion !== undefined
                            ? art.visualizacion.atributos
                            : null
                        }
                        atributo={art.id}
                        editar={this.props.editar}
                        arrayAtributos={this.props.atributos}
                        setFileValor={(e) => this.setFileValor(e)}
                      />
                    );
                  })}
                </form>
                {this.state.errorsData
                  ? this.state.errorsData.map((error, i) => {
                      return (
                        <div
                          className="alert alert-danger"
                          role="alert"
                          key={i}
                        >
                          {error}
                        </div>
                      );
                    })
                  : null}
              </div>
            </div>
          </div>
          {this.renderButtonsAction()}
        </div>
      </div>
    ) : null;
  }

  /* Render Botones */

  renderButtonsAction() {
    return this.state.buttonDisable ? (
      <div className="right-footer d-flex justify-content-center align-items-stretch">
        <button
          disabled={this.state.buttonDisable}
          className="btn btn-primary mr-2"
          onClick={() => this.handleButtonShow()}
        >
          <span
            className="spinner-grow spinner-grow-sm"
            role="status"
            aria-hidden="true"
          ></span>
          <span className="sr-only">Loading...</span>
          <span
            className="spinner-grow spinner-grow-sm"
            role="status"
            aria-hidden="true"
          ></span>
          <span className="sr-only">Loading...</span>
          <span
            className="spinner-grow spinner-grow-sm"
            role="status"
            aria-hidden="true"
          ></span>
          <span className="sr-only">Loading...</span>
          Guardando...
        </button>
      </div>
    ) : (
      <div className="right-footer d-flex justify-content-center align-items-stretch">
        <button
          disabled={this.state.buttonDisable}
          className="btn btn-secondary mr-2"
          onClick={() => this.handleCloseThis(false)}
        >
          Cancelar
        </button>
        <button
          disabled={this.state.buttonDisable}
          className="btn btn-primary"
          onClick={() => this.handleSubmit()}
        >
          Guardar
        </button>
      </div>
    );
  }
}
export default RightOverlay;
