import React, { Component } from "react";

class LoaderWithText extends Component {
  render() {
    return (
      <div className="d-flex justify-content-center align-items-center">
        <div className="btn btn-primary" type="button">
          <span
            className="spinner-border mr-2"
            role="status"
            aria-hidden="true"
          ></span>
          Procesando...
        </div>
      </div>
    );
  }
}

export default LoaderWithText;
