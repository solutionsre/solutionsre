import React, { Component, Fragment } from 'react';
import store from '../../redux/store';
import URLS from '../../urls';

class Favoritos extends Component {

    constructor(props) {
        super(props)
        this.state = {
            id: this.props.id
        }
        this.fetchData = this.fetchData.bind(this)

    }

    fetchData() {
        var myHeaders = new Headers();
        myHeaders.append("Authorization", "Bearer " + sessionStorage.getItem('token'));
        myHeaders.append("Content-Type", "application/json");

        var raw = JSON.stringify({
            "configuraciones": {
                "configuracion": [
                    {
                        "clave": "FAVORITOS_MENU",
                        "operacion": this.props.operacion,
                        "valor": this.props.id
                    }
                ]
            }
        })

        var requestOptions = {
            method: "POST",
            headers: myHeaders,
            body: raw,
            redirect: 'follow'
        };

        fetch(store.getState().serverUrl + URLS.CONFIGURACION_GUARDAR, requestOptions)
            .then(response => response.text())
            .then(result => {
                this.fetchMenu();
            })
            .catch(error => console.log('error', error));
    }

    fetchMenu() {
        var myHeaders = new Headers();
        myHeaders.append("Authorization", "Bearer " + sessionStorage.getItem('token'));

        var requestOptions = {
            method: 'GET',
            headers: myHeaders,
            redirect: 'follow'
        };

        fetch(store.getState().serverUrl + URLS.AUTENTICACION_OBTENER_MENU, requestOptions)
            .then(response => response.json())
            .then(result => {
                sessionStorage.setItem('userMenu', JSON.stringify(result.menu));
                
                window.location.reload();
            })
            .catch(error => console.log('error', error));
    }


    render() {
        return (
            <Fragment>
                <i onClick={this.fetchData} className="fi fi-rs-star mr-2"></i>
            </Fragment>
        );
    }
}

export default Favoritos;