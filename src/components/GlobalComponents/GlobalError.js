import React, { Component } from 'react';

class GlobalError extends Component {

    constructor(props) {
        super(props)
    }

    render() {
        return (
            <div className="global-error">
                Error
            </div>
        );
    }
}

export default GlobalError;