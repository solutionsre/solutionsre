import React, { Component } from 'react';

class AlertForm extends Component {
    render() {

        var type = this.props.type;
        var divClass = "alert alert-success text-left mt-4";

        switch (type) {
            case "ERROR":
                divClass = 'alert alert-warning text-left mt-4';
                break;
            case "OK":
                divClass = 'alert alert-success text-left mt-4';
                break;
            default:
                break;
        }
        if (this.props.show === false) {
            return false;
        } else {
            return (
                <div className={divClass} role="alert">
                    <h6 className="alert-heading"><i className="fi fi-rs-triangle-warning mr-2"></i> {this.props.type}</h6>
                    <p>{this.props.description}</p>
                    <hr />
                    <p className="mb-0">Secuencia: {this.props.code}</p>
                </div>
            );
        }
    }
}

export default AlertForm;