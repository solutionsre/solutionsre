import React, { Component } from "react";
// import $ from 'jquery';
import Loader from "../GlobalComponents/Loader";
import NumberFormat from "react-number-format";
import DatePicker, { registerLocale } from "react-datepicker";
import _ from "lodash";
import "wijmo/wijmo.touch";
import { FlexGrid, FlexGridColumn } from "wijmo/wijmo.react.grid";
import { FlexGridFilter } from 'wijmo/wijmo.react.grid.filter';
import { CollectionView } from "wijmo/wijmo";
import { FlexGridSearch } from 'wijmo/wijmo.react.grid.search';
import StringMask from "string-mask";
import { InputNumber, ComboBox, InputDate } from "wijmo/wijmo.react.input";
import { connect } from "react-redux";

import en from "date-fns/locale/en-US";
import es from "date-fns/locale/es";
import URLS from "../../urls";
import store from "../../redux/store";

import $ from "jquery";

class Input extends Component {
  constructor(props) {
    super(props);

    this.theGrid = React.createRef();
    this.theSearch = React.createRef();
    
    this.state = {
      disabled: false,
      required: false,
      style: null,
      language: "en",
      tipologiaInfo: [],
      showModalState: false,
      modalLoading: false,
      mensajeError: true,
      buttonDownload: false,
      editFile: false,
      value: undefined,
      texto: null,
      reset: false,
      inputFileLoading: false,
      valueSeleccion: null,
      data: [],
      opcionSeleccionada: null,
      inputFileName: false,
      inputInterno: "",
      tipologiaDatos: [],
    };

    this.modalShow = this.modalShow.bind(this);
    this.obtenerResultados = this.obtenerResultados.bind(this);
    this.fetchAvaliablesData = this.fetchAvaliablesData.bind(this);
    this.renderGrilla = this.renderGrilla.bind(this);
    this.onSelectionChanged = this.onSelectionChanged.bind(this);
    this.setInputValue = this.setInputValue.bind(this);
    this.onchangeInputAttr = this.onchangeInputAttr.bind(this);
    this.onChangeLocal = this.onChangeLocal.bind(this);
    this.resetInputFile = this.resetInputFile.bind(this);
    this.descargarAdjunto = this.descargarAdjunto.bind(this);
    this.downloadFile = this.downloadFile.bind(this);
    this.handleDependencias = this.handleDependencias.bind(this);
    this.changeHerencia = this.changeHerencia.bind(this);
    this.changeBlanquear = this.changeBlanquear.bind(this);
    this.buscarValor = this.buscarValor.bind(this);
    this.changeSelect = this.changeSelect.bind(this);


  }

  componentWillMount() {
    store.dispatch({
      type: "SET_DEPENDENCIA_COMBO",
      name: null,
      data: null,
    });

    if (this.props.tipologia === "SELECCION") {
      if (this.props.editar) {
        this.fetchAvaliablesData(this.props.value);
      }
    }

    if (this.props.tipo === "ADJUNTO") {
      if (this.props.editar === true) {
        if (this.props.value !== undefined) {
          if (this.state.editFile === false) {
            if (this.state.reset !== true) {
              this.obtenerAdjuntos();
              return false;
            }
          }
        }
      }
    }
  }

  /*  onChangeLocal(e) {
    if (this.props.editar) {
      var inputId = document.getElementById("adjuntoSpan" + this.props.id);
      inputId.innerHTML = _.get(e, `_tbx.files[0].name`, "");
    }
    console.log("onChangeLocal", e);
    this.setState({
      editFile: true,
      inputFile: e,
      inputFileFile: e._tbx.files[0].name,
      inputFileName: e._tbx.files[0].name,
      value: e._tbx.files[0].name,
      buttonDownload: false,
    });
    var inputId = document.getElementById("adjuntoSpan" + this.props.id);
    inputId.innerHTML = _.get(e, `_tbx.files[0].name`, "");
    this.props.onChange(e);
  } */

  onChangeLocal(e) {
    e.preventDefault();
    if (this.props.editar) {
      var inputId = document.getElementById("adjuntoSpan" + this.props.id);
      inputId.innerHTML = _.get(e, `target.files[0].name`, "");
    }
    this.setState({
      editFile: true,
      inputFile: e,
      inputFileFile: _.get(e, `target.files[0].name`, ""),
      inputFileName: _.get(e, `target.files[0].name`, ""),
      buttonDownload: false,
    });
    this.props.onChange(e);
  }

  resetInputFile(e) {
    /*  e.preventDefault();
    this.fileInput.value = ""; */
    $("#adjunto" + this.props.id).val("");
    this.fileInput.value = "";

    const fileValor = {
      archivo: "",
      adjunto: "",
      interno: "",
    };
    this.props.setFileValor(fileValor);

    this.setState({
      editFile: true,
      inputFileName: "",
      inputFileFile: "",
      buttonDownload: false,
      value: "",
      reset: true,
    });
    if (this.props.editar) {
      var inputId = document.getElementById("adjuntoSpan" + this.props.id);
      inputId.innerHTML = "";
    }
    /* var inputId = document.getElementById("adjuntoSpan" + this.props.id);
    inputId.innerHTML = ""; */
    this.props.onChange(e);
  }

  obtenerAdjuntos() {
    this.handleCalculado();
    this.setState({
      inputFileLoading: true,
    });

    const myHeaders = new Headers();
    myHeaders.append(
      "Authorization",
      "Bearer " + sessionStorage.getItem("token"),
    );
    myHeaders.append("Content-Type", "application/json");

    var raw = {
      datos: {
        objeto: this.props.objeto,
        opcion: this.props.opcion,
        atributo: this.props.atributo,
        interno: this.props.value,
      },
    };

    const requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: JSON.stringify(raw),
      redirect: "follow",
    };

    fetch(store.getState().serverUrl + URLS.OBTENER_ADJUNTOS, requestOptions)
      .then((response) => {
        if (response.status == 401) {
          sessionStorage.clear();
          window.location.reload(false);
        }else{
          return response.json();
        }
      })
      .then((result) => {
        const fileValor = {
          archivo: result.archivo,
          adjunto: result.adjunto,
          interno: this.props.value,
        };
        this.props.setFileValor(fileValor);
        this.setState({
          inputFileName: result.archivo,
          inputFileFile: result.adjunto,
          inputInterno: this.props.value,
          inputFileLoading: false,
        });
        
        var inputId = document.getElementById("adjuntoSpan" + this.props.id);
        inputId.innerHTML = result.archivo;

        if (result.adjunto === "") {
          this.setState({
            buttonDownload: false,
          });
        } else {
          this.setState({
            buttonDownload: true,
          });
        }
      })
      .catch((error) => console.log("error", error));
    {
    }
  }

  downloadFile(file, extention, filename) {
    const linkSource = `data:application/${extention};base64,${file}`;
    const downloadLink = document.createElement("a");
    const fileName = filename;

    downloadLink.href = linkSource;
    downloadLink.download = fileName;
    downloadLink.click();
  }

  descargarAdjunto(e) {
    e.preventDefault();
    this.downloadFile(
      this.state.inputFileFile,
      this.state.inputFileName.split(".")[1],
      this.state.inputFileName,
    );
  }

  fetchAvaliablesData(busqueda) {
    var filters;

    if (this.props.arregloAtributos !== undefined) {
      if (
        this.props.arregloAtributos.dependencias &&
        this.props.arregloAtributos.dependencias.length > 0
      ) {
        filters = this.props.arregloAtributos.dependencias.map(
          (dependencia) => {
            var id = document.getElementById(
              "input" + dependencia.contenido,
            ).value;

            return {
              atributo: dependencia.busqueda,
              contenido: id,
            };
          },
        );
      } else {
        filters = [];
      }
      if (busqueda) {
        filters.push({ atributo: this.props.atr_valor, contenido: busqueda });
      }
    } else if (busqueda) {
      filters = [{ atributo: this.props.atr_valor, contenido: busqueda }];
    } else {
      filters = [];
    }

    var raw = {
      idObjeto: parseInt(this.props.origen),
      idOpcion: parseInt(this.props.opcion),
      filtros: {
        busquedas: filters,
      },
    };

    const myHeaders = new Headers();
    myHeaders.append(
      "Authorization",
      "Bearer " + sessionStorage.getItem("token"),
    );
    myHeaders.append("Content-Type", "application/json");

    const requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: JSON.stringify(raw),
      redirect: "follow",
    };

    fetch(store.getState().serverUrl + URLS.OBTENER_INFORMACION, requestOptions)
      .then((response) => {
        if (response.status == 401) {
          sessionStorage.clear();
          window.location.reload(false);
        }else{
          return response.json();
        }
      })
      .then((result) => {
        if (busqueda === "") {
          var inputCode = "inputCode" + [this.props.id];
          this.setState({
            [inputCode]: null,
            [inputValue]: undefined,
          });
          return false;
        } else {
          if (busqueda) {
            var inputCode = "inputCode" + [this.props.id];
            var inputValue = "inputValue" + [this.props.id];
            if (result.length === 0) {

              this.setState({
                [inputCode]: null,
                [inputValue]: undefined,
                valueSeleccion: 0,
              });
            } else {
              this.setState({
                [inputCode]: result[0][this.props.atr_valor],
                [inputValue]: result[0][this.props.atr_nombre],
                valueSeleccion: result[0][this.props.atr_valor],
              });

              //var probando = "inputValue" + this.props.id;
            }
            return false;
          }
        }

        var dataGrid = new CollectionView(result);
        this.setState({
          modalLoading: false,
          dataGrid: dataGrid,
        });
      })
      .catch((error) => console.log("error", error));
  }

  obtenerResultados() {
    this.fetchAvaliablesData();
    this.setState({
      modalLoading: true,
    });
  }

  initializeGrid(ctl) {
    this.flexGrid = ctl;
    this.flexGrid.onSelectionChanged(null);

    /** para el search grid */
    let theGrid = ctl;
    let theSearch = this.theSearch.current.control;
    theSearch.grid = theGrid;

  }

  onSelectionChanged() {
    if (Object.entries(this.state.dataGrid._view).length !== 0) {
      this.setState({
        atrr_valorSelect:
          this.state.dataGrid._view[this.flexGrid.selection._row][
            this.props.atr_valor
          ],
        atrr_nombreSelect:
          this.state.dataGrid._view[this.flexGrid.selection._row][
            this.props.atr_nombre
          ],
      });
    }
  }

  setInputValue(e) {
    e.preventDefault();
    e.stopPropagation();
    
    var inputCode = "inputCode" + [this.props.id];
    var inputValue = "inputValue" + [this.props.id];

    this.setState({
      mensajeError: false,
      [inputValue]: this.state.atrr_nombreSelect,
      [inputCode]: this.state.atrr_valorSelect,
      valueSeleccion: this.state.atrr_valorSelect,
    });
    var inputChange = e;

    inputChange.target = $("#id" + this.state.atrr_valorSelect);
    inputChange.target.value = this.state.atrr_valorSelect;
    inputChange.target.name = this.props.id;
    

    this.props.onChange(inputChange);
    this.modalShow(e);
  }

  /* Render Grilla */

  renderGrilla() {
    return (
      <div className="d-flex flex-column justify-content-between w-100 mb-4 mt-4">
        
        {Object.entries(this.state.dataGrid).length !== 0 ? (
          <>
          <div className="d-flex flex-column justify-content-between w-100 mb-4 mt-1">
            <FlexGridSearch ref={this.theSearch} placeholder='Ingrese texto a buscar'/>
          </div>
          <FlexGrid
            ref={this.theGrid}
            initialized={this.initializeGrid.bind(this)}
            itemsSource={this.state.dataGrid}
            deferResizing={false}
            showMarquee={false}
            preserveSelectedState={true}
            allowSorting={true}
            selectionChanged={this.onSelectionChanged}
            selectionMode="Row"
            
          >
            {this.props.atributos
              .filter((attr) => attr.visible === "S")
              .map((attr) => {
                var ancho = attr.porcentaje;
                if (!ancho.includes("*")){
                  ancho = Number(ancho); 
                }

                return (
                  <FlexGridColumn
                    key={attr.orden}
                    binding={attr.id}
                    header={attr.descripcion}
                    isReadOnly={true}
                    width={ancho}
                    format={attr.mascara}
                    align={attr.alineacion}
                  ></FlexGridColumn>
                );
              })}

            <FlexGridFilter />
          </FlexGrid></>
        ) : (
          <Loader />
        )}
      </div>
    );
  }

  modalShow(e) {
    e.preventDefault();
    e.stopPropagation();
    this.setState((prevState) => ({
      showModalState: !prevState.showModalState,
    }));
    this.obtenerResultados();
  }

  /* Modal */

  modal() {
    if (this.state.showModalState) {
      return (
        <div className="custom-modal custom-modal-big modalSeleccionRender">
          <div className="custom-modal-content">
            <div className="custom-modal-header">
              <div className="row d-flex justify-content-between align-items-center">
                <div>
                  <h6> {this.props.descripcion} </h6>
                </div>
                <div>
                  <button
                    className="btn btn-close"
                    onClick={(e) => this.modalShow(e)}
                  >
                    <i className="fi fi-rs-cross" />
                  </button>
                </div>
              </div>
            </div>
            <div className="right-body bg-white">
              {this.state.modalLoading ? (
                <Loader />
              ) : (
                <div> {this.renderGrilla()} </div>
              )}
            </div>
            <div className="right-footer d-flex justify-content-center align-items-stretch">
              <button
                className="btn btn-secondary mr-2"
                onClick={(e) => this.modalShow(e)}
              >
                Cancelar
              </button>
              <button
                className="btn btn-primary seleccionar"
                onClick={(e) => this.setInputValue(e)}
              >
                Seleccionar
              </button>
            </div>
          </div>
        </div>
      );
    }
  }

  onchangeInputAttr(e) {
    
    if (e.target) {
      var { value, name } = e.target;
    } else if (e._tbx) {
      var value = e._tbx.value;
      var name = e._tbx.name;
    }
    var inputValue = "inputValue" + this.props.id;
    var inputCode = "inputCode" + this.props.id;
    
    if (value === undefined || value === ''){
      value = null;
    }else{
      value = value.replaceAll(".", "").replaceAll(",", "");
    }

    this.setState({
      [inputValue]: "",
      [inputCode]: value,
      mensajeError: false,
    });
    this.fetchAvaliablesData(value);
    this.props.onChange(e);
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props.refrescar && this.props.refrescar === this.props.id) {
      this.obtenerInformacion(this.props.id, this.props.valorFiltro, true);
    }

    if (prevProps.editable !== this.props.editable) {
      if (this.props.editable === "N") {
        this.setState({
          disabled: true,
        });
      } else if (this.props.editable === "S") {
        this.setState({
          disabled: false,
        });
      }
    }
    if (prevProps.obligatorio !== this.props.obligatorio) {
      if (this.props.obligatorio === "S") {
        this.setState({
          required: true,
        });
      } else if (this.props.obligatorio === "N") {
        this.setState({
          required: false,
        });
      }
    }
  }

  componentDidMount() {

    if (this.props.value) {
      if (
        this.props.tipologia === "FECHA" ||
        this.props.tipologia === "PERIODO"
      ) {
        const date = this.props.value.split("/").reverse().join("/");
        const value = new Date(date);
        this.setState({ value: value });
      } else {
        this.setState({ value: this.props.value });
      }
    }

    if (
      this.props.tipologia === "MULTIPLE" ||
      this.props.tipologia === "COMBO" ||
      this.props.tipologia === "VISUALIZACION"
    ) {
      if (this.props.refrescar && this.props.refrescar === this.props.id) {
        this.obtenerInformacion(this.props.id, this.props.valorFiltro, true);
      }else{
        this.obtenerInformacion(this.props.id);
      }
    }
    
    if (this.props.obligatorio === "S") {
      this.setState({
        required: true,
      });
    }
    if (this.props.editable === "N") {
      this.setState({
        disabled: true,
      });
    }
    if (this.props.estilo === "MAYUSCULAS") {
      this.setState({
        style: { textTransform: "uppercase" },
      });
    }
    // if (this.props.tipologia === "ADJUNTO") {
    //     if (this.props.dia === 7) {
    //         this.setState({
    //             required: true
    //         })
    //     }
    // }
    this.languagesConfig();

   

  }

  languagesConfig() {
    registerLocale("es", es);
    registerLocale("en", en);

    this.setState({
      language: JSON.parse(
        sessionStorage.getItem("defaultLanguage"),
      ).localizacion.toLowerCase(),
    });
  }

  obtenerInformacion(refrescar, id, noStore) {
    /*  this.handleCalculado(); */
    
    if (this.props.arrayAtributos) {
      this.props.arrayAtributos.map((atributo) => {
        if (refrescar === atributo.id ) {
          var myHeaders = new Headers();
          var origenDatos = "Informacion" + refrescar;
                    
          myHeaders.append(
            "Authorization",
            "Bearer " + sessionStorage.getItem("token"),
          );
          myHeaders.append("Content-Type", "application/json");

          var filters;
          if (atributo.dependencias && atributo.dependencias.length > 0) {
            if (id == undefined) {
              id = "-1";
            }

            filters = atributo.dependencias.map((dependencia) => {
              this.state.tipologiaInfo.map((tipologia) => {});
              return {
                atributo: dependencia.busqueda,
                contenido: id,
              };
            });
          }
          
          var raw = JSON.stringify({
            idObjeto: parseInt(atributo.origen),
            filtros: {
              busquedas: filters,
            },
          });

          var requestOptions = {
            method: "POST",
            headers: myHeaders,
            body: raw,
            redirect: "follow",
          };

          fetch(
            store.getState().serverUrl +
              URLS.OBTENER_INFORMACION,
            requestOptions,
          )
            .then((response) => {
              if (response.status == 401) {
                sessionStorage.clear();
                window.location.reload(false);
              }else{
                return response.json();
              }
            })
            .then((result) => {

              if (!noStore) {
                store.dispatch({
                  type: "SET_TIPOLOGIA_INFO",
                  name: origenDatos,
                  data: result,
                });
                
                if (atributo.dependientes && atributo.dependientes.length > 0) {
                  atributo.dependientes.map((dependiente) => {
                    store.dispatch({
                      type: "SET_DEPENDENCIA_COMBO",
                      name: dependiente.refrescar,
                      data: this.state.value,
                    });
                  });                  
                }
              }else{
                store.dispatch({
                  type: "SET_DEPENDENCIA_COMBO",
                  name: null,
                  data: null,
                });
              }
              
              if (this.state.value === undefined && result && result.length > 0) {
                this.setState({
                  tipologiaInfo: result,
                  loading: false,
                  value: result[0][this.props.atr_valor],
                  texto: result[0][this.props.atr_nombre],
                });
                
                var combo = document.getElementById("input" + this.props.id);
                var comboValor = Object.getOwnPropertyDescriptor(window.HTMLSelectElement.prototype, "value").set;
                comboValor.call(combo, result[0][this.props.atr_valor]);
                var comboEvento = new Event('change', { bubbles: true});
                combo.dispatchEvent(comboEvento);
              }else{
                this.setState({
                  loading: false,
                  tipologiaInfo: result,
                });
                var combo = document.getElementById("input" + this.props.id);
                var comboEvento = new Event('change', { bubbles: true});
                combo.dispatchEvent(comboEvento);
              }

            })
            .catch((error) => console.log("error", error));
        }
      });
    } else {
      var myHeaders = new Headers();
      myHeaders.append(
        "Authorization",
        "Bearer " + sessionStorage.getItem("token"),
      );
      myHeaders.append("Content-Type", "application/json");
      var origenDatos = "Informacion" + refrescar;

      var raw = JSON.stringify({ idObjeto: parseInt(this.props.origen) });

      var requestOptions = {
        method: "POST",
        headers: myHeaders,
        body: raw,
        redirect: "follow",
      };
      fetch(
        store.getState().serverUrl + URLS.OBTENER_INFORMACION,
        requestOptions,
      )
        .then((response) => {
          if (response.status == 401) {
            sessionStorage.clear();
            window.location.reload(false);
          }else{
            return response.json();
          }
        })    
        .then((result) => {
          store.dispatch({
            type: "SET_TIPOLOGIA_INFO",
            name: origenDatos,
            data: result,
          });
          if (this.props.tipologia === "COMBO" && this.props.value === undefined && result && result.length > 0) {
            this.setState({
              loading: false,
              tipologiaInfo: result,
              value: result[0][this.props.atr_valor],
              texto: result[0][this.props.atr_nombre],
            });
            var combo = document.getElementById("input" + this.props.id);
            var comboValor = Object.getOwnPropertyDescriptor(window.HTMLSelectElement.prototype, "value").set;
            comboValor.call(combo, result[0][this.props.atr_valor]);
            var comboEvento = new Event('change', { bubbles: true});
            combo.dispatchEvent(comboEvento);            
          }else{
            this.setState({
              loading: false,
              tipologiaInfo: result,
            });
            var combo = document.getElementById("input" + this.props.id);
            var comboEvento = new Event('change', { bubbles: true});
            combo.dispatchEvent(comboEvento);

          }
        })
        .catch((error) => console.log("error", error));
    }
  }


  /* Proceso de Calculado */

  handleCalculado(e) {
    // Procesar Numericos

    var arrayTemp = [];
    if (this.props.arrayAtributos) {
      this.props.arrayAtributos.map((attr) => {
        if (attr.calculado !== "" && attr.tipo === "NUMERICO") {
          arrayTemp.push(attr);
        }
      });

      arrayTemp.map((attr) => {
        
        var procesar = true;

        var calculado = attr.calculado;
        let decimales = "";

        for (var i = 0; i < attr.decimales; i++) {
          decimales = decimales + "0";
        }

        this.props.arrayAtributos.map((origenes) => {
          if (calculado.includes("<" + origenes.id + ">")) {
            var input;
            if (origenes.tipologia === "COMBO"){
              input = $("#input" + origenes.id).children(':selected').text();
            }else {
              input = $("input[name = " + origenes.id + "]").val();
            }
            
            if (input !== undefined) {

              if (input === "") {
                if (origenes.tipo === "NUMERICO") {
                  input = 0;
                } else {
                  procesar = false;
                }
              } else {
                if (origenes.tipo === "FECHA") {
                  var texto = input;
                  var divisor = 1000 * 60 * 60 * 24;
                  var contenido =
                    texto.substring(6, 10) +
                    "-" +
                    texto.substring(3, 5) +
                    "-" +
                    texto.substring(0, 2);

                  input = Date.parse(contenido);
                  input = parseInt(input) / divisor;
                } else {
                  input = input.replaceAll(",", "");
                }
              }
            }

            calculado = calculado.replaceAll("<" + origenes.id + ">", input);
          }
        });

        if (procesar === true) {
          var numeral = require("numeral");
          var resultado = eval(calculado);
          var numeralTotal = numeral(resultado).format("0,0." + decimales);
        } else {
          var numeralTotal = "";
        }
        $("input[name = " + attr.id + "]").val(numeralTotal); 
      });
      
      // Procesar Caracteres

      var arrayTemp = [];

      this.props.arrayAtributos.map((attr) => {
        if (attr.calculado !== "" && attr.tipo === "CARACTER") {
          arrayTemp.push(attr);
        }
      });

      arrayTemp.map((attr) => {
        var procesar = true;
        var calculado = attr.calculado;

        this.props.arrayAtributos.map((origenes) => {
          if (calculado.includes("<" + origenes.id + ">")) {
            var input;
            if (origenes.tipologia === "COMBO"){
              input = $("#input" + origenes.id + " option:selected" ).text();
            }else {
              input = $("input[name = " + origenes.id + "]").val();
            }

            if (input === "") {
              procesar = false;
            }

            calculado = calculado.replaceAll("<" + origenes.id + ">", input);
          }
        });

        if (procesar === true) {
          var resultadoMascara = calculado;
        } else {
          var resultadoMascara = "";
        }

        $("input[name = " + attr.id + "]").val(resultadoMascara);
      });

      // Procesar Fechas

      var arrayTemp = [];

      this.props.arrayAtributos.map((attr) => {
        if (attr.calculado !== "" && attr.tipo === "FECHA") {
          arrayTemp.push(attr);
        }
      });

      arrayTemp.map((attr) => {
        var divisor = 1000 * 60 * 60 * 24;
        var procesar = true;
        var calculado = attr.calculado;

        this.props.arrayAtributos.map((origenes) => {
          if (calculado.includes("<" + origenes.id + ">")) {
            let value = e;
            /*  if (e) {
              const mnth = ("0" + (e.getMonth() + 1)).slice(-2);
              const day = ("0" + e.getDate()).slice(-2);
              value = [day, mnth, e.getFullYear()].join("/");
            } */
            var input = value;
            /* var input = $("input[name = " + origenes.id + "]").val(); */

            /*  if (input === "") {
              procesar = false;
            } else {
              if (origenes.tipo === "FECHA") {
                var texto = input;
                var contenido =
                  texto.substring(6, 10) +
                  "-" +
                  texto.substring(3, 5) +
                  "-" +
                  texto.substring(0, 2);

                input = Date.parse(contenido);
                input = parseInt(input) / divisor;
              }
            }
 */
            calculado = calculado.replaceAll("<" + origenes.id + ">", input);
          }
        });

        /* if (procesar === true) {
          var resultado = Number(parseInt(eval(calculado) * divisor));
          var fecha = new Date(resultado);
          var mascara = "";
          var dia = fecha.getDate();
          var mes = fecha.getMonth() + 1;
          var anio = fecha.getFullYear();

          if (dia < 10) {
            dia = "0" + dia;
          }

          if (mes < 10) {
            mes = "0" + mes;
          }

          mascara = dia + "/" + mes + "/" + anio;
        } else {
          var mascara = "";
        } */

        /*  $("input[name = " + attr.id + "]").val(mascara); */
      });
    }
  }

  changeSelect = (e) => {
    
    this.setState({
      value: e.target.value,
    });    
    
    this.changeHerencia(e);
    this.changeBlanquear(e);
    this.handleDependencias(e);
    this.props.onChange(e);
  };

  changeHerencia(e) {
    const tipologiaInfo = this.state.tipologiaInfo;
    if (this.props.condiciones) {
      this.props.condiciones.map((condicion) => {
        if (
          condicion.eva_atributo === this.props.id &&
          condicion.des_propiedad === "HERENCIA"
        ) {
          if (e.target) {
            var { value, name } = e.target;
          } else if (e._tbx) {
            var value = e.selectedValue;
            var name = e._tbx.name;
          }
          if (value === parseInt(condicion.eva_valor)) {
            tipologiaInfo.map((tipologia) => {
              if (tipologia.id == condicion.eva_valor) {
                var input = document.getElementsByName(condicion.des_atributo);
                $("input[name = " + condicion.des_atributo + "]").val(
                  tipologia[condicion.des_valor],
                );
              }
            });
          }
        }
      });
    }
  }

  changeBlanquear(e) {
    /* e.preventDefault(); */
    if (this.props.condiciones) {
      this.props.condiciones.map((condicion) => {
        if (
          condicion.eva_atributo === this.props.id &&
          condicion.des_propiedad === "BLANQUEAR"
        ) {
          if (e.target) {
            var { value, name } = e.target;
          } else if (e._tbx) {
            var value = e.selectedValue;
            var name = e._tbx.name;
          }
          if (value === parseInt(condicion.eva_valor)) {
            document.getElementsByName(condicion.des_atributo)[1].value = "";
          }
        }
      });
    }
  }

  buscarValor(id) {
    this.props.arrayAtributos.map((attr) => {
      if (attr.id === id) {
        return attr.origen;
      }
    });
  }

  handleDependencias(e) {
    if (this.props.arrayAtributos) {
      this.props.arrayAtributos.map((attr) => {
        if (e.target) {
          var { value, name } = e.target;
        } else if (e._tbx) {
          var value = e.selectedValue;
          var name = e._tbx.name;
        }
        
        if (
          attr.id === name &&
          attr.dependientes &&
          attr.dependientes.length > 0
        ) {
          attr.dependientes.map((dependiente) => {
            store.dispatch({
              type: "SET_DEPENDENCIA_COMBO",
              name: dependiente.refrescar,
              data: value,
            });
          });
        }
        
      });
    }
  }

  /* Render Input */

  renderInput() {
    var origenDatos = "Informacion" + this.props.id;


    switch (this.props.tipologia) {
      // Multiple es checkbox
      case "MULTIPLE":
        return (
          <div className="form-check checkbok-heigth">
            <label id={"label" + this.props.id} htmlFor={this.props.id}>
              {this.state.required && <span>*</span>}
              {this.props.descripcion}
            </label>
            <div className="invalid-feedback"> Campo obligatorio </div>
            {this.state.tipologiaInfo.map((item) => {
              if (this.props.value != undefined && ("," + this.props.value + ",").includes(item[this.props.atr_valor])){
                return (
                  <div
                    className="form-group"
                    key={`multiple${item[this.props.atr_valor]}`}
                  >
                    <div className="form-check">
                      <input
                        className="form-check-input"
                        name={this.props.id}
                        type="checkbox"
                        checked="true"                      
                        value={item[this.props.atr_valor]}
                        /*  id={`checkbox${item[this.props.atr_valor]}`} */
                        onChange={this.props.onChangeMultiple}
                        id={"input" + this.props.id}
                      />
                      <label
                        id={"label" + this.props.id}
                        className="form-check-label"
                        htmlFor={`checkbox${item[this.props.atr_valor]}`}
                      >
                        {this.state.required && <span>*</span>}
                        {item[this.props.atr_nombre]}
                      </label>
                    </div>
                  </div>
                );
              }else{
                return (
                  <div
                    className="form-group"
                    key={`multiple${item[this.props.atr_valor]}`}
                  >
                    <div className="form-check">
                      <input
                        className="form-check-input"
                        name={this.props.id}
                        type="checkbox"
                        value={item[this.props.atr_valor]}
                        /*  id={`checkbox${item[this.props.atr_valor]}`} */
                        onChange={this.props.onChangeMultiple}
                        id={"input" + this.props.id}
                      />
                      <label
                        id={"label" + this.props.id}
                        className="form-check-label"
                        htmlFor={`checkbox${item[this.props.atr_valor]}`}
                      >
                        {this.state.required && <span>*</span>}
                        {item[this.props.atr_nombre]}
                      </label>
                    </div>
                  </div>
                );
              }
            })}
          </div>
        );
      case "VISUALIZACION":
        return (
          <div className="wj-labeled-input">
            <select
              className=""
              onChange={this.props.onChange}
              name={this.props.id}
              value={this.state.value}
              id={"input" + this.props.id}
            >
              {this.state.tipologiaInfo.map((item) => {
                return (
                  <option
                    key={`visual${item[this.props.atr_valor]}`}
                    value={item[this.props.atr_valor]}
                  >
                    {item[this.props.atr_nombre]}
                  </option>
                );
              })}
            </select>
            <label id={"label" + this.props.id} htmlFor={this.props.id}>
              {this.state.required && <span>*</span>}
              {this.props.descripcion}
            </label>
            <div className="wj-error"> Campo obligatorio </div>
            <small className="form-text text-muted">
              {this.props.observaciones}
            </small>
          </div>
        );
      // Combo = select
      case "COMBO":
        return (
          <div className="wj-labeled-input">
            {
             <select
              className="select-input"
              onChange={(e) => {
                this.changeSelect(e);
              }}
              required={this.state.required}
              name={this.props.id}
              value={this.state.value}
              defaultValue={this.state.value}
              id={"input" + this.props.id}
            > 
              {this.state.tipologiaInfo.map((item) => {
                return (
                  <option
                    key={"combo" + item[this.props.atr_valor]}
                    value={item[this.props.atr_valor]}
                  >
                    {item[this.props.atr_nombre]}
                  </option>
                );
              })}
            </select> 

            /*
            <ComboBox
              id={"input" + this.props.id}
              name={this.props.id}
              displayMemberPath={this.props.atr_nombre}
              selectedValuePath={this.props.atr_valor}
              itemsSource={this.props.tipologiaInfo.get(origenDatos)}
              selectedIndexChanged={(e) => {
                this.changeSelect(e);
              }}
              text=""
            />
            */
          }
            <label id={"label" + this.props.id} htmlFor={this.props.id}>
              {this.state.required && <span>*</span>}
              {this.props.descripcion}
            </label>
            <div className="wj-error"> Campo obligatorio </div>
            <small className="form-text text-muted">
              {this.props.observaciones}
            </small>
          </div>
        );
      // Selección, sale a obtener información general
      case "SELECCION":
        switch (this.props.tipo) {
          case "CARACTER":
            var inputValue = "inputValue" + this.props.id;
            return (
              <div className="d-flex">
                <div className="wj-labeled-input">
                  <input
                    /*  id={`id${this.props.id}`} */
                    name={this.props.id}
                    /* value={this.props.value} */
                    size={this.props.longitud}
                    style={this.state.style}
                    className="SELECCION"
                    aria-describedby={`help${this.props.nombre}`}
                    // placeholder={this.props.descripcion}
                    maxLength={this.props.longitud}
                    disabled={this.state.disabled}
                    required={this.state.required}
                    id={"input" + this.props.id}
                    value={
                      this.state.mensajeError === false
                        ? this.state[inputValue]
                        : ""
                    }
                    onBlur={(e) => this.onchangeInputAttr(e)}
                  />

                  {/* <ComboBox
                    id={"input" + this.props.id}
                    name={this.props.id}
                    selectedValue={this.props.value}
                    className=""
                    isDisabled={this.state.disabled}
                    isRequired={this.state.required}
                    textChanged={(e) => {
                      this.onchangeInputAttr(e);
                    }}
                  /> */}
                  <label id={"label" + this.props.id} htmlFor={this.props.id}>
                    {this.state.required && <span>*</span>}
                    {this.props.descripcion}
                  </label>
                  <div className="wj-error"> Campo obligatorio </div>
                  {this.state.mensajeError ? (
                    <div className="wj-error d-block"> Valor incorrecto </div>
                  ) : (
                    false
                  )}
                  <small className="form-text text-muted">
                    {this.props.observaciones}
                  </small>
                </div>
                <button
                  className="btn-search ml-2 mr-2"
                  onClick={(e) => this.modalShow(e)}
                >
                  <i className="fi fi-rs-search"></i>
                </button>
                <input
                  className=""
                  type="text"
                  disabled={true}
                  value={this.state[inputValue]}
                />
              </div>
            );
          case "NUMERICO":
            var inputValue = "inputValue" + this.props.id;
            var inputCode = "inputCode" + this.props.id;
            return (
              <div className="align-items-center d-flex">
                <div className="wj-labeled-input">
                  {/*  <NumberFormat
                    id={`id${this.props.id}`}
                    name={this.props.id}
                    decimalScale={this.props.decimales}
                    prefix={this.props.prefijo}
                    suffix={this.props.sufijo}
                    style={this.state.style}
                    className="select-input border-bottom text-right"
                    id={"input" + this.props.id}
                    aria-describedby={`help${this.props.nombre}`}
                    allowNegative={this.props.minimo >= 0}
                    required={this.state.required}
                    disabled={this.state.disabled}
                    // value={this.state.valueSeleccion}
                    value={
                      this.state.mensajeError === false
                        ? this.state[probandoCode]
                        : ""
                    }
                    onChange={(e) => this.onchangeInputAttr(e)}
                    isAllowed={(values) => {
                      const { floatValue } = values;
                      return (
                        floatValue >= parseInt(this.props.minimo) &&
                        floatValue <= parseInt(this.props.maximo)
                      );
                    }}
                  /> */}
                  <InputNumber
                    id={`id${this.props.id}`}
                    name={this.props.id}
                    isRequired={false}
                    isDisabled={this.state.disabled}
                    handleWheel={false}
                    value={
                      this.state.mensajeError === false
                        ? this.state[inputCode]
                        : null
                    }
                    className="w-100 h-calc"
                    lostFocus={(e) => this.onchangeInputAttr(e)}
                    format={this.props.mascara}
                    max={parseInt(this.props.maximo)}
                    min={parseInt(this.props.minimo)}
                  />
                  <label id={"label" + this.props.id} htmlFor={this.props.id}>
                    {this.state.required && <span>*</span>}
                    {this.props.descripcion}
                  </label>
                  <div className="wj-error"> Campo obligatorio </div>
                  {this.state.mensajeError ? (
                    <div className="wj-error d-block"> Valor incorrecto </div>
                  ) : (
                    false
                  )}

                  <small className="form-text text-muted">
                    {this.props.observaciones}
                  </small>
                </div>
                <div className="d-flex col-9 pr-0">
                  <button
                    className="btn-search ml-2 mr-2"
                    onClick={(e) => this.modalShow(e)}
                  >
                    <i className="fi fi-rs-search mx-2"></i>
                  </button>
                  <input
                    id={"inputLabel" + this.props.id}
                    className="w-100"
                    type="text"
                    disabled={true}
                    value={this.state[inputValue]}
                  />
                </div>
              </div>
            );
          case "FECHA":
            return (
              <div className="wj-labeled-input">
                <NumberFormat
                  name={this.props.id}
                  value={this.props.value}
                  format="##/##/####"
                  mask={["d", "d", "m", "m", "y", "y", "y", "y"]}
                  style={this.state.style}
                  className=""
                  aria-describedby={`help${this.props.nombre}`}
                  placeholder={this.props.descripcion}
                  required={this.state.required}
                  disabled={this.state.disabled}
                  onChange={this.props.onChange}
                  id={"input" + this.props.id}
                />
                <label id={"label" + this.props.id} htmlFor={this.props.id}>
                  {this.state.required && <span>*</span>}
                  {this.props.descripcion}
                </label>
                <div className="wj-error"> Campo obligatorio </div>
                <small className="form-text text-muted">
                  {this.props.observaciones}
                </small>
              </div>
            );
          default:
            return null;
        }
      case "TEXTO":
        switch (this.props.tipo) {
          case "CARACTER":
            return (
              <div className="wj-labeled-input">
                {/* <input
                  name={this.props.id}
                  value={this.props.value}
                  size={this.props.longitud}
                  style={this.state.style}
                  className=""
                  aria-describedby={`help${this.props.nombre}`}
                  // placeholder={this.props.descripcion}
                  maxLength={this.props.longitud}
                  disabled={this.state.disabled}
                  id={"input" + this.props.id}
                  required={this.state.required}
                  onChange={this.props.onChange}
                  onKeyUp={() => this.handleCalculado()}
                /> */}
                <ComboBox
                  id={"input" + this.props.id}
                  name={this.props.id}
                  selectedValue={this.props.value}
                  className=""
                  isDisabled={this.state.disabled}
                  isRequired={this.state.required}
                  textChanged={(e) => {
                    this.props.onChange(e);
                    this.handleCalculado();
                  }}
                  maxLength={this.props.longitud}
                />
                <label id={"label" + this.props.id} htmlFor={this.props.id}>
                  {this.state.required && <span>*</span>}
                  {this.props.descripcion}
                </label>
                <div className="wj-error"> Campo obligatorio </div>
                <small className="form-text text-muted">
                  {this.props.observaciones}
                </small>
              </div>
            );
          case "NUMERICO":
            

            return (
              <div className="wj-labeled-input">
                {/* <NumberFormat
                  name={this.props.id}
                  thousandsGroupStyle="thousand"
                  value={this.props.value}
                  decimalScale={this.props.decimales}
                  prefix={this.props.prefijo}
                  id={"input" + this.props.id}
                  suffix={this.props.sufijo}
                  style={this.state.style}
                  className=""
                  aria-describedby={`help${this.props.nombre}`}
                  allowNegative={this.props.minimo >= 0}
                  required={this.state.required}
                  disabled={this.state.disabled}
                  isNumericString={true}
                  type="text"
                  decimalSeparator="."
                  onChange={this.props.onChange}
                  onKeyUp={() => {
                    
                    this.handleCalculado();
                  }}
                  isAllowed={(values) => {
                    const { floatValue } = values;
                    return (
                      floatValue >= parseInt(this.props.minimo) &&
                      floatValue <= parseInt(this.props.maximo)
                    );
                  }}
                /> */}
                
                <InputNumber
                  id={"input" + this.props.id}
                  isRequired={false}
                  handleWheel={false}
                  name={this.props.id}
                  value={
                    this.props.value === undefined || this.props.value === null
                      ? null
                      : this.props.value
                  } 
                  
                  isDisabled={this.state.disabled}
                  format={this.props.mascara}
                  aria-describedby={`help${this.props.nombre}`}
                  className="w-100 h-calc"
                  textChanged={(e) => {
                    this.props.onChange(e);
                    this.handleCalculado();
                  }}
                  max={parseInt(this.props.maximo)}
                  min={parseInt(this.props.minimo)}
                />
                <label id={"label" + this.props.id} htmlFor={this.props.id}>
                  {this.state.required && <span>*</span>}
                  {this.props.descripcion}
                </label>
                <div className="wj-error"> Campo obligatorio </div>
                <small className="form-text text-muted">
                  {this.props.observaciones}
                </small>
              </div>
            );

          /* case "FECHA":
            return (
              <div>
                <NumberFormat
                  name={this.props.id}
                  value={this.props.value}
                  format="##/##/####"
                  mask={["d", "d", "m", "m", "y", "y", "y", "y"]}
                  style={this.state.style}
                  className=""
                  aria-describedby={`help${this.props.nombre}`}
                  placeholder={this.props.descripcion}
                  required={this.state.required}
                  id={"input" + this.props.id}
                  disabled={this.state.disabled}
                  onChange={this.props.onChange}
                  onKeyUp={() => this.handleCalculado()}
                />
                <div className="invalid-feedback"> Campo obligatorio </div>
                <small className="form-text text-muted">
                  {this.props.observaciones}
                </small>
              </div>
            );
          default:
            return null; */
        }
      case "PASSWORD":
        return (
          <div className="wj-labeled-input">
            {/* <input
              name={this.props.id}
              value={this.props.value}
              type={this.props.tipologia.toLowerCase()}
              size={this.props.longitud}
              style={this.state.style}
              className=""
              id={"input" + this.props.id}
              aria-describedby={`help${this.props.nombre}`}
              
              maxLength={this.props.longitud}
              disabled={this.state.disabled}
              required={this.state.required}
              onChange={this.props.onChange}
            /> */}
            <ComboBox
              inputType={this.props.tipologia.toLowerCase()}
              id={"input" + this.props.id}
              name={this.props.id}
              selectedValue={this.props.value}
              className=""
              isDisabled={this.state.disabled}
              isRequired={this.state.required}
              textChanged={(e) => {
                this.props.onChange(e);
              }}
            />

            <label id={"label" + this.props.id} htmlFor={this.props.id}>
              {this.state.required && <span>*</span>}
              {this.props.descripcion}
            </label>
            <small className="form-text text-muted">
              {this.props.observaciones}
            </small>
            <div className="wj-error"> Campo obligatorio </div>
          </div>
        );
      case "FECHA":
        return (
          <div className="wj-labeled-input">
            {/* <DatePicker
              name={this.props.id}
              className=""
              aria-describedby={`help${this.props.nombre}`}
              selected={this.state.value}
              placeholderText="dd/mm/yyyy"
              dateFormat="dd/MM/yyyy"
              id={"input" + this.props.id}
              required={this.state.required}
              disabled={this.state.disabled}
              locale={this.state.language}
              customInput={
                <NumberFormat
                  format="##/##/####"
                  mask={["d", "d", "m", "m", "y", "y", "y", "y"]}
                />
              }
              onChange={(date) => {
                this.setState({ value: date });
                var value = "";
                if (date) {
                  const mnth = ("0" + (date.getMonth() + 1)).slice(-2);
                  const day = ("0" + date.getDate()).slice(-2);
                  value = [day, mnth, date.getFullYear()].join("/");
                }
                
                this.props.onChange({
                  target: { name: this.props.id, value: value },
                });
                this.handleCalculado(date);
              }}
            /> */}
            <InputDate
              name={this.props.id}
              id={"input" + this.props.id}
              isRequired={this.state.required}
              isDisabled={this.state.disabled}
              value={this.state.value ? this.state.value : null}
              closeOnSelection={true}
              textChanged={(e) => {
                var value = e._tbx.value;
                this.props.onChange({
                  target: { name: this.props.id, value: value },
                });
                this.handleCalculado(value);
              }}
            />
            <label id={"label" + this.props.id} htmlFor={this.props.id}>
              {this.state.required && <span>*</span>}
              {this.props.descripcion}
            </label>
            <div className="wj-error"> Campo obligatorio </div>
            <small className="form-text text-muted">
              {this.props.observaciones}
            </small>
          </div>
        );
      case "PERIODO":
        return (
          <div className="wj-labeled-input">
            {/*  <DatePicker
              name={this.props.id}
              className=""
              aria-describedby={`help${this.props.nombre}`}
              selected={this.state.value}
              style={this.state.style}
              required={this.state.required}
              disabled={this.state.disabled}
              id={"input" + this.props.id}
              locale={this.state.language}
              showMonthYearPicker
              dateFormat="MM/yyyy"
              placeholderText="mm/yyyy"
              customInput={
                <NumberFormat
                  format="##/####"
                  mask={["m", "m", "y", "y", "y", "y"]}
                />
              }
              onChange={(date) => {
                this.setState({ value: date });
                var value = "";
                if (date) {
                  const mnth = ("0" + (date.getMonth() + 1)).slice(-2);
                  value = [mnth, date.getFullYear()].join("/");
                }
                this.props.onChange({
                  target: { name: this.props.id, value: value },
                });
                
                this.handleCalculado(date);
              }}
            /> */}
            <InputDate
              name={this.props.id}
              id={"input" + this.props.id}
              isRequired={this.state.required}
              isDisabled={this.state.disabled}
              value={this.state.value ? this.state.value : null}
              selectionMode="Month"
              format="MM/yyyy"
              closeOnSelection={true}
              textChanged={(e) => {
                var value = e._tbx.value;
                this.props.onChange({
                  target: { name: this.props.id, value: value },
                });
                this.handleCalculado(value);
              }}
            />

            <label id={"label" + this.props.id} htmlFor={this.props.id}>
              {this.state.required && <span>*</span>}
              {this.props.descripcion}
            </label>
            <div className="wj-error"> Campo obligatorio </div>
            <small className="form-text text-muted">
              {this.props.observaciones}
            </small>
          </div>
        );
      case "ADJUNTO":
        if (this.state.inputFileLoading) {
          return <Loader></Loader>;
        } else {
          return (
            <div className="custom-file">
              {/* <span id="label-adjunto">Seleccione</span> */}
              <input
                ref={(ref) => (this.fileInput = ref)}
                className="custom-file-input adjunto"
                type="file"
                name={this.props.id}
                size={this.props.longitud}
                style={this.state.style}
                aria-describedby={`help${this.props.nombre}`}
                maxLength={this.props.longitud}
                required={this.state.required}
                onChange={(e) => {
                  this.onChangeLocal(e);
                }}
                lang={this.state.language}
                id={"adjunto" + this.props.id}
                custom-name={
                  this.state.inputFileName !== false
                    ? this.state.inputFileName
                    : ""
                }
                interno={this.state.inputInterno}
              />

              <label id={"label" + this.props.id} className="custom-file-label">
                <span
                  id={`adjuntoSpan${this.props.id}`}
                  style={this.state.style}
                >
                  {" "}
                  {this.state.value}{" "}
                </span>
              </label>

              {this.state.inputFileName !== false &&
                this.state.inputFileName !== "" && (
                  <div className="d-flex align-items-center justify-content-between border-bottom">
                    <div className="d-flex align-items-center justify-content-start mt-3 mb-3">
                      {this.state.inputFileName !== false && (
                        <button
                          onClick={(e) => this.resetInputFile(e)}
                          className="btn-transparent mr-3 mb-0 mt-0 text-danger"
                        >
                          <i className="fi fi-rs-trash"></i>
                        </button>
                      )}
                      <p className="mb-0 mt-0">{this.state.inputFileName}</p>
                    </div>
                    {this.state.buttonDownload !== false && (
                      <div className="mt-3 mb-3">
                        <button
                          onClick={(e) => this.descargarAdjunto(e)}
                          className="btn-transparent mr-3 mb-0 mt-0 text-success"
                        >
                          <i className="fi fi-rs-download"></i>
                        </button>
                      </div>
                    )}
                  </div>
                )}

              {/* <div className="invalid-feedback"> Campo obligatorio </div> */}
              <small className="form-text text-muted">
                {this.props.observaciones}
              </small>
            </div>
          );
        }
      case "OBSERVACIONES":
        return (
          <div className="wj-labeled-input">
             <textarea
              rows={5}
              id={"input" + this.props.id}
              name={this.props.id}
              value={this.props.value}
              className="w-100"
              style={{
                display: "table-cell",
                padding: "4px 8px",
                border: "0",
                width: "100%",
                height: "100%",
                color: "inherit",
                background: "0 0",
                minHeight: "2em",
                margin: "0",
                backgroundColor: "initial",
                borderBottom: "1px solid rgba(0, 0, 0, .1)"
              }}
              disabled={this.state.disabled}
              required={this.state.required}
              onChange={(e) => {
                this.props.onChange(e);
              }}
              maxLength={this.props.longitud}
            />
          <label id={"label" + this.props.id} htmlFor={this.props.id}>
            {this.state.required && <span>*</span>}
            {this.props.descripcion}
          </label>
          <div className="wj-error"> Campo obligatorio </div>
          <small className="form-text text-muted">
            {this.props.observaciones}
          </small>
        </div>
        );
      default:
        return null;
    }
    // Selección
  }

  /* Render */

  render() {
    const { tipologiaInfo } = this.props;

    return this.state.loading ? (
      <Loader />
    ) : (
      <div className="form-group">
        {this.modal()}

        {this.props.visible === "S" ? (
          <div>
            {this.props.tipologia === "ADJUNTO" ? (
              <span className="label-adjunto" id="label-adjunto">
                {this.state.required && <span>*</span>}
                {this.props.descripcion} 
              </span>
            ) : null}
            {this.renderInput()}
            {this.handleCalculado()}
          </div>
        ) : null}
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    tipologiaInfo: state.tipologiaInfo,
    refrescar: state.refrescar,
    valorFiltro: state.valorFiltro,
  };
};

export default connect(mapStateToProps)(Input);