import React, { Component } from "react";

class AlertModal extends Component {
  render() {
    var data;
    var status;
    
    if (this.props.data.nombreArchivo || this.props.data.archivo) {
      data = "La operación fue realizada con éxito.";
      status = "success";
    } else {
      if (typeof this.props.data === "object") {
        data = this.props.data.Mensajes[0].mensaje;
        status = "error";
      }else if (this.props.data !== "") {
        data = this.props.data;
        status = "error";
      } else {
        data = "La operación fue realizada con éxito.";
        status = "success";
      }
    }

    if (this.props.show === true) {
      return (
        <div className="custom-modal">
          <div className="custom-modal-content">
            <div className="custom-modal-header">
              <div className="row d-flex justify-content-between align-items-center">
                <div>
                  <h6> {this.props.title} </h6>
                </div>
                <div>
                  <button
                    className="btn btn-close"
                    onClick={this.props.handleShowHide()}
                  >
                    <i className="fi fi-rs-cross" />
                  </button>
                </div>
              </div>
            </div>
            <div className="right-body bg-white text-center d-flex align-items-center justify-content-center">
              {status === "error" ? (
                <div className="padding-topGrande padding-BotonPequeño">
                  <i
                    className="right-icon-big-no fi fi-rs-cross marginLeftRight-auto"
                  ></i>
                  <h6> {data} </h6>
                </div>
              ) : (
                <div>
                  <i
                    className="right-icon-big fi fi-rs-check marginLeftRight-auto"
                  ></i>
                  <h6> {data} </h6>
                </div>
              )}
            </div>
          </div>
        </div>
      );
    } else {
      return false;
    }
  }
}

export default AlertModal;
