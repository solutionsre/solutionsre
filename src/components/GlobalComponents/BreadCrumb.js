import React, { Component } from 'react';

class BreadCrumb extends Component {
    render() {
        return (
            <div className="bread-crumb">
                <span>{this.props.data}</span>
            </div>
        )
    }
}

export default BreadCrumb;
