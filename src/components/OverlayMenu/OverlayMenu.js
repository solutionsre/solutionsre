import React, { Component } from "react";
import Input from "../GlobalComponents/Input";
import CheckboxTree from "react-checkbox-tree";
import store from "../../redux/store";
import paginationFactory from "react-bootstrap-table2-paginator";
import BootstrapTable from "react-bootstrap-table-next";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import cellEditFactory, { Type } from "react-bootstrap-table2-editor";
import GlobalError from "../GlobalComponents/GlobalError";
import URLS from "../../urls";

class OverlayMenu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      overlayShow: false,
      user: [],
      columnsSections: [],
      rowsSections: [],
      selectedRows: [],
      selectedRowsId: [],
      menu: null,
      checked: [],
      expanded: [],
      buttonDisable: false,
      validation: "needs-validation",
      errorsUser: [],
      errorMessages: [],
      errorCritico: false,
      atributosValor: [],
    };
  }

  componentDidUpdate(prevProps) {
    if (prevProps.overlayShow !== this.props.overlayShow) {
      this.setState({
        overlayShow: this.props.overlayShow,
      });
      this.handleMenuTreeNodes();
    }


    if (prevProps.user !== this.props.user) {
      if (this.props.user) {
        this.setState({
          user: this.props.user,
        });
        
        this.fetchDataSections(this.props.user.id);
      } else {
        this.setState({
          user: [],
        });
        this.fetchDataSections(0);
      }
    }
  }

  componentWillMount() {
    
    if (this.props.tipo !== undefined && this.props.tipo === "PROCESO"){
      if (this.props.accion === "MODIFICAR"){
       this.setState({
         atributosValor: this.props.datosEdicion,
       });
      }else{
       this.setState({
         atributosValor: [],
       });
      }
   }


    this.fetchConfigSections();
    // console.log(this.props)
    if (this.props.user) {
      const atributos = this.props.atributos.filter(
        (attr) => attr.actualizable === "S"
      );

      this.setState({
        user: this.props.user,
        atributos: atributos,
      });
      this.fetchDataSections(this.props.user.id);
    } else {
      this.setState({
        atributos: this.props.atributos,
      });
      this.fetchDataSections(0);
    }



  }

  fetchConfigSections() {
    const columnsSections = this.state.columnsSections;
    const selectedRows = this.state.selectedRows;
    const selectedRowsId = this.state.selectedRowsId;

    this.props.sections.forEach((section, i) => {
      columnsSections[i] = [];
      selectedRows[i] = [];
      selectedRowsId[i] = [];

      section.visualizacion.atributos.forEach((art) => {
        var alienacion;

        switch (art.alineacion) {
          case "IZQUIERDA":
            alienacion = "left";
            break;
          case "DERECHA":
            alienacion = "right";
            break;
          case "CENTRO":
            alienacion = "center";
            break;
          default:
            alienacion = art.alineacion;
            break;
        }

        var editor = {};
        var editable = false;

        if (art.tipologia === "CHECKBOX") {
          editable = true;
          editor = {
            type: Type.CHECKBOX,
            value: "S:N",
          };
          editable = (cell, row, rowIndex) => {
            return <Input type="checkbox" checked={cell} />;
          };
        }

        columnsSections[i].push({
          id: art.id,
          dataField: art.id,
          editable: editable,
          editor: editor,
          text: art.descripcion,
          sort: true,
          align: alienacion,
          headerAlign: "center",
          hidden:
            art.visible === "N" ||
            art.descripcion === "Habilitado" ||
            art.descripcion === "Posicion",
          headerStyle: { width: art.porcentaje + "%", wordBreak: "break-word" },
          style: { width: art.porcentaje + "%", wordBreak: "break-word" },
          tipologia: art.tipologia,
          visible: art.visible,
        });
      });
    });
  }

  fetchDataSections(userId) {
    const rowsSections = this.state.rowsSections;
    const selectedRows = this.state.selectedRows;
    const selectedRowsId = this.state.selectedRowsId;

    this.props.sections.forEach((section, i) => {
      const myHeaders = new Headers();
      myHeaders.append(
        "Authorization",
        "Bearer " + sessionStorage.getItem("token")
      );
      myHeaders.append("Content-Type", "application/json");

      var raw;
      var requestOptions;

      if (
        section.tipologia === "VISTA" ||
        section.tipologia === "HABILITACION"
      ) {
        rowsSections[i] = [];

        // console.log(section.visualizacion.id)

        raw = JSON.stringify({
          idObjeto: parseInt(section.visualizacion.id),
          filtros: {
            generales: [
              {
                atributo: parseInt(section.visualizacion.atributos[1].id),
                contenido: parseInt(userId),
              },
            ],
          },
        });

        requestOptions = {
          method: "POST",
          headers: myHeaders,
          body: raw,
          redirect: "follow",
        };

        fetch(
          store.getState().serverUrl + this.props.urls.obtenerInformacion,
          requestOptions
        )
          .then((response) => {
            if (response.status == 401) {
              sessionStorage.clear();
              window.location.reload(false);
            } else {
              return response.json();
            }
          })
          .then((result) => {
            // console.log(result)
            if (!result["Mensaje"]) {
              rowsSections[i] = result;
              selectedRows[i] = [];
              selectedRowsId[i] = [];

              const column = this.props.sections[
                i
              ].visualizacion.atributos.find((a) => a.nombre === "HABILITADO");

              if (column) {
                rowsSections[i].forEach((row) => {
                  if (row[column.id] === "S") {
                    selectedRows[i].push(row);
                    selectedRowsId[i].push(row.id);
                  }
                });
              }

              this.setState({
                selectedRows: selectedRows,
                selectedRowsId: selectedRowsId,
              });
            }
          })
          .catch((error) => {
            console.log("error", error);
            this.setState({
              errorCritico: true,
            });
          });
      } else if (section.tipologia === "MENU") {
        raw = JSON.stringify({
          usuario: parseInt(userId),
          aplicativos: "",
          idOpcion: parseInt(this.props.opcionId),
        });

        requestOptions = {
          method: "POST",
          headers: myHeaders,
          body: raw,
          redirect: "follow",
        };

        fetch(
          store.getState().serverUrl + this.props.urls.obtenerMenu,
          requestOptions
        )
          .then((response) => {
            if (response.status == 401) {
              sessionStorage.clear();
              window.location.reload(false);
            } else {
              return response.json();
            }
          })
          .then((result) => {
            if (!result["Mensaje"]) {
              const checked = [];
              if (this.props.user && result.menu.principal) {
                result.menu.principal.forEach((menus) => {
                  Object.entries(menus).forEach((menu) => {
                    menu[1].forEach((app) => {
                      if (app.menu) {
                        app.menu.forEach((menu) => {
                          menu.menu.forEach((menu) => {
                            if (menu.acciones) {
                              menu.acciones.forEach((action) => {
                                if (menu.habilitado === "1") {
                                  checked.push(menu.id + action.id);
                                }
                              });
                            } else {
                              if (menu.habilitado === "1") {
                                checked.push(menu.id);
                              }
                            }
                          });
                        });
                      }
                    });
                  });
                });
              }

              this.setState({
                menu: result.menu,
                checked: checked,
              });

              this.handleMenuTreeNodes();
            }
          })
          .catch((error) => console.log("error", error));
      }
    });
  }

  handleChange(e) {
    if (this.props.tipo !== undefined && this.props.tipo === "PROCESO"){
      this.handleOnChangeFiltros(e);
    }else{
      if (e.target) {
        var { value, name, type } = e.target;
        var target = e.target;
      } else if (e.showDropDownButton) {
        var value = e.selectedValue;
        var name = e._tbx.name;
        var type = e._tbx.type;
        var target = e._tbx;
      } else {
        var value = e.value;
        var name = e._tbx.name;
        var type = e._tbx.type;
        var target = e._tbx;
      }
  
      this.setState(({ user }) => ({
        user: {
          ...user,
          [name]: value,
        },
      }));
    }
      
  }

  handleOnSelect = (i, rowId) => {
    const rows = this.state.rowsSections;
    const row = rows[i].find((r) => r.id === rowId);
    const column = this.state.columnsSections[i].find(
      (c) => c.text === "Habilitado"
    );
    const selectedRows = this.state.selectedRows;
    const selectedRowsId = this.state.selectedRowsId;

    // console.log(column)
    // return false;
    // console.log(this.state.columnsSections[i])
    // return false;

    // Si ya estaba seleccionada, se elimina.
    if (selectedRows[i] && selectedRows[i].includes(row)) {
      row[column.id] = "N";
      selectedRows[i] = selectedRows[i].filter((r) => r !== row);
      selectedRowsId[i] = selectedRows[i].map((r) => r.id);
    } else {
      row[column.id] = "S";
      selectedRows[i].push(row);
      selectedRowsId[i].push(row.id);
    }

    this.setState({
      rowsSections: rows,
      selectedRows: selectedRows,
      selectedRowsId: selectedRowsId,
    });

    if (this.props.sections[i].id === "1004011") {
      this.handleMenuTreeNodes();
    }
  };

  handleOnSelectAll = (i) => {
    const rows = this.state.rowsSections;
    const column = this.state.columnsSections[i].find(
      (c) => c.text === "Habilitado"
    );
    const selectedRows = this.state.selectedRows;
    const selectedRowsId = this.state.selectedRowsId;

    // Si hay alguna seleccionada, se eliminan todas.
    if (selectedRows[i] && selectedRows[i].length) {
      rows[i].forEach((row) => {
        row[column.id] = "N";
      });
      selectedRows[i] = [];
      selectedRowsId[i] = [];

      // Si no hay alguna ninguna seleccionada, se agregan todas.
    } else {
      rows[i].forEach((row) => {
        row[column.id] = "S";
      });
      selectedRows[i] = rows[i];
      selectedRowsId[i] = rows[i].map((r) => r.id);
    }

    if (this.props.sections[i].id === "1004011") {
      this.handleMenuTreeNodes();
    }
  };

  handleMenuTreeNodes() {

    const nodes = [];
    if (this.state.menu) {
      const section = this.props.sections.find((s) => s.tipologia === "MENU");
      this.state.menu.principal.forEach((menus) => {
        Object.entries(menus).forEach((menu) => {
          menu[1].forEach((app) => {
            var enabled = true;
            if (app.habilitado === "1") {
              if (section.id === "1004017") {
                const columnNameId = this.state.columnsSections[2].find(
                  (c) => c.text === "Nombre"
                ).id;
                const currentApp = this.state.rowsSections[2].find(
                  (a) => a[columnNameId] === app.descripcion
                );
                if (currentApp) {
                  enabled = currentApp["1004020003"] === "S";
                } else {
                  return null;
                }
              }
              if (app.menu) {
                const childrens = app.menu.map((menu) => {
                  const childrens = menu.menu.map((menu) => {
                    if (menu.acciones) {
                      const actions = menu.acciones.map((action) => {
                        return {
                          label: action.descripcion,
                          value: menu.id + action.id,
                          disabled: !enabled,
                        };
                      });
                      return {
                        label: menu.texto,
                        value: menu.id,
                        disabled: !enabled,
                        children: actions,
                      };
                    } else {
                      return {
                        label: menu.texto,
                        value: menu.id,
                        disabled: !enabled,
                      };
                    }
                  });
                  if (childrens && childrens.length > 0) {
                    return {
                      label: menu.texto,
                      value: menu.id,
                      disabled: !enabled,
                      children: childrens,
                    };
                  } else {
                    return {
                      label: menu.texto,
                      value: menu.id,
                      disabled: !enabled,
                    };
                  }
                });
                nodes.push({
                  label: app.descripcion,
                  value: app.id,
                  disabled: !enabled,
                  children: childrens,
                });
              } else {
                nodes.push({
                  label: app.descripcion,
                  value: app.id,
                  disabled: !enabled,
                });
              }
            }
          });
        });
      });
    }

    this.setState({
      menuTreeNodes: nodes,
    });
  }

  handleCloseErroModal() {
    this.setState({
      estado: 0,
      buttonDisable: false,
      validation: "needs-validation",
      errorsUser: [],
      errorMessages: [],
    });
  }

  handleCloseThis(render) {
    if (this.props.tipo !== undefined && this.props.tipo === "PROCESO"){
      var accionConfirmar = this.props.acciones.find(
        (item) => item.tipo === "CONFIRMAR",
      );  
      this.props.overlayShowHide(accionConfirmar);
      this.setState({
        estado: 0,
        buttonDisable: false,
        validation: "needs-validation",
        errorsUser: [],
        errorMessages: [],
        atributosValor: [],
        checked: [],
        expanded: [],

      });
    }else{
      this.props.overlayShowHide();

      this.setState({
        estado: 0,
        buttonDisable: false,
        validation: "needs-validation",
        errorsUser: [],
        errorMessages: [],
      });
      if (!this.props.user) {
        this.setState({
          user: [],
          checked: [],
          expanded: [],
        });
        this.fetchDataSections(0);
      } else {
        this.setState({
          user: this.props.user,
          checked: [],
          expanded: [],
        });
        this.fetchDataSections(this.props.user.id);
      }
      if (render) {
        store.dispatch({
          type: "RE_RENDER",
          rerender: true,
        });
      }
    }

  }

  handleKeyDown(e) {
    if (e.key === "Enter") {
      this.handleSubmit();
    }
  }

  handleValidation() {
    var validacion = true;
    const errorsUser = [];

    if (this.state.user) {
      this.state.atributos.forEach((art) => {
        if (this.state.user[art.id]) {
          if (art.tipologia === "TEXTO") {
            if (art.tipo === "CARACTER" && art.estilo === "MAYUSCULAS") {
              const newState = this.state.user;
              newState[art.id] = newState[art.id].toUpperCase();
              this.setState({
                user: newState,
              });
            }
            if (art.tipo === "NUMERICO") {
              if (
                (art.maximo &&
                  parseFloat(this.state.user[art.id]) >
                    parseFloat(art.maximo)) ||
                (art.minimo &&
                  parseFloat(this.state.user[art.id]) < parseFloat(art.minimo))
              ) {
                const newState = this.state.user;
                newState[art.id] = "";
                validacion = false;
                errorsUser.push(
                  "El numero ingresado en " +
                    art.descripcion +
                    " sobre pasa los limites"
                );
              }
            }
          }
          if (art.tipologia === "PASSWORD" && art.estilo === "MAYUSCULAS") {
            const newState = this.state.user;
            newState[art.id] = newState[art.id].toUpperCase();
            this.setState({
              user: newState,
            });
          }
        } else {
          if (art.obligatorio === "S") {
            validacion = false;
          }
        }
      });
    } else {
      validacion = false;
    }

    this.setState({
      errorsUser: errorsUser,
    });

    return validacion;
  }

  handleValidationProceso() {
    var validacion = true;
    const errorsUser = [];

    if (this.state.atributosValor) {
      this.state.atributos.forEach((art) => {
        if (this.state.atributosValor[art.id] && art.visible === "S") {
          if (art.tipologia === "TEXTO") {
            if (art.tipo === "CARACTER" && art.estilo === "MAYUSCULAS") {
              const newState = this.state.atributosValor;
              newState[art.id] = newState[art.id].toUpperCase();
              this.setState({
                atributosValor: newState,
              });
            }
            if (art.tipo === "NUMERICO") {
              if (
                (art.maximo &&
                  parseFloat(this.state.atributosValor[art.id]) >
                    parseFloat(art.maximo)) ||
                (art.minimo &&
                  parseFloat(this.state.atributosValor[art.id]) < parseFloat(art.minimo))
              ) {
                const newState = this.state.atributosValor;
                newState[art.id] = "";
                validacion = false;
                errorsUser.push(
                  "El numero ingresado en " +
                    art.descripcion +
                    " sobre pasa los limites"
                );
              }
            }
          }
          if (art.tipologia === "PASSWORD" && art.estilo === "MAYUSCULAS") {
            const newState = this.state.atributosValor;
            newState[art.id] = newState[art.id].toUpperCase();
            this.setState({
              atributosValor: newState,
            });
          }
        } else {
          if (art.obligatorio === "S" && art.visible === "S") {
            validacion = false;
          }
        }
      });
    } else {
      validacion = false;
    }

    this.setState({
      errorsUser: errorsUser,
    });

    return validacion;
  }

  async handleOnChangeFiltros(e) {

    if (e.target) {
      var { value, name, type } = e.target;
    } else if (e.inputType === "text") {
      var value = e.selectedValue;
      var name = e._tbx.name;
      var type = e._tbx.type;
    } else if (e.inputType === "tel") {
      var value = e._tbx.value;
      var name = e._tbx.name;
      var type = e._tbx.type;
    }
    if (name != undefined) {
      if (type === "file") {
        var file = e.target.files[0];
        const fileToBase64 = (file) =>
          new Promise((resolve, reject) => {
            const reader = new FileReader();
            reader.readAsDataURL(file);
  
            reader.onload = () => 
              resolve(reader.result.replace(/^data:.+;base64,/, ""));
  
            reader.onerror = (error) => reject(error);
          });
        var value = await fileToBase64(file);
  
        this.setState({ fileValor: e });
      }
      const prevState = this.state;
      this.setState({
        atributosValor: {  
          ...prevState.atributosValor,
          [name]: value,
        },
      });
    }
  }

  handleOnChangeFiltrosMultiple(e) {
    if (e.target) {
      var { value, name } = e.target;
    } else if (e.inputType === "text") {
      var value = e.selectedValue;
      var name = e._tbx.name;
    } else if (e.inputType === "tel") {
      var value = e._tbx.value;
      var name = e._tbx.name;
    }
    const prevState = this.state;
    var checkboxs;
    if (prevState.atributosValor[name] === undefined || prevState.atributosValor[name] === "") {
      checkboxs = [];
    } else {
      checkboxs = prevState.atributosValor[name].split(",");
    }

    // Si ya estaba seleccionada, se elimina.
    if (checkboxs.includes(value)) {
      this.setState({
        atributosValor: {
          ...prevState.atributosValor,
          [name]: checkboxs.filter((v) => v !== value).toString(),
        },
      });

      // Si no estaba seleccionada, se agrega.
    } else {
      checkboxs.push(value);
      this.setState({
        atributosValor: {
          ...prevState.atributosValor,
          [name]: checkboxs.toString(),
        },
      });
    }
    
  }



  handleSubmit() {
    this.setState({
      validation: "needs-validation was-validated",
    });
    
    if (this.props.tipo !== undefined && this.props.tipo === "PROCESO"){
      if (this.handleValidationProceso()) { 
        this.sendFormProceso();
      }
    }else{
      if (this.handleValidation()) {
        this.sendForm();
      }
    }
  }

  sendFormProceso() {

    var accionConfirmar = this.props.acciones.find(
      (item) => item.tipo === "CONFIRMAR",
    );

    var myHeaders;
    var raw;
    var requestOptions;

    myHeaders = new Headers();
    myHeaders.append("Authorization", "Bearer " + sessionStorage.getItem("token"));
    myHeaders.append("Content-Type", "application/json");

    const user = this.state.user;
    const atributos = this.state.atributos.map((attr) => {
      const id = attr.id;
      var value = null;
      if (this.state.atributosValor[id] != undefined && this.state.atributosValor[id] != ""){
          value = this.state.atributosValor[id];
      }

      return {
        id: id,
        valor: value,
      };
    });
    if (this.props.accion === "MODIFICAR"){
      atributos.push({
        id: this.props.atributoSeleccion,
        valor: this.props.valorSeleccion,
      });
    }

    const sectores = this.state.rowsSections.map((section, i) => {
      const registros = section.map((row, j) => {
        const atributos = Object.entries(row).map((art) => {
          return {
            id: art[0],
            valor: art[1],
          };
        });

        return {
          posicion: j + 1,
          atributos: atributos,
        };
      });

      return {
        objeto: this.props.sections[i].id,
        visualizacion: this.props.sections[i].visualizacion.id,
        registros: registros,
      };
    });

    raw = {
      idObjeto: parseInt(this.props.objectId),
      idOpcion: parseInt(this.props.opcionId),
      idAccion: parseInt(accionConfirmar.id),
      idFormato: "",
      datos: {
        atributos: atributos,
        sectores: sectores,
      },
    };
    console.log("a grabar", raw);
    requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: JSON.stringify(raw),
      redirect: "follow",
    };

    fetch(store.getState().serverUrl + URLS.PROCESOS_PROCESAR, requestOptions,)
        .then((response) => {
          if (response.status == 401) {
            sessionStorage.clear();
            window.location.reload(false);
          } else {
            return response.text();
          }
        })
        .then((result) => {
          if (result === "" || result === "{}") {
            this.setState({
              buttonDisable: false,
              estado: 200,
            });
          } else {
            const error = JSON.parse(result);
            // console.log(error)
            if (error.Mensajes) {
              this.setState({
                buttonDisable: false,
                estado: 400,
                errorMessages: error.Mensajes,
              });
            } else {
              this.setState({
                buttonDisable: false,
                estado: 400,
                errorMessages: [{ mensaje: error.Mensaje }],
              });
            }
          }
        })
        .catch((error) => {
          // console.log('error', error)
          this.setState({
            buttonDisable: false,
            estado: 400,
            errorMessages: error,
          });
        });
  }


  sendForm() {
    const myHeaders = new Headers();
    myHeaders.append(
      "Authorization",
      "Bearer " + sessionStorage.getItem("token")
    );
    myHeaders.append("Content-Type", "application/json");

    const user = this.state.user;
    const atributos = this.state.atributos.map((attr) => {
      const id = attr.id;
      var value = user[id];

      if (attr.tipologia === "PASSWORD") {
        value = btoa(value);
      }

      return {
        id: id,
        valor: value,
      };
    });

    const sectores = this.state.rowsSections.map((section, i) => {
      // console.log(this.state.rowsSections)
      // console.log(section)
      const registros = section.map((row, j) => {
        const atributos = Object.entries(row).map((art) => {
          return {
            id: art[0],
            valor: art[1],
          };
        });

        return {
          posicion: j + 1,
          atributos: atributos,
        };
      });

      return {
        objeto: this.props.sections[i].id,
        visualizacion: this.props.sections[i].visualizacion.id,
        registros: registros,
      };
    });

    const section = this.props.sections.filter((s) => s.tipologia === "MENU");
    const menu = this.state.menu.principal.map((menus) => {
      Object.entries(menus).forEach((menu) => {
        menu[1].forEach((app) => {
          if (app.menu) {
            var enabled = true;

            if (section.id === "1004017") {
              const currentApp = this.state.rowsSections[2].find(
                (a) => a.id === parseInt(app.id)
              );

              if (currentApp) {
                enabled = currentApp["1004020003"] === "S";
              }
            }

            app.menu.forEach((menu) => {
              menu.habilitado = "0";
              menu.menu.forEach((m) => {
                m.habilitado = "0";
                if (m.acciones) {
                  m.acciones.forEach((action) => {
                    if (
                      enabled &&
                      this.state.checked.includes(m.id + action.id)
                    ) {
                      action.habilitado = "1";
                      m.habilitado = "1";
                      menu.habilitado = "1";
                    } else {
                      action.habilitado = "0";
                    }
                  });
                } else {
                  if (enabled && this.state.checked.includes(m.id)) {
                    m.habilitado = "1";
                    menu.habilitado = "1";
                  } else {
                    m.habilitado = "0";
                  }
                }
              });
            });
          }
        });
      });
      return menus;
    });

    const raw = JSON.stringify({
      datos: {
        objeto: this.props.objectId,
        atributos: atributos,
        sectores: sectores,
        menu: {
          principal: menu,
        },
      },
    });

    var method = null;
    var url = null;

    if (this.props.user) {
      url = this.props.urls.editar;
      method = "POST";
    } else {
      url = this.props.urls.agregar;
      method = "POST";
    }

    // console.log(raw)
    // return false;

    const requestOptions = {
      method: method,
      headers: myHeaders,
      body: raw,
      redirect: "follow",
    };

    fetch(store.getState().serverUrl + url, requestOptions)
      .then((response) => {
        if (response.status == 401) {
          sessionStorage.clear();
          window.location.reload(false);
        } else {
          return response.text();
        }
      })
      .then((result) => {
        // console.log(result)
        if (result === "") {
          this.setState({
            buttonDisable: false,
            estado: 200,
          });
        } else {
          const error = JSON.parse(result);
          // console.log(error)
          if (error.Mensajes) {
            this.setState({
              buttonDisable: false,
              estado: 400,
              errorMessages: error.Mensajes,
            });
          } else {
            this.setState({
              buttonDisable: false,
              estado: 400,
              errorMessages: [{ mensaje: error.Mensaje }],
            });
          }
        }
      })
      .catch((error) => {
        // console.log('error', error)
        this.setState({
          buttonDisable: false,
          estado: 400,
          errorMessages: error,
        });
      });
  }

  render() {
    if (this.state.errorCritico) {
      return <GlobalError></GlobalError>;
    } else {
      if (this.props.overlayShow) {
        switch (this.state.estado) {
          case 400:
            return this.renderErrorModal();
          case 200:
            return this.renderSuccessModal();
          default:
            return this.renderInputModal();
        }
      } else {
        return null;
      }
    }
  }

  renderErrorModal() {
    return (
      <div className="right-overlay">
        <div className="right-content right-content-big bg-white shadow-lg">
          <div className="right-header">
            <div className="row d-flex justify-content-between align-items-center">
              <div className="titleContainer">
                <h6>Error</h6>
              </div>
              <div className="buttonContainer">
                <button
                  className="btn btn-close"
                  onClick={() => this.handleCloseThis(false)}
                >
                  <i className="fi fi-rs-cross"></i>
                </button>
              </div>
            </div>
          </div>
          <div className="right-body bg-white">
            <div className="row">
              <div className="error col-lg-12 d-flex flex-column justify-content-center align-items-center">
                <div className="iconContainer">
                  <i className="fi fi-rs-cross"></i>
                </div>
                <p>Ocurrió un error </p>
                {this.state.errorMessages
                  ? this.state.errorMessages.map((error, i) => {
                      return (
                        <div
                          className="alert alert-danger"
                          role="alert"
                          key={i}
                        >
                          {error.mensaje}
                        </div>
                      );
                    })
                  : null}
              </div>
            </div>
          </div>
          <div className="right-footer d-flex justify-content-center align-items-stretch">
            <button
              disabled={this.state.buttonDisable}
              className="btn btn-primary"
              onClick={() => this.handleCloseErroModal()}
            >
              Volver
            </button>
          </div>
        </div>
      </div>
    );
  }

  renderSuccessModal() {
    return (
      <div className="right-overlay">
        <div className="right-content right-content-big bg-white shadow-lg">
          <div className="right-header">
            <div className="row d-flex justify-content-between align-items-center">
              <div className="titleContainer">
                {this.props.user ? (
                  <h6>Actualizado con éxito</h6>
                ) : (
                  <h6>Añadido con éxito!</h6>
                )}
              </div>
              <div className="buttonContainer">
                <button
                  className="btn btn-close"
                  onClick={() => this.handleCloseThis(true)}
                >
                  <i className="fi fi-rs-cross"></i>
                </button>
              </div>
            </div>
          </div>
          <div className="right-body bg-white">
            <div className="row">
              <div className="success col-lg-12 d-flex flex-column justify-content-center align-items-center">
                <div className="iconContainer">
                  <i className="fi fi-rs-social-network"></i>
                </div>
                {this.props.user ? (
                  <p>Se actualizó con éxito!</p>
                ) : (
                  <p>Se añadió con éxito!</p>
                )}
              </div>
            </div>
          </div>
          <div className="right-footer d-flex justify-content-center align-items-stretch">
            <button
              disabled={this.state.buttonDisable}
              className="btn btn-primary"
              onClick={() => this.handleCloseThis(true)}
            >
              Cerrar
            </button>
          </div>
        </div>
      </div>
    );
  }

  renderInputModal() {
    return (
      <div className="right-overlay">
        <div className="right-content right-content-big bg-white shadow-lg">
          <div className="right-header">
            <div className="row d-flex justify-content-between align-items-center">
              <div className="titleContainer">
                <h6>Guardar</h6>
              </div>
              <div className="buttonContainer">
                <button
                  className="btn btn-close"
                  onClick={() => this.handleCloseThis(false)}
                >
                  <i className="fi fi-rs-cross"></i>
                </button>
              </div>
            </div>
          </div>
          <div className="right-body bg-white">
            <div className="row">
              <div className="col-lg-12">
                <form
                  className={this.state.validation}
                  noValidate
                  autoComplete="off"
                  onKeyDown={(e) => this.handleKeyDown(e)}
                >
                  {this.renderUserAttributes()}
                  {this.renderSections()}
                </form>
              </div>
            </div>
          </div>
          {this.renderButtonsAction()}
        </div>
      </div>
    );
  }

  renderUserAttributes() {
    return (
      <div className="right-body bg-white">
        <div className="row">
          <div className="col-lg-12">
            <label className="text-muted">* Campos obligatorios</label>
            {this.state.atributos.map((art) => {
              if (this.props.tipo !== undefined && this.props.tipo === "PROCESO"){
                return (
                  <Input
                    alineacion={art.alineacion}
                    ancho={art.ancho}
                    atr_valor={art.atr_valor}
                    atr_nombre={art.atr_nombre}
                    decimales={art.decimales}
                    descripcion={art.descripcion}
                    editable={art.editable}
                    estilo={art.estilo}
                    id={art.id}
                    key={art.id}
                    longitud={art.longitud}
                    mascara={art.mascara}
                    maximo={art.maximo}
                    minimo={art.minimo}
                    nombre={art.nombre}
                    obligatorio={art.obligatorio}
                    observaciones={art.observaciones}
                    onChange={(e) => this.handleChange(e)}
                    onChangeMultiple={(e) => this.handleOnChangeFiltrosMultiple(e)}
                    orden={art.orden}
                    origen={art.origen}
                    prefijo={art.prefijo}
                    sufijo={art.sufijo}
                    tipo={art.tipo}
                    tipologia={art.tipologia}
                    value={art.value}
                    visible={art.visible}
                    visualizacion={art.visualizacion}
                    atributos={art.visualizacion.atributos}
                  />
                );
            }else{
              return (
                <Input
                  alineacion={art.alineacion}
                  ancho={art.ancho}
                  atr_valor={art.atr_valor}
                  atr_nombre={art.atr_nombre}
                  decimales={art.decimales}
                  descripcion={art.descripcion}
                  editable={art.editable}
                  estilo={art.estilo}
                  id={art.id}
                  key={art.id}
                  longitud={art.longitud}
                  mascara={art.mascara}
                  maximo={art.maximo}
                  minimo={art.minimo}
                  nombre={art.nombre}
                  obligatorio={art.obligatorio}
                  observaciones={art.observaciones}
                  onChange={(e) => this.handleChange(e)}
                  onChangeMultiple={(e) => this.handleOnChangeFiltrosMultiple(e)}
                  orden={art.orden}
                  origen={art.origen}
                  prefijo={art.prefijo}
                  sufijo={art.sufijo}
                  tipo={art.tipo}
                  tipologia={art.tipologia}
                  value={this.state.user[art.id]}
                  visible={art.visible}
                  visualizacion={art.visualizacion}
                  atributos={art.visualizacion.atributos}
                />
              );
            }
            })}
            {this.state.errorsUser
              ? this.state.errorsUser.map((error, i) => {
                  return (
                    <div className="alert alert-danger" role="alert" key={i}>
                      {error}
                    </div>
                  );
                })
              : null}
          </div>
        </div>
      </div>
    );
  }

  renderSections() {
    return this.props.sections.map((section, i) => {
      if (
        section.tipologia === "VISTA" ||
        section.tipologia === "HABILITACION"
      ) {
        const selectRow = {
          mode: "checkbox",
          clickToSelect: false,
          selected: this.state.selectedRowsId[i],
          onSelectAll: () => this.handleOnSelectAll(i),
          onSelect: (row) => this.handleOnSelect(i, row.id),
        };

        const optionsPagination = {
          hideSizePerPage: true, // Hide the sizePerPage dropdown always
          showTotal: false,
          disablePageTitle: false,
        };

        const { SearchBar } = Search;
        return (
          <ToolkitProvider
            keyField="id"
            data={this.state.rowsSections[i]}
            columns={this.state.columnsSections[i]}
            search
          >
            {(props) => (
              <div className="container" id={section.id} key={section.id}>
                <div className="container divider2 mt-5 mb-5" />
                <h5 className="global-title-2 pb-1">{section.descripcion}</h5>
                <div className="row">
                  {this.state.columnsSections[i] &&
                  this.state.rowsSections[i] ? (
                    <div className="col-12 mt-1">
                      <SearchBar
                        className="input-search"
                        placeholder="Buscar"
                        {...props.searchProps}
                      />
                      <BootstrapTable
                        key={"456456465461231231234"}
                        cellEdit={cellEditFactory({
                          mode: "click",
                          blurToSave: true,
                        })}
                        striped
                        hover
                        headerClasses="header-class"
                        rowStyle={{ fontSize: "14px" }}
                        selectRow={selectRow}
                        pagination={paginationFactory(optionsPagination)}
                        {...props.baseProps}
                      />
                    </div>
                  ) : (
                    <p className="text-center"> Sin datos para Mostrar </p>
                  )}
                </div>
              </div>
            )}
          </ToolkitProvider>
        );
      } else if (section.tipologia === "MENU") {
        return (
          <div className="container pt-5" id={section.id} key={section.id}>
            <div className="container divider2 mt-5 mb-5" />
            <h5 className="global-title-2 pb-4">{section.descripcion}</h5>
            {this.state.menu ? (
              this.renderMenu()
            ) : (
              <p className="text-center"> El Menu aun no esta disponible </p>
            )}
          </div>
        );
      } else {
        return null;
      }
    });
  }

  renderMenu() {
    
    return this.state.menuTreeNodes ? (
      <div className="row">
        <div className="col-12">
          <div className="w-100">
            <CheckboxTree
              nodes={this.state.menuTreeNodes}
              iconsClass="fa5"
              checked={this.state.checked}
              expanded={this.state.expanded}
              onCheck={(checked) => this.setState({ checked })}
              onExpand={(expanded) => this.setState({ expanded })}
              showExpandAll={true}
              showNodeIcon={false}
            />
          </div>
        </div>
      </div>
    ) : null;
  }

  renderButtonsAction() {
    return (
      <div className="right-footer d-flex justify-content-center align-items-stretch">
        <button
          disabled={this.state.buttonDisable}
          className="btn btn-secondary mr-2"
          onClick={() => this.handleCloseThis(false)}
        >
          Cancelar
        </button>
        <button
          disabled={this.state.buttonDisable}
          className="btn btn-primary"
          onClick={() => this.handleSubmit()}
        >
          <span>Guardar</span>
        </button>
      </div>
    );
  }
}
export default OverlayMenu;
