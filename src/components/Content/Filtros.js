// Generic
import React, { Component, Fragment } from "react";
import $ from "jquery";
import Tooltip from "react-simple-tooltip";
import store from "../../redux/store";
import { Link } from "react-scroll";
import XLSX from "xlsx";
import URLS from "../../urls";

// Components
import Input from "../GlobalComponents/Input";
import RightOverlay from "../RightOverlay/RightOverlay";
import OverlayMenu from "../OverlayMenu/OverlayMenu";
import AlertModal from "../GlobalComponents/AlertModal";

// Css
import "../../assets/css/owl.carousel.min.css";
import "../../assets/css/owl.theme.default.css";
import Loader from "../GlobalComponents/Loader";

// Images
import PDF from "../../assets/images/PDF.svg";
import XLSXi from "../../assets/images/XLSX.svg";
import XML from "../../assets/images/XML.svg";
import TXT from "../../assets/images/TXT.svg";
import JSONI from "../../assets/images/JSON.svg";

class Filtros extends Component {
  constructor(props) {
    super(props);
    this.state = {
      overlayContent: "",
      overlayShow: false,
      overlayShowEdit: false,
      modal: false,
      selectedRows: [],
      buttonDisable: false,
      modalBlanqueo: false,
      buttonDisableBlanqueo: false,
      blanqueoOk: false,
      modeloLoading: false,
      modalModeloShow: false,
      modeloData: false,
      importarLoading: false,
      modalimportarShow: false,
      importarData: false,
      descargarLoading: false,
      descargarOpciones: false,
      visualizarLoading: false,
      modalVisualizarShow: false,
      visualizarData: false,
      descargarData: false,
      deleteError: false,
      modalError: false
    };
    this.handleDropDownColumnas = this.handleDropDownColumnas.bind(this);
    this.overlayShowHide = this.overlayShowHide.bind(this);
    this.overlayShowHideEdit = this.overlayShowHideEdit.bind(this);
    this.modalOpen = this.modalOpen.bind(this);
    this.handleEliminar = this.handleEliminar.bind(this);
    this.handleFiltrosAvanzados = this.handleFiltrosAvanzados.bind(this);
    this.modalBlanqueoOpen = this.modalBlanqueoOpen.bind(this);
    this.handleBlanquearContra = this.handleBlanquearContra.bind(this);
    this.handleModalModelo = this.handleModalModelo.bind(this);
    this.handleModalImportar = this.handleModalImportar.bind(this);
    this.handleModalVisualizar = this.handleModalVisualizar.bind(this);
    this.handleModalDescargar = this.handleModalDescargar.bind(this);
    this.modalError = this.modalError.bind(this);
    this.OpenmodalError = this.OpenmodalError.bind(this);
  }

  componentWillMount() {
    this.setState({
      selectedRows: [],
    });
  }

  componentDidUpdate(prevProps) {
    if (this.props.selectedRows !== prevProps.selectedRows) {
      if (this.props.grillaTipo === "wijmo") {
        if (this.props.selectedRows !== undefined) {
          var arrayTemporal = [];
          this.props.selectedRows.map((item) => {
            arrayTemporal.push(item._data);
          });
          if (this.props.selectedRows !== this.state.selectedRows) {
            this.setState({
              selectedRows: arrayTemporal,
            });
          }
        }
      } else {
        if (this.props.selectedRows !== this.state.selectedRows) {
          this.setState({
            selectedRows: this.props.selectedRows,
          });
        }
      }
    }
  } // End componentDidUpdate

  overlayShowHideBlanqueo() {
    this.setState((prevState) => ({
      blanqueoOk: !prevState.blanqueoOk,
    }));
  }

  overlayShowHide() {
    this.setState((prevState) => ({
      stateAgregarModal: 0,
      overlayShow: !prevState.overlayShow,
    }));
  }

  overlayShowHideEdit() {
    this.setState((prevState) => ({
      stateEditarModal: 0,
      overlayShowEdit: !prevState.overlayShowEdit,
    }));
  }

  handleModalModelo() {
    this.setState((prevState) => ({
      modalModeloShow: !prevState.modalModeloShow,
    }));
  }

  handleModalImportar() {
    this.setState((prevState) => ({
      modalImportarShow: !prevState.modalImportarShow,
    }));
  }

  handleModalVisualizar() {
    this.setState((prevState) => ({
      modalVisualizarShow: !prevState.modalVisualizarShow,
    }));
  }

  handleModalDescargar() {
    this.setState((prevState) => ({
      modalDescargarShow: !prevState.modalDescargarShow,
    }));
  }

  handleDropDownColumnas() {
    $("#dropdownColumnasContent").toggleClass("active");
  }

  handleFiltrosAvanzados() {
    this.props.handleSendFiltrosAvanzados();
    this.handleDropDownColumnas();
  }

  OpenmodalError() {
    this.setState((prevState) => ({
      modalError: !prevState.modalError,
    }));
  }

  handleAgregar(atributos) {
    //console.log(atributos)
    const myHeaders = new Headers();
    myHeaders.append(
      "Authorization",
      "Bearer " + sessionStorage.getItem("token"),
    );
    myHeaders.append("Content-Type", "application/json");

    var atributosOrdenados = [];
    var atributoAdjunto = [];

    atributos.map((item) => {
      if (typeof item.valor === "object") {
        atributoAdjunto.push(item);
      } else {
        atributosOrdenados.push(item);
      }
    });

    if (atributoAdjunto[0] !== undefined) {
      atributosOrdenados.push(atributoAdjunto[0]);
    }

    var cabecera = parseInt(this.props.idCabecera);
    var opcion = parseInt(this.props.opcion);
    var objeto = parseInt(this.props.objectId);

    var raw = JSON.stringify({
      datos: {
        objeto: objeto,
        opcion: opcion,
        atributos: atributos,
        relacionados: this.props.relacionados,
        cabecera: cabecera,
      },
    });

    // return false;

    var requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: raw,
      redirect: "follow",
    };

    fetch(store.getState().serverUrl + this.props.urls.agregar, requestOptions)
      .then((response) => {
        if (response.status == 401) {
          sessionStorage.clear();
          window.location.reload(false);
        }else{
          return response.text();
        }
      })
      .then((result) => {
        if (result === "") {
          this.setState({
            stateAgregarModal: 200,
          });
          if (this.props.opcion === "2040100") {
            this.props.renderizar();
          }
        } else {
          const error = JSON.parse(result);

          if (error.Mensajes) {
            this.setState({
              stateAgregarModal: 400,
              errorMessagesAgregar: error.Mensajes,
            });
            if (this.props.opcion === "2040100") {
              this.props.renderizar();
            }
          } else {
            this.setState({
              stateAgregarModal: 400,
              errorMessagesAgregar: [{ mensaje: error.Mensaje }],
            });
            if (this.props.opcion === "2040100") {
              this.props.renderizar();
            }
          }
        }
      })
      .catch((error) => {
        this.setState({
          stateAgregarModal: 400,
          errorMessagesAgregar: JSON.stringify(error),
        });
      });
  }

  handleEditar(atributos) {
    const myHeaders = new Headers();
    myHeaders.append(
      "Authorization",
      "Bearer " + sessionStorage.getItem("token"),
    );
    myHeaders.append("Content-Type", "application/json");

    var atributosOrdenados = [];
    var atributoAdjunto = [];

    atributos.map((item) => {
      if (typeof item.valor === "object") {
        atributoAdjunto.push(item);
      } else {
        atributosOrdenados.push(item);
      }
    });

    if (atributoAdjunto[0] !== undefined) {
      atributosOrdenados.push(atributoAdjunto[0]);
    }

    var accionEditar = this.props.acciones.find(
      (item) => item.descripcion === "Modificar",
    ).id;

    var objeto = parseInt(this.props.objectId);
    var opcion = parseInt(this.props.opcion);
    var accion = parseInt(accionEditar);
    var cabecera = parseInt(this.props.idCabecera);

    var raw = JSON.stringify({
      datos: {
        objeto: objeto,
        opcion: opcion,
        atributos: atributosOrdenados,
        relacionados: this.props.relacionados,
        cabecera: cabecera,
        accion: accion,
      },
    });

    var requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: raw,
      redirect: "follow",
    };

    fetch(store.getState().serverUrl + this.props.urls.editar, requestOptions)
      .then((response) => {
        if (response.status == 401) {
          sessionStorage.clear();
          window.location.reload(false);
        }else{
          return response.text();
        }
      })
      .then((result) => {
        if (result === "") {
          this.setState({
            stateEditarModal: 200,
          });
          if (this.props.opcion === "2040100") {
            this.props.renderizar();
          }
        } else {
          const error = JSON.parse(result);
          if (error.Mensajes) {
            this.setState({
              stateEditarModal: 400,
              errorMessagesEditar: error.Mensajes,
            });
            if (this.props.opcion === "2040100") {
              this.props.renderizar();
            }
          } else {
            this.setState({
              stateEditarModal: 400,
              errorMessagesEditar: [{ mensaje: error.Mensaje }],
            });
            if (this.props.opcion === "2040100") {
              this.props.renderizar();
            }
          }
        }
      })
      .catch((error) => {
        this.setState({
          stateEditarModal: 400,
          errorMessagesEditar: JSON.stringify(error),
        });
      });
  }

  handleBlanquearContra() {
    this.setState({
      buttonDisableBlanqueo: true,
    });

    var email = this.props.selectedRows[0]["1004010004"];
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");

    var raw = JSON.stringify({
      email: email,
    });

    var requestOptions = {
      method: "POST",
      headers: myHeaders,
      redirect: "follow",
      body: raw,
    };

    fetch(
      store.getState().serverUrl + URLS.SEGURIDAD_RECUPERAR_CLAVE,
      requestOptions,
    )
      .then((response) => {
        if (response.status == 401) {
          sessionStorage.clear();
          window.location.reload(false);
        }else{
          return response.text();
        }
      })
      .then((result) => {
        this.setState({
          buttonDisableBlanqueo: false,
          modalBlanqueo: false,
          blanqueoOk: true,
        });

        return (
          <RightOverlay
            overlayShowHide={() => this.overlayShowHideBlanqueo()}
            overlayShow={this.state.blanqueoOk}
            state={200}
            edicion={this.props.edicion}
          />
        );
      })
      .catch((error) => console.log("error", error));
  }

  handleEliminar() {
    this.setState({
      buttonDisable: true,
    });
    var atributos = Object.entries(this.state.selectedRows).map((item) => {
      return {
        id: this.props.objectSelectedId,
        valor: item[1][this.props.objectSelectedId],
      };
    });

    var myHeaders = new Headers();
    myHeaders.append(
      "Authorization",
      "Bearer " + sessionStorage.getItem("token"),
    );
    myHeaders.append("Content-Type", "application/json");

    var accionEliminar = this.props.acciones.find(
      (item) => item.descripcion === "Eliminar",
    ).id;

    var objeto = parseInt(this.props.objectId);
    var opcion = parseInt(this.props.opcion);
    var accion = parseInt(accionEliminar);
    var cabecera = parseInt(this.props.idCabecera);

    var raw = JSON.stringify({
      datos: {
        objeto: objeto,
        opcion: opcion,
        atributos: atributos,
        relacionados: this.props.relacionados,
        accion: accion,
        cabecera: cabecera,
      },
    });

    var requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: raw,
      redirect: "follow",
    };

    fetch(store.getState().serverUrl + this.props.urls.eliminar, requestOptions)
      .then((response) => {
        if (response.status == 401) {
          sessionStorage.clear();
          window.location.reload(false);
        }else{
          return response.text();
        }
      })
      .then((result) => {
        var mensaje;
        if (result.includes("Mensajes")) {
          mensaje = JSON.parse(result);
          this.setState({
            modalError: true,
            buttonDisable: false,
            modal: false,
            mensajesError: mensaje,
          });
          if (this.props.opcion === "2040100") {
            this.props.renderizar();
          }
        } else {
          if (result === "") {
            this.setState({
              buttonDisable: false,
              modal: false,
            });
            store.dispatch({
              type: "RE_RENDER",
              rerender: true,
            });
            if (this.props.opcion === "2040100") {
              this.props.renderizar();
            }
          } else {
            this.setState({
              buttonDisable: false,
              modal: false,
            });
            store.dispatch({
              type: "RE_RENDER",
              rerender: true,
            });
            if (this.props.opcion === "2040100") {
              this.props.renderizar();
            }
          }
        }
      })
      .catch((error) => {
        console.log("error", error);
        this.setState({
          buttonDisable: false,
          estado: 400,
        });
      });
  }

  async handleImportar(file) {
    if (file) {
      var myHeaders = new Headers();
      myHeaders.append(
        "Authorization",
        "Bearer " + sessionStorage.getItem("token"),
      );
      myHeaders.append("Content-Type", "application/json");

      const fileToBase64 = (file) =>
        new Promise((resolve, reject) => {
          const reader = new FileReader();
          reader.readAsDataURL(file);
          reader.onload = () =>
            resolve(reader.result.replace(/^data:.+;base64,/, ""));
          reader.onerror = (error) => reject(error);
        });

      const content = await fileToBase64(file);
      var cabecera = parseInt(this.props.idCabecera);

      var raw;

      if (this.props.opcionId === undefined) {
        raw = JSON.stringify({
          idObjeto: parseInt(this.props.objectId),
          contenido: content,
          idCabecera: cabecera,
        });
      } else {
        raw = JSON.stringify({
          idObjeto: parseInt(this.props.objectId),
          idOpcion: parseInt(this.props.opcionId),
          contenido: content,
          idCabecera: cabecera,
        });
      }
      
      var requestOptions = {
        method: "POST",
        headers: myHeaders,
        body: raw,
        redirect: "follow",
      };

      this.setState({
        importarLoading: true,
      });

      fetch(
        store.getState().serverUrl + this.props.urls.importar,
        requestOptions,
      )
        .then((response) => {
          if (response.status == 401) {
            sessionStorage.clear();
            window.location.reload(false);
          }
          if (response.status === 201) {
            return response.text();
          } else {
            return response.json();
          }
        })

        .then((result) => {
          this.setState({
            importarLoading: false,
            importarData: result,
          });
          store.dispatch({
            type: "RE_RENDER",
            rerender: true,
          });
          this.handleModalImportar();
        })
        .catch((error) => {
          this.setState({
            importarLoading: false,
            importarData: error,
          });
          this.handleModalImportar();
          console.log("error", error);
        });
    }
  }

  handleModelo() {
    this.setState({
      modeloLoading: true,
    });

    var myHeaders = new Headers();
    myHeaders.append(
      "Authorization",
      "Bearer " + sessionStorage.getItem("token"),
    );
    myHeaders.append("Content-Type", "application/json");

    var requestOptions = {
      method: "GET",
      headers: myHeaders,
      redirect: "follow",
    };

    var cabecera = parseInt(this.props.idCabecera);

    // Preguntamos si viene o no id de opción dependiendo del componente
    let propiedades;

    if (
      this.props.opcionId === undefined &&
      this.props.idCabecera === undefined
    ) {
      propiedades = this.props.objectId;
    } else if (this.props.idCabecera === undefined) {
      propiedades = this.props.objectId + "/" + this.props.opcionId;
    } else {
      propiedades =
        this.props.objectId + "/" + this.props.opcionId + "/" + cabecera;
      cabecera = cabecera;
    }

    //SOLO por tema registraciones
    if (
      this.props.diaSeleccionado != undefined &&
      this.props.idRegistrante != undefined
    ) {
      propiedades = propiedades + "/" +  this.props.diaSeleccionado;
      propiedades = propiedades + "/" + this.props.idRegistrante[0];
    }


    fetch(
      store.getState().serverUrl + this.props.urls.modelo + propiedades,
      requestOptions,
    )
      .then((response) => {
        if (response.status == 401) {
          sessionStorage.clear();
          window.location.reload(false);
        }else{
          return response.json();
        }
      })
      .then((result) => {
        const content = XLSX.read(result.contenido);

        XLSX.writeFile(content, result.nombreArchivo);

        this.setState({
          modeloLoading: false,
          modeloData: result,
        });
        this.handleModalModelo();
      })
      .catch((error) => {
        this.setState({
          modeloData: error,
          modeloLoading: false,
        });
        this.handleModalModelo();
        console.log("error", error);
      });
  }

  handleOpcionesClose() {
    this.setState((prevState) => ({
      descargarOpciones: !prevState.descargarOpciones,
    }));
  }

  handleDescargar() {

    if (this.props.acciones[4].formatos === undefined) {
      this.fetchDescargar();
    } else {
      this.handleOpcionesClose();
    }
  }

  fetchDescargar(formato) {
    
    this.setState((prevState) => ({
      descargarOpciones: !prevState.descargarOpciones,
      descargarLoading: true,
    }));

    var atributos;
    var atributoFinal = null;

    for (let i = 0; i < this.props.atributos.length; i++) {
      if (this.props.atributos[i].clave === "PRINCIPAL") {
        atributoFinal = this.props.atributos[i].id;
      }
    }

    if (this.state.selectedRows !== null) {
      atributos = Object.entries(this.state.selectedRows).map((item) => {
        return {
          id: atributoFinal,
          valor: item[1][this.props.objectSelectedId],
        };
      });
    } else {
      atributos = [];
    }

    var myHeaders = new Headers();
    myHeaders.append(
      "Authorization",
      "Bearer " + sessionStorage.getItem("token"),
    );

    myHeaders.append("Content-Type", "application/json");

    var accionDescargar = this.props.acciones.find(
      (item) => item.descripcion === "Descargar",
    ).id;

    var objeto = parseInt(this.props.objectId);
    var opcion = parseInt(this.props.opcion);
    var accion = parseInt(accionDescargar);
    var cabecera = parseInt(this.props.idCabecera);

    var raw = JSON.stringify({
      datos: {
        objeto: objeto,
        opcion: opcion,
        accion: accion,
        formato: formato,
        atributos: atributos,
        cabecera: cabecera,
        relacionados: this.props.relacionados,
        /* selectedRows: this.state.selectedRows, */
      },
    });

    var requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: raw,
      redirect: "follow",
    };

    
    fetch(
      store.getState().serverUrl + this.props.urls.descargar,
      requestOptions,
    )
      .then((response) => {
        if (response.status == 401) {
          sessionStorage.clear();
          window.location.reload(false);
        }else{
          return response.json();
        }
      })
      .then((result) => {
        var contenido = result.contenido;
        var nombreArchivo = result.nombreArchivo;
        this.downloadFile(contenido, formato, nombreArchivo);

        this.setState({
          descargarLoading: false,
          descargarData: result,
        });
        this.handleModalDescargar();
      })
      .catch((error) => {
        this.setState({
          descargarLoading: false,
          descargarData: error,
        });
        this.handleModalDescargar();
        console.log("error", error);
      });
  }

  handleVisualizar() {
    this.setState({
      visualizarLoading: true,
    });

    var atributos;

    if (this.state.selectedRows !== null) {
      atributos = Object.entries(this.state.selectedRows).map((item) => {
        var atributos = this.props.atributos;
        let atributoUnico;

        for (let i = 0; i < atributos.length; i++) {
          const atributo = atributos[i];
          if (atributo.clave === "PRINCIPAL") {
            atributoUnico = atributo.id;
          }
        }
        return {
          id: atributoUnico,
          valor: item[1][this.props.objectSelectedId],
        };
      });
    } else {
      atributos = [];
    }

    var accionVisualizar = this.props.acciones.find(
      (item) => item.descripcion === "Visualizar",
    ).id;

    var objeto = parseInt(this.props.objectId);
    var opcion = parseInt(this.props.opcion);
    var accion = parseInt(accionVisualizar);
    var cabecera = parseInt(this.props.idCabecera);

    var raw = JSON.stringify({
      datos: {
        objeto: objeto,
        opcion: opcion,
        accion: accion,
        formato: "PDF",
        atributos: atributos,
        relacionados: this.props.relacionados,
        cabecera: cabecera,
      },
    });

    var myHeaders = new Headers();

    myHeaders.append(
      "Authorization",
      "Bearer " + sessionStorage.getItem("token"),
    );
    myHeaders.append("Content-Type", "application/json");

    var requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: raw,
      redirect: "follow",
    };

    fetch(
      store.getState().serverUrl + this.props.urls.visualizar,
      requestOptions,
    )
      .then((response) => {
        if (response.status == 401) {
          sessionStorage.clear();
          window.location.reload(false);
        }else{
          return response.json();
        }
      })
      .then((result) => {
        var contenido = result.contenido;
        var nombreArchivo = result.nombreArchivo;
        this.downloadFile(contenido, "pdf", nombreArchivo);

        this.setState({
          visualizarLoading: false,
          visualizarData: result,
        });
        this.handleModalVisualizar();
      })
      .catch((error) => {
        console.log("error", error);
        this.setState({
          visualizarLoading: false,
          visualizarData: error,
        });
        this.handleModalVisualizar();
      });
  }

  downloadFile(file, extention, filename) {
    const linkSource = `data:application/${extention};base64,${file}`;
    const downloadLink = document.createElement("a");
    const fileName = filename;

    downloadLink.href = linkSource;
    downloadLink.download = fileName;
    downloadLink.click();
  }

  modal() {
    if (this.state.modal === true) {
      return (
        <div className="custom-modal">
          <div className="custom-modal-content">
            <div className="custom-modal-header">
              <div className="row d-flex justify-content-between align-items-center">
                <div>
                  <h6> Eliminar </h6>
                </div>
                <div>
                  <button className="btn btn-close" onClick={this.modalOpen}>
                    <i className="fi fi-rs-cross" />
                  </button>
                </div>
              </div>
            </div>
            <div className="right-body bg-white text-center mt-5">
              ¿Estás seguro que deseas eliminarlo?
            </div>
            {this.buttonsModal()}
          </div>
        </div>
      );
    }
  }

  modalError() {
    if (this.state.modalError === true) {
      return (
        <div className="custom-modal">
          <div className="custom-modal-content">
            <div className="custom-modal-header">
              <div className="row d-flex justify-content-between align-items-center">
                <div>
                  <h6> Ocurrió un error </h6>
                </div>
                <div>
                  <button
                    className="btn btn-close"
                    onClick={this.OpenmodalError}
                  >
                    <i className="fi fi-rs-cross" />
                  </button>
                </div>
              </div>
            </div>
            <div className="right-body bg-white text-center mt-5">
              {this.state.mensajesError.Mensajes.map((item) => {
                return (
                  <div
                    className="alert alert-danger text-left mt-4"
                    role="alert"
                    key={item.secuencia}
                  >
                    <h6 className="alert-heading">
                      <i className="fi fi-rs-triangle-warning mr-2"></i>{" "}
                      {item.tipo}
                    </h6>
                    <p>{item.mensaje}</p>
                    <hr />
                    <p className="mb-0">Secuencia: {item.secuencia}</p>
                  </div>
                );
              })}
            </div>
          </div>
        </div>
      );
    }
  }

  modalOpen() {
    this.setState((prevState) => ({
      modal: !prevState.modal,
    }));
  }

  buttonsModal() {
    if (this.state.buttonDisable === false) {
      return (
        <div className="right-footer d-flex justify-content-center align-items-stretch">
          <button className="btn btn-secondary mr-2" onClick={this.modalOpen}>
            Cancelar
          </button>
          <button className="btn btn-primary" onClick={this.handleEliminar}>
            Aceptar
          </button>
        </div>
      );
    } else {
      return (
        <div className="right-footer d-flex justify-content-center align-items-stretch">
          <button
            disabled={this.state.buttonDisable}
            className="btn btn-primary mr-2"
            onClick={() => this.modalBlanqueoOpen()}
          >
            <span
              className="spinner-grow spinner-grow-sm"
              role="status"
              aria-hidden="true"
            />
            <span className="sr-only">Loading...</span>
            <span
              className="spinner-grow spinner-grow-sm"
              role="status"
              aria-hidden="true"
            />
            <span className="sr-only">Loading...</span>
            <span
              className="spinner-grow spinner-grow-sm"
              role="status"
              aria-hidden="true"
            />
            <span className="sr-only">Loading...</span>
            Eliminando...
          </button>
        </div>
      );
    }
  }

  buttonsModalBlanqueo() {
    if (this.state.buttonDisableBlanqueo === false) {
      return (
        <div className="right-footer d-flex justify-content-center align-items-stretch">
          <button
            className="btn btn-secondary mr-2"
            onClick={this.modalBlanqueoOpen}
          >
            Cancelar
          </button>
          <button
            className="btn btn-primary"
            onClick={this.handleBlanquearContra}
          >
            Aceptar
          </button>
        </div>
      );
    } else {
      return (
        <div className="right-footer d-flex justify-content-center align-items-stretch">
          <button
            disabled={this.state.buttonDisableBlanqueo}
            className="btn btn-primary mr-2"
          >
            <span
              className="spinner-grow spinner-grow-sm"
              role="status"
              aria-hidden="true"
            />
            <span className="sr-only">Loading...</span>
            <span
              className="spinner-grow spinner-grow-sm"
              role="status"
              aria-hidden="true"
            />
            <span className="sr-only">Loading...</span>
            <span
              className="spinner-grow spinner-grow-sm"
              role="status"
              aria-hidden="true"
            />
            <span className="sr-only">Loading...</span>
            Enviando correo...
          </button>
        </div>
      );
    }
  }

  elementoAgregar() {
    const action = this.props.acciones.find(
      (action) => action.nombre === "ACCIONES.AGREGAR",
    );
    if (action) {
      return (
        <Fragment>
          {this.props.showButtonAdd ? (
            <button
              className="btn btn-primary"
              onClick={() => this.overlayShowHide()}
            >
              <span>{action.descripcion}</span>
            </button>
          ):(
            <Loader/>
          )}

          {this.props.menu === "seguridad" ? (
            <OverlayMenu
              menu={this.props.menu}
              objectId={this.props.objectId}
              opcionId={this.props.opcionId}
              atributos={this.props.atributos}
              sections={this.props.sectores}
              overlayShowHide={this.overlayShowHide}
              overlayShow={this.state.overlayShow}
              urls={this.props.urls}
            />
          ) : (
            <RightOverlay
              atributos={this.props.atributos}
              dia={this.props.dia}
              data={[]}
              errorMessages={this.state.errorMessagesAgregar}
              handleState={(state) =>
                this.setState({ stateAgregarModal: state })
              }
              handleSubmit={(atributos) => this.handleAgregar(atributos)}
              overlayShowHide={() => this.overlayShowHide()}
              overlayShow={this.state.overlayShow}
              state={this.state.stateAgregarModal}
              onChangeSeleccion={(e) => this.hangleOnChangeSeleccion(e)}
              objeto={this.props.objectId}
              opcion={this.props.opcion}
              size={this.props.size}
              edicion={this.props.edicion}
              editando={false}
            />
          )}
        </Fragment>
      );
    }
  }

  elementoEditar() {
    const action = this.props.acciones.find(
      (action) => action.nombre === "ACCIONES.MODIFICAR",
    );
    if (action) {
      if (this.state.selectedRows && this.state.selectedRows.length === 1) {
        return (
          <Fragment>
            <div className="">
              <Tooltip
                content="Editar seleccionado"
                placement="bottom"
                className="global-link-1 text-success btn-acciones"
                onClick={() => this.overlayShowHideEdit()}
              >
                <i className="fi fi-rs-pen-clip" />
              </Tooltip>
            </div>
            {this.props.menu === "seguridad" ? (
              <OverlayMenu
                menu={this.props.menu}
                objectId={this.props.objectId}
                opcionId={this.props.opcionId}
                user={this.state.selectedRows[0]}
                atributos={this.props.atributos}
                sections={this.props.sectores}
                overlayShowHide={this.overlayShowHideEdit}
                overlayShow={this.state.overlayShowEdit}
                urls={this.props.urls}
              />
            ) : (
              <RightOverlay
                atributos={this.props.atributos}
                data={this.state.selectedRows[0]}
                errorMessages={this.state.errorMessagesEditar}
                handleState={(state) =>
                  this.setState({ stateEditarModal: state })
                }
                handleSubmit={(atributos) => this.handleEditar(atributos)}
                overlayShowHide={() => this.overlayShowHideEdit()}
                overlayShow={this.state.overlayShowEdit}
                state={this.state.stateEditarModal}
                objeto={this.props.objectId}
                opcion={this.props.opcion}
                editar={true}
                size={this.props.size}
                edicion={this.props.edicion}
                editando={true}
              />
            )}
          </Fragment>
        );
      } else {
        return (
          <Fragment>
            <div className="">
              <Tooltip
                content="Editar seleccionado"
                placement="bottom"
                className="global-link-1 disabled btn-acciones"
              >
                <i className="fi fi-rs-pen-clip" />
              </Tooltip>
            </div>
          </Fragment>
        );
      }
    }
  }

  elementoEliminar() {
    const action = this.props.acciones.find(
      (action) => action.nombre === "ACCIONES.ELIMINAR",
    );
    if (action) {
      if (this.state.selectedRows && this.state.selectedRows.length > 0) {
        return (
          <Fragment>
            <div className="">
              <Tooltip
                content="Eliminar seleccionados"
                placement="bottom"
                className="global-link-1 text-danger btn-acciones"
                onClick={() => this.modalOpen(this.state.selectedRows)}
              >
                <i className="fi fi-rs-trash" />
              </Tooltip>
            </div>
          </Fragment>
        );
      } else {
        return (
          <Fragment>
            <div className="">
              <Tooltip
                content="Eliminar seleccionados"
                placement="bottom"
                className="global-link-1 disabled btn-acciones"
              >
                <i className="fi fi-rs-trash" />
              </Tooltip>
            </div>
          </Fragment>
        );
      }
    }
  }

  elementoDescargar() {
    var action = this.props.acciones.find(
      (action) => action.nombre === "ACCIONES.DESCARGAR",
    );
    if (action) {
      return (
        <Fragment>
          <div className="">
            {this.state.descargarLoading === false ? (
              <Tooltip
                placement="bottom"
                className="global-link-1 btn-acciones"
                content={action.observaciones}
                onClick={() => this.handleDescargar()}
              >
                <i className="fi fi-rs-download" />
              </Tooltip>
            ) : (
              <Loader></Loader>
            )}
            {this.state.descargarOpciones ? (
              <div className="custom-modal">
                <div className="custom-modal-content">
                  <div className="custom-modal-header">
                    <div className="row d-flex justify-content-between align-items-center">
                      <div>
                        <h6> Seleccione opción </h6>
                      </div>
                      <div>
                        <button
                          className="btn btn-close"
                          onClick={() => this.handleOpcionesClose()}
                        >
                          <i className="fi fi-rs-cross" />
                        </button>
                      </div>
                    </div>
                  </div>
                  <div className="right-body bg-white text-center d-flex align-items-center justify-content-center">
                    <div className="d-flex justify-content-between align-items-center contenedor-formatos">
                      {action.formatos.map((item) => {
                        return (
                          <button
                            className="button-formatos"
                            onClick={() => this.fetchDescargar(item.extension)}
                            key={item.id}
                          >
                            {item.extension === "PDF" ? (
                              <img src={PDF} alt="PDF" />
                            ) : (
                              false
                            )}
                            {item.extension === "XLSX" ? (
                              <img src={XLSXi} alt="XLSX" />
                            ) : (
                              false
                            )}
                            {item.extension === "XML" ? (
                              <img src={XML} alt="XML" />
                            ) : (
                              false
                            )}
                            {item.extension === "JSON" ? (
                              <img src={JSONI} alt="JSON" />
                            ) : (
                              false
                            )}
                            {item.extension === "TXT" ? (
                              <img src={TXT} alt="TXT" />
                            ) : (
                              false
                            )}
                            {item.nombre}
                          </button>
                        );
                      })}
                    </div>
                  </div>
                </div>
              </div>
            ) : (
              <div></div>
            )}
          </div>
          <AlertModal
            handleShowHide={() => this.handleModalDescargar}
            title="Descarga"
            show={this.state.modalDescargarShow}
            data={this.state.descargarData}
          ></AlertModal>
        </Fragment>
      );
    }
  }

  elementoModelo() {
    const action = this.props.acciones.find(
      (action) => action.nombre === "ACCIONES.MODELO",
    );
    if (action) {
      return (
        <Fragment key={action.nombre}>
          <div className="">
            {this.state.modeloLoading === false ? (
              <Tooltip
                placement="bottom"
                className="global-link-1 btn-acciones"
                content={action.observaciones}
                onClick={() => this.handleModelo()}
              >
                <i className="fi fi-rs-file-download" />
              </Tooltip>
            ) : (
              <Loader></Loader>
            )}
          </div>
          <AlertModal
            handleShowHide={() => this.handleModalModelo}
            title="Descarga de modelo"
            show={this.state.modalModeloShow}
            data={this.state.modeloData}
          ></AlertModal>
        </Fragment>
      );
    }
  }

  elementoImportar() {
    const action = this.props.acciones.find(
      (action) => action.nombre === "ACCIONES.IMPORTAR",
    );
    if (action) {
      return (
        <Fragment>
          <div className="">
            {this.state.importarLoading === false ? (
              <Tooltip
                placement="bottom"
                className="global-link-1 btn-acciones"
                content={action.observaciones}
              >
                <label htmlFor="file" style={{ margin: "0px" }}>
                  <input
                    type="file"
                    accept=".xlsx"
                    id="file"
                    style={{ display: "none" }}
                    onChange={(e) => this.handleImportar(e.target.files[0])}
                  />
                  <i className="fi fi-rs-file-upload" />
                </label>
              </Tooltip>
            ) : (
              <Loader></Loader>
            )}
            <AlertModal
              handleShowHide={() => this.handleModalImportar}
              title="Importación de datos"
              show={this.state.modalImportarShow}
              data={this.state.importarData}
            ></AlertModal>
          </div>
        </Fragment>
      );
    }
  }

  elementoVisualizar() {
    const action = this.props.acciones.find(
      (action) => action.nombre === "ACCIONES.VISUALIZAR",
    );
    if (action) {
      return (
        <Fragment>
          <div className="">
            {this.state.visualizarLoading === false ? (
              <Tooltip
                placement="bottom"
                className="global-link-1 btn-acciones"
                content={action.observaciones}
                onClick={() => this.handleVisualizar()}
              >
                <i className="fi fi-rs-file-pdf" />
              </Tooltip>
            ) : (
              <Loader></Loader>
            )}
          </div>
          <AlertModal
            handleShowHide={() => this.handleModalVisualizar}
            title="Descarga de modelo"
            show={this.state.modalVisualizarShow}
            data={this.state.visualizarData}
          ></AlertModal>
        </Fragment>
      );
    }
  }

  modalBlanqueo() {
    if (this.state.modalBlanqueo) {
      return (
        <div className="custom-modal">
          <div className="custom-modal-content">
            <div className="custom-modal-header">
              <div className="row d-flex justify-content-between align-items-center">
                <div>
                  <h6> Blanqueo de contraseña </h6>
                </div>
                <div>
                  <button
                    className="btn btn-close"
                    onClick={this.modalBlanqueoOpen}
                  >
                    <i className="fi fi-rs-cross" />
                  </button>
                </div>
              </div>
            </div>
            <div className="right-body bg-white text-center">
              ¿Estás seguro que deseas blanquear la contraseña?
              <br></br>Se enviará un correo electrónico al usuario con las
              instrucciones
            </div>
            {this.buttonsModalBlanqueo()}
          </div>
        </div>
      );
    } else {
      return false;
    }
  }

  modalBlanqueoOpen() {
    this.setState((prevState) => ({
      modalBlanqueo: !prevState.modalBlanqueo,
    }));
  }

  elementoBlanquear() {
    const action = this.props.acciones.find(
      (action) => action.nombre === "SEG_USUARIOS.BLANQUEAR",
    );
    if (action) {
      if (this.state.selectedRows && this.state.selectedRows.length === 1) {
        return (
          <Fragment>
            <div className="">
              <Tooltip
                content="Balqueo de contraseña"
                placement="bottom"
                className="global-link-1 ml-2 mr-2"
                onClick={() => this.modalBlanqueoOpen()}
              >
                <i className="fi fi-rs-unlock"></i>
              </Tooltip>
            </div>
          </Fragment>
        );
      } else {
        return (
          <Fragment>
            <Tooltip
              content="Balqueo de contraseña"
              placement="bottom"
              className="global-link-1 disabled ml-2 mr-2"
            >
              <i className="fi fi-rs-unlock"></i>
            </Tooltip>
          </Fragment>
        );
      }
    }
  }

  elementoFiltroAvanzado() {
    if (
      this.props.visualizacion &&
      this.props.visualizacion.avanzados === "1"
    ) {
      return (
        <button
          onClick={this.handleDropDownColumnas}
          className="d-flex align-items-center btn global-link-1 dropdown-block"
        >
          Búsqueda Avanzada
          <i className="fas fa-chevron-down ml-1" />
        </button>
      );
    }
  }

  columnasFiltradoAvanzado() {
    if (
      this.props.visualizacion &&
      this.props.visualizacion.avanzados === "1"
    ) {
      return (
        <div id="dropdownColumnasContent" className="dropdown-block-content">
          <h6 className="global-title-2 mb-3">Búsqueda avanzada</h6>
          <div className="filters">
            {this.props.visualizacion.columnas.map((item) => {
              return (
                <div className="filter" key={item.id}>
                  {item.filtros.map((subitem) => {
                    return (
                      <Input
                        alineacion={subitem.alineacion}
                        ancho={subitem.ancho}
                        atr_nombre={subitem.atr_nombre}
                        atr_valor={subitem.atr_valor}
                        atributo={subitem.id}
                        decimales={subitem.decimales}
                        descripcion={subitem.descripcion}
                        editable={subitem.editable}
                        estilo={subitem.estilo}
                        id={subitem.id}
                        key={subitem.id}
                        longitud={subitem.longitud}
                        mascara={subitem.mascara}
                        maximo={subitem.maximo}
                        minimo={subitem.minimo}
                        nombre={subitem.nombre}
                        obligatorio={subitem.obligatorio}
                        observaciones={subitem.observaciones}
                        onChange={this.props.handleChangeFiltroAvanzado}
                        onChangeMultiple={this.props.onChangeMultiple}
                        orden={subitem.orden}
                        origen={subitem.origen}
                        tipo={subitem.tipo}
                        tipologia={subitem.tipologia}
                        visible={subitem.visible}
                      />
                    );
                  })}
                </div>
              );
            })}
          </div>
          <div className="flex-xl-row flex-lg-row flex-md-column flex-sm-column flex-column justify-content-end mt-4">
            <input
              onClick={this.handleDropDownColumnas}
              className="btn btn-secondary mr-2 mb-2"
              type="button"
              value="Cancelar"
            />
            <input
              onClick={this.handleFiltrosAvanzados}
              className="btn btn-primary mb-2"
              type="button"
              value="Aplicar"
            />
          </div>
        </div>
      );
    }
  }

  sectores() {
    return (
      <ul className="nav">
        {this.props.showSectores && this.props.sectores
          ? this.props.sectores.map((item) => {
              return item.disabled ? (
                <li
                  className="nav-link"
                  style={{ color: "grey" }}
                  key={item.id}
                >
                  {item.descripcion}
                </li>
              ) : (
                <li className="nav-item" key={item.id}>
                  <Link
                    className="nav-link"
                    activeClass="active"
                    containerId="sectores"
                    to={"sector" + item.id}
                    spy={true}
                    smooth={true}
                    offset={0}
                    duration={1000}
                  >
                    {item.descripcion}
                  </Link>
                </li>
              );
            })
          : null}
      </ul>
    );
  }

  render() {
    return (
      <div className="filtros bg-white sticky-topNuevo shadow-sm">
        <div className="container">
          <div className="row">
            <div className="col-12 d-flex justify-content-between align-items-center flex-wrap">
              <div className="d-flex justify-content-start align-items-center flex-xl-row flex-lg-row flex-md-row flex-sm-row flex-column container-filter-search">
                {this.props.search}
                {this.elementoFiltroAvanzado()}
              </div>
              <div className="d-flex justify-content-between align-items-center buttons-filtros flex-wrap">
                {this.props.acciones ? (
                  <>
                    {this.elementoBlanquear()}
                    {this.elementoVisualizar()}
                    {this.elementoImportar()}
                    {this.elementoModelo()}
                    {this.elementoDescargar()}
                    {this.elementoEditar()}
                    {this.elementoEliminar()}
                    {this.elementoAgregar()}
                  </>
                ) : null}
                {this.sectores()}
              </div>
            </div>
            {this.columnasFiltradoAvanzado()}
            {this.modal()}
            {this.modalBlanqueo()}
            {this.modalError()}
          </div>
        </div>
      </div>
    );
  }
}
export default Filtros;
