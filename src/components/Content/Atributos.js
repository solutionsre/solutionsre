import React, { Component, Fragment } from "react";
import $ from "jquery";
import Tooltip from "react-simple-tooltip";
import { connect } from "react-redux";
import { scroller } from "react-scroll";
// Componentes Globales
import Loader from "../GlobalComponents/Loader";
import Input from "../GlobalComponents/Input";
import store from "../../redux/store";
import URLS from "../../urls";
import { obtenerInformacion, cabeceras } from "../../actions/globalActions";
// Wijmo
import * as wjcOlap from "wijmo/wijmo.olap";
import { CollectionView } from "wijmo/wijmo";
// Imagenes
import PDF from "../../assets/images/PDF.svg";
import XLSXi from "../../assets/images/XLSX.svg";
import XML from "../../assets/images/XML.svg";
import TXT from "../../assets/images/TXT.svg";
import JSONI from "../../assets/images/JSON.svg";

// Css
import "./Content.css";


class Atributos extends Component {
  constructor(props) {
    super(props);
    this.state = {
      atributos: [],
      errorsData: [],
      errorMessages: [],
      toggle: true,
      descargarOpciones: false,
      descargarLoading: false,
      showModal: false,
      state: 0,
      agrupados: "",
    };
    this.setFileValor = this.setFileValor.bind(this);
    this.renderStates = this.renderStates.bind(this);
    this.renderErrorModal = this.renderErrorModal.bind(this);
  }

  componentDidMount() {

    store.dispatch({
      type: "SET_DATA",
      payload: [],
    });
    store.dispatch({
      type: "SET_CABECERA_GRILLA",
      payload: null,
    });
    store.dispatch({
      type: "SET_OBJETO_REPORTES",
      payload: this.props.acciones[0].refrescos[0].objeto,
    });
    store.dispatch({
      type: "SET_ITEM_SELECCIONADO_INTERFACES",
      payload: [],
    });
    store.dispatch({
      type: "SET_ITEM_SELECCIONADO_PROCESOS",
      payload: [],
    });
    store.dispatch({
      type: "SET_MISA",
      payload: false,
    });
  }

  handleToogleFiltros() {
    const toggle = this.state.toggle;
    $("#filterWrapper").toggle(!toggle);
    $("#filter-toggle").find("i").toggleClass("fi-rs-arrow-small-down fi-rs-arrow-small-up");
    this.setState({
      toggle: !toggle,
    });
  }

  async handleOnChangeFiltros(e) {
    
    if (this.props.tipologia === "INTERFACES") {
      this.props.handleOnChangeFiltros(e);
    } else if (this.props.tipologia === "PROCESOS") {
      this.props.handleOnChangeFiltros(e);
    } else {
      if (e.target) {
        
        // Detectamos el check para agregar
        if(e.target.type === "checkbox" && e.target.checked){
            var { value, name } = e.target;
            const prevAtris = this.state.atributos;
            let aux = []
            let valor = value
                        
            if(prevAtris[name]){
              aux = prevAtris[name].split(",")
              aux.push(valor)
            } else {
              aux.push(valor)
            }

            
            const prevState = this.state;
            this.setState({
              atributos: {
                ...prevState.atributos,
                [name]: aux.toString(),
              },
            });
            return false
          // Eliminamos del Array               
          } else if(e.target.type === "checkbox" && !e.target.checked) {
            var { value, name } = e.target;
            const prevAtris = this.state.atributos;
            
            let aux = []
            let aux2 = []
            let valor = value

            if(prevAtris[name]){
              aux = prevAtris[name].split(",")
              aux2 = aux.filter((item) => item !== value)
            } 


            const prevState = this.state;
            this.setState({
              atributos: {
                ...prevState.atributos,
                [name]: aux2.toString(),
              },
            });
            return false

          } else {

            var { value, name } = e.target;

          }


          
          


      } else if (e.inputType === "text") {
        var value = e.selectedValue;
        var name = e._tbx.name;
      } else if (e.inputType === "tel") {
        var value = e._tbx.value;
        var name = e._tbx.name;
      } else if (e.inputType === "file") {
        var file = e._tbx.files[0];

        const fileToBase64 = (file) =>
          new Promise((resolve, reject) => {
            const reader = new FileReader();
            reader.readAsDataURL(file);

            reader.onload = () =>
              resolve(reader.result.replace(/^data:.+;base64,/, ""));

            reader.onerror = (error) => reject(error);
          });

        var value = await fileToBase64(file);
        var name = e._tbx.name;
        this.setState({ fileValor: e });
      }

      
      const prevState = this.state;
      this.setState({
        atributos: {
          ...prevState.atributos,
          [name]: value,
        },
      });
      

    }
  }

  validacion() {
    var atributos = this.props.atributos;

    var validacion = true;
    const errorsData = [];

    if (this.state.atributos) {
      atributos.map((art) => {
        if (this.state.atributos[art.id]) {
          if (art.tipologia === "TEXTO") {
            if (art.tipo === "CARACTER" && art.estilo === "MAYUSCULAS") {
              var input = $("label[id = label" + art.id + "]");
              input.removeClass("is-invalid");
              const newState = this.state.atributos;

              newState[art.id] = newState[art.id].toUpperCase();
              this.setState({
                atributos: newState,
              });
            }
            if (art.tipo === "NUMERICO") {
              var input = $("label[id = label" + art.id + "]");
              input.removeClass("is-invalid");
              if (
                (art.maximo &&
                  parseFloat(this.state.atributos[art.id]) >
                    parseFloat(art.maximo)) ||
                (art.minimo &&
                  parseFloat(this.state.atributos[art.id]) <
                    parseFloat(art.minimo))
              ) {
                const newState = this.state.atributos;
                newState[art.id] = "";
                validacion = false;
                errorsData.push(
                  "El numero ingresado en " +
                    art.descripcion +
                    " supera los limites",
                );
              }
            }
          } else if (art.tipologia === "ADJUNTO") {
            if (this.state.fileValor.adjunto === "") {
              var input = $("label[id = label" + art.id + "]");
              input.addClass("is-invalid");
              validacion = false;
            }
          } else if (art.tipologia === "FECHA") {
            var input = $("label[id = label" + art.id + "]");
            input.removeClass("is-invalid");
          }
        } else {
          if (art.obligatorio === "S") {
            var input = $("label[id = label" + art.id + "]");
            input.addClass("is-invalid");
            validacion = false;
            errorsData.push("El campo " + art.descripcion + " es obligatorio");
          }
        }

        return validacion;
      });
    } else {
      validacion = false;
    }

    this.setState({
      errorsData: errorsData,
    });

    return validacion;
  }

  alertaError(mensaje) {
    if (this.props.tipologia === "INTERFACES") {
      var errores = this.props.filtersErrors;
    } else if (this.props.tipologia === "PROCESOS") {
      var errores = this.props.filtersErrors;
    } else {
      var errores = this.state.errorsData;
    }
    return (
      <Fragment>
        {errores.map((error) => {
          return (
            <div className="alert alert-danger" role="alert">
              {error}
            </div>
          );
        })}
      </Fragment>
    );
  }

  filtrarData(action, rawData) {
    if (this.validacion()) {
      store.dispatch({
        type: "SET_LOADING_DATA",
        payload: true,
      });
      var actionUrl;
      if (this.props.tipologia === "INTERFACES") {
        actionUrl = store.getState().serverUrl + URLS.INTERFACES_APLICAR_FILTROS;
      } else if (this.props.tipologia === "PROCESOS") {
        actionUrl = store.getState().serverUrl + URLS.PROCESOS_APLICAR_FILTROS;
      }
      if (actionUrl) {
        var myHeaders;
        var raw;
        var requestOptions;

        myHeaders = new Headers();
        myHeaders.append("Authorization", "Bearer " + sessionStorage.getItem("token"));
        myHeaders.append("Content-Type", "application/json");

        var arrayTemp = [];

        const atributos = Object.entries(this.state.atributos).map((art) => {
          const id = art[0];
          var value = art[1];

          if (
            this.props.atributos.find((attr) => attr.id === art[0]) &&
            this.props.atributos.find((attr) => attr.id === art[0])
              .tipologia === "ADJUNTO"
          ) {
            if (this.state.fileValor) {
              value = {
                archivo: this.state.fileValor._tbx.files[0].name,
                adjunto: value,
              };
            }
          }

          if (
            this.props.atributos.find((attr) => attr.id === art[0]) !==
            undefined
          ) {
            if (value === null) {
              value = "";
            }

            arrayTemp.push({
              id: id,
              valor: value,
            });
          }
        });

        var arrayTemporal = [];

        for (let i = 0; i < this.props.atributos.length; i++) {
          if (
            (this.props.atributos[i].visible === "S" &&
              this.props.atributos[i].calculado === "") ||
            this.props.atributos[i].clave === "PRINCIPAL"
          ) {
            var encontrado = false;
            for (let z = 0; z < arrayTemp.length; z++) {
              if (arrayTemp[z].id === this.props.atributos[i].id) {
                arrayTemporal.push({
                  atributo: arrayTemp[z].id,
                  contenido: arrayTemp[z].valor,
                });
                encontrado = true;
              }
            }
            if (encontrado === false) {
              arrayTemporal.push({
                atributo: this.props.atributos[i].id,
                contenido: "",
              });
            }
          }
        }

        raw = {
          idObjeto: parseInt(this.props.componentId),
          idOpcion: parseInt(this.props.id),
          idAccion: parseInt(action.id),
          idFormato: "",
          filtros: {
            procesos: arrayTemporal,
          },
        };

        requestOptions = {
          method: "POST",
          headers: myHeaders,
          body: JSON.stringify(raw),
          redirect: "follow",
        };
        var respuestaOk;
        fetch(actionUrl, requestOptions)
            .then((response) => {
              if (response.status == 401) {
                sessionStorage.clear();
                window.location.reload(false);
              }else{
                return response.text();
              }
            })
            .then((result) => {
            obtenerInformacion(rawData);
          });
      }
    }
  }

  sinEvento(action) {
    if (this.validacion()) {
      store.dispatch({
        type: "SET_LOADING_DATA",
        payload: true,
      });
      var actionUrl;
      
      actionUrl = store.getState().serverUrl + URLS.OBTENER_INFORMACION;

      if (actionUrl) {
        var myHeaders;
        var raw;
        var requestOptions;
        var objeto;
        var atributos;

        action.refrescos.forEach((refresh) => {
          objeto = refresh.objeto;

          myHeaders = new Headers();
          myHeaders.append("Authorization", "Bearer " + sessionStorage.getItem("token"));
          myHeaders.append("Content-Type", "application/json");

          var arrayTemp = [];

          const atributos = Object.entries(this.state.atributos).map((art) => {
            const id = art[0];
            var value = art[1];

            if (
              this.props.atributos.find((attr) => attr.id === art[0]) &&
              this.props.atributos.find((attr) => attr.id === art[0])
                .tipologia === "ADJUNTO"
            ) {
              if (this.state.fileValor) {
                var adjunto;

                if (this.state.fileValor.adjunto === "") {
                  adjunto = this.state.fileValor.adjunto;
                } else {
                  adjunto = this.state.fileValor.adjunto;
                }
                value = {
                  interno: this.state.fileValor.interno,
                  archivo: this.state.fileValor.archivo,
                  adjunto: adjunto,
                };
              } else {
                value = {
                  interno: value,
                  archivo: "",
                  adjunto: "",
                };
              }
            }

            if (
              this.props.atributos.find((attr) => attr.id === art[0]) !==
              undefined
            ) {
              if (value === null) {
                value = "";
              }

              arrayTemp.push({
                id: id,
                valor: value,
              });
            }
          });

          var arrayTemporal = [];

          for (let i = 0; i < this.props.atributos.length; i++) {
            if (
              (this.props.atributos[i].visible === "S" &&
                this.props.atributos[i].calculado === "") ||
              this.props.atributos[i].clave === "PRINCIPAL"
            ) {
              var encontrado = false;
              for (let z = 0; z < arrayTemp.length; z++) {
                if (arrayTemp[z].id === this.props.atributos[i].id) {
                  arrayTemporal.push({
                    atributo: arrayTemp[z].id,
                    contenido: arrayTemp[z].valor,
                  });
                  encontrado = true;
                }
              }
              if (encontrado === false) {
                arrayTemporal.push({
                  atributo: this.props.atributos[i].id,
                  contenido: "",
                });
              }
            }
          }

          raw = JSON.stringify({
            idObjeto: parseInt(objeto),
            idOpcion: parseInt(this.props.id),
            idAccion: parseInt(action.id),
            idFormato: "",
            filtros: {
              procesos: arrayTemporal,
            },
          });

          requestOptions = {
            method: "POST",
            headers: myHeaders,
            body: raw,
            redirect: "follow",
          };
          var respuestaOk;
          fetch(actionUrl, requestOptions)
            .then((response) => {
              respuestaOk = response.ok;
              return response.json();
            })
            .then((result) => {
              if (respuestaOk) {
                let data = [];
                data = new CollectionView(result);
                data.currentItem = null;
                store.dispatch({
                  type: "SET_DATA_REPORTES",
                  payload: data,
                });
                store.dispatch({
                  type: "SET_LOADING_DATA",
                  payload: false,
                });
              } else {
                store.dispatch({
                  type: "SET_LOADING_DATA",
                  payload: false,
                });
                this.setState({
                  state: 400,
                  buttonDisable: false,
                  errorMessages: result.Mensajes,
                });
                this.renderStates();
              }
            });
        });
      }
    }
  }

  procesarData(action, rawData) {
   
    if (this.validacion()) {
      $("#filterWrapper").toggle(false);
      $("#filter-toggle").find("i").addClass("fi fi-rs-arrow-small-down");
      $("#filter-toggle").find("i").removeClass("fi fi-rs-arrow-small-up");
      store.dispatch({
        type: "SET_LOADING_DATA",
        payload: true,
      });

      var actionUrl;
      if (this.props.tipologia === "REPORTES") {
        actionUrl = store.getState().serverUrl + URLS.REPORTES_PROCESAR;
      } else if (this.props.tipologia === "INTERFACES") {
        actionUrl = store.getState().serverUrl + URLS.INTERFACES_PROCESAR;
      } else if (this.props.tipologia === "PROCESOS") {
        actionUrl = store.getState().serverUrl + URLS.PROCESOS_PROCESAR;
      }

      if (actionUrl) {
        var myHeaders;
        var raw;
        var requestOptions;
        var objeto;
        var cabecera;

        /* objeto = 4002001; */
        action.refrescos.forEach((refresh) => {
          objeto = refresh.objeto;
          cabecera = "N";
          myHeaders = new Headers();
          myHeaders.append("Authorization", "Bearer " + sessionStorage.getItem("token"));
          myHeaders.append("Content-Type", "application/json");

          var arrayTemp = [];

          const atributos = Object.entries(this.state.atributos).map((art) => {
            const id = art[0];
            var value = art[1];

            if (
              this.props.atributos.find((attr) => attr.id === art[0]) &&
              this.props.atributos.find((attr) => attr.id === art[0])
                .tipologia === "ADJUNTO"
            ) {
              if (this.state.fileValor) {
                var adjunto;

                if (this.state.fileValor.adjunto === "") {
                  adjunto = this.state.fileValor.adjunto;
                } else {
                  adjunto = this.state.fileValor.adjunto;
                }
                value = {
                  interno: this.state.fileValor.interno,
                  archivo: this.state.fileValor.archivo,
                  adjunto: adjunto,
                };
              } else {
                value = {
                  interno: value,
                  archivo: "",
                  adjunto: "",
                };
              }
            }

            if (
              this.props.atributos.find((attr) => attr.id === art[0]) !==
              undefined
            ) {
              if (value === null) {
                value = "";
              }

              arrayTemp.push({
                id: id,
                valor: value,
              });
            }
          });
          if (this.props.tipologia === "REPORTES") {
            var arrayTemporal = [];
            for (let j = 0; j < this.props.sectores.length; j++) {
              if (this.props.sectores[j].id === objeto) {
                cabecera = this.props.sectores[j].variable;
              }
            }

            for (let i = 0; i < this.props.atributos.length; i++) {
              if (
                (this.props.atributos[i].visible === "S" &&
                  this.props.atributos[i].calculado === "") ||
                this.props.atributos[i].clave === "PRINCIPAL"
              ) {
                var encontrado = false;
                for (let z = 0; z < arrayTemp.length; z++) {
                  if (arrayTemp[z].id === this.props.atributos[i].id) {
                    arrayTemporal.push({
                      atributo: arrayTemp[z].id,
                      contenido: arrayTemp[z].valor,
                    });
                    encontrado = true;
                  }
                }
                if (encontrado === false) {
                  arrayTemporal.push({
                    atributo: this.props.atributos[i].id,
                    contenido: "",
                  });
                }
              }
            }

            raw = JSON.stringify({
              idObjeto: parseInt(objeto),
              idOpcion: parseInt(this.props.id),
              idAccion: parseInt(action.id),
              idFormato: "GRILLA",
              filtros: {
                procesos: arrayTemporal,
              },
            });

            requestOptions = {
              method: "POST",
              headers: myHeaders,
              body: raw,
              redirect: "follow",
            };
            
          } 

          fetch(actionUrl, requestOptions)
            .then((response) => {
              if (response.status == 401) {
                sessionStorage.clear();
                window.location.reload(false);
              }else if (response.status === 201) {
                return response.text();
              } else {
                return response.json();
              }
            })
            .then((result) => {
              if (result.Mensajes) {
                this.setState({
                  loadingData: false,
                  state: 400,
                  buttonDisable: false,
                  errorMessages: result.Mensajes,
                });
                this.renderStates();
              } else {
                this.setState({
                  toggle: false,
                });
                this.props.tipologia === "INTERFACES" || this.props.tipologia === "PROCESOS"
                  ? this.obtenerResultadosFinal()
                  : cabeceras(rawData, cabecera);
              }
            })
            .catch((error) => console.log("error", error));
        });
      }
    }
  }

  renderStates() {
    switch (this.state.state) {
      case 400:
        this.setState({
          showModal: true,
        });
        return this.renderErrorModal();

      default:
    }
  }

  handleCloseModal() {
    this.setState((prevState) => ({
      showModal: !prevState.showModal,
    }));
  }

  accionesBotones(action) {
    
    store.dispatch({
      type: "SET_FILTROS_APLICADOS",
      payload: true,
    });
    
    store.dispatch({
      type: "MISA",
      payload: true,
    });
    
    switch (action.tipo) {
      case "IMPORTAR":
        this.setState({
          toggle: false,
        });

        this.props.fetchData(action);
        break;
      case "AGREGAR":
        this.props.handleAgregar(action);
        break;
      case "MODIFICAR":
        this.props.handleModificar(action);
        break;
      case "ELIMINAR":
        this.props.handleEliminar(action);
        break;
      case "ADJUNTO":
        this.setState({
          toggle: false,
        });
        this.props.handleDescargar(action);
        
        break;
      case "SELECCION":
        this.setState({
          toggle: false,
        });
        this.props.handleDescargar(action);
        
        break;
      case "FILTRAR":
        if (this.props.tipologia === "INTERFACES") {
          
          this.setState({
            toggle: false,
          });
          
          this.props.fetchData(action);
        } else if (this.props.tipologia === "PROCESOS") {
          
          this.setState({
            toggle: false,
          });
          
          this.props.fetchData(action);
        } else {
          
          var myHeaders;
          var rawData;
          var requestOptions;
          var objeto;
          var atributos;
          action.refrescos.forEach((refresh) => {
            objeto = refresh.objeto;

            myHeaders = new Headers();
            myHeaders.append("Authorization", "Bearer " + sessionStorage.getItem("token"));
            myHeaders.append("Content-Type", "application/json");

            atributos = Object.entries(this.state.atributos).map((item) => {
              return {
                atributo: item[0],
                contenido: item[1],
              };
            });
            
            rawData = JSON.stringify({
              idObjeto: parseInt(objeto),
              idOpcion: parseInt(this.props.id),
              idAccion: parseInt(action.id),
              idFormato: "",
              filtros: {
                procesos: "",
              },
            });
          });

          this.filtrarData(action, rawData);
          store.dispatch({
            type: "SET_FILTROS_APLICADOS",
            payload: true,
          });
        }
        break;
      case "SIN_EVENTO":
        
        if (this.validacion()) {
          
          $("#filterWrapper").toggle(false);
          $("#filter-toggle").find("i").addClass("fi fi-rs-arrow-small-down");
          $("#filter-toggle").find("i").removeClass("fi fi-rs-arrow-small-up");
          var myHeaders;
          var rawData;
          var requestOptions;
          var objeto;
          var atributos;
          var cabecera;

          cabecera = "N";

          action.refrescos.forEach((refresh) => {
            objeto = refresh.objeto;

            myHeaders = new Headers();
            myHeaders.append("Authorization", "Bearer " + sessionStorage.getItem("token"));
            myHeaders.append("Content-Type", "application/json");

            var arrayTemp = [];
            for (let j = 0; j < this.props.sectores.length; j++) {
              if (this.props.sectores[j].id === objeto) {
                cabecera = this.props.sectores[j].variable;
              }
            } 
            
            const atributos = Object.entries(this.state.atributos).map((art) => {
              const id = art[0];
              var value = art[1];

              if (
                this.props.atributos.find((attr) => attr.id === art[0]) &&
                this.props.atributos.find((attr) => attr.id === art[0])
                  .tipologia === "ADJUNTO"
              ) {
                if (this.state.fileValor) {
                  value = {
                    archivo: this.state.fileValor._tbx.files[0].name,
                    adjunto: value,
                  };
                }
              }

              if (
                this.props.atributos.find((attr) => attr.id === art[0]) !==
                undefined
              ) {
                if (value === null) {
                  value = "";
                }

                arrayTemp.push({
                  id: id,
                  valor: value,
                });
              }
            });

            var arrayTemporal = [];

            for (let i = 0; i < this.props.atributos.length; i++) {
              if (
                (this.props.atributos[i].visible === "S" &&
                  this.props.atributos[i].calculado === "") ||
                this.props.atributos[i].clave === "PRINCIPAL"
              ) {
                var encontrado = false;
                for (let z = 0; z < arrayTemp.length; z++) {
                  if (arrayTemp[z].id === this.props.atributos[i].id) {
                    arrayTemporal.push({
                      atributo: arrayTemp[z].id,
                      contenido: arrayTemp[z].valor,
                    });
                    encontrado = true;
                  }
                }
                if (encontrado === false) {
                  arrayTemporal.push({
                    atributo: this.props.atributos[i].id,
                    contenido: "",
                  });
                }
              }
            }
           
            rawData = JSON.stringify({
              idObjeto: parseInt(objeto),
              idOpcion: parseInt(this.props.id),
              idAccion: parseInt(action.id),
              idFormato: "",
              filtros: {
                procesos: arrayTemporal,
              },
            });
          });

          this.setState({
            toggle: false,
          });
          
          cabeceras(rawData, cabecera);
        }
        break;
      case "ACCION":
        if (this.props.tipologia === "INTERFACES") {
          this.props.fetchData(action);
        } else if (this.props.tipologia === "PROCESOS") {
          this.props.fetchData(action);
        } 
        break;
      case "PROCESO":
        
        if (this.props.tipologia === "INTERFACES") {
          this.props.fetchData(action);
        } else if (this.props.tipologia === "PROCESOS") {
          this.props.fetchData(action);
        } else {
          var myHeaders;
          var rawData;
          var requestOptions;
          var objeto;
          var atributos;
          action.refrescos.forEach((refresh) => {
            objeto = refresh.objeto;

            myHeaders = new Headers();
            myHeaders.append("Authorization", "Bearer " + sessionStorage.getItem("token"));
            myHeaders.append("Content-Type", "application/json");

            atributos = Object.entries(this.state.atributos).map((item) => {
              return {
                atributo: item[0],
                contenido: item[1],
              };
            });

            rawData = JSON.stringify({
              idObjeto: parseInt(objeto),
              idOpcion: parseInt(this.props.id),
              idAccion: parseInt(action.id),
              idFormato: "",
              filtros: {
                procesos: "",
              },
            });
          });
          this.procesarData(action, rawData);
        }
        break;
    }
  }

  setFileValor = (e) => {
    this.setState({
      fileValor: e,
    });
  };

  renderInputs() {
    const filtersSection = this.props.sectores.find(
      (s) => s.tipologia === "FILTROS",
    );
    return filtersSection ? (
      <div className="container-fluid p-4 shadow-sm" id="filterWrapper">
        <div className="row">
          <div className="col-12">
            <div className="filters">
              {filtersSection.columnas.map((item) => {
                return (
                  <div className="filter" key={item.id}>
                    {item.filtros.map((subitem) => {
                      return (
                        <Input
                          alineacion={subitem.alineacion}
                          ancho={subitem.ancho}
                          atr_nombre={subitem.atr_nombre}
                          atr_valor={subitem.atr_valor}
                          decimales={subitem.decimales}
                          descripcion={subitem.descripcion}
                          editable={subitem.editable}
                          estilo={subitem.estilo}
                          id={subitem.id}
                          key={subitem.id}
                          longitud={subitem.longitud}
                          mascara={subitem.mascara}
                          maximo={subitem.maximo}
                          minimo={subitem.minimo}
                          nombre={subitem.nombre}
                          obligatorio={subitem.obligatorio}
                          observaciones={subitem.observaciones}
                          orden={subitem.orden}
                          origen={subitem.origen}
                          prefijo={subitem.prefijo}
                          sufijo={subitem.sufijo}
                          tipo={subitem.tipo}
                          tipologia={subitem.tipologia}
                          visible={subitem.visible}
                          visualizacion={subitem.visualizacion}
                          atributos={subitem.visualizacion.atributos}
                          value={this.state.atributos[subitem.id]}
                          onChange={(e) => this.handleOnChangeFiltros(e)}
                          onChangeSeleccion={(e) => 
                            this.hangleOnChangeSeleccion(e)
                          }
                          onChangeMultiple={(e) =>
                            this.handleOnChangeFiltrosMultiple(e)
                          }
                          setFileValor={(e) => this.setFileValor(e)}
                        />
                      );
                    })}
                  </div>
                );
              })}
            </div>
            {this.alertaError()}
          </div>
        </div>
      </div>
    ) : null;
  }

  handleOpcionesClose() {
    this.setState((prevState) => ({
      descargarOpciones: !prevState.descargarOpciones,
    }));
  }

  handleDescargar(action) {
    
    if (this.validacion()) {
      var formato;
      if (action.formatos.length > 0){
        formato = action.formatos[0].nombre;
        this.handleOpcionesClose();
      }else{
        formato = "PDF";
        this.fetchDescargar(formato, action.tipo, action.atributo, action.id);
      }
    }
  }

  handleModalDescargar() {
    this.setState((prevState) => ({
      modalDescargarShow: !prevState.modalDescargarShow,
    }));
  }

  fetchDescargar(extension, tipo, atributoSeleccion, accionID) {
    
    this.setState((prevState) => ({
      descargarOpciones: !prevState.descargarOpciones,
      descargarLoading: true,
    }));
    var myHeaders = new Headers();
    myHeaders.append(
      "Authorization",
      "Bearer " + sessionStorage.getItem("token"),
    );
    myHeaders.append("Content-Type", "application/json");

    const atributos = Object.entries(this.state.atributos).map((item) => {
      return {
        atributo: item[0],
        contenido: item[1],
      };
    });

    var objeto = parseInt(this.props.componentId);
    var opcion = parseInt(this.props.id);
    var accion = parseInt(accionID);
    var urlDescarga;


    var seleccionado;
    if (tipo === "ADJUNTO") {
      if (this.flexGrid && this.flexGrid.selectedRows[0]){
        const atributos = Object.entries(this.flexGrid.selectedRows[0].dataItem).map((item) => {
        
          if (item[0] === atributoSeleccion) {
            seleccionado = item[1];
          }
        });
        if (seleccionado) {
          var raw = {
            datos: {
              objeto: objeto,
              opcion: opcion,
              atributo: null,
              interno: seleccionado,
            },
          };
          urlDescarga = URLS.OBTENER_ADJUNTOS;
        }else{
          urlDescarga = null;
          this.setState({
            descargarLoading: false,
            descargarData: "Debe seleccionar un elemento de la grilla.",
          });
          this.handleModalDescargar();
        }
      }else{
        urlDescarga = null;
        this.setState({
          descargarLoading: false,
          descargarData: "Debe seleccionar un elemento de la grilla.",
        });
        this.handleModalDescargar();
      }
    }else{
      if (tipo === "SELECCION") {
        if (this.flexGrid && this.flexGrid.selectedRows[0]) {
          const atributos = Object.entries(this.flexGrid.selectedRows[0].dataItem).map((item) => {
            return {
              atributo: item[0],
              contenido: item[1],
            };
          });
          var raw = {
            idObjeto: objeto,
            idOpcion: opcion,
            idAccion: accion,
            idFormato: extension,
            filtros: {
              procesos: atributos,
            },
          };
          urlDescarga = URLS.REPORTES_PROCESAR;
        }else{
          urlDescarga = null;
          this.setState({
            descargarLoading: false,
            descargarData: "Debe seleccionar un elemento de la grilla.",
          });
          this.handleModalDescargar();
        }
      }else{
        var raw = {
          idObjeto: objeto,
          idOpcion: opcion,
          idAccion: accion,
          idFormato: extension,
          filtros: {
            procesos: atributos,
          },
        };
        urlDescarga = URLS.REPORTES_PROCESAR;
      }
    }

    var requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: JSON.stringify(raw),
      redirect: "follow",
    };

    

    var status;
    if (urlDescarga != null){
      fetch(store.getState().serverUrl + urlDescarga, requestOptions)
      .then((response) => {
        if (response.status == 401) {
          sessionStorage.clear();
          window.location.reload(false);
        }        
        status = response.status;
        return response.json();
      })
      .then((result) => {
        if (status === 500) {
          this.setState({
            descargarLoading: false,
            descargarData: result,
          });
          this.handleModalDescargar();
        } else {
          var contenido;
          var nombreArchivo;
          if (tipo === "ADJUNTO"){
            contenido = result.adjunto;
            nombreArchivo = result.archivo;
          }else{
            contenido = result.contenido;
            nombreArchivo = result.nombreArchivo;
          }
          this.downloadFile(contenido, extension, nombreArchivo);
          this.setState({
            descargarLoading: false,
            descargarData: result,
          });
          this.handleModalDescargar();
        }
      })
      .catch((error) => {
        this.setState({
          descargarLoading: false,
          descargarData: error,
        });
        this.handleModalDescargar();
        console.log("error", error);
      });
    }
  }


  downloadFile(file, extention, filename) {
    const linkSource = `data:application/${extention};base64,${file}`;
    const downloadLink = document.createElement("a");
    const fileName = filename;

    downloadLink.href = linkSource;
    downloadLink.download = fileName;
    downloadLink.click();
  }

  elementoDescargar(acciones) {
    var action = acciones.find((action) => (action.tipo === "DESCARGA"));
    
    if (action) {
      return (
        <Fragment>
          <div className="ml-2">
            {this.state.descargarLoading === false ? (
              <Tooltip
                placement="bottom"
                className="global-link-1 btn-acciones"
                content={action.observaciones}
                onClick={() => this.handleDescargar(action)}
              >
                <i className="fi fi-rs-download" />
              </Tooltip>
            ) : (
              <Loader></Loader>
            )}
            {this.state.descargarOpciones ? (
              <div className="custom-modal">
                <div className="custom-modal-content">
                  <div className="custom-modal-header">
                    <div className="row d-flex justify-content-between align-items-center">
                      <div>
                        <h6> Seleccione opción </h6>
                      </div>
                      <div>
                        <button
                          className="btn btn-close"
                          onClick={() => this.handleOpcionesClose()}
                        >
                          <i className="fi fi-rs-cross" />
                        </button>
                      </div>
                    </div>
                  </div>
                  <div className="right-body bg-white text-center d-flex align-items-center justify-content-center">
                    <div className="contenedor-formatos">
                      {action.formatos.map((item) => {
                        return (
                          <button
                            className="button-formatos"
                            onClick={() => this.fetchDescargar(item.nombre, action.tipo, action.atributo, action.id)}
                            key={item.id}
                          >
                            {item.extension === "PDF" ? (
                              <img src={PDF} alt="PDF" />
                            ) : (
                              false
                            )}
                            {item.extension === "XLSX" ? (
                              <img src={XLSXi} alt="XLSX" />
                            ) : (
                              false
                            )}
                            {item.extension === "XML" ? (
                              <img src={XML} alt="XML" />
                            ) : (
                              false
                            )}
                            {item.extension === "JSON" ? (
                              <img src={JSONI} alt="JSON" />
                            ) : (
                              false
                            )}
                            {item.extension === "TXT" ? (
                              <img src={TXT} alt="TXT" />
                            ) : (
                              false
                            )}{" "}
                            {item.nombre}
                          </button>
                        );
                      })}
                    </div>
                  </div>
                </div>
              </div>
            ) : (
              <div></div>
            )}
          </div>
        </Fragment>
      );
    }
  }

  renderErrorModal() {
    return (
      <>
        <div className="custom-modal">
          <div className="custom-modal-content">
            <div className="custom-modal-header">
              <div className="row d-flex justify-content-between align-items-center">
                <div>
                  <h6> {this.props.tipologia} </h6>
                </div>
                <div>
                  <button
                    className="btn btn-close"
                    onClick={() => this.handleCloseModal()}
                  >
                    <i className="fi fi-rs-cross" />
                  </button>
                </div>
              </div>
            </div>
            <div className="mt-5">
              <div className="d-flex justify-content-center">
                <i className="right-icon-big-no fi fi-rs-cross" />
              </div>
              <div className="d-flex flex-column justify-content-center mt text-center">
                <p>
                  <b>Ocurrió un error</b>
                </p>

                {this.state.errorMessages
                  ? this.state.errorMessages.map((error, i) => {
                      return (
                        <div
                          className="alert alert-danger mx-5"
                          role="alert"
                          key={i}
                        >
                          <div className="wordBreak">
                            {error.mensaje}
                          </div>
                        </div>
                      );
                    })
                  : null}
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }

  botones(acciones) {
    
    return acciones.map((item, i) => {
      let disabled = false;
      
      switch (item.tipo) {
        case "PROCESO":
          if (this.props.filtrosAplicados === true) {
            disabled = false;
          } else {
            disabled = true;
          }
          break;
        default:
          break;
      }
      return item.tipo === "DESCARGA"  ? null : (
        <button
          className="ml-2 btn btn-primary"
          disabled={disabled}
          onClick={() => {
            this.accionesBotones(item);
          }}
          key={item.id}
          id={item.id}
        >
          <span>{item.descripcion}</span>
        </button>
      );
    });
  }

  render() {
    return (
      <Fragment>
        <div className="bg-white sticky-topNuevo shadow-sm">
          <div className="d-flex justify-content-between">
            <div className="filtros bg-white shadow-sm">
              <div className="container">
                <div className="row">
                  <div className="col-12 d-flex justify-content-between align-items-center flex-wrap">
                    <div className="d-flex justify-content-start align-items-center flex-xl-row flex-lg-row flex-md-row flex-sm-row flex-column container-filter-search">
                      <button
                        className="btn-round"
                        onClick={() => this.handleToogleFiltros()}
                        id="filter-toggle"
                      >
                        <i className="fi fi-rs-arrow-small-up"></i>
                      </button>
                      <h2 className="global-title-2 m-0">
                        {this.props.filtersSection.descripcion}
                      </h2>
                    </div>
                    <div className="d-flex justify-content-between align-items-center buttons-filtros">
                      <ul className="nav d-flex justify-content-between align-items-center">
                        {this.elementoDescargar(this.props.acciones)}
                        {this.botones(this.props.acciones)}
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div>{this.renderInputs()}</div>
          {this.state.showModal ? this.renderErrorModal() : null}
        </div>
      </Fragment>
    );
  }
}

const mapStateToProps = (state) => ({
  filtrosAplicados: state.filtrosAplicados,
  itemSeleccionadoInterfaces: state.itemSeleccionadoInterfaces,
  itemSeleccionadoProcesos: state.itemSeleccionadoProcesos,
  dataObInfo: state.dataObInfo,
  filtersErrors: state.filtersErrors,
  
});
export default connect(mapStateToProps)(Atributos);
