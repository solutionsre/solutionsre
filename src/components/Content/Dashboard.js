// Generic
import React, { Component, useEffect } from "react";
import { Link } from "react-router-dom";

// Components
import TopNav from "./TopNav";
import Reportes from "./Tipologias/Reportes";
import URLS from "../../urls";
import store from "../../redux/store";
import Loader from "../GlobalComponents/Loader";

// Css
import "./Content.css";

// images
import flowmanager from "../../assets/images/flowmanager.png";
import bgboxorange from "../../assets/images/bg-box-orange.jpg";
import { data } from "jquery";

class Dashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: null,
      loading: true,
      currentApp: this.props.id,
      sectores: null,
    };

    this.obtenerConfiguracion = this.obtenerConfiguracion.bind(this);
  }

  componentWillMount() {
    store.dispatch({
      type: "SET_DATA",
      payload: [],
    });
    this.obtenerConfiguracion(this.state.currentApp);
  }

  componentDidUpdate(prevProps, prevState) {
    if (
      prevState.currentApp !== this.state.currentApp ||
      prevProps.id !== this.props.id
    ) {
      this.obtenerConfiguracion(this.props.id);
    }
  }

  async obtenerConfiguracion(currentApp) {
    this.setState({
      loading: true,
    });
    var myHeaders = new Headers();
    myHeaders.append(
      "Authorization",
      "Bearer " + sessionStorage.getItem("token"),
    );
    myHeaders.append("Content-Type", "application/json");

    var requestOptions = {
      method: "GET",
      headers: myHeaders,
      redirect: "follow",
    };

    await fetch(
      store.getState().serverUrl + URLS.ESCRITORIO + currentApp,
      requestOptions,
    )
      .then(async (response) => {
        if (response.status == 401) {
          sessionStorage.clear();
          window.location.reload(false);
        }else{
          return await response.json();
        }
      })
      .then((result) => {
        if (result !== null && result !== undefined)  {
          this.setState({
            data: result,
            loading: false,
            sectores: result.componentes.sectores,
          });
        }
      });
  }

  render() {
    var app = null;

    if (this.props.app) {
      const userMenu = JSON.parse(sessionStorage.getItem("userMenu"));
      if (this.props.app === "Seguridad") {
        app = userMenu.principal[1].seguridad[0];
      } else {
        const apps = userMenu.principal[0].aplicativos;
        app = apps.find((app) => app.descripcion === this.props.app);
      }
    } else {
      app = JSON.parse(sessionStorage.getItem("defaultApp"));
    }

    const menu = app.menu;
    var favoritos = [];

    if (menu) {
      for (let i = 0; i < menu.length; i++) {
        if (menu[i].menu !== undefined) {
          for (let z = 0; z < menu[i].menu.length; z++) {
            if (menu[i].menu[z].favorito === "1") {
              favoritos.push([
                menu[i].menu[z].texto,
                menu[i].menu[z].id,
                menu[i].menu[z].tipologia,
              ]);
            }
          }
        }
      }
    }

    const renderClass = (espacios) => {
      switch (espacios) {
        case 1:
          return "col-md-4";
        case 2:
          return "col-md-8";
        case 3:
          return "col-md-12";
        default:
      }
    };

    return (
      <div className="content-container">
        <div className="sticky-topNuevo">
          <TopNav
            app={this.props.app}
            nombre="Escritorio"
            ruta={this.props.app + "/Dashboard"}
          />
        </div>


        {this.state.loading ? (
          <div className="container col-12 mt-4">
            <Loader />
          </div>
        ) : (
          <div className="container">
            <div className="main">
              {this.state.data
                ? this.state.data.componentes.renglones.map((renglon, index) => {
                    return (
                      <div className="row" key={index}>

                        {renglon.cajas.map((caja, index) => {
                          return (
                            <div
                              id="1"
                              key={caja.id}
                              className={renderClass(caja.espacios)}
                            >
                              <Reportes
                                altura={renglon.altura}
                                espacios={caja.espacios}
                                posicion={caja.posicion}
                                totales={caja.totales}
                                componentes={caja.componentes}
                                sectores={caja.componentes.sectores}
                                contenedor={caja.contenedor}
                                id={caja.componentes.opcion}
                              />
                            </div>
                          );
                        })}
                      </div>
                    );
                  })
                : 
                  <div className="containerImg">
                    <h1>TESTTT</h1>
                    <img src="../../assets/images/Isologo-Oficial-Horizontal.png" />
                  </div>
                }
            </div>

            {/* <div className="row">
                            <div className="col-xl-8 col-lg-8 col-sm-8 col-md-12 col-12">
                              <div className="dashboard-box" style={boxorangestyle}>
                                <div className="box-interna">
                                  <div className="icon">
                                    <img src={flowmanager} alt="" />
                                  </div>
                                  <div>
                                    <h2 className="global-title-2">
                                      Bienvenido a {this.props.app}
                                    </h2>
                                  </div>
                                </div>
                              </div>
                            </div>
                            
                            <div className="col-xl-4 col-lg-4 col-sm-12 col-md-12 col-12">
                              {favoritos.length !== undefined && (
                                <h2 className="global-title-2 mt-5">Favoritos</h2>
                              )}
                              {favoritos
                                ? favoritos.map((data, key) => {
                                    return (
                                      <li className="dashboard-fav" key={key}>
                                        <Link
                                          to={
                                            "/" + this.props.app + "/" + data[2] + "/" + data[1]
                                          }
                                        >
                                          <i className="fi fi-rs-star mr-2"></i>
                                          {data[0]}
                                        </Link>
                                      </li>
                                    );
                                  })
                                : null}
                            </div>
                          </div> */}
          </div>
        )}


      </div>
    );
  }
}

export default Dashboard;
