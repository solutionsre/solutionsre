// Generic
import React, { Component, Fragment } from 'react';
import store from '../../../redux/store';
import CheckboxTree from 'react-checkbox-tree';
import Konva, { Stage, Layer, Label, Tag, Text, Circle, Star, Rect, Line } from 'react-konva';
import Tooltip from 'react-simple-tooltip';
import Loader from '../../GlobalComponents/Loader';
import $ from 'jquery';

// Components
import URLS from '../../../urls';
import TopNav from '../TopNav';
import RightOverlay from '../../RightOverlay/RightOverlay';

// Css
import '../Content.css';
import BootstrapTable from 'react-bootstrap-table-next';

// Images


class Definiciones extends Component {

    constructor(props) {
        super(props)
        this.state = {
            checkedStructure: [],
            expandedStructure: [],
            selectedStructure: null,
            selectedDefinicion: null,
            showNewDefinitionModal: false,
            stateNewDefinitionModal: 0,
            showHistoricalModal: false,
            clonedDefinition: null,
            showCloneDefinitionModal: false,
            stateCloneDefinitionModal: 0,
            showDeleteDefinitionModal: false,
            showColisionModal: false,
            historical: undefined,
            showSaveModal: false,
            saved: true,
            showAvaliablesModal: false,
            drawMode: false,
            selectedPolygon: null,
            newPolygon: null,
            lastPoints: null,
            currentPoints: null,
            collisionPolygons: [],
      showSuccessMessage: false,
      range: false,
        };

        this.fetchConfig = this.fetchConfig.bind(this);
        this.fetchDefinitionData = this.fetchDefinitionData.bind(this);
        this.handleCloseMenu = this.handleCloseMenu.bind(this);
    }

    componentDidMount() {
        this.fetchConfig();
    }

    async fetchConfig() {
        var myHeaders = new Headers();
        myHeaders.append("Authorization", "Bearer " + sessionStorage.getItem('token'));
        myHeaders.append("Content-Type", "application/json");

        var requestOptions = {
            method: 'GET',
            headers: myHeaders,
            redirect: 'follow'
        };

        await fetch(store.getState().serverUrl + URLS.MAPAS_ESTRUCTURA + this.props.id, requestOptions)
            .then(response => response.json())
            .then(result => {
                this.setState({
                    atributos: result.componentes.atributos,
                    acciones: result.componentes.acciones,
                    componentId: result.componentes.id,
                    descripcion: result.componentes.descripcion,
                    disponibles: result.componentes.disponibles,
                    estructura: result.componentes.estructura,
                    habilitado: result.componentes.habilitado,
                    nombre: result.componentes.nombre,
                    observaciones: result.componentes.observaciones,
                    tipologia: result.componentes.tipologia,
                    nombreMenu: result.componentes.menu,
                    rutaMenu: result.componentes.ruta,
                    editDefinitionAtributos: [ 
                        result.componentes.atributos.find(attr => attr.nombre === "NOMBRE"),
                        result.componentes.atributos.find(attr => attr.nombre === "OBSERVACIONES")
                    ]
                })

                this.handleStructuresTreeNodes()
            })
            .catch(error => console.log('error', error));
    }

    async fetchDefinitionData(id) {
        var myHeaders = new Headers();
        myHeaders.append("Authorization", "Bearer " + sessionStorage.getItem('token'));

        var requestOptions = {
            method: 'GET',
            headers: myHeaders,
            redirect: 'follow'
        };

        await fetch(store.getState().serverUrl + URLS.MAPAS_OBTENER_INFORMACION + id, requestOptions)
            .then(response => response.json())
            .then(result => {
                const background = new Image()
                background.src = "data:image/jpeg;base64," + result.imagen;
                result.background = background

                this.setState({
                    selectedDefinicion: result
                })
            })
            .catch(error => console.log('error', error));
    }

    fetchAvaliablesData() {
        
        const myHeaders = new Headers();
        myHeaders.append("Authorization", "Bearer " + sessionStorage.getItem('token'));
        myHeaders.append("Content-Type", "application/json");

        const definition = this.state.selectedDefinicion
        const avaliable = this.state.disponibles.find(a => a.origen === definition.origen)

        const filters = avaliable.visualizacion.relacionados.map(a => {
            return {
                "atributo": a.relacionado,
                "contenido": definition[a.atributo]
            }
        })

        const raw = JSON.stringify({
            "idObjeto": parseInt(avaliable.visualizacion.id),
            "idOpcion": parseInt(this.props.id),
            "filtros":{
                "busquedas": filters
            }
        })

        const requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: raw,
            redirect: 'follow'
        };
        
        this.setState({disabled: true})
        fetch(store.getState().serverUrl + URLS.MAPAS_OBTENER_DISPONIBLES, requestOptions)
            .then(response => response.json())
            .then(result => {
                this.setState({
                    atributosDisponibles: avaliable.visualizacion.atributos,
                    dataDisponibles: result,
                    devolucionesDisponibles: avaliable.devoluciones,
                    nombreDisponibles: avaliable.nombre,
                    showAvaliablesModal: true,
                    disabled: false
                })
            })
            .catch(error => console.log('error', error));
    }

    handleStructuresTreeNodes() {
        var nodes = []

        if (this.state.estructura) {
            nodes = this.state.estructura.map(pais => {
                if (pais.estructura) {
                    const childrens = pais.estructura.map(componente => {
                        if (componente.estructura) {
                            const childrens = componente.estructura.map(planta => {
                                return {
                                    "label": planta.descripcion,
                                    "value": [pais.referencia, componente.referencia, planta.referencia].join('-')
                                }
                            })

                            return {
                                "label": componente.descripcion,
                                "value": [pais.referencia, componente.referencia].join('-'),
                                "children": childrens
                            }
                        } else {
                            return {
                                "label": componente.descripcion,
                                "value": [pais.referencia, componente.referencia].join('-')
                            }
                        }
                    })

                    return {
                        "label": pais.descripcion,
                        "value": pais.referencia,
                        "children": childrens
                    }
                } else {
                    return {
                        "label": pais.descripcion,
                        "value": pais.referencia
                    }
                }
            })
        }

        this.setState({
            structuresTreeNodes: nodes
        })
    }

    handleSelectStructureNode(e) {
        const value = e.value

        if (value !== this.state.checkedStructure[0]){
            const originsArray = value.split("-")
            const structure = this.handleFindStructure(originsArray)
            structure.originsArray = originsArray
            
            this.setState({
                checkedStructure: [value],
                selectedStructure: structure,
                selectedDefinicion: null
            })
        }
    }

    handleSelectCheckboxStructureNode(e) {
        if (e.length) {
            const value = e.find(item => !this.state.checkedStructure.includes(item))

            if (value !== this.state.checkedStructure[0]) {
                const originsArray = value.split("-")
                const structure = this.handleFindStructure(originsArray)
                structure.originsArray = originsArray

                this.setState({
                    checkedStructure: [value],
                    selectedStructure: structure,
                    selectedDefinicion: null
                })
            }
        }
    }

    handleFindStructure(originsArray){
        var structure = this.state.estructura

        originsArray.forEach(referencia => {
            if (structure.estructura) {
                structure = structure.estructura.find(s => s.referencia === referencia)
            } else {
                structure = structure.find(s => s.referencia === referencia)
            }
        })
        return structure
    }

    handleSelectDefinition(id) {
        // $("#wrapper").toggleClass("toggled")
        this.fetchDefinitionData(id)
    }

    handleOpenHistoricalModal() {
        this.setState({
            showHistoricalModal: true
        })
    }

    handleOnChangeHistorical(e) {
        this.setState({
            historical: e.target.value
        })
    }

    handleCancelHistoricalModal() {
        this.setState({
            showHistoricalModal: false,
            historical: undefined
        })
    }

    handleOpenDeleteDefinitionModal() {
        this.setState({
            showDeleteDefinitionModal: true
        })
    }

    handleCancelDeleteDefinitionModal() {
        this.setState({
            showDeleteDefinitionModal: false
        })
    }

    handleSubmitDeleteDefinitionModal(id) {
        this.setState({
            showDeleteDefinitionModal: false
        })
        this.handleDeleteDefinition(id)
    }

    handleSubmitHistoricalModal() {
        // $("#wrapper").toggleClass("toggled")
        this.fetchDefinitionData(this.state.historical)

        this.setState({
            showHistoricalModal: false,
            historical: undefined
        })
    }

    handleOpenColisionModal() {
        this.setState({
            showColisionModal: true
        })
    }

    handleCancelColisionModal() {
        this.setState({
            showColisionModal: false
        })
        this.handleCancelDrawMode()
    }

    handleCancelSubmitModal(){
        this.setState({
            showSuccessMessage: false
        })
    }
    handleCloseMenu() {
        if (this.state.saved){
            // $("#wrapper").toggleClass("toggled");

            this.handlePolygonDeselect()

            this.setState({
                selectedDefinicion: null
            })
        } else {
            this.setState({
                showSaveModal: true
            })
        }
    }

    handleCancelSaveModal() {
        this.setState({
            showSaveModal: false
        })
    }

    handleSubmitSaveModal() {
        // $("#wrapper").toggleClass("toggled");

        this.handlePolygonDeselect()

        this.setState({
            showSaveModal: false,
            selectedDefinicion: null,
            saved: true
        })
    }

    handleOpenNewDefinitionModal() {
        const newDefinition = {
            [this.state.atributos.find(attr => attr.nombre === "INTERNO").id]: "0",
            [this.state.atributos.find(attr => attr.nombre === "COD_ORIGEN").id]: this.state.selectedStructure.origen,
            [this.state.atributos.find(attr => attr.nombre === "REF_ORIGEN").id]: this.state.selectedStructure.referencia,
        }

        this.setState({
            newDefinition: newDefinition,
            stateNewDefinitionModal: 0,
            showNewDefinitionModal: true
        })
    }

    handleCloseNewDefinitionModal() {
        this.setState({
            newDefinition: null,
            showNewDefinitionModal: false
        })
    }

    handleOpenEditDefinitionModal(definition) {
        const editDefinition = {
            [this.state.atributos.find(attr => attr.nombre === "INTERNO").id]: definition.definicion,
            [this.state.atributos.find(attr => attr.nombre === "NOMBRE").id]: definition.nombre,
            [this.state.atributos.find(attr => attr.nombre === "OBSERVACIONES").id]: definition.observaciones,
        }

        this.setState({
            editDefinition: editDefinition,
            stateEditDefinitionModal: 0,
            showEditDefinitionModal: true
        })
    }

    handleCloseEditDefinitionModal() {
        this.setState({
            editDefinition: null,
            showEditDefinitionModal: false
        })
    }

    async handleOpenCloneDefinitionModal(definition) {
        const clonedDefinitionData = {
            [this.state.atributos.find(attr => attr.nombre === "COD_ORIGEN").id]: this.state.selectedStructure.origen,
            [this.state.atributos.find(attr => attr.nombre === "REF_ORIGEN").id]: this.state.selectedStructure.referencia,
            [this.state.atributos.find(attr => attr.nombre === "DESDE").id]: definition.desde,
            [this.state.atributos.find(attr => attr.nombre === "NOMBRE").id]: definition.nombre,
            [this.state.atributos.find(attr => attr.nombre === "OBSERVACIONES").id]: definition.observaciones
        }

        await this.setState({
            clonedDefinition: definition,
            clonedDefinitionData: clonedDefinitionData
        })
        
        this.setState({
            stateCloneDefinitionModal: 0,
            showCloneDefinitionModal: true
        })
    }

    handleCloseCloneDefinitionModal() {
        this.setState({
            clonedDefinition: null,
            clonedDefinitionData: null,
            showCloneDefinitionModal: false
        })
    }

    async handleSaveNewDefinition(atributos) {
        const myHeaders = new Headers();
        myHeaders.append("Authorization", "Bearer " + sessionStorage.getItem('token'));
        myHeaders.append("Content-Type", "application/json");

        const raw = JSON.stringify({
            "datos": {
                "objeto": this.state.componentId,
                "atributos": atributos
            }
        });

        // console.log(raw)

        const requestOptions = {
            method: "POST",
            headers: myHeaders,
            body: raw,
            redirect: 'follow'
        };

        await fetch(store.getState().serverUrl + URLS.MAPAS_AGREGAR, requestOptions)
            .then(response => response.text())
            .then(result => {
                if (result === '') {
                    this.handleUpdateData()
                    this.setState({
                        stateNewDefinitionModal: 200
                    })
                } else {
                    const error = JSON.parse(result)
                    if (error.Mensajes) {
                        this.setState({
                            stateNewDefinitionModal: 400,
                            errorMessagesNewDefinition: error.Mensajes
    
                        })
                    } else {
                        this.setState({
                            stateNewDefinitionModal: 400,
                            errorMessagesNewDefinition: [{mensaje: error.Mensaje}]
    
                        })
                    }
                }
            })
            .catch(error => {
                console.log('error', error)
                this.setState({
                    stateNewDefinitionModal: 400,
                    errorMessagesNewDefinition: JSON.stringify(error)
                })
            })
    }

    async handleEditDefinition(atributos) {
        const myHeaders = new Headers();
        myHeaders.append("Authorization", "Bearer " + sessionStorage.getItem('token'));
        myHeaders.append("Content-Type", "application/json");

        const raw = JSON.stringify({
            "datos": {
                "objeto": this.state.componentId,
                "atributos": atributos
            }
        });

        // console.log(raw)

        const requestOptions = {
            method: "POST",
            headers: myHeaders,
            body: raw,
            redirect: 'follow'
        };

        await fetch(store.getState().serverUrl + URLS.MAPAS_MODIFICAR, requestOptions)
            .then(response => response.text())
            .then(result => {
                if (result === '') {
                    this.handleUpdateData()
                    this.setState({
                        stateEditDefinitionModal: 200
                    })
                } else {
                    const error = JSON.parse(result)
                    console.log(error)
                    if (error.Mensajes) {
                        this.setState({
                            stateEditDefinitionModal: 400,
                            errorMessagesEditDefinition: error.Mensajes
    
                        })
                    } else {
                        this.setState({
                            stateEditDefinitionModal: 400,
                            errorMessagesEditDefinition: [{mensaje: error.Mensaje}]
    
                        })
                    }
                }
            })
            .catch(error => {
                console.log('error', error)
                this.setState({
                    stateEditDefinitionModal: 400,
                    errorMessagesEditDefinition: JSON.stringify(error)

                })
            })
    }

    async handleCloneDefinition(atributos) {
        const myHeaders = new Headers();
        myHeaders.append("Authorization", "Bearer " + sessionStorage.getItem('token'));
        myHeaders.append("Content-Type", "application/json");

        const raw = JSON.stringify({
            "datos": {
                "objeto": this.state.componentId,
                "asignacion": this.state.clonedDefinition.asignacion,
                "atributos": atributos
            }
        });
        // console.log(raw)

        const requestOptions = {
            method: "POST",
            headers: myHeaders,
            body: raw,
            redirect: 'follow'
        };

        await fetch(store.getState().serverUrl + URLS.MAPAS_COPIAR, requestOptions)
            .then(response => response.text())
            .then(result => {
                if (result === '') {
                    this.handleUpdateData()
                    this.setState({
                        stateCloneDefinitionModal: 200
                    })
                } else {
                    const error = JSON.parse(result)
                    console.log(error)
                    if (error.Mensajes) {
                        this.setState({
                            stateCloneDefinitionModal: 400,
                            errorMessagesCloneDefinition: error.Mensajes
    
                        })
                    } else {
                        this.setState({
                            stateCloneDefinitionModal: 400,
                            errorMessagesCloneDefinition: [{mensaje: error.Mensaje}]
    
                        })
                    }
                }
            })
            .catch(error => {
                console.log('error', error)
                this.setState({
                    stateCloneDefinitionModal: 400,
                    errorMessagesCloneDefinition: JSON.stringify(error)
                })
            })
    }

    handleSaveDefinition() {
        this.handlePolygonDeselect()

        const myHeaders = new Headers();
        myHeaders.append("Authorization", "Bearer " + sessionStorage.getItem('token'));
        myHeaders.append("Content-Type", "application/json");

        const definition = this.state.selectedDefinicion
        var elements = []
        
        if (definition.elementos){
            elements = definition.elementos.map(element => {
                return({
                    elemento: element.elemento,
                    referencia: element.referencia,
                    variables: element.variables
                })
            })
        }

        const raw = JSON.stringify({
            "datos" : {
                "objeto": this.state.componentId,
                "asignacion": definition.asignacion,
                "elementos": elements
            }
        })
        const requestOptions = {
            method: "POST",
            headers: myHeaders,
            body: raw,
            redirect: 'follow'
        };
       
        fetch(store.getState().serverUrl + URLS.MAPAS_GUARDAR, requestOptions)
            .then(response => response.text())
            .then(() => {
                this.setState({
                    saved: true,
                    showSuccessMessage: true
                })

                this.handleUpdateData()
            })
            .catch(error => console.log('error', error));
    }

    handleDeleteDefinition(id) {
        const myHeaders = new Headers();
        myHeaders.append("Authorization", "Bearer " + sessionStorage.getItem('token'));
        myHeaders.append("Content-Type", "application/json");

        const raw = JSON.stringify({
            "datos":{
                "objeto": this.state.componentId,
                "definicion": id
            }
        })
        
        const requestOptions = {
            method: "POST",
            headers: myHeaders,
            body: raw,
            redirect: 'follow'
        };

        fetch(store.getState().serverUrl + URLS.MAPAS_ELIMINAR, requestOptions)
            .then(response => response.text())
            .then(() => {
                this.handleUpdateData()
            })
            .catch(error => console.log('error', error));
    }

    async handleUpdateData() {
        await this.fetchConfig()
        if (this.state.selectedStructure) {
            const originsArray = this.state.selectedStructure.originsArray
            const newStructure =  this.handleFindStructure(originsArray)
            newStructure.originsArray = originsArray
            
            if (this.state.selectedDefinicion) {
                const newDefinition = newStructure.definiciones.find(d => d.definicion === this.state.selectedDefinicion.definicion)
                this.fetchDefinitionData(newDefinition.asignacion)
            }

            this.setState({
                selectedStructure: newStructure
            })
        }
    }

    handleKeyDown(e) {
        switch (e.key) {
            case "Enter":
                if (this.state.drawMode && this.state.newPolygon && !this.state.collisionPolygons.length){
                    this.handleSubmitDrawMode()
                }
                break;
            case "End":
                if (this.state.drawMode && this.state.newPolygon && !this.state.collisionPolygons.length){
                    this.handleSubmitDrawMode()
                }
                break;
            case "Escape":
                if (this.state.drawMode) {
                    this.handleCancelDrawMode()
                }
                this.handlePolygonDeselect()
                break;
            default:
                break;
        }
    }

    handleDrawMode() {
        
           if(this.state.disabled == true){

           }
           else{
            $("#btn-draw").toggleClass("active")

            const drawMode = this.state.drawMode
            this.handlePolygonDeselect()

            const definition = this.state.selectedDefinicion
            const collisionPolygons = this.state.collisionPolygons
            collisionPolygons.forEach(p => {
            let polygon = definition.elementos.find(polygon => polygon.id === p.id)
            polygon.variables.fill = "#ffffff90"
            })

            this.setState({
                drawMode: !drawMode,
                selectedDefinicion: definition,
             newPolygon: null,
                collisionPolygons: [],
                drawfigures: true
            })

           }
            
    }

   

    handleSubmitDrawMode() {
       

        this.fetchAvaliablesData()
        
    }

    handleCancelDrawMode() {
        $("#btn-draw").toggleClass("active")

        const definition = this.state.selectedDefinicion
        const collisionPolygons = this.state.collisionPolygons
        collisionPolygons.forEach(p => {
            let polygon = definition.elementos.find(polygon => polygon.id === p.id)
            polygon.variables.fill = "#ffffff90"
        })

        this.setState({
            drawMode: false,
            selectedDefinicion: definition,
            newPolygon: null,
            collisionPolygons: []
        })
    }

    handlefiguras(e){
        var result = "";
       switch(e){
        case "R":
            result = "R"
            break;
        case "C":
             result = "C"
            break;
        case "E": 
            result = "E"
            break;
       }
       this.setState({typesShapes: result})
    }

    handleCloseAvaliablesModal() {

        $("#btn-draw").toggleClass("active")
        this.setState({
            atributosDisponibles: null,
            dataDisponibles: null,
            devolucionesDisponibles: null,
            nombreDisponibles: null,
            selectedPolygon: null,
            showAvaliablesModal: false,
            drawMode: false
        })
    }

    handleSubmitAvaliablesModal(atributos) {
        const definition = this.state.selectedDefinicion
        var polygon = null

        if (this.state.selectedPolygon) {
            polygon = definition.elementos.find(elemt => elemt.id === this.state.selectedPolygon.id)
            Object.entries(atributos).forEach((item) => {
                polygon[item[0]] = JSON.stringify(item[1])
            })

        } else {
            polygon = this.state.newPolygon

            Object.entries(atributos).forEach((item) => {
                polygon[item[0]] = JSON.stringify(item[1])
            })

            definition.elementos.push(polygon)
        }
       

        $("#btn-draw").toggleClass("active")

        this.setState({
            drawMode: false,
            atributosDisponibles: null,
            dataDisponibles: null,
            devolucionesDisponibles: null,
            nombreDisponibles: null,
            selectedDefinicion: definition,
            newPolygon: null,
            selectedPolygon: polygon,
            saved: false,
            showAvaliablesModal: false
        })
    }

    // REVISAR ESCALA AL SELECCIONAR UNA POSICION 
    // handleStageZoom(e) {
    //     const stage = e.currentTarget
    //     var scaleBy = 1.01;

    //     e.evt.preventDefault();
    //     var oldScale = stage.scaleX();

    //     var pointer = stage.getPointerPosition();

    //     var mousePointTo = {
    //       x: (pointer.x - stage.x()) / oldScale,
    //       y: (pointer.y - stage.y()) / oldScale,
    //     };

    //     var newScale =
    //       e.evt.deltaY > 0 ? oldScale * scaleBy : oldScale / scaleBy;

    //     stage.scale({ x: newScale, y: newScale });

    //     var newPos = {
    //       x: pointer.x - mousePointTo.x * newScale,
    //       y: pointer.y - mousePointTo.y * newScale,
    //     };
    //     stage.position(newPos);
    //     stage.batchDraw();
    // }

    handleStageContentMousemove(e) {
        const stage = e.currentTarget
        if (this.state.drawMode && this.state.lastPoints) {
            this.setState({
                currentPoints: {
                    'x': (stage.getPointerPosition().x),
                    'y': (stage.getPointerPosition().y)
                }
            })
        }
    }

    handleStageContentMousedown(e) {
        const stage = e.currentTarget
        

        if (this.state.drawMode) {
            var polygon

            if (this.state.newPolygon) {
                polygon = this.state.newPolygon
                 

            } else {
                const definition = this.state.selectedDefinicion
                var newElementId = "0"

                if (definition.elementos && definition.elementos.length) {
                    const lastElementId = definition.elementos[definition.elementos.length - 1].id
                    newElementId = JSON.stringify(parseInt(lastElementId) + 1)
                } else {
                    definition.elementos = []
                }
        let tamanio = 20
        this.state.selectedDefinicion.elementos.map((item) => {
          tamanio = item.variables.tamanio
        })
                polygon = {
                    id: newElementId,
                    elemento: definition.elemento,
                    referencia: "",
                    variables: {
                        fill: "#ffffff90",
                        poligono: [0, 0],
                        stroke: "blue",
                        x: JSON.stringify(stage.getPointerPosition().x),
                        y: JSON.stringify(stage.getPointerPosition().y),
            forma: this.state.typesShapes,
            tamanio: tamanio
            
          },
        };
            }

            this.handlePolygonCollision(polygon)


            if (!this.state.collisionPolygons.length){
                this.setState({
                    newPolygon: polygon,
                    lastPoints: {
                        'x': stage.getPointerPosition().x,
                        'y': stage.getPointerPosition().y
                    },
                    currentPoints: {
                        'x': polygon.variables.x,
                        'y': polygon.variables.y
                    },
                    
                })
                
                this.handleSubmitDrawMode()
                this.setState({
                    drawfigures : false,
                })
            }else{
                this.handleOpenColisionModal()
            }
            
        } else {
            
            this.handlePolygonDeselect()
        }
    }

    handlePolygonEnter(e) {
        if (!this.state.drawMode) {
            const definition = this.state.selectedDefinicion
            const polygon = definition.elementos.find(obj => obj.id === e.currentTarget.attrs.id)
            if (polygon !== this.state.selectedPolygon) {
                polygon.variables.stroke = 'red'

                this.setState({
                    selectedDefinicion: definition
                })
            }
            const center = {}
            const points = { 
                xMax: 0,
                xMin: 0,
                yMax: 0,
                yMin: 0
            }
        

            center.x = polygon.variables.x
            center.y = polygon.variables.y
            
            polygon.center = center
            polygon.position = "down"

            if (center.y < 30){
                polygon.position = "up"
            }
            if (center.x < 100){
                polygon.position = "left"
            } else if (center.x > 1100){
                polygon.position = "right"
            }

            this.setState({
                overPolygon: polygon
            })
        }
    }

    handlePolygonLeave(e) {
        if (!this.state.drawMode) {
            const definition = this.state.selectedDefinicion
            const polygon = definition.elementos.find(obj => obj.id === e.currentTarget.attrs.id)
            if (polygon !== this.state.selectedPolygon) {
                polygon.variables.stroke = "#500000"

                this.setState({
                    selectedDefinicion: definition
                })
            }
            this.setState({
                overPolygon: null
            })
        }
    }

    handlePolygonClick(e) {
        if (!this.state.drawMode) {
            this.handlePolygonSelect(e.currentTarget)
        }
    }

    handlePolygonDragEnd(e) {
        const definition = this.state.selectedDefinicion
        const polygon = definition.elementos.filter(obj => obj.id === e.target.attrs.id)
        
        if (polygon.length > 0) {
            const x = polygon[0].variables.x
            const y = polygon[0].variables.y
            polygon[0].variables.x = e.currentTarget._lastPos.x
            polygon[0].variables.y = e.currentTarget._lastPos.y
            this.handlePolygonCollision(polygon[0])
            if (this.state.collisionPolygons.length){
                polygon[0].variables.x = x
                polygon[0].variables.y = y
                this.handleOpenColisionModal()
            }

        }


        // EN CASO DE ACTUALIZAR LA NUEVA POSICION DE UN POLIGONO, 
        // SE DEBERA GUARDAR e.target._lastPos
         // console.log("El elemento " + e.target.attrs.id + " se ha movido de su posicion original (" + polygon.variables.x + "," + polygon.variables.y + ") a la posicion (" + e.target._lastPos.x + "," + e.target._lastPos.y + ")")
    }

    handlePolygonSelect(polygon){
        this.handlePolygonDeselect()

        const definition = this.state.selectedDefinicion
        const currentPolygon = definition.elementos.find(obj => obj.id === polygon.attrs.id)
        currentPolygon.variables.stroke = "blue"

        this.setState({
            selectedDefinicion: definition,
            selectedPolygon: currentPolygon
        })
    }

    handlePolygonDeselect(){
        if (this.state.selectedPolygon) {
            const definition = this.state.selectedDefinicion
            const selectedPolygon = definition.elementos.find(obj => obj.id === this.state.selectedPolygon.id)
            selectedPolygon.variables.stroke = "#500000"
            
            this.setState({
                selectedDefinicion: definition,
                selectedPolygon: null
            })   
        }
    }

    handlePolygonEdit(){
        this.fetchAvaliablesData()
    }

    handlePolygonDelete(){
        if (this.state.selectedPolygon) {
            const definition = this.state.selectedDefinicion
            definition.elementos = definition.elementos.filter(obj => obj.id !== this.state.selectedPolygon.id)

            this.setState({
                selectedDefinicion: definition,
                selectedPolygon: null,
                saved: false
            })   
        }
    }

  CambiarTamaño() {
    this.setState({
      range: !this.state.range,
    });
  }
    handlePolygonCollision(currentPolygon){
        const definition = this.state.selectedDefinicion
        const polygons = definition.elementos
        var mensajeColision = ""
        var mostrarMensaje = false

        if (polygons){
            var collisionPolygons = this.state.collisionPolygons
            polygons.forEach(polygon => {
                var collision = false
                if (polygon.id !== currentPolygon.id) {
                     // PRIMERO VERIFICO QUE EL NUEVO POLIGONO NO ESTE DENTRO DE OTRO
                    //modificar para colision
                    const firstPoint = {
                        x: parseInt(currentPolygon.variables.x),
                        y: parseInt(currentPolygon.variables.y)
                    }
                    collision = this.handlePolygonIsInside(firstPoint, polygon)
                }



/*



                // DEBO REVISAR EL SEGMENTO FORMADO POR EL ULTIMO PUNTO Y EL PRIMER PUNTO CREADO PARA CERRAR EL POLIGONO
                //modificar para colision
                if (!collision && currentPolygon.variables.poligono.length > 4) {
                    let closeSegment = {
                        p: {
                             
                            x: currentPolygon.variables.poligono[currentPolygon.variables.poligono.length - 2] + parseInt(currentPolygon.variables.x),
                            y: currentPolygon.variables.poligono[currentPolygon.variables.poligono.length - 1] + parseInt(currentPolygon.variables.y)
                        },
                        q: {
                            x: currentPolygon.variables.poligono[0] + parseInt(currentPolygon.variables.x),
                            y: currentPolygon.variables.poligono[1] + parseInt(currentPolygon.variables.y)
                        }
                    }

                    collision = this.handleSegmentCollision(closeSegment, polygon)
                }

                // DEBO REVISAR TODOS SEGMENTOS DEL POLIGONO
                for (let index = 0; !collision && index < currentPolygon.variables.poligono.length - 2; index = index + 2) {
                    let currentSegment = {
                        p: {
                            x: currentPolygon.variables.poligono[index] + parseInt(currentPolygon.variables.x),
                            y: currentPolygon.variables.poligono[index + 1] + parseInt(currentPolygon.variables.y)
                        },
                        q: {
                            x: currentPolygon.variables.poligono[index + 2] + parseInt(currentPolygon.variables.x),
                            y: currentPolygon.variables.poligono[index + 3] + parseInt(currentPolygon.variables.y)
                        }
                    }

                    collision = this.handleSegmentCollision(currentSegment, polygon)
                }
*/







                if (collision) {
                  //  this.setState({errorMessagesColision : "La definicion del local se superpone con otra ya existente"})

                    collisionPolygons.push(polygon)
                    polygon.variables.fill = "red"
                } else {
                    collisionPolygons = collisionPolygons.filter(p => p.id !== polygon.id)
                    polygon.variables.fill = "#ffffff90"
                }
            })

            this.setState({
                collisionPolygons: collisionPolygons,
                selectedDefinicion: definition,
            })
        }
    }

    handlePolygonIsInside(point, polygon){

        var colision = false

        let dx = polygon.variables.x - point.x;
        let dy = polygon.variables.y - point.y;
        let distancia = Math.sqrt(dx * dx + dy * dy );
        let SumaRadios = 20 + 20;

        if(distancia < SumaRadios){
            colision = true;
        }
        return colision


    /*

        var leftCollision = false
        const leftSegment = {
            p: point,
            q: {
                x: point.x - 2000,
                y: point.y
            }
        }
        var rightCollision = false
        const rightSegment = {
            p: point,
            q: {
                x: point.x + 2000,
                y: point.y
            }
        }
        //modificar para colision
        for (let index = 0; !leftCollision && index < polygon.variables.poligono.length - 2; index = index + 2) {
            let segment = {
                p: {
                    x: polygon.variables.poligono[index] + parseInt(polygon.variables.x),
                    y: polygon.variables.poligono[index + 1] + parseInt(polygon.variables.y)
                },
                q: {
                    x: polygon.variables.poligono[index + 2] + parseInt(polygon.variables.x),
                    y: polygon.variables.poligono[index + 3] + parseInt(polygon.variables.y)
                }
            }

            leftCollision = this.handleSegmentIntersection(leftSegment, segment)
        }
        
        // DEBO REVISAR EL SEGMENTO DEL POLIGONO FORMADO POR EL ULTIMO PUNTO Y EL PRIMER PUNTO CREADO PARA CERRAR EL POLIGONO
        //modificar para colision
        if (!leftCollision && polygon.variables.poligono.length > 4) {
            let segment = {
                p: {
                    x: polygon.variables.poligono[polygon.variables.poligono.length - 2] + parseInt(polygon.variables.x),
                    y: polygon.variables.poligono[polygon.variables.poligono.length - 1] + parseInt(polygon.variables.y)
                },
                q: {
                    x: polygon.variables.poligono[0] + parseInt(polygon.variables.x),
                    y: polygon.variables.poligono[1] + parseInt(polygon.variables.y)
                }
            }

            leftCollision = this.handleSegmentIntersection(leftSegment, segment)
        }
        //modificar para colision
        for (let index = 0; !rightCollision && index < polygon.variables.poligono.length - 2; index = index + 2) {
            let segment = {
                p: {
                    x: polygon.variables.poligono[index] + parseInt(polygon.variables.x),
                    y: polygon.variables.poligono[index + 1] + parseInt(polygon.variables.y)
                },
                q: {
                    x: polygon.variables.poligono[index + 2] + parseInt(polygon.variables.x),
                    y: polygon.variables.poligono[index + 3] + parseInt(polygon.variables.y)
                }
            }

            rightCollision = this.handleSegmentIntersection(rightSegment, segment)
        }
        
        // DEBO REVISAR EL SEGMENTO DEL POLIGONO FORMADO POR EL ULTIMO PUNTO Y EL PRIMER PUNTO CREADO PARA CERRAR EL POLIGONO
        //modificar para colision
        if (!rightCollision && polygon.variables.poligono.length > 4) {
            let segment = {
                p: {
                    x: polygon.variables.poligono[polygon.variables.poligono.length - 2] + parseInt(polygon.variables.x),
                    y: polygon.variables.poligono[polygon.variables.poligono.length - 1] + parseInt(polygon.variables.y)
                },
                q: {
                    x: polygon.variables.poligono[0] + parseInt(polygon.variables.x),
                    y: polygon.variables.poligono[1] + parseInt(polygon.variables.y)
                }
            }

            rightCollision = this.handleSegmentIntersection(rightSegment, segment)
        }

        return (leftCollision && rightCollision)
*/



    }

    /*
    handleSegmentCollision(currentSegment, polygon) {
        var collision = false
        //modificar para colision
        for (let index = 0; !collision && index < polygon.variables.poligono.length - 2; index = index + 2) {
            let segment = {
                p: {
                    x: polygon.variables.poligono[index] + parseInt(polygon.variables.x),
                    y: polygon.variables.poligono[index + 1] + parseInt(polygon.variables.y)
                },
                q: {
                    x: polygon.variables.poligono[index + 2] + parseInt(polygon.variables.x),
                    y: polygon.variables.poligono[index + 3] + parseInt(polygon.variables.y)
                }
            }

            collision = this.handleSegmentIntersection(currentSegment, segment)
        }
        
        // DEBO REVISAR EL SEGMENTO DEL POLIGONO FORMADO POR EL ULTIMO PUNTO Y EL PRIMER PUNTO CREADO PARA CERRAR EL POLIGONO
        //modificar para colision
        if (!collision && polygon.variables.poligono.length > 4) {
            let segment = {
                p: {
                    x: polygon.variables.poligono[polygon.variables.poligono.length - 2] + parseInt(polygon.variables.x),
                    y: polygon.variables.poligono[polygon.variables.poligono.length - 1] + parseInt(polygon.variables.y)
                },
                q: {
                    x: polygon.variables.poligono[0] + parseInt(polygon.variables.x),
                    y: polygon.variables.poligono[1] + parseInt(polygon.variables.y)
                }
            }

            collision = this.handleSegmentIntersection(currentSegment, segment)
        }

        return collision
    }

    handleSegmentIntersection(currentSegment, segment) {

        function onSegment(p, q, r) { 
            if (q.x <= Math.max(p.x, r.x) && q.x >= Math.min(p.x, r.x) && 
                q.y <= Math.max(p.y, r.y) && q.y >= Math.min(p.y, r.y)) {
                return true; 
            }
    
            return false; 
        } 
  
        function orientation(p, q, r) { 
            var val = (q.y - p.y) * (r.x - q.x) - (q.x - p.x) * (r.y - q.y);
            var result
            
            if (val === 0) {
                result = 0
            } else if (val > 0) {
                result= 1
            } else {
                result = 2
            }

            return result
        } 

        const o1 = orientation(currentSegment.p, currentSegment.q, segment.p); 
        const o2 = orientation(currentSegment.p, currentSegment.q, segment.q); 
        const o3 = orientation(segment.p, segment.q, currentSegment.p); 
        const o4 = orientation(segment.p, segment.q, currentSegment.q); 
    
        // General case 
        if (o1 !== o2 && o3 !== o4)  {
            return true; 
        }
    
        // Special Cases 
        // p1, q1 and p2 are colinear and p2 lies on segment p1q1 
        if (o1 === 0 && onSegment(currentSegment.p, segment.p, currentSegment.q)) {
            return true
        }

        // p1, q1 and q2 are colinear and q2 lies on segment p1q1 
        if (o2 === 0 && onSegment(currentSegment.p, segment.q, currentSegment.q)) {
            return true
        }

        // p2, q2 and p1 are colinear and p1 lies on segment p2q2 
        if (o3 === 0 && onSegment(segment.p, currentSegment.p, segment.q)) {
            return true
        }

        // p2, q2 and q1 are colinear and q1 lies on segment p2q2 
        if (o4 === 0 && onSegment(segment.p, currentSegment.q, segment.q)) {
            return true
        }
        
        return false // Doesn't fall in any of the above cases 
    }
*/
  
    obtenerLimiteLiGui(ExcluirForma, layer, stage){
        var vertical = [];
        var horizontal = [];

        stage.find('.object').forEach((guideItem) => {
            if (guideItem === ExcluirForma) {
              return;
            }
            var box = guideItem;
            vertical.push([box.attrs.x, box.attrs.x + 20, box.attrs.x + 20 / 2]);
            horizontal.push([box.attrs.y, box.attrs.y + 20, box.attrs.y + 20 / 2]);
        });
        this.setState({
            vertical: vertical.flat(),
            horizontal: horizontal.flat(),
        })

        return {
            vertical: vertical.flat(),
            horizontal: horizontal.flat(),
          };
    } 

    obtenerLineasBorOb(node){
        var box = node;
        var absPos = node.absolutePosition();

        return {
          vertical: [
            {
              guide: Math.round(box.attrs.x),
              offset: Math.round(absPos.x - box.attrs.x),
              snap: 'start',
            },
            {
              guide: Math.round(box.attrs.x + 20 / 2),
              offset: Math.round(absPos.x - box.attrs.x - 20 / 2),
              snap: 'center',
            },
            {
              guide: Math.round(box.attrs.x + 20),
              offset: Math.round(absPos.x - box.attrs.x - 20),
              snap: 'end',
            },
          ],
          horizontal: [
            {
              guide: Math.round(box.attrs.y),
              offset: Math.round(absPos.y - box.attrs.y),
              snap: 'start',
            },
            {
              guide: Math.round(box.attrs.y + 20 / 2),
              offset: Math.round(absPos.y - box.attrs.y - 20 / 2),
              snap: 'center',
            },
            {
              guide: Math.round(box.attrs.y + 20),
              offset: Math.round(absPos.y - box.attrs.y - 20),
              snap: 'end',
            },
          ],
        };
    }

    obtenerGuias(FrenoLineasGuia, lazosdeitems){
        var resultadoV = [];
        var resultadoH = [];
        
        FrenoLineasGuia.vertical.forEach((lineaguia) => {
            lazosdeitems.vertical.forEach((lazodeitem) => {
                var diferencia = Math.abs(lineaguia - lazodeitem.guide);
                if(diferencia > 5){
                    resultadoV.push({
                    lineaguia: lineaguia,
                    diferencia: diferencia,
                    snap: lazodeitem.snap,
                    offset: lazodeitem.offset
                });
               }
            });
            
        });

        FrenoLineasGuia.horizontal.forEach((lineaguia) => {
            lazosdeitems.horizontal.forEach((lazodeitem) => {
                var diferencia = Math.abs(lineaguia - lazodeitem.guide);
                if(diferencia > 5){
                    resultadoH.push({
                    lineaguia: lineaguia,
                    diferencia: diferencia,
                    snap: lazodeitem.snap,
                    offset: lazodeitem.offset
                });
               }
            });
            
        });

        const guias = [];
        
        var minV = resultadoV.sort((a, b) => a.diferencia - b.diferencia)[0];
        var minH = resultadoH.sort((a, b) => a.diferencia - b.diferencia)[0];

      
        if(minV){
            guias.push({
                lineaguia: minV.lineaguia,
                offset: minV.offset,
                orientacion: 'V',
                snap: minV.snap
            });
        }

        if(minH){
            guias.push({
                lineaguia: minH.lineaguia,
                offset: minH.offset,
                orientacion: 'H',
                snap: minH.snap
            });
        }
       
        return guias; 
    }

    DibujarGuias(guias, layer, stage){
       
        guias.forEach((lg) => {
    
            if(lg.orientacion === 'H'){
                let guia = {
                    points: [-6000, 0, 6000, 0],
                    stroke: "black",
                    strokeWidth: 1,
                    name: "linea-guia",
                    dash: [4, 6],
                    x: 0,
                    y: lg.lineaguia
                }
                this.setState({
                    hLines : [guia]
                })
            }
            else if(lg.orientacion === 'V'){
                let guia = {
                    points: [0, -6000, 0, 6000],
                    stroke: "black",
                    strokeWidth: 1,
                    name: "linea-guia",
                    dash: [4, 6],
                    x: lg.lineaguia,
                    y: 0
                };
                this.setState({
                    vLines : [guia]
                })
            }
  
        });

       
    }
    
    Arrastrar(e){
            const layer = e.target.getLayer();
            const stage = e.target.getStage();

            var frenoslineasguia = this.obtenerLimiteLiGui(e.target, layer, stage); //A estas dos funciones les paso el stage que obtuve aqui para utilizarlo en los siguientes momentos.
            var lazoobjeto = this.obtenerLineasBorOb(e.target,layer, stage);
            var guias = this.obtenerGuias(frenoslineasguia, lazoobjeto);

            if(!guias.length){
                return;
            }

            this.DibujarGuias(guias, layer, stage);

            this.setState({
                isDragging: true
            })

        
            var absPos = e.target.absolutePosition();

            /*
            guias.forEach((lg) => {
                switch(lg.snap){
                    case 'start': {
                        switch(lg.orientacion){
                            case "V": {
                                absPos.x = lg.lineaguia  + lg.offset;
                                break;
                            }
                            case "H": {
                                absPos.y = lg.lineaguia + lg.offset;
                                break;
                            }
                            default: 
                                break;
                        }
                        break;
                    }
                    case "center": {
                        switch(lg.orientacion){
                            case "V": {
                                absPos.x = lg.lineaguia  + lg.offset;
                                break;
                            }
                            case "H": {
                                absPos.y = lg.lineaguia + lg.offset;
                                break;
                            }
                            default: 
                                break;
                        }
                        break;
                    }
                    case "end": {
                        switch(lg.orientacion){
                            case "V": {
                                absPos.x = lg.lineaguia  + lg.offset;
                                break;
                            }
                            case "H": {
                                absPos.y = lg.lineaguia + lg.offset;
                                break;
                            }
                            default: 
                                break;
                        }
                        break;
                    }
                    default: 
                        break;
                }
            });
            */
           /*
            guias.forEach((lg) => {
                switch (lg.orientacion) {
                  case 'V': {
                    absPos.x = lg.lineaguia + lg.offset;
                    break;
                  }
                  case 'H': {
                    absPos.y = lg.lineaguia + lg.offset;
                    break;
                  }
                }
              });
            */
            e.target.absolutePosition(absPos);
    }
    ArrastrarFinal(e){
        this.setState({
            vLines: [],
            hLines: []
        })
       //layer.find('linea-guia').forEach((l) => l.destroy()); //Aqui debo tambien tomar la ventana, buscar las lineas dibujadas y eliminarlas para que no queden en el mapa.
    }
  Valor(e) {
    
    if(this.state.selectedPolygon.variables.tamanio !== null){
      this.state.selectedPolygon.variables.tamanio = e.target.value
    }
   
    this.setState({
      sliderT: e.target.value,
      
    });
  }
   
    render() {
        return (
            <div className="content-container">
                <TopNav app={this.props.app} nombre={this.state.nombreMenu} ruta={this.state.rutaMenu} />

                <div className="container">
                    <div className='main'>
                        <div className="row">
                            <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <div className="definitions-wrapper d-flex" id="wrapper">
                                    {this.state.selectedDefinicion ?
                                        this.renderMenu()
                                    :
                                        <div className="left-wrapper bg-light border-right" id="sidebar-wrapper">
                                            {this.state.estructura && this.state.structuresTreeNodes ?
                                                <Fragment>
                                                    {this.renderEstructuras()}

                                                    <div className="container divider2 mt-4 mb-4"/>

                                                    {this.renderDefiniciones()}
                                                </Fragment>
                                                :
                                                <Loader />
                                            }
                                        </div>
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

    renderEstructuras() {
        return (
            this.state.structuresTreeNodes ?
                <div className="structures-content">
                    <h2 className="global-title-2 mb-4">
                        Definiciones
                    </h2>
                    <div className="structures-list">
                        <CheckboxTree
                            nodes={this.state.structuresTreeNodes}
                            iconsClass="fa5"
                            checked={this.state.checkedStructure}
                            checkModel={false}
                            expanded={this.state.expandedStructure}
                            noCascade={true}
                            onClick={e => this.handleSelectStructureNode(e)}
                            onCheck={e => this.handleSelectCheckboxStructureNode(e)}
                            onExpand={expanded => this.setState({ expandedStructure: expanded })}
                            showNodeIcon={false}
                            showExpandAll={true}
                        />
                    </div>
                </div>
            :
                null
        )
    }

    renderDefiniciones() {
        return (
            this.state.selectedStructure ?
                <div className="definitions-content">
                    <div className="d-flex justify-content-between align-items-center w-100 mb-4 mt-4">
                        <h2 className="global-title-2">
                            Definiciones
                        </h2>
                        <Tooltip content={"Nueva asignación"} placement="left">
                            <button className="btn-round" onClick={() => this.handleOpenNewDefinitionModal()}>
                                <i className="fi fi-rs-plus"/>
                            </button>
                        </Tooltip>
                    </div>
                    <div className="definitions-list mt-6">
                        {this.state.selectedStructure.definiciones ?
                            <ul className="generic-u">
                                { this.state.selectedStructure.definiciones.map(definition => {
                                    return ( 
                                        <li className="navigation-li" key={definition.definicion}>
                                            <span className="rct-node-clickable" onClick={() => this.handleSelectDefinition(definition.asignacion)}> {definition.nombre} </span>
                                            
                                            <span className="icons">
                                                <Tooltip content={"Históricos"} placement="top">
                                                    <span onClick={() => this.handleOpenHistoricalModal()}>
                                                        <i className="historicals fi fi-rs-clock mr-2" />
                                                    </span>
                                                </Tooltip>
                                                {this.renderHistoricalModal(definition)}
                                                <Tooltip content={"Editar"} placement="top">
                                                    <span onClick={() => this.handleOpenEditDefinitionModal(definition)}>
                                                        <i className="edit fi fi-rs-pen-clip mr-2"/>
                                                    </span>
                                                </Tooltip>
                                                <Tooltip content={"Clonar"} placement="top">
                                                    <span onClick={() => this.handleOpenCloneDefinitionModal(definition)}>
                                                        <i className="clone fi fi-rs-copy-alt mr-2"/>
                                                    </span>
                                                </Tooltip>
                                                <Tooltip content={"Eliminar"} placement="top">
                                                    <span onClick={() => this.handleOpenDeleteDefinitionModal()}>
                                                        <i className="delete fi fi-rs-trash mr-2"/>
                                                    </span>
                                                </Tooltip>
                                                {this.renderDeleteDefinitionModal(definition)}
                                            </span>
                                        </li>
                                    )
                                })}
                            </ul>
                        :
                            null
                        }
                    </div>
                    <RightOverlay
                        atributos={this.state.atributos}
                        data={this.state.newDefinition}
                        errorMessages={this.state.errorMessagesNewDefinition}
                        handleState={(state) => this.setState({ stateNewDefinitionModal: state})}
                        handleSubmit={(atributos) => this.handleSaveNewDefinition(atributos)}
                        overlayShowHide={() => this.handleCloseNewDefinitionModal()}
                        overlayShow={this.state.showNewDefinitionModal}
                        state={this.state.stateNewDefinitionModal}
                    />
                    <RightOverlay
                        atributos={this.state.editDefinitionAtributos}
                        data={this.state.editDefinition}
                        errorMessages={this.state.errorMessagesEditDefinition}
                        handleState={(state) => this.setState({ stateEditDefinitionModal: state})}
                        handleSubmit={(atributos) => this.handleEditDefinition(atributos)}
                        overlayShowHide={() => this.handleCloseEditDefinitionModal()}
                        overlayShow={this.state.showEditDefinitionModal}
                        state={this.state.stateEditDefinitionModal}
                    />
                    <RightOverlay
                        atributos={this.state.atributos}
                        data={this.state.clonedDefinitionData}
                        errorMessages={this.state.errorMessagesCloneDefinition}
                        handleState={(state) => this.setState({ stateCloneDefinitionModal: state})}
                        handleSubmit={(atributos) => this.handleCloneDefinition(atributos)}
                        overlayShowHide={() => this.handleCloseCloneDefinitionModal()}
                        overlayShow={this.state.showCloneDefinitionModal}
                        state={this.state.stateCloneDefinitionModal}
                    />
                </div>
            :
                null
        )
    }

    renderHistoricalModal(definition) {
        return (
            this.state.showHistoricalModal &&
                <div className="custom-modal custom-modal-big">
                    <div className="custom-modal-content">
                        <div className="custom-modal-header">
                            <div className="row d-flex justify-content-between align-items-center">
                                <div>
                                    <h6> Histórico </h6>
                                </div>
                                <div>
                                    <button className="btn btn-close" onClick={() => this.handleCancelHistoricalModal()}>
                                        <i className="fi fi-rs-cross" />
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div className="right-body bg-white text-center">
                            <h6> {definition.nombre} </h6>

                            { definition.historicos ?
                                <select className="form-control" onChange={(e) => this.handleOnChangeHistorical(e)} value={this.state.historical}>
                                    <option value="">Selecciona una opción</option>
                                        { definition.historicos.map((item) => {
                                            return (
                                                <option
                                                    key={item.asignacion}
                                                    value={item.asignacion}
                                                >
                                                {item.fecha + '-' + item.hora + ' - ' + item.usuario}
                                                </option>
                                            )
                                        })}
                                </select>
                            :
                                <div>
                                    <h5> La definicion no posee históricos </h5>
                                </div>
                            }
                        </div>
                        <div className="right-footer d-flex justify-content-center align-items-stretch">
                            <button className="btn btn-secondary mr-2" onClick={() => this.handleCancelHistoricalModal()}>
                                Cancelar
                            </button>
                            <button className="btn btn-primary" disabled={this.state.historical === undefined} onClick={() => this.handleSubmitHistoricalModal()}>
                                Aceptar
                            </button>
                        </div>
                    </div>
                </div>
        )
    }

    renderDeleteDefinitionModal(definition) {
        return (
            this.state.showDeleteDefinitionModal &&
                <div className="custom-modal">
                    <div className="custom-modal-content">
                        <div className="custom-modal-header">
                            <div className="row d-flex justify-content-between align-items-center">
                                <div>
                                    <h6> Eliminar Definicion </h6>
                                </div>
                                <div>
                                    <button className="btn btn-close" onClick={() => this.handleCancelDeleteDefinitionModal()}>
                                        <i className="fi fi-rs-cross"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div className="right-body bg-white text-center">
                            <h6> ¿Estás seguro que deseas eliminarla? </h6>
                            <h6> {definition.nombre} </h6>
                        </div>
                        <div className="right-footer d-flex justify-content-center align-items-stretch">

                            <button className="btn btn-secondary mr-2" onClick={() => this.handleCancelDeleteDefinitionModal()}>
                                Cancelar
                            </button>
                            <button className="btn btn-primary" onClick={() => this.handleSubmitDeleteDefinitionModal(definition.definicion)}>
                                Aceptar
                            </button>
                        </div>
                    </div>
                </div>
        )
    }

    renderColisionModal() {
        return (
            this.state.showColisionModal &&
                <div className="custom-modal">
                    <div className="custom-modal-content">
                        <div className="custom-modal-header">
                            <div className="row d-flex justify-content-between align-items-center">
                                <div>
                                    <h6> Superposicion de Locales </h6>
                                </div>
                                <div>
                                    <button className="btn btn-close" onClick={() => this.handleCancelColisionModal()}>
                                        <i className="fi fi-rs-cross"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div className="right-body bg-white text-center">
                            <h6> La definicion que desea crear se superpone con otra existente... </h6>
                        </div>
                        <div className="right-footer d-flex justify-content-center align-items-stretch">

                            <button className="btn btn-secondary mr-2" onClick={() => this.handleCancelColisionModal()}>
                            Aceptar
                            </button>
                        </div>
                    </div>
                </div>
        )
    }
    renderSubmitModal(){
        return (
            this.state.showSuccessMessage &&
            <div className="custom-modal">
                <div className="custom-modal-content">
                    <div className="custom-modal-header">
                        <div className="row d-flex justify-content-between align-items-center">
                            <div>
                                <h6>Actualizacion Exitosa</h6>
                            </div>
                            <div>
                                <button className="btn btn-close" onClick={() => this.handleCancelSubmitModal()}>
                                    <i className="fi fi-rs-cross"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div className="right-body bg-white">
                        <div className="row mt-5">
                            <div className="col-lg-12 d-flex flex-column justify-content-center align-items-center">
                                <i className="right-icon-big fi fi-rs-social-network"></i>
                                <p>
                                <b>Se actualizó con éxito!</b>
                                </p>
                            </div>
                       </div>
                    </div>
                    <div className="right-footer d-flex justify-content-center align-items-stretch">

                        <button className="btn btn-secondary mr-2" onClick={() => this.handleCancelSubmitModal()}>
                        Aceptar
                        </button>
                    </div>
                </div>
            </div> 
                
        )
    }
    
    renderMenu() {
        return (
            this.state.selectedDefinicion ?
                <div id="page-content-wrapper" className="right-wrapper">
                    <nav className="navbar navbar-expand-lg navbar-light bg-light border-bottom pl-3 pr-3">
                        <button className="btn-round" onClick={() => this.handleCloseMenu()} id="menu-toggle">
                            <i className="fi fi-rs-cross"/>
                        </button>

                        <div className="navbar-menu"> 
                            <h2 className="global-title-2 m-0 pl-2">
                                {this.state.selectedDefinicion.nombre}
                            </h2>

                            <div className="actions">
                                {/* <Tooltip placement="bottom" className="global-link-1 ml-2 mr-2" content={"Deshacer"}>
                                    <i className="fas fa-undo"/>
                                    </Tooltip>
                                    <Tooltip placement="bottom" className="global-link-1 ml-2 mr-2" content={"Rehacer"}>
                                    <i className="fas fa-redo"/>
                                </Tooltip> */}
                                <button className="btn btn-primary" onClick={() => this.handleSaveDefinition()}>
                                    <i className="fi fi-rs-disk mr-2"/> Guardar
                                </button>
                            </div>
                        </div>
                    </nav>
                    {this.renderSubmitModal()}
                    {this.renderSaveModal()}
                    {this.renderMaps()}
                    {this.state.showAvaliablesModal &&
                        <AvaliablesModal
                            atributos={this.state.atributosDisponibles}
                            devoluciones={this.state.devolucionesDisponibles}
                            disponibles={this.state.dataDisponibles}
                            handleClose={() => this.handleCloseAvaliablesModal()}
                            handleSubmit={(atributos) => this.handleSubmitAvaliablesModal(atributos)}
                            selectedPolygon={this.state.selectedPolygon}
                            title={this.state.nombreDisponibles}
                        />
                    }   
                </div>
            :
                null
        )
    }

    renderSaveModal() {
        return (
            this.state.showSaveModal &&
                <div className="custom-modal">
                    <div className="custom-modal-content">
                        <div className="custom-modal-header">
                            <div className="row d-flex justify-content-between align-items-center">
                                <div>
                                    <h6> Hay datos sin guardar </h6>
                                </div>
                                <div>
                                    <button className="btn btn-close" onClick={() => this.handleCancelSaveModal()}>
                                        <i className="fi fi-rs-cross"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div className="right-body bg-white text-center">
                            <h6> ¿Estás seguro que deseas eliminarlos? </h6>
                        </div>
                        <div className="right-footer d-flex justify-content-center align-items-stretch">

                            <button className="btn btn-secondary mr-2" onClick={() => this.handleCancelSaveModal()}>
                                Cancelar
                            </button>
                            <button className="btn btn-primary" onClick={() => this.handleSubmitSaveModal()}>
                                Aceptar
                            </button>
                        </div>
                    </div>
                </div>
        )
    }

    renderMaps() {
    const slider = document.getElementById("myRange");
    const output = document.getElementById("demo");
        
        return (
            this.state.selectedDefinicion ?
                <div className="container-stage" id='container-stage' tabIndex={1} onKeyDown={(e) => this.handleKeyDown(e)}>
                    <Stage
                        className="konvas-stage"
                        container='container-stage'
                        id ='Konvas-stage'
                        width={1280}
                        height={720}
                        // onContentWheel={(e) => this.handleStageZoom(e)}
                        onContentMousedown={(e) => this.handleStageContentMousedown(e)}
                        onContentMousemove={(e) => this.handleStageContentMousemove(e)}
                    >
                        <Layer id="layer-background" className="konvas-layer background">
                            <Konva.Image
                                x={0}
                                y={0}
                                image={this.state.selectedDefinicion.background}
                                width={1280}
                                height={720}
                            />
                        </Layer>
                        <Layer id="layer-polygons" className="konvas-layer polygons" onDragMove = {(e) => this.Arrastrar(e)} onDragEnd = {(e) => this.ArrastrarFinal(e)}>
                            {
                                
                                this.state.selectedDefinicion.elementos &&
                                this.state.selectedDefinicion.elementos.map((item, key) => {
                                    return (
                                       item.variables.forma == "C" ? 
                                      <Circle
                                      key={key}
                                      id={item.id}
                                      name={'object'}
                                      radius={item.variables.tamanio}
                                      x={parseInt(item.variables.x)}
                                      y={parseInt(item.variables.y)}
                                      closed
                                      stroke={item.variables.stroke}
                                      strokeWidth={1}
                                      fill={item.variables.fill}
                                      draggable = {true}
                                      // draggable={!this.state.drawMode}
                                      onMouseEnter={(e) => this.handlePolygonEnter(e)}
                                      onMouseLeave={(e) => this.handlePolygonLeave(e)}
                                      onClick={(e) => this.handlePolygonClick(e)}
                                      onDragEnd={(e) => this.handlePolygonDragEnd(e)}
                                      />
                                      : 
                                     
                                      item.variables.forma == "E" ? 
                                        <Star
                                        key={key}
                                        id={item.id}
                                        name={'object'}
                    innerRadius={item.variables.tamanio /2} //10
                    outerRadius={item.variables.tamanio} //20
                                        x={parseInt(item.variables.x)}
                                        y={parseInt(item.variables.y)}
                                        closed
                                        stroke={item.variables.stroke}
                                        strokeWidth={1}
                                        fill={item.variables.fill}
                                        draggable = {true}
                                        onMouseEnter={(e) => this.handlePolygonEnter(e)}
                                        onMouseLeave={(e) => this.handlePolygonLeave(e)}
                                        onClick={(e) => this.handlePolygonClick(e)}
                                        onDragEnd={(e) => this.handlePolygonDragEnd(e)}
                                         />
                                        : 
                                        item.variables.forma == "R" ? 

                                        <Rect
                                        key={key}
                                        id={item.id}
                                        name={'object'}
                    width={item.variables.tamanio} //30
                    height={item.variables.tamanio} //30
                                        x={parseInt(item.variables.x)}
                                        y={parseInt(item.variables.y)}
                                        closed
                                        stroke={item.variables.stroke}
                                        strokeWidth={1}
                                        fill={item.variables.fill}
                                        draggable = {true}
                                        onMouseEnter={(e) => this.handlePolygonEnter(e)}
                                        onMouseLeave={(e) => this.handlePolygonLeave(e)}
                                        onClick={(e) => this.handlePolygonClick(e)}
                                        onDragEnd={(e) => this.handlePolygonDragEnd(e)}
                                        />
                                        :
                                        <></>
                                       
                                    )
                                })
                                

                                
                            }
                            {
                                this.state.isDragging ? 
                                this.state.hLines.map((item, i) => {
                                    return(
                                        <Line key={i} {...item}/>
                                    )
                                })
                                
                                : 
                                <></>
                            }
                            {
                                this.state.isDragging ? 
                                this.state.vLines.map((item, i) => {
                                    return(
                                        <Line key={i} {...item}/>
                                    )
                                })
                                : 
                                <></>
                            }
                            { this.state.overPolygon &&
                                <Label
                                    x={parseInt(this.state.overPolygon.center.x)}
                                    y={parseInt(this.state.overPolygon.center.y)}
                                >
                                    <Tag
                                        fill='black'
                                        pointerDirection={this.state.overPolygon.position}
                                        pointerWidth={5}
                                        pointerHeight={5}
                                        lineJoin='round'
                                    />
                                    <Text
                                        text={this.state.overPolygon.descripcion}
                                        fontSize={15}
                                        padding={10}
                                        fill='white'
                                    />
                                </Label>
                            }
                        </Layer>
                        {/* The layer where the new polygon is being drawn */}
                        {this.state.drawMode && this.state.newPolygon && this.state.currentPoints &&
                            
                            <Layer id="layer-new-polygon">
                               { this.state.typesShapes == "C" ? 
                                     <Circle
                                     id={"new-polygon"}
                                     name={"new-polygon"}
                    radius={this.state.newPolygon.variables.tamanio} //20
                                     x={parseInt(this.state.newPolygon.variables.x)}
                                     y={parseInt(this.state.newPolygon.variables.y)}
                                     fill={this.state.newPolygon.variables.fill}
                                     strokeWidth={1}
                                     stroke={"black"}
                                     />
                                     :
                                     <></>
                               
                                }
                                {this.state.typesShapes == "E" ?
                                     <Star
                                     id={"new-polygon"}
                                     name={"new-polygon"}
                    innerRadius={this.state.newPolygon.variables.tamanio / 2} //10
                    outerRadius={this.state.newPolygon.variables.tamanio} //20
                                     x={parseInt(this.state.newPolygon.variables.x)}
                                     y={parseInt(this.state.newPolygon.variables.y)}
                                     fill={this.state.newPolygon.variables.fill}
                                     strokeWidth={1}
                                     stroke={"black"}
                                     />
                                    :
                                    <></>
                                }
                                {this.state.typesShapes == "R" ?
                                     <Rect
                                     id={"new-polygon"}
                                     name={"new-polygon"}
                    width={this.state.newPolygon.variables.tamanio} //30
                    height={this.state.newPolygon.variables.tamanio} //30
                                     x={parseInt(this.state.newPolygon.variables.x)}
                                     y={parseInt(this.state.newPolygon.variables.y)}
                                     fill={this.state.newPolygon.variables.fill}
                                     strokeWidth={1}
                                     stroke={"black"}
                                     />
                                     :
                                     <></>
                                }

                                 
                               
                            </Layer>
                        }
                    </Stage>
                    <div className="button-group-float">
                        {/* <div className="button-group-float-zoom">
                            <button className="btn-zoom">
                                <i className="fi fi-rs-search-plus"/>
                            </button>
                            <button className="btn-zoom">
                                <i className="fi fi-rs-search-minus"/>
                            </button>
                        </div> */}

                        <div className="button-group-float-polygon">
            {this.state.range === true  && this.state.selectedPolygon && this.state.selectedPolygon.variables.tamanio !== null ? (
              <div className="slider-container">
                <input
                  className="slider"
                  type="range"
                  min={1}
                  max={100}
                  defaultValue={this.state.selectedPolygon.variables.tamanio}
                  id="myRange"
                  style={{ width: "100%" }}
                  onChange={(e) => this.Valor(e)}
                />
                <p className="parrafo">Valor: {this.state.sliderT}</p>
            
              </div>
             ) : (
                null
            )}

                            { this.state.selectedPolygon &&
                                <div id="btn-draw" className="polygon-actions">
                                    {/* <span className="button drag" onClick={() => {console.log("drag")}}>
                                        <i className="fas fa-arrows-alt"/>
                                    </span> */}
                                    <span className="button edit" onClick={() => this.handlePolygonEdit()}>
                                        <i className="fi fi-rs-pen-clip"/>
                                    </span>
                                    <span className="button delete" onClick={() => this.handlePolygonDelete()}>
                                        <i className="fi fi-rs-trash"/>
                                    </span>
                <span
                  className="button edit"
                  onClick={() => this.CambiarTamaño()}
                >
                  <i className="fi-rs-expand-arrows-alt" />
                </span>

                                </div>
                            }
                            <div className="polygon-draw-actions">
                            { this.state.drawMode && this.state.drawfigures &&
                                    <div className="draw-actions">
                                        <span className={this.state.typesShapes == "R" ? "button cancel" : "button disabled"}  onClick={() => this.handlefiguras("R")}>
                                            <i className="fi fi-rs-square"></i>
                                        </span>

                                        <span className={this.state.typesShapes  == "E" ? "button cancel" : "button disabled"}  onClick={() => this.handlefiguras("E")}>
                                            <i className="fi fi-rs-star"></i>
                                        </span>  

                                        <span className={this.state.typesShapes == "C" ? "button cancel" : "button disabled"}  onClick={() => this.handlefiguras("C")}>
                                            <i className="fi fi-rs-circle"></i>
                                        </span>
                                    </div>
                           }
                                <span id="btn-draw" className="button draw"  onClick={() => this.handleDrawMode()}>
                                    <i className="fi fi-rs-draw-polygon"/>
                                </span>
                            </div>
                        </div>
                    </div>
                    {this.renderColisionModal()}
                </div>
            :
                <Loader />
        )
    }
}

export default Definiciones

class AvaliablesModal extends Component {
    constructor(props) {
        super(props)
        this.state = {
            value: null,
            columns: null,
            selectRowId: []
        };
    }

    componentDidMount() {
        if (this.props.selectedPolygon){
            const atributos = {}
            Object.entries(this.props.devoluciones).forEach((item) => {
                atributos[item[0]]= this.props.disponibles[item[1]]
            })

            this.setState({
                selectedRowId: parseInt(this.props.selectedPolygon.referencia),
                atributos: atributos
            })
        }

        this.configColumns()
    }
    
    configColumns(){
        const columns = []

        this.props.atributos.forEach(art => {

            columns.push({
                id: art.id,
                dataField: art.id,
                text: art.descripcion,
                sort: true,
                align: art.alineacion,
                headerAlign: "center",
                hidden: art.visible === "N",
                headerStyle: { width: art.porcentaje + "%", wordBreak: "break-word" },
                style: { width: art.porcentaje + "%", wordBreak: "break-word" },
                tipologia: art.tipologia,
                visible: art.visible
            })
        })
        this.setState({
            columns: columns
        })
    }

    handleOnSelect(row) {
        const atributos = {}
        
        if (row.id === this.state.selectedRowId){
            this.setState({
                selectedRowId: null,
                atributos: atributos
            })
        } else {
            Object.entries(this.props.devoluciones).forEach((item) => {
                atributos[item[0]]= row[item[1]]
            })

            this.setState({
                selectedRowId: row.id,
                atributos: atributos
            })
        }
    }

    handleClose() {
        this.props.handleClose()
        this.setState({
            selectedRowId: null,
            atributos: null
        })
    }

    handleSubmit(){
        this.props.handleSubmit(this.state.atributos)
        this.setState({
            selectedRowId: null,
            atributos: null
        })
    }

    renderButtons() {
        return (
            <div className="right-footer d-flex justify-content-center align-items-stretch">
                <button className="btn btn-secondary mr-2" onClick={() => this.handleClose()}>
                    <span> Cancelar </span>
                </button>
                <button className="btn btn-primary" disabled={!this.state.selectedRowId} onClick={() => this.handleSubmit()}>
                    <span> Seleccionar </span>
                </button>
            </div>
        )
    }

    render() {
        const selectRow = {
            mode: 'checkbox',
            clickToSelect: true,
            style: { backgroundColor: '#FF7F32', color: '#fff' },
            selected: [this.state.selectedRowId],
            onSelect: (row) => this.handleOnSelect(row)
        }

        return (
            <div className="right-overlay">
                <div className="right-content right-content-big bg-white shadow-lg">
                    <div className="right-header">
                        <div className="row d-flex justify-content-between align-items-center">
                            <div>
                                <h6>{this.props.title}</h6>
                            </div>
                            <div>
                                <button className="btn btn-close" onClick={() => this.props.handleClose()}>
                                    <i className="fi fi-rs-cross"/>
                                </button>
                            </div>
                        </div>
                    </div>

                    <div className="right-body bg-white">
                        <div className="row">
                            <div className="col-lg-12">
                                { this.state.columns && this.props.disponibles &&
                                    <div className="col-12 mt-4">
                                        <BootstrapTable
                                            keyField="id"
                                            data={this.props.disponibles}
                                            columns={this.state.columns}
                                            striped
                                            hover
                                            headerClasses="header-class"
                                            rowStyle={{ fontSize: '14px' }}
                                            selectRow={selectRow}
                                        />
                                    </div>
                                }
                            </div>
                        </div>
                    </div>
                    {this.renderButtons()}
                </div>
            </div>
        )
    }
}
