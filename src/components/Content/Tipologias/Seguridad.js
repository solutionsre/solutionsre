// Generic
import React, { Component } from "react";
import BootstrapTable from "react-bootstrap-table-next";
import ToolkitProvider from "react-bootstrap-table2-toolkit";
import paginationFactory from "react-bootstrap-table2-paginator";
import { connect } from "react-redux";

// Components
import store from "../../../redux/store";
import URLS from "../../../urls";
import TopNav from "../TopNav";
import Filtros from "../Filtros";
import Loader from "../../GlobalComponents/Loader";

// Css
import "../Content.css";

class Seguridad extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      columnsUsers: [],
      rowsUsers: [],
      selectedUser: [],
      selectedUserId: [],
    };
  }

  componentDidUpdate(prevProps) {
    if (this.props.id !== prevProps.id) {
      this.setState({
        loading: true,
        columnsUsers: [],
        rowsUsers: [],
        selectedUser: [],
        selectedUserId: [],
      });
      this.fetchConfig();
    }
  }

  componentWillMount() {
    const urls = {
      configuracion: URLS.SEGURIDAD_CONFIGURACION,
      obtenerInformacion: URLS.OBTENER_INFORMACION,
      obtenerMenu: URLS.SEGURIDAD_OBTENER_MENU,
      agregar: URLS.SEGURIDAD_AGREGAR,
      editar: URLS.SEGURIDAD_MODIFICAR,
      eliminar: URLS.SEGURIDAD_ELMINAR,
      importar: URLS.SEGURIDAD_IMPORTAR,
      modelo: URLS.SEGURIDAD_MODELO,
      descargar: URLS.SEGURIDAD_DESCARGAR,
    };

    this.setState({
      urls: urls,
    });

    this.fetchConfig();
  }

  componentWillReceiveProps() {
    this.setState({
      selectedUser: [],
      selectedUserId: [],
    });
    this.fetchDataUsers();
  }

  fetchConfig() {
    if (this.props.id === "") {
      return false;
    } else {
      var myHeaders = new Headers();
      myHeaders.append(
        "Authorization",
        "Bearer " + sessionStorage.getItem("token"),
      );
      myHeaders.append("Content-Type", "application/json");

      var requestOptions = {
        method: "GET",
        headers: myHeaders,
        redirect: "follow",
      };
      fetch(
        store.getState().serverUrl +
          URLS.SEGURIDAD_CONFIGURACION +
          this.props.id,
        requestOptions,
      )
        .then((response) => {
          if (response.status == 401) {
            sessionStorage.clear();
            window.location.reload(false);
          }else{
            return response.json();
          }
        })
        .then((result) => {
         // console.log(result);
          if (result.componentes) {
            this.setState({
              acciones: result.componentes.acciones,
              atributos: result.componentes.atributos,
              componentId: result.componentes.id,
              opcionId: result.componentes.opcion,
              descripcion: result.componentes.descripcion,
              habilitado: result.componentes.habilitado,
              objectSelectedId: result.componentes.atributos[0].id,
              observaciones: result.componentes.observaciones,
              nombre: result.componentes.nombre,
              nombreMenu: result.componentes.menu,
              rutaMenu: result.componentes.ruta,
              sectores: result.componentes.sectores,
              tipologia: result.componentes.tipologia,
              visualizacion: result.componentes.visualizacion,
            });
            this.fetchConfigUsers();
            this.fetchDataUsers();
          }
        })
        .catch((error) => console.log("error", error));
    }
  }

  fetchConfigUsers() {
    const columns = [];
    this.state.visualizacion.atributos.forEach((art) => {
      columns.push({
        align: art.alineacion,
        dataField: art.id,
        headerAlign: "center",
        headerStyle: { width: art.porcentaje + "%", wordBreak: "brek-word" },
        hidden: art.visible !== "S",
        id: art.id,
        sort: true,
        style: { width: art.porcentaje + "%", wordBreak: "break-word" },
        text: art.descripcion,
        uniqueData: art.id,
      });
    });

    this.setState({
      columnsUsers: columns,
    });
  }

  fetchDataUsers() {
    var myHeaders = new Headers();
    myHeaders.append(
      "Authorization",
      "Bearer " + sessionStorage.getItem("token"),
    );
    myHeaders.append("Content-Type", "application/json");

    var raw = JSON.stringify({
      idObjeto: parseInt(this.state.visualizacion.id),
      filtros: {
        generales: [
          {
            atributo: parseInt(this.state.visualizacion.atributos[1].id),
            contenido: "1",
          },
        ],
      },
    });

    var requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: raw,
      redirect: "follow",
    };

    fetch(
      store.getState().serverUrl + URLS.OBTENER_INFORMACION,
      requestOptions,
    )
      .then((response) => {
        if (response.status == 401) {
          sessionStorage.clear();
          window.location.reload(false);
        }else{
          return response.json();
        }
      })
     .then((result) => {
        this.setState({
          rowsUsers: result,
          loading: false,
        });
      })
      .catch((error) => console.log("error", error));
  }

  handleOnSelectUser = (row) => {
    if (this.state.selectedUserId && this.state.selectedUserId[0] === row.id) {
      this.setState({
        selectedUser: [],
        selectedUserId: [],
      });
    } else {
      this.setState({
        selectedUser: [row],
        selectedUserId: [row.id],
      });
    }
  };

  render() {
    return (
      <div id="sectores" className="content-container">
        <TopNav
          app={this.props.app}
          nombre={this.state.nombreMenu}
          ruta={this.state.rutaMenu}
        />
        {this.state.loading ? (
          <div className="container col-12 mt-4">
            <Loader />
          </div>
        ) : (
          this.renderUsers()
        )}
      </div>
    );
  }

  renderUsers() {
    const selectRowUser = {
      mode: "checkbox",
      clickToSelect: true,
      style: { backgroundColor: "#FF7F32", color: "#fff" },
      hideSelectAll: true,
      selected: this.state.selectedUserId,
      onSelect: this.handleOnSelectUser,
    };

    const MySearch = (props) => {
      let input;
      const handleClick = () => {
        props.onSearch(input.value);
      };
      return (
        <div className="search-group search-group-300 mr-2" key={4898794654654}>
          <input
            className="form-control input-search"
            ref={(n) => (input = n)}
            type="text"
            value={props.searchText}
            onChange={handleClick}
          />
          <button className="button-search">
            <i className="fi fi-rs-search"></i>
          </button>
        </div>
      );
    };

    const optionsPagination = {
      hideSizePerPage: true, // Hide the sizePerPage dropdown always
      showTotal: false,
      disablePageTitle: false,
    };

    const rowStyle = { fontSize: "14px" };

    return this.state.columnsUsers.length ? (
      <ToolkitProvider
        keyField="id"
        data={this.state.rowsUsers}
        columns={this.state.columnsUsers}
        search={<MySearch />}
      >
        {(props) => (
          <div className="main">
            <Filtros
              search={
                <MySearch
                  className="input-search"
                  placeholder="Buscar"
                  {...props.searchProps}
                />
              }
              acciones={this.state.acciones}
              atributos={this.state.atributos}
              menu="seguridad"
              objectId={this.state.componentId}
              opcionId={this.state.opcionId}
              objectSelectedId={this.state.objectSelectedId}
              sectores={this.state.sectores}
              selectedRows={this.state.selectedUser}
              showSectores={false}
              urls={this.state.urls}
              visualizacion={this.state.visualizacion}
              showButtonAdd={true}
              size="big"
              edicion={this.state.edicion}
            />
            <div className="container">
              <div className="row">
                <div className="col-12 mt-4">
                  <BootstrapTable
                    key={"456456465461231231234"}
                    striped
                    hover
                    headerClasses="header-class"
                    rowStyle={rowStyle}
                    selectRow={selectRowUser}
                    pagination={paginationFactory(optionsPagination)}
                    {...props.baseProps}
                  />
                </div>
              </div>
            </div>
          </div>
        )}
      </ToolkitProvider>
    ) : null;
  }
}

const mapStateToProps = (state) => ({
  rerender: state.rerender,
});

export default connect(mapStateToProps)(Seguridad);
