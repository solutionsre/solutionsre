// Generic
import React, { Component } from "react";
import store from "../../../redux/store";
import BootstrapTable from "react-bootstrap-table-next";
import ToolkitProvider from "react-bootstrap-table2-toolkit";
import paginationFactory from "react-bootstrap-table2-paginator";
import { connect } from "react-redux";
import FullCalendar from "@fullcalendar/react";
import dayGridPlugin from "@fullcalendar/daygrid";
import interactionPlugin from "@fullcalendar/interaction"; // needed for dayClick
import { scroller } from "react-scroll";

// Components
import URLS from "../../../urls";
import TopNav from "../TopNav";
import Filtros from "../Filtros";
import Loader from "../../GlobalComponents/Loader";
import AlertForm from "../../GlobalComponents/AlertForm";

// Css
import "../Content.css";

class Registraciones extends Component {
  constructor(props) {
    super(props);

    this.theCalendar = React.createRef();

    this.state = {
      loading: true,
      loadingCalendarSearch: false,
      calendarLang: "en",
      calendarFocus: new Date(),
      calendarStart: null,
      calendarEnd: null,
      columnsSector1: [],
      columnsSector3: [],
      event: null,
      events: [],
      totales: [],
      relacionados: [],
      rowsSection1: [],
      rowsSection3: [],
      rowSelectionSection1: null,
      rowIdSelectionSection1: [],
      rowSelectionSection3: [],
      rowIdSelectionSection3: [],
      width: 0, 
      height: 0,
      calendarioCompleto: true,
      tituloMes : "",
      totalOperaciones : "",
      totalImporte: "",
    };
  }

  componentDidMount() {
    this.setState({ width: window.innerWidth, height: window.innerHeight });
    this.ponerTituloMesCalendario(this.state.calendarFocus);
  }
  
  componentDidUpdate(prevProps, prevState) {
    if (this.props.id !== prevProps.id) {
      this.setState({
        loading: true,
        calendarFocus: new Date(),
        calendarStart: null,
        calendarEnd: null,
        event: null,
        columnsSector1: [],
        columnsSector3: [],
        rowsSection1: [],
        rowsSection3: [],
        rowSelectionSection1: null,
        rowIdSelectionSection1: [],
        rowSelectionSection3: [],
        rowIdSelectionSection3: [],
        errorPantalla: false,
      });
      this.fetchConfig();
    }
    if (prevState.event !== this.state.event) {
      this.setState({
        rowSelectionSection3: [],
      });
    }
    if (
      prevState.rowSelectionSection3 !== this.state.rowSelectionSection3 &&
      !this.state.rowSelectionSection3
    ) {
      this.setState({
        rowIdSelectionSection3: [],
      });
    }
    if (this.state.sectores) {
      const sectores = this.state.sectores;
      if (prevState.event) {
        sectores[2]["disabled"] = false;
      } else {
        sectores[2]["disabled"] = true;
      }
    }
  }

  componentWillMount() {
    const urls = {
      agregar: URLS.REGISTRACIONES_AGREGAR,
      editar: URLS.REGISTRACIONES_MODIFICAR,
      eliminar: URLS.REGISTRACIONES_ELMINAR,
      importar: URLS.REGISTRACIONES_IMPORTAR,
      modelo: URLS.REGISTRACIONES_MODELO,
      visualizar: URLS.REGISTRACIONES_VISUALIZAR,
      descargar: URLS.REGISTRACIONES_DESCARGAR,
    };

    this.setState({
      urls: urls,
    });

    this.fetchConfig();
  }

  componentWillReceiveProps() {
    
    if (this.state.rowSelectionSection1) {
      //this.handleCalendar(this.state.rowSelectionSection1);
      this.fetchCalendarData(this.state.rowSelectionSection1);
    }
    
    if (this.state.event) {
      this.fetchSection3Data();
    }
    this.setState({
      rowSelectionSection3: null,
      rowIdSelectionSection3: [],
    });
  }

  fetchConfig() {
    var myHeaders = new Headers();
    myHeaders.append(
      "Authorization",
      "Bearer " + sessionStorage.getItem("token"),
    );
    myHeaders.append("Content-Type", "application/json");

    var requestOptions = {
      method: "GET",
      headers: myHeaders,
      redirect: "follow",
    };
    fetch(
      store.getState().serverUrl +
        URLS.REGISTRACIONES_CONFIGURACION +
        this.props.id,
      requestOptions,
    )
      .then((response) => {
        if (response.status == 401) {
          sessionStorage.clear();
          window.location.reload(false);
        }else{
          return response.json();
        }
      })
      .then((result) => {
        if (result.Mensajes === undefined) {
          console.log("conf", result);
          this.setState({
            componentId: result.componentes.sectores[2].id,
            acciones: result.componentes.acciones,
            descripcion: result.componentes.descripcion,
            habilitado: result.componentes.habilitado,
            nombre: result.componentes.nombre,
            observaciones: result.componentes.observaciones,
            sectores: result.componentes.sectores,
            opcion: result.componentes.opcion,
            tipologia: result.componentes.tipologia,
            nombreMenu: result.componentes.menu,
            rutaMenu: result.componentes.ruta,
            tableCalendar:
              result.componentes.sectores[0].visualizacion.atributos,
          });
          this.fetchSection1Config();
          this.fetchCalendarConfig();
          this.fetchSection3Config();
          this.fetchSection1Data();
        } else {
          this.setState({
            errorPantalla: true,
            mensajesErrorPantalla: result.Mensajes,
            loading: false,
          });
        }
      })
      .catch((error) => console.log("error", error));
  }

  fetchSection1Config() {
    const columns = [];
    
    this.state.sectores[0].visualizacion.atributos.forEach((art) => {
      
      columns.push({
        align: art.alineacion,
        dataField: art.id,
        headerAlign: "center",
        headerStyle: { width: art.porcentaje + "%" },
        hidden: art.visible !== "S",
        id: art.id,
        sort: true,
        style: { width: art.porcentaje + "%" },
        text: art.descripcion,
        uniqueData: art.id,
      });
    });

    const relacionadoSector1 = {
      objeto: this.state.sectores[0].id,
      atributo: this.state.sectores[0].visualizacion.atributos[0].id,
      valor: null,
    };


    this.setState(({ relacionados }) => ({
      columnsSector1: columns,
      relacionados: [...relacionados, relacionadoSector1],
    }));
  }

  fetchCalendarConfig() {
    var mostrar = true;
    
    if (this.state.sectores[1].configuraciones[0] && this.state.sectores[1].configuraciones[0].CalendarioCompleto != undefined)
      mostrar = this.state.sectores[1].configuraciones[0].CalendarioCompleto;

    this.setState({
      calendarLang: JSON.parse(sessionStorage.getItem("defaultLanguage")).localizacion,
      calendarioCompleto: mostrar, 
    });
  }

  fetchSection3Config() {
    function columnFormatter(cell, row, rowIndex, formatExtraData) {
      var result = cell;
      var numeral = require("numeral");

      if (
        result &&
        formatExtraData.type === "NUMERICO" &&
        formatExtraData.mask
      ) {
        if (formatExtraData.decimals) {
          let decimales = "";

          for (var i = 0; i < formatExtraData.decimals; i++) {
            decimales = decimales + "0";
          }

          result = numeral(result).format("0,0." + decimales);
        }
      }

      return <span> {result} </span>;
    }

    const columns = [];

    this.state.sectores[2].visualizacion.atributos.forEach((art) => {
      const formatData = {
        type: art.tipo,
        mask: art.mascara,
        decimals: parseInt(art.longitud.split(",")[1]),
      };

      columns.push({
        align: art.alineacion,
        dataField: art.id,
        formatter: columnFormatter,
        formatExtraData: formatData,
        headerAlign: "center",
        headerStyle: { width: art.porcentaje + "%" },
        hidden: art.visible !== "S",
        id: art.id,
        sort: true,
        style: { width: art.porcentaje + "%" },
        text: art.descripcion,
        uniqueData: art.id,
      });
    });

    this.setState({
      columnsSector3: columns,
      objectSelectedId: this.state.sectores[2].atributos[0].id,
      loading: false,
    });
  }

  fetchSection1Data() {
    var myHeaders = new Headers();
    myHeaders.append("Authorization", "Bearer " + sessionStorage.getItem("token"));
    myHeaders.append("Content-Type", "application/json");

    var raw = JSON.stringify({
      idObjeto: parseInt(this.state.sectores[0].id),
      idOpcion: parseInt(this.props.id),
    });

    var requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: raw,
      redirect: "follow",
    };

    fetch(
      store.getState().serverUrl + URLS.OBTENER_INFORMACION,
      requestOptions,
    )
      .then((response) => {
        if (response.status == 401) {
          sessionStorage.clear();
          window.location.reload(false);
        }else{
          return response.json();
        }
      })
      .then((result) => {
        var tempRows = [];
        for (let value of Object.values(result)) {
          tempRows.push(value);
        }

        this.setState({
          rowsSection1: tempRows,
        });
      })
      .catch((error) => console.log("error", error));
  }

  fetchCalendarData(row) {
    var myHeaders = new Headers();
    
    const formatter = new Intl.NumberFormat(JSON.parse(sessionStorage.getItem("defaultLanguage")).localizacion, {
      style: "currency",
      currency: (row[2001002060] === "" ? JSON.parse(sessionStorage.getItem("defaultLanguage")).divisa : row[2001002060]),
      minimumFractionDigits: 0,
    });

    myHeaders.append("Authorization", "Bearer " + sessionStorage.getItem("token"));
    myHeaders.append("Content-Type", "application/json");

    const relacionado = [];

    this.state.sectores[1].visualizacion.relacionados.forEach((element) => {
      relacionado.push({
        atributo: element.relacionado,
        contenido: row[element.atributo],
      });
    });

    var raw = JSON.stringify({
      idObjeto: parseInt(this.state.sectores[1].id),
      idOpcion: parseInt(this.props.id),
      filtros: {
        generales: [relacionado],
      },
    });

    var requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: raw,
      redirect: "follow",
    };

    fetch(
      store.getState().serverUrl + URLS.OBTENER_INFORMACION,
      requestOptions,
    )
      .then((response) => {
        if (response.status == 401) {
          sessionStorage.clear();
          window.location.reload(false);
        }else{
          return response.json();
        }
      })
      .then((result) => {
        const events = [];
        const totales = [];
        const eventsData = this.state.sectores[1].visualizacion.atributos.map(
          (atr) => atr.id,
        );

        result.forEach((item) => {
          const date = new Date(
            item[eventsData[1]].split("/").reverse().join("/"),
          );
          var backgroundColor = null;

          if (item[eventsData[3]] === 0) {
            backgroundColor = "#f4a002";
          } else {
            backgroundColor = "#1ABD5B";
          }
          var mesTotal = date.getFullYear() + "_" + date.getMonth(); 
          var totalMes = totales[totales.findIndex((x) => x.id === mesTotal)];
          if (!totalMes) {
            
            totales.push({
              id: mesTotal,
              operaciones: item[eventsData[4]],
              importes: formatter.format(item[eventsData[5]]),
            });
          }


          events.push({
            id: item[eventsData[1]],
            start: date,
            display: "auto",
            backgroundColor: backgroundColor,
            borderColor: backgroundColor,
            int_registrante: item[eventsData[0]],
            operaciones: item[eventsData[2]],
            importes: item[eventsData[3]],
            objeto: item,
          });
        });

        this.setState({
          events: events,
          totales: totales,
        });
        this.ponerTituloMesCalendario(this.theCalendar.current.getApi().getDate());

      })
      .catch((error) => console.log("error", error));
  }

  fetchSection3Data() {
    var myHeaders = new Headers();
    myHeaders.append("Authorization", "Bearer " + sessionStorage.getItem("token"));
    myHeaders.append("Content-Type", "application/json");

    const relacionado = [];

    this.state.sectores[2].visualizacion.relacionados.forEach((element) => {
      relacionado.push({
        atributo: parseInt(element.relacionado),
        contenido: this.state[element.objeto][element.atributo],
      });
    });

    var raw = JSON.stringify({
      idObjeto: parseInt(this.state.sectores[2].id),
      idOpcion: parseInt(this.props.id),
      filtros: {
        generales: relacionado,
      },
    });

    var requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: raw,
      redirect: "follow",
    };

    fetch(
      store.getState().serverUrl + URLS.OBTENER_INFORMACION,
      requestOptions,
    )
      .then((response) => {
        if (response.status == 401) {
          sessionStorage.clear();
          window.location.reload(false);
        }else{
          return response.json();
        }
      })
      .then((result) => {
        this.setState({
          rowsSection3: result,
          loadingCalendarSearch: true
        });
      })
      .catch((error) => console.log("error", error));
  }

  handleOnSelectSection1 = (row) => {
    const relacionados = this.state.relacionados;
    relacionados[0]["valor"] = row.id;
    const relacionadoSector2 = {
      objeto: this.state.sectores[1].id,
      atributo: this.state.sectores[1].visualizacion.atributos[1].id,
      valor: null,
    };

    
    console.log("info", row.id);
    this.setState({
      [this.state.sectores[0].id]: row,
      rowSelectionSection1: row,
      rowIdSelectionSection1: [row.id],
      relacionados: [relacionados[0], relacionadoSector2],
      event: null,
      rowsSection3: [],
    });

    this.handleCalendar(row);
    

    scroller.scrollTo("sector2001003", {
      containerId: "sectores",
      spy: true,
      smooth: true,
      offset: 100,
      duration: 1000,
    });
  };

  ponerTituloMesCalendario (fecha) {

    const meses = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];

    var mes = meses[fecha.getMonth()];
    var anio = fecha.getFullYear();
    var titulo = mes + " de " + anio;
    var mesTotal = fecha.getFullYear() + "_" + fecha.getMonth(); 
    var totalMes = this.state.totales[this.state.totales.findIndex((x) => x.id === mesTotal)];
    var totalOperaciones = "";
    var totalImporte = "";
    if (totalMes){
      totalOperaciones = totalMes.operaciones;
      totalImporte = totalMes.importes;
    }

    this.setState({
      tituloMes : titulo,
      totalOperaciones : totalOperaciones,
      totalImporte: totalImporte,
    });

  };

  handleMesAnterior() {
    this.theCalendar.current.getApi().prev();
    this.ponerTituloMesCalendario(this.theCalendar.current.getApi().getDate());
    this.setState({
      calendarFocus : this.theCalendar.current.getApi().getDate(),
    });
  }

  handleMesSiguiente() {
    this.theCalendar.current.getApi().next();
    this.ponerTituloMesCalendario(this.theCalendar.current.getApi().getDate());
    this.setState({
      calendarFocus : this.theCalendar.current.getApi().getDate(),
    });
  }

  handleFechaActual() {
    this.theCalendar.current.getApi().today();
    this.ponerTituloMesCalendario(this.theCalendar.current.getApi().getDate());
    this.setState({
      calendarFocus : this.theCalendar.current.getApi().getDate(),
    });
  }


  handleCalendar(row) {
    
    const initialDate = row[2001002057].split("/").reverse().join("-") + "-01";

    this.theCalendar.current.getApi().gotoDate(initialDate);
    this.setState({
      calendarFocus: new Date(initialDate),
    });

    this.fetchCalendarData(row);
  }


  handleEvent(e) {

    this.setState({
      diaSeleccionado: e.dateStr,
      loadingCalendarSearch: false
    });

    let fecha = new Date(e.dateStr);
    let dia = fecha.getUTCDate();

    this.setState({
      dia: dia,
    });

    if (this.state.rowSelectionSection1) {
      const date = e.dateStr.split("-").reverse().join("/");
      var event =
        this.state.events[this.state.events.findIndex((x) => x.id === date)];

      if (!event) {
        event = {
          registrante: null,
          operaciones: null,
          importes: null,
          objeto: {
            2001003001: null,
            2001003002: date,
            2001003003: null,
            2001003004: null,
          },
        };
      }

      const relacionados = this.state.relacionados;
      relacionados[1]["valor"] = date;

      this.setState({
        event: event,
        [this.state.sectores[1].id]: event.objeto,
        relacionados: relacionados,
        rowSelectionSection3: [],
        rowIdSelectionSection3: [],
      });

      this.fetchSection3Data();

      scroller.scrollTo("sector2001004", {
        containerId: "sectores",
        spy: true,
        smooth: true,
        offset: 100,
        duration: 1000,
      });
    } else {
      this.setState({
        event: null,
        rowSelectionSection3: null,
        rowIdSelectionSection3: [],
      });
    }
  }

  handleOnSelectSection3 = (row) => {
    const rows = this.state.rowSelectionSection3 || [];

    // Si ya estaba seleccionada, se elimina.
    if (rows && rows.includes(row)) {
      const r = rows.filter((r) => r !== row);
      this.setState({
        rowSelectionSection3: r,
        rowIdSelectionSection3: r.map((r) => r.id),
      });

      // Si no estaba seleccionada, se agrega.
    } else {
      rows.push(row);
      this.setState({
        rowSelectionSection3: rows,
        rowIdSelectionSection3: rows.map((r) => r.id),
      });
    }

  };

  handleOnSelectAllSection3 = () => {
    // Si hay alguna seleccionada, se eliminan todas.
    if (
      this.state.rowSelectionSection3 &&
      this.state.rowSelectionSection3.length
    ) {
      this.setState({
        rowSelectionSection3: [],
        rowIdSelectionSection3: [],
      });

      // Si no hay alguna ninguna seleccionada, se agregan todas.
    } else {
      this.setState({
        rowSelectionSection3: this.state.rowsSection3,
        rowIdSelectionSection3: this.state.rowsSection3.map((r) => r.id),
      });
    }
  };

  render() {
    return (
      <div id="sectores" className="content-container registraciones">
        <TopNav
          app={this.props.app}
          nombre={this.state.nombreMenu}
          ruta={this.state.rutaMenu}
        />
        {this.state.errorPantalla ? (
          <div className="main">
            <div className="container">
              <div className="row">
                <div className="col-12">
                  {this.state.mensajesErrorPantalla.map((mensaje) => {
                    return (
                      <AlertForm
                        key={mensaje.secuencia}
                        description={mensaje.mensaje}
                        code={mensaje.secuencia}
                        type={mensaje.tipo}
                      ></AlertForm>
                    );
                  })}
                </div>
              </div>
            </div>
          </div>
        ) : (
          false
        )}
        {this.state.loading ? (
          <div className="container col-12 mt-4">
            <Loader />
          </div>
        ) : (
          this.renderSector1()
        )}
      </div>
    );
  }

  renderSector1() {
    const selectRowSection1 = {
      mode: "checkbox",
      clickToSelect: true,
      style: { backgroundColor: "#FF7F32", color: "#fff" },
      hideSelectAll: true,
      selected: this.state.rowIdSelectionSection1,
      onSelect: this.handleOnSelectSection1,
    };

    const MySearch = (props) => {
      let input;
      const handleClick = () => {
        props.onSearch(input.value);
      };
      return (
        <div className="search-group search-group-300 mr-2" key={4898794654654}>
          <input
            className="form-control input-search"
            ref={(n) => (input = n)}
            type="text"
            onChange={handleClick}
          />
          <button className="button-search">
            <i className="fi fi-rs-search"></i>
          </button>
        </div>
      );
    };

    const optionsPagination = {
      hideSizePerPage: true, // Hide the sizePerPage dropdown always
      showTotal: false,
      disablePageTitle: false,
    };

    const rowStyle = { fontSize: "14px" };


    return this.state.columnsSector1.length ? (
      <ToolkitProvider
        keyField="id"
        data={this.state.rowsSection1}
        columns={this.state.columnsSector1}
        search={<MySearch />}
      >
        {(props) => (
          <div className="main">
            <div className="container" id="sector2001002">
              <div className="row">
                <div className="col-12 mt-4" >
                  <h5 className="global-title-2 pb-4">
                    {this.state.sectores[0].descripcion}
                  </h5>
                  <div>
                    <Filtros
                        search={
                          <MySearch
                            className="input-search"
                            placeholder="Buscar"
                            {...props.searchProps}
                          />
                        }
                        acciones={this.state.sectores[0].acciones}
                        visualizacion={this.state.sectores[0].visualizacion}
                        atributos={this.state.sectores[0].visualizacion.atributos}
                        sectores={this.state.sectores}
                        showSectores={true}
                        showButtonAdd={true}
                      />
                      <BootstrapTable
                        key={"456456465461231231234"}
                        striped
                        hover
                        headerClasses="header-class"
                        rowStyle={rowStyle}
                        selectRow={selectRowSection1}
                        pagination={paginationFactory(optionsPagination)}
                        {...props.baseProps}
                      />
                  </div>
                </div>
              </div>
            </div>


            <div className="container divider2 mt-5 mb-5" />

            {this.renderSector2()}

            <div className="container divider2 mt-5 mb-5" />

            {this.renderSector3()}
          </div>
        )}
      </ToolkitProvider>
    ) : null;
  }

  renderSector2() {
    
    const formatter = new Intl.NumberFormat(JSON.parse(sessionStorage.getItem("defaultLanguage")).localizacion, {
      style: "currency",
      currency: (this.state.rowSelectionSection1 && this.state.rowSelectionSection1[2001002060] != "" ? this.state.rowSelectionSection1[2001002060] : JSON.parse(sessionStorage.getItem("defaultLanguage")).divisa),
      minimumFractionDigits: 0,
    });

    const formatterCantidad = new Intl.NumberFormat(JSON.parse(sessionStorage.getItem("defaultLanguage")).localizacion, {
      minimumFractionDigits: 0,
    });


    const renderEventContent = (eventInfo) => {
      return (
        <div>
          <div className="text-right">
            <b>{formatterCantidad.format(eventInfo.event._def.extendedProps.operaciones)}</b>
            <div>
              {formatter.format(eventInfo.event._def.extendedProps.importes)}
            </div>
          </div>
        </div>
      );
    };
    
    return (
      <div className="container" id="sector2001003">
        <div className="row">
          <div className="col-12 mt-4">
            <h5 className="global-title-2 pb-4">
              {this.state.sectores[1].descripcion}
            </h5>
          </div>
          <div className="col-xl-5 col-lg-5 col-md-12 col-sm-12 col-12">
            <ul className="list-group list-group-custom">
              {this.state.tableCalendar.map((item) => {
                if (item.visible === "S") {
                  return (
                    <li className="list-group-item" key={item.id}>
                      <div className="d-flex justify-content-between flex-xl-row flex-lg-row flex-md-column flex-sm-column flex-column">
                        <span>{item.descripcion}</span>
                        <div>
                          {this.state.rowSelectionSection1 ? (
                            this.state.rowSelectionSection1[item.id]
                          ) : (
                            <span> --- </span>
                          )}
                        </div>
                      </div>
                    </li>
                  );
                }
              })}
            </ul>
          </div>
          <div className="col-xl-7 col-lg-7 col-md-12 col-sm-12 col-12">
            <div className="d-flex mb-4 justify-content-around" id="referencia-calendario">
              <div className="d-flex align-items-center flex-xl-row flex-lg-row flex-md-column flex-sm-column flex-column mr-xl-4 mr-lg-4 mr-md-0 mr-sm-0 mr-0">
                <div className="box-dia mr-3 sin-venta" />
                Días sin venta
              </div>
              <div className="d-flex align-items-center flex-xl-row flex-lg-row flex-md-column flex-sm-column flex-column mr-xl-4 mr-lg-4 mr-md-0 mr-sm-0 mr-0">
                <div className="box-dia mr-3 con-venta" />
                Días con venta
              </div>
              <div className="d-flex align-items-center flex-xl-row flex-lg-row flex-md-column flex-sm-column flex-column">
                <div className="box-dia mr-3 con-venta-0" />
                Días con venta 0
              </div>
            </div>
            <div class="fc fc-media-screen fc-direction-ltr fc-theme-standard">
              <div class="fc-header-toolbar fc-toolbar ">
                <div class="fc-toolbar-chunk">
                  <h2 class="fc-toolbar-title">{this.state.tituloMes}</h2>
                </div>
                <div class="fc-toolbar-chunk">
                </div>
                <div class="fc-toolbar-chunk">
                  <button disabled="" class="fc-today-button fc-button fc-button-primary" type="button" onClick={() => this.handleFechaActual()}>Hoy</button>
                  <div class="fc-button-group">
                    <button class="fc-prev-button fc-button fc-button-primary" type="button" aria-label="prev" onClick={() => this.handleMesAnterior()}>
                      <span class="fc-icon fc-icon-chevron-left"></span>
                    </button>
                    <button class="fc-next-button fc-button fc-button-primary" type="button" aria-label="next" onClick={() => this.handleMesSiguiente()}>
                      <span class="fc-icon fc-icon-chevron-right"></span>
                    </button>
                  </div>
                </div>
              </div>
            </div>
            <FullCalendar
              ref={this.theCalendar}
              defaultAllDay={true}
              events={this.state.events}
              plugins={[dayGridPlugin, interactionPlugin]}
              eventContent={renderEventContent}
              dateClick={(e) => {
                this.handleEvent(e);
              }}
              selectable={true}
              headerToolbar={false}
              showNonCurrentDates={this.state.calendarioCompleto}
              locale={this.state.calendarLang}
              initialView="dayGridMonth"
              initialDate={this.state.calendarFocus}
              buttonText={{ today: "Hoy" }}
              /*datesSet={{
                start: this.state.calendarStart,
                end: this.state.calendarEnd,
              }}*/ 
            />
            <div class="fc fc-media-screen fc-direction-ltr fc-theme-standard" style={{ paddingTop: "10px" }}>
              <div class="fc-header-toolbar fc-toolbar " style={{ padding: "20px", backgroundColor: "#e8e8e8", height: "50px" }}>
                <div class="fc-toolbar-chunk" style={{ width: "25%" }}>
                  <h2 class="fc-toolbar-title" style={{ fontSize: "20px" }}>Total Operaciones:</h2>
                </div>
                <div class="fc-toolbar-chunk" style={{ width: "25%", paddingRight: "20px" }}>
                  <h2 class="fc-toolbar-title" style={{ fontSize: "20px", textAlign: "right" }}>{formatterCantidad.format(this.state.totalOperaciones)}</h2>
                </div>
                <div class="fc-toolbar-chunk" style={{ width: "25%" }}>
                  <h2 class="fc-toolbar-title" style={{ fontSize: "20px" }}>Total Importe:</h2>
                </div>
                <div class="fc-toolbar-chunk" style={{ width: "25%" }}>
                  <h2 class="fc-toolbar-title" style={{ fontSize: "20px", textAlign: "right" }}>{this.state.totalImporte}</h2>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
    
  }

  renderSector3() {
    const selectRowSection3 = {
      mode: "checkbox",
      clickToSelect: true,
      style: { backgroundColor: "#FF7F32", color: "#fff" },
      selected: this.state.rowIdSelectionSection3,
      onSelectAll: this.handleOnSelectAllSection3,
      onSelect: this.handleOnSelectSection3,
    };

    const MySearch = (props) => {
      let input;
      const handleClick = () => {
        props.onSearch(input.value);
      };
      return (
        <div className="search-group search-group-300 mr-2" key={4898794654654}>
          <input
            className="form-control input-search"
            ref={(n) => (input = n)}
            type="text"
            onChange={handleClick}
          />
          <button className="button-search">
            <i className="fi fi-rs-search"></i>
          </button>
        </div>
      );
    };

    const optionsPagination = {
      hideSizePerPage: true, // Hide the sizePerPage dropdown always
      showTotal: false,
      disablePageTitle: false,
    };
    
    return this.state.rowSelectionSection1 &&
      this.state.event &&
      this.state.columnsSector3.length ? (
      <div className="container" id="sector2001004" style={{overflowY: 'visible'}}>
        <div className="row">
          <div className="col-12 mt-4">
            <h5 className="global-title-2 pb-4">
              {this.state.sectores[2].descripcion}
            </h5>
            <ToolkitProvider
              keyField="id"
              data={this.state.rowsSection3}
              columns={this.state.columnsSector3}
              search={<MySearch />}
            >
              {(props) => (
                <div>
                  <Filtros
                    search={
                      <MySearch
                        className="input-search"
                        placeholder="Buscar"
                        {...props.searchProps}
                      />
                    }
                    acciones={this.state.sectores[2].acciones}
                    atributos={this.state.sectores[2].atributos}
                    menu="registraciones"
                    objectId={this.state.componentId}
                    objectSelectedId={this.state.objectSelectedId}
                    opcionId={this.props.id}
                    opcion={this.state.opcion}
                    relacionados={this.state.relacionados}
                    selectedRows={this.state.rowSelectionSection3}
                    urls={this.state.urls}
                    dia={this.state.dia}
                    diaSeleccionado={this.state.diaSeleccionado}
                    idRegistrante={this.state.rowIdSelectionSection1}
                    visualizacion={this.state.sectores[2].visualizacion}
                    showButtonAdd={this.state.loadingCalendarSearch}
                    size="big"
                    edicion={this.state.sectores[2].edicion}      
                  />
                  <BootstrapTable
                    key="456456465464568974"
                    striped
                    hover
                    headerClasses="header-class"
                    selectRow={selectRowSection3}
                    pagination={paginationFactory(optionsPagination)}
                    {...props.baseProps}
                  />
                </div>
              )}
            </ToolkitProvider>
          </div>
        </div>
      </div>
    ) : null;
  }
}

const mapStateToProps = (state) => ({
  rerender: state.rerender,
});

export default connect(mapStateToProps)(Registraciones);
