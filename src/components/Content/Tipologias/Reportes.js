// Generic
import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import Tooltip from "react-simple-tooltip";
import $ from "jquery";
// Wijmo
import * as wjcOlap from "wijmo/wijmo.olap";
import * as Olap from "wijmo/wijmo.react.olap";
import "wijmo/wijmo.touch";
import * as wijmo from "wijmo/wijmo";
import { FlexGrid, FlexGridColumn } from "wijmo/wijmo.react.grid";
import { GroupPanel as FlexGridGroup } from "wijmo/wijmo.react.grid.grouppanel";
import { FlexGridSearch } from 'wijmo/wijmo.react.grid.search';
import { FlexGridFilter } from 'wijmo/wijmo.react.grid.filter';
import { CollectionView } from "wijmo/wijmo";
import * as wjcGrid from "wijmo/wijmo.grid";
import * as wjChart from "wijmo/wijmo.react.chart";
import * as wjChartAnimate from "wijmo/wijmo.react.chart.animation";
import * as wjcCore from "wijmo/wijmo";
import * as pdf from "wijmo/wijmo.pdf";
import * as gridPdf from "wijmo/wijmo.grid.pdf";
import * as gridXlsx from "wijmo/wijmo.grid.xlsx";
import * as wjPdf from "wijmo/wijmo.pdf";
import { FlexGridPdfConverter } from "wijmo/wijmo.grid.pdf";

// Components
import store from "../../../redux/store";
import URLS from "../../../urls";
import TopNav from "../TopNav";
import Loader from "../../GlobalComponents/Loader";
import Input from "../../GlobalComponents/Input";
import AlertModal from "../../GlobalComponents/AlertModal";
import Atributos from "../Atributos";
// Images
import PDF from "../../../assets/images/PDF.svg";
import XLSXi from "../../../assets/images/XLSX.svg";
import XML from "../../../assets/images/XML.svg";
import TXT from "../../../assets/images/TXT.svg";
import JSONI from "../../../assets/images/JSON.svg";

import CAJA_ARRIBA from "../../../assets/images/Arriba.png";
import CAJA_IGUAL from "../../../assets/images/Igual.png";
import CAJA_ABAJO from "../../../assets/images/Abajo.png";
import CAJA_ACEPTADO from "../../../assets/images/Aceptado.png";
import CAJA_ADVERTENCIA from "../../../assets/images/Advertencia.png";

// Css
import "../Content.css";
import "wijmo/styles/wijmo.css";
//set Spanish culture
import "wijmo/cultures/wijmo.culture.es";

class Reportes extends Component {
  constructor(props) {
    super(props);

    this.theGrid = React.createRef();
    this.theSearch = React.createRef();
    

    this.state = {
      state: 0,
      loading: true,
      acciones: [],
      atributos: [],
      componentId: "",
      descripcion: "",
      habilitado: "",
      id: this.props.id,
      nombre: "",
      observaciones: "",
      sectores: [],
      tipologia: "",
      nombreMenu: "",
      rutaMenu: "",
      toggle: true,
      loadingData: false,
      data: {},
      dataGrafico: {},
      engine: {},
      showPivotPanelModal: false,
      filtros: false,
      descargarOpciones: false,
      descargarLoading: false,
      modalDescargarShow: false,
      descargarData: false,
      errorMessages: false,
      showModal: false,
      componentes: [],
      agrupados: "",
      alturaGrilla: this.props.altura-50
    };

    this.handleShowPivotPanelModal = this.handleShowPivotPanelModal.bind(this);
    this.renderGrilla = this.renderGrilla.bind(this);
    this.elementoDescargar = this.elementoDescargar.bind(this);
    this.handleModalDescargar = this.handleModalDescargar.bind(this);
    this.downloadFile = this.downloadFile.bind(this);
    this.renderErrorModal = this.renderErrorModal.bind(this); 
    this.renderStates = this.renderStates.bind(this); 
    this.procesarInformacion = this.procesarInformacion.bind(this); 
    this.obtenerInformacionDashboard = this.obtenerInformacionDashboard.bind(this);
  }

  componentDidUpdate(prevProps) {
    
    if (this.props.id !== prevProps.id) {
      this.setState({
        data: {},
        loading: true,
      });
      this.fetchConfig();
      store.dispatch({
        type: "MISA",
        payload: false,
      });
      
    }
  }

  componentWillMount() {
    
    this.setState({
      buttonDisable: false,
      loading: true,
      acciones: [],
      atributos: [],
      componentId: "",
      descripcion: "",
      habilitado: "",
      id: this.props.id,
      nombre: "",
      observaciones: "",
      sectores: [],
      tipologia: "",
      nombreMenu: "",
      rutaMenu: "",
      toggle: true,
      loadingData: false,
      data: {},
      engine: {},
      showPivotPanelModal: false,
      filtros: false,
    });

    this.fetchConfig();
    
    store.dispatch({
      type: "SET_FILTROS_APLICADOS",
      payload: true,
    });

    store.dispatch({
      type: "MISA",
      payload: false,
    });

  }

  renderStates() {
    switch (this.state.state) {
      case 400:
        this.setState({
          showModal: true,
        });
        return this.renderErrorModal();

      default:
    }
  } 

  renderErrorModal() {
    return (
      <>
        {this.state.showModal ? (
          <div className="custom-modal">
            <div className="custom-modal-content">
              <div className="custom-modal-header">
                <div className="row d-flex justify-content-between align-items-center">
                  <div>
                    <h6> Reportes </h6>
                  </div>
                  <div>
                    <button
                      className="btn btn-close"
                      onClick={() => this.handleCloseModal()}
                    >
                      <i className="fi fi-rs-cross" />
                    </button>
                  </div>
                </div>
              </div>
              <div className="mt-5">
                <div className="d-flex justify-content-center">
                  <i className="right-icon-big-no fi fi-rs-cross" />
                </div>
                <div className="d-flex flex-column justify-content-center mt text-center">
                  <p>
                    <b>Ocurrió un error</b>
                  </p>

                  {this.state.errorMessages
                    ? this.state.errorMessages.map((error, i) => {
                        return (
                          <div
                            className="alert alert-danger mx-5"
                            role="alert"
                            key={i}
                          >
                            <div style={{ wordBreak: "break-word" }}>
                              {error.mensaje}
                            </div>
                          </div>
                        );
                      })
                    : null}
                </div>
              </div>
            </div>
          </div>
        ) : null}
      </>
    );
  } 

  fetchConfig() {
    
    var contenedor = "";

    if (this.props.componentes && this.props.componentes.contenedor === "S") {
      contenedor = "S";
    }else{
      contenedor = "N";
    }


    var myHeaders = new Headers();
    myHeaders.append(
      "Authorization",
      "Bearer " + sessionStorage.getItem("token"),
    );
    myHeaders.append("Content-Type", "application/json");
    
    var requestOptions = {
      method: "GET",
      headers: myHeaders,
      redirect: "follow",
    };
    fetch(
      store.getState().serverUrl + URLS.CONFIGURACION + this.props.id + "/" + contenedor,
      requestOptions,
    )
      .then((response) => {
        if (response.status == 401) {
          sessionStorage.clear();
          window.location.reload(false);
        }else{
          return response.json();
        }
      })
      .then((result) => {

        const atributos = {};

        if (result.componentes.sectores && result.componentes.sectores.length) {
          result.componentes.sectores
            .find((s) => s.tipologia === "FILTROS")
            .columnas.forEach((column) => {
              column.filtros.forEach((attr) => {
                atributos[attr.id] = "";
              });
            });
        }

        this.setState({
          componentes: result.componentes,
          acciones: result.componentes.acciones,
          atributos: atributos,
          componentId: result.componentes.id,
          descripcion: result.componentes.descripcion,
          habilitado: result.componentes.habilitado,
          nombre: result.componentes.nombre,
          observaciones: result.componentes.observaciones,
          sectores: result.componentes.sectores,
          tipologia: result.componentes.tipologia,
          nombreMenu: result.componentes.menu,
          rutaMenu: result.componentes.ruta,
          loading: false,
        });

        
        if (!this.props.componentes) {
          
          const viewsSections = this.state.sectores.filter(
            (s) => s.tipologia === "VISTA" || s.tipologia === "TEMPORAL",
          );
          {
            viewsSections.map((section) => {
              var altoGrilla = 650;
              if (!isNaN(this.props.altura) )
                altoGrilla = this.props.altura - 70;
              if (section.visualizacion.agrupados === 'S' )
                altoGrilla = altoGrilla - 56;
              if (section.busqueda === 'S' )
                altoGrilla = altoGrilla - 40;

              this.setState({
                agrupados: section.visualizacion.agrupados,
                busqueda: section.busqueda,
                alturaGrilla: altoGrilla,
              });
            });
          }
        }
        
        if (
          this.props.componentes &&
          this.props.componentes.contenedor === "S"
        ) {
          const action = this.props.componentes.acciones.find(
            (a) => a.accion === "1",
          );
          
          if (action.tipo === 'PROCESO'){
            this.procesarInformacion(action);
          }else { 
            this.obtenerInformacionDashboard(action);
          }
        }
        
      })
      .catch((error) => console.log("error", error));
  }

  obtenerInformacionDashboard(action) {
    var actionUrl;
    actionUrl = store.getState().serverUrl + URLS.OBTENER_INFORMACION;
    
    
    if (actionUrl) {
      var myHeaders;
      var raw;
      var requestOptions;
      var objeto;
      var atributos;

      action.refrescos.forEach((refresh) => {
        objeto = refresh.objeto;

        myHeaders = new Headers();
        myHeaders.append("Authorization", "Bearer " + sessionStorage.getItem("token"));
        myHeaders.append("Content-Type", "application/json");

        if (action.tipo === "SIN_EVENTO") {
          if (this.props.componentes.sectores[0].columnas[0]){
            atributos = Object.entries(
              this.props.componentes.sectores[0].columnas[0].filtros,
            ).map((item) => {
              return {
                atributo: item[1].id,
                contenido: "",
              };
            });
          }else{
            atributos = "";
          }
        } else {
          atributos = "";
        }
        
        raw = JSON.stringify({
          idObjeto: parseInt(objeto),
          idAccion: parseInt(action.id),
          idFormato: "",
          filtros: {
            procesos: atributos,
          },
        });

        requestOptions = {
          method: "POST",
          headers: myHeaders,
          body: raw,
          redirect: "follow",
        };

        fetch(actionUrl, requestOptions)
          .then((response) => {
            if (response.status == 401) {
              sessionStorage.clear();
              window.location.reload(false);
            }else{
              return response.json();
            }
          })
          .then((result) => {
            
            this.setState({
              buttonDisable: false
            });

            if (this.props.componentes.modalidad === "REPORTE") {
              const viewsSections = this.props.sectores.filter(
                (s) => s.modalidad === "GRILLA",
              );
              
              viewsSections.map((section) => {
                var altoGrilla = 650;

                if (!isNaN(this.props.altura) )
                  altoGrilla = this.props.altura - 70;

                if (section.visualizacion.agrupados === 'S' )
                  altoGrilla = altoGrilla - 56;
                if (section.busqueda === 'S' )
                  altoGrilla = altoGrilla - 40;
                
                  this.setState({
                  agrupados: section.visualizacion.agrupados,
                  busqueda: section.busqueda,
                  alturaGrilla: altoGrilla,
                });
              });

              
              store.dispatch({
                type: "SET_TIPOLOGIA_INFO",
                name: objeto,
                data: JSON.parse(JSON.stringify(result), (key, value) => {
                          if (typeof value === 'string') {
                              let fecha = value.match(/^(\d{2})\/(\d{2})\/(\d{4})$/);
                              if (fecha) {
                                  return new Date(+fecha[3], +fecha[2]-1, +fecha[1]);
                              }
                          }
                          return value;
                      }),
              });


              store.dispatch({
                type: "MISA",
                payload: true,
              });          
              this.setState({
                loadingData: false,
                filtros: false,
              });
            } else if (this.props.componentes.modalidad === "GRAFICO") {
              var data = result;
              this.setState({
                loadingData: false,
                dataGrafico: data,
                filtros: false,
              });
              
            }else{
              
              this.setState({
                loadingData: false,
                filtros: false,
              });

            }
          })
          .catch((error) => console.log("error", error));
      });
    }
  }

  procesarInformacion(action) {
    var actionUrl;
    
    actionUrl = store.getState().serverUrl + URLS.REPORTES_PROCESAR;
    
    if (actionUrl) {
      var myHeaders;
      var raw;
      var requestOptions;
      var objeto;

      objeto = 4002001;

      myHeaders = new Headers();
      myHeaders.append("Authorization", "Bearer " + sessionStorage.getItem("token"));
      myHeaders.append("Content-Type", "application/json");

      const atributos = Object.entries(this.state.atributos).map((item) => {
        return {
          atributo: item[0],
          contenido: item[1],
        };
      });

      raw = JSON.stringify({
        idObjeto: parseInt(objeto),
        idOpcion: parseInt(this.props.id),
        idAccion: parseInt(action.id),
        idFormato: "GRILLA",
        filtros: {
          procesos: atributos,
        },
      });

      requestOptions = {
        method: "POST",
        headers: myHeaders,
        body: raw,
        redirect: "follow",
      };

      fetch(actionUrl, requestOptions)
        .then((response) => {
          if (response.status == 401) {
            sessionStorage.clear();
            window.location.reload(false);
          }
          if (response.status === 201) {
            return response.text();
          } else {
            return response.json();
          }
        })
        .then((result) => {
          if (result.Mensajes) {
            this.setState({
              loadingData: false,
              state: 400,
              buttonDisable: false,
              errorMessages: result.Mensajes,
            });
            this.renderStates();
          } else {
            
            this.obtenerInformacionDashboard(action);
          }
        })
        .catch((error) => console.log("error", error));
    }
  } 

  handleShowPivotPanelModal() {
    this.setState({
      showPivotPanelModal: true,
    });
  }

  handleCancelPivotPanelModal() {
    this.setState({
      showPivotPanelModal: false,
    });
  }

  handleSubmitPivotPanelModal() {
    this.setState({
      showPivotPanelModal: false,
    });
  }

  initializePivotGrid(pivotGrid) {
    this.pivotGrid = pivotGrid;
  }

  initializedGrid(ctl) {
    if (this.state.agrupados === "S") {
      ctl.columnFooters.rows.push(new wjcGrid.GroupRow());
      ctl.bottomLeftCells.setCellData(0, 0, "S");
      this.flexGrid = ctl;
      this.setState({
        grilla: ctl,
      });
    } else {
      this.flexGrid = ctl;
    }

    if (this.state.busqueda === "S") {
      /** para el search grid */
      let theGrid = ctl;
      let theSearch = this.theSearch.current.control;
      theSearch.grid = theGrid;
    }
  }

  exportPDF() {
    const viewsSections = this.state.sectores.filter(
      (s) => s.tipologia === "VISTA" || s.tipologia === "TEMPORAL",
    );

    let doc = new pdf.PdfDocument({
      pageSettings: {
        layout: pdf.PdfPageOrientation.Landscape,
        size: pdf.PdfPageSize.Letter,
        margins: {
          left: 20,
          top: 0,
          right: 20,
          bottom: 0,
        },
      },

      ended: (ctl, args) => pdf.saveBlob(args.blob, "Reporte.pdf"),
    });
    doc.setFont(new pdf.PdfFont("helvetica"));

    let settings = {
      styles: {
        cellStyle: {
          backgroundColor: "#ffffff" /* Blanco */,
          borderColor: "#000000" /* Negro */,
        },
        altCellStyle: {
          backgroundColor: "#e7e3e3" /* Gris */,
        },
        groupCellStyle: {
          backgroundColor: "#c1c1c1" /* Gris */,
        },
        headerCellStyle: {
          backgroundColor: "#c1c1c1" /* Naranja */,
          color: "#000000" /* Negro */,
        },
      },
    };
    {
      viewsSections.map((section) => {
        switch (section.modalidad) {
          case "PIVOT":
            gridPdf.FlexGridPdfConverter.export(
              this.pivotGrid,
              "Reporte.pdf",
              settings,
            );
            break;
          case "GRAFICO":
          case "GRILLA":
            gridPdf.FlexGridPdfConverter.draw(
              this.flexGrid,
              doc,
              750,
              null,
              settings,
            );
            break;
        }
      });
    }

    doc.end();
  }

  exportXlsx() {
    const viewsSections = this.state.sectores.filter(
      (s) => s.tipologia === "VISTA" || s.tipologia === "TEMPORAL",
    );
    {
      viewsSections.map((section) => {
        switch (section.modalidad) {
          case "PIVOT":
            // create book with current view
            let book = gridXlsx.FlexGridXlsxConverter.saveAsync(
              this.pivotGrid,
              {
                includeColumnHeaders: true,
                includeRowHeaders: true,
              },
            );
            book.sheets[0].name = "Reporte";
            // save the book
            book.saveAsync("Reporte.xlsx");
            break;
          case "GRAFICO":
          case "GRILLA":
            gridXlsx.FlexGridXlsxConverter.saveAsync(
              this.flexGrid,
              {},
              "Reporte.xlsx",
              null,
              null,
              true,
            );
            break;
        }
      });
    }
  }

  renderCaja(sector) {
    var agrupacion;
    var ancho;
    const sectorId = JSON.stringify(sector.id);

    return (
      <>
        <div
          key={sector.id}
          className="cajaPortal"
        >
          {this.props.componentes &&
          this.props.componentes.contenedor === "S" ? null : (
            <h6 className="align-items-center d-flex">
              {sector.descripcion} 
            </h6>
          )}

          {(this.props.componentes && 
          this.props.componentes.contenedor === "S" && 
          this.props.tipologiaInfo.get(sector.id)) ? (
            <div className="cajaPortal">
              {this.props.tipologiaInfo.get(sector.id)[0][sector.visualizacion.atributos[0].id] !== null && this.props.tipologiaInfo.get(sector.id)[0][sector.visualizacion.atributos[0].id] !== '' ? (
                <div className="cajaIcono">
                  <img src={(this.props.tipologiaInfo.get(sector.id)[0][sector.visualizacion.atributos[0].id] === 'Arriba') ? (CAJA_ARRIBA) : ((this.props.tipologiaInfo.get(sector.id)[0][sector.visualizacion.atributos[0].id] === 'Abajo') ? (CAJA_ABAJO) : ((this.props.tipologiaInfo.get(sector.id)[0][sector.visualizacion.atributos[0].id] === 'Aceptado') ? (CAJA_ACEPTADO) : ((this.props.tipologiaInfo.get(sector.id)[0][sector.visualizacion.atributos[0].id] === 'Advertencia') ? (CAJA_ADVERTENCIA) : (CAJA_IGUAL)))) } />
                </div>
              ) : null }
              <div className="cajaValor" style={{textAlign: sector.visualizacion.atributos[1].alineacion}} dangerouslySetInnerHTML={{__html : this.props.tipologiaInfo.get(sector.id)[0][sector.visualizacion.atributos[1].id]}}>
              </div>
            </div>
          ) : (
            this.props.loadingData ? (
              <Loader /> 
              ) : null
          )}
        </div>
      </>
    );
  }

  renderGrilla(sector) {
    var agrupacion;
    var ancho;
    var tipo;
    const sectorId = JSON.stringify(sector.id);
    var columnasFiltro = [];
    var cabeceraGrilla;

    if (this.props.cabecerasGrilla !== null) {
      cabeceraGrilla = this.props.cabecerasGrilla;
    }else{
      cabeceraGrilla = sector;
    }

    return (
      <>
        <div
          key={sector.id}
          className="d-flex flex-column  w-100 contenedor"
          style={{height: "100%",  maxHeight: "100%", paddingLeft: "5px" }}
        >
          {this.props.componentes &&
            this.props.componentes.contenedor === "S" ? null : (
            <div className="col-12 d-flex justify-content-between align-items-center flex-wrap">
              <h2 className="global-title-2 m-0" style={{ paddingTop: "5px" }} >
                {sector.descripcion}
              </h2>
              {!this.props.componentes ? (
                <div style={{ padding: "0.5rem 0" }}>
                  {(this.props.componentes &&
                    this.props.componentes.contenedor === "S" &&
                    this.props.tipologiaInfo.get(sector.id)) || (this.state.componentes &&
                      this.state.componentes.contenedor === "N" &&
                      Object.entries(this.props.dataObInfo).length !== 0) ? (
                    <>
                      <Tooltip
                        content="Exportar PDF"
                        placement="bottom"
                        className="global-link-1 btn-acciones text-center"
                        onClick={this.exportPDF.bind(this)}
                        arrow={15}
                        background="#000"
                        color="#fff"
                        padding={13}
                        radius={8}
                      >
                        <i className="fi fi-rs-file-pdf"></i>
                      </Tooltip>
                    </>
                  ) : null}
                </div>
              ) : null}
            </div>
            )}


          {this.state.busqueda === "S" ? (
              <div className="d-flex flex-column justify-content-between w-100 mb-1">
                <FlexGridSearch ref={this.theSearch} placeholder='Ingrese texto a buscar'/>
              </div>
          ) : null}
          {this.state.agrupados === "S" ? (
              <FlexGridGroup
              grid={this.state.grilla}
              placeholder={"Arrastrar Columnas para Crear Grupos"}
            />
          ) : null}
          {(this.props.componentes && 
          this.props.componentes.contenedor === "S" && 
          this.props.tipologiaInfo.get(sector.id)) || (this.state.componentes && 
            this.state.componentes.contenedor === "N" && 
            Object.entries(this.props.dataObInfo).length !== 0) ? (
            <FlexGrid 
              initialized={this.initializedGrid.bind(this)}
              itemsSource={(this.props.componentes && 
                this.props.componentes.contenedor === "S" ?  this.props.tipologiaInfo.get(sector.id) : this.props.dataObInfo) }
              deferResizing={true}
              showMarquee={false}
              preserveSelectedState={true}
              allowPinning="SingleColumn"
              allowSorting={true}
              selectionMode={"Row"}
              height={this.state.alturaGrilla}
              maxHeight={this.state.alturaGrilla}
              style={{height: this.state.alturaGrilla,  maxHeight: this.state.alturaGrilla, width: "100%"}}
              >
              {cabeceraGrilla.visualizacion.atributos
                .filter((attr) => attr.visible === "S")
                .map((attr) => {
                  agrupacion = "None";

                  if (attr.agrupacion !== "") {
                    agrupacion = attr.agrupacion;
                  }
                  ancho = attr.porcentaje;
                  if (!ancho.includes("*")){
                    ancho = Number(ancho); 
                  }

                  if (attr.filtrar === "S"){
                    columnasFiltro.push(attr.id + "");
                  }

                  
                  if (attr.tipo === 'FECHA')
                    tipo = "Date";
                  else
                    tipo = "String";
                  return (
                    <FlexGridColumn
                      key={attr.orden}
                      binding={attr.id}
                      header={attr.descripcion}
                      isReadOnly={true}
                      width={ancho}
                      dataType={tipo}
                      format={attr.mascara}
                      align={attr.alineacion}
                      aggregate={agrupacion}
                    />
                  );
                })}
              <FlexGridFilter filterColumns={columnasFiltro}/>
            </FlexGrid>
          ) : (
            this.props.loadingData ? (
              <Loader /> 
              ) : null
          )          
          }
        </div>
      </>
    );
  }

  handleCloseModal() {
    this.setState((prevState) => ({
      showModal: !prevState.showModal,
    }));
  } 

  handleDescargar(action) {
    
    var formato;
    if (action.formatos.length > 0){
      formato = action.formatos[0].nombre;
    }else{
      formato = "PDF";
    }
    this.fetchDescargar(formato, action.tipo, action.atributo, action.id);
  }

  fetchDescargar(extension, tipo, atributoSeleccion, accionID) {
    
    this.setState((prevState) => ({
      descargarOpciones: !prevState.descargarOpciones,
      descargarLoading: true,
    }));
    var myHeaders = new Headers();
    myHeaders.append(
      "Authorization",
      "Bearer " + sessionStorage.getItem("token"),
    );
    myHeaders.append("Content-Type", "application/json");

    const atributos = Object.entries(this.state.atributos).map((item) => {
      return {
        atributo: item[0],
        contenido: item[1],
      };
    });

    var objeto = parseInt(this.state.componentId);
    var opcion = parseInt(this.state.id);
    var accion = parseInt(accionID);
    var urlDescarga;

      var seleccionado;
      if (tipo === "ADJUNTO") {
        if (this.flexGrid && this.flexGrid.selectedRows[0]){
          const atributos = Object.entries(this.flexGrid.selectedRows[0].dataItem).map((item) => {
          
            if (item[0] === atributoSeleccion) {
              seleccionado = item[1];
            }
          });
          if (seleccionado) {
            var raw = {
              datos: {
                objeto: objeto,
                opcion: opcion,
                atributo: null,
                interno: seleccionado,
              },
            };
            urlDescarga = URLS.OBTENER_ADJUNTOS;
          }else{
            urlDescarga = null;
            this.setState({
              descargarLoading: false,
              descargarData: "Debe seleccionar un elemento de la grilla.",
            });
            this.handleModalDescargar();
          }
        }else{
          urlDescarga = null;
          this.setState({
            descargarLoading: false,
            descargarData: "Debe seleccionar un elemento de la grilla.",
          });
          this.handleModalDescargar();
        }
      }else{
        if (tipo === "SELECCION") {
          if (this.flexGrid && this.flexGrid.selectedRows[0]) {
            const atributos = Object.entries(this.flexGrid.selectedRows[0].dataItem).map((item) => {
              console.log("info: ", item);
              return {
                atributo: item[0],
                contenido: item[1],
              };
            });
            var raw = {
              idObjeto: objeto,
              idOpcion: opcion,
              idAccion: accion,
              idFormato: extension,
              filtros: {
                procesos: atributos,
              },
            };
            urlDescarga = URLS.REPORTES_PROCESAR;
          }else{
            urlDescarga = null;
            this.setState({
              descargarLoading: false,
              descargarData: "Debe seleccionar un elemento de la grilla.",
            });
            this.handleModalDescargar();
          }
        }else{
          var raw = {
            idObjeto: objeto,
            idOpcion: opcion,
            idAccion: accion,
            idFormato: extension,
            filtros: {
              procesos: atributos,
            },
          };
          urlDescarga = URLS.REPORTES_PROCESAR;
        }
      }
    var requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: JSON.stringify(raw),
      redirect: "follow",
    };

    

    var status;
    if (urlDescarga != null){
      fetch(store.getState().serverUrl + urlDescarga, requestOptions)
      .then((response) => {
        if (response.status == 401) {
          sessionStorage.clear();
          window.location.reload(false);
        }        
        status = response.status;
        return response.json();
      })
      .then((result) => {
        if (status === 500) {
          this.setState({
            descargarLoading: false,
            descargarData: result,
          });
          this.handleModalDescargar();
        } else {
          var contenido;
          var nombreArchivo;
          if (tipo === "ADJUNTO"){
            contenido = result.adjunto;
            nombreArchivo = result.archivo;
          }else{
            contenido = result.contenido;
            nombreArchivo = result.nombreArchivo;
          }
          this.downloadFile(contenido, extension, nombreArchivo);
          this.setState({
            descargarLoading: false,
            descargarData: result,
          });
          this.handleModalDescargar();
        }
      })
      .catch((error) => {
        this.setState({
          descargarLoading: false,
          descargarData: error,
        });
        this.handleModalDescargar();
        console.log("error", error);
      });
    }
  }

  downloadFile(file, extention, filename) {
    const linkSource = `data:application/${extention};base64,${file}`;
    const downloadLink = document.createElement("a");
    const fileName = filename;

    downloadLink.href = linkSource;
    downloadLink.download = fileName;
    downloadLink.click();
  }

  handleModalDescargar() {
    this.setState((prevState) => ({
      modalDescargarShow: !prevState.modalDescargarShow,
    }));
  }

  elementoDescargar(acciones) {
    
    return acciones.map((action) => {
      var formato;

      if (action.formatos.length > 0){
        formato = action.formatos[0].nombre;
      }else{
        formato = "PDF";
      }
      
      return action.tipo != "DESCARGA" && action.tipo != "SELECCION" && action.tipo != "ADJUNTO"  ? null : (        
        <div>
          <Tooltip
            placement="bottom"
            className="global-link-1 btn-acciones"
            content={action.descripcion}
            onClick={() => this.fetchDescargar(formato, action.tipo, action.atributo, action.id)}
          >
            <i className="fi fi-rs-download" />
          </Tooltip>
        
          <AlertModal
            handleShowHide={() => this.handleModalDescargar}
            title="Descarga"
            show={this.state.modalDescargarShow}
            data={this.state.descargarData}
          ></AlertModal>
        </div>
      );
    });
  }

  renderGraficoDashboard(principal) {

    var palette = [
      "rgba(241,158,84,1)",
      "rgba(134,128,123,1)",
      "rgba(144,205,151,1)",
      "rgba(246,170,201,1)",
      "rgba(191,165,84,1)",
      "rgba(188,153,199,1)",
      "rgba(237,221,70,1)",
      "rgba(240,126,110,1)",
      "rgba(140,140,140,1)",
    ];


    return (
      <div>
        <div className="filtros bg-white shadow-sm">
          <div className="container">
            <div className="row">
              <div className="d-flex justify-content-between align-items-center flex-wrap">
                <div className="d-flex justify-content-start align-items-center flex-xl-row flex-lg-row flex-md-row flex-sm-row flex-column container-filter-search">
                  {this.props.componentes &&
                  this.props.componentes.contenedor === "S" ? (
                    <>
                      <h2 className="global-title-1 m-0" style={{width: "100%"}}>
                        {this.props.componentes.descripcion}
                      </h2>
                    </>
                  ) : null}
                </div>
              </div>
            </div>
          </div>
        </div>
        {this.state.dataGrafico.length > 0 ? (
          <div className="container">
            {this.props.loadingData ? (
              <div className="col-12 mt-3">
                <Loader />
              </div>
            ) : (
              <>
                {this.props.componentes.sectores.map((sector, i) => {
                  if (sector.modalidad === "GRAFICO") {
                    return (
                      <div className="container-fluid mt-3" key={i}>
                        <wjChart.FlexChart
                          header={this.props.componentes.descripcion}
                          bindingX={sector.dimensiones[0].nombre}
                          chartType={this.props.componentes.formatos.nombre}
                          itemsSource={this.state.dataGrafico}
                          palette={palette}
                        >
                          <wjChart.FlexChartLegend position="Bottom"></wjChart.FlexChartLegend>
                          {principal ? (
                            <wjChart.FlexChartSeries
                              binding={principal.nombre}
                              name={principal.descripcion}
                              chartType={principal.tipo}
                            ></wjChart.FlexChartSeries>
                          ) : null}
                          {this.props.componentes.sectores[1].medidas.map(
                            (item, i) => {
                              if (i !== 0) {
                                return (
                                  <wjChart.FlexChartSeries
                                    binding={item.nombre}
                                    name={item.descripcion}
                                    chartType={item.tipo}
                                    key={i}
                                  >
                                    
                                  </wjChart.FlexChartSeries>
                                );
                              }
                            },
                          )}
                          {principal ? (
                            <wjChart.FlexChartAxis
                              wjProperty="axisY"
                              position={principal.alineacion}
                              title={principal.observaciones}
                              axisLine={true}
                              min={parseInt(principal.minimo)}
                            ></wjChart.FlexChartAxis>
                          ) : null}
                          <wjChartAnimate.FlexChartAnimation></wjChartAnimate.FlexChartAnimation>
                        </wjChart.FlexChart>
                      </div>
                    );
                  }
                })}
              </>
            )}
          </div>
        ) : null}
      </div>
    );
  }

  renderGrafico(principal) {
    
    var palette = [
      "rgba(136,189,230,1)",
      "rgba(251,178,88,1)",
      "rgba(144,205,151,1)",
      "rgba(246,170,201,1)",
      "rgba(191,165,84,1)",
      "rgba(188,153,199,1)",
      "rgba(237,221,70,1)",
      "rgba(240,126,110,1)",
      "rgba(140,140,140,1)",
    ];

    return (
      <div>
        <div className="container">
          {this.props.loadingData ? (
            <div className="col-12 mt-3">
              <Loader />
            </div>
          ) : (
            <>
              {this.state.componentes.sectores.map((sector, i) => {
                if (sector.modalidad === "GRAFICO") {
                  if (this.props.dataObInfo && Object.entries(this.props.dataObInfo).length !== 0) {
                    return (
                      <div className="container-fluid mt-3" key={i}>
                        <wjChart.FlexChart
                          header={this.state.componentes.descripcion}
                          bindingX={sector.dimensiones[0].nombre}
                          chartType={this.state.componentes.formatos.nombre}
                          itemsSource={this.props.dataObInfo}
                          palette={palette}
                        >
                          <wjChart.FlexChartLegend position="Bottom"></wjChart.FlexChartLegend>
                          {principal ? (
                            <wjChart.FlexChartSeries
                              binding={principal.nombre}
                              name={principal.descripcion}
                              chartType={principal.tipo}
                            ></wjChart.FlexChartSeries>
                          ) : null}
                          {this.state.componentes.sectores[1].medidas.map(
                            (item, i) => {
                              if (i !== 0) {
                                return (
                                  <wjChart.FlexChartSeries
                                    binding={item.nombre}
                                    name={item.descripcion}
                                    chartType={item.tipo}
                                    key={i}
                                  >
                                    
                                  </wjChart.FlexChartSeries>
                                );
                              }
                            },
                          )}
                          {principal ? (
                            <wjChart.FlexChartAxis
                              wjProperty="axisY"
                              position={principal.alineacion}
                              title={principal.observaciones}
                              axisLine={true}
                              min={parseInt(principal.minimo)}
                            ></wjChart.FlexChartAxis>
                          ) : null}
                          <wjChartAnimate.FlexChartAnimation></wjChartAnimate.FlexChartAnimation>
                        </wjChart.FlexChart>
                      </div>
                    );
                  }
                }
              })}
            </>
          )}
        </div>
      </div>
    );
  }

  render() {
    if (this.props.componentes) {
      switch (this.props.componentes.modalidad) {
        case "REPORTE":
          return (
            <>
              {this.state.errorMessages ? this.renderErrorModal() : null}
              <div
                className="box shadow mt-3 "
                style={{ height: this.props.altura, maxHeight: this.props.altura }}
              >
                {this.props.sectores && this.props.sectores.length ? (
                  this.renderSections(
                    this.props.sectores,
                    this.props.componentes.acciones,
                  )
                ) : (
                  <>
                    <div className="container">
                      <div className="row">
                        <div className="col-12 text-center mt-5">
                          <div className="alert alert-warning">
                            <i className="fi fi-rs-triangle-warning mr-3" />
                            En este momento no es posible mostrar el componente.
                            Intente nuevamente más tarde.
                          </div>
                        </div>
                      </div>
                    </div>
                  </>
                )}
              </div>
            </>
          );
        case "GRAFICO":
          return (
            <>
              <div
                className="box shadow overflow-auto mt-3 "
                style={{ height: this.props.altura }}
              >
                {this.renderGraficoDashboard(
                  this.props.componentes.sectores[1].medidas[0],
                )}
              </div>
            </>
          );
          case "CAJA":
            return (
              <>
                {this.state.errorMessages ? this.renderErrorModal() : null}
                <div
                  className="box shadow mt-3 "
                  style={{ height: this.props.altura, maxHeight: this.props.altura }}
                >
                  {this.props.sectores && this.props.sectores.length ? (
                    this.renderSections(
                      this.props.sectores,
                      this.props.componentes.acciones,
                    )
                  ) : (
                    <>
                      <div className="container">
                        <div className="row">
                          <div className="col-12 text-center mt-5">
                            <div className="alert alert-warning">
                              <i className="fi fi-rs-triangle-warning mr-3" />
                              En este momento no es posible mostrar el componente.
                              Intente nuevamente más tarde.
                            </div>
                          </div>
                        </div>
                      </div>
                    </>
                  )}
                </div>
              </>
            );
       }
    } else {
      return (
        <>
          <div className="content-container">
            <TopNav
              app={this.props.app}
              nombre={this.state.nombreMenu}
              ruta={this.state.rutaMenu}
            />
            {this.state.loading ? (
              <div className="container col-12 mt-4">
                <Loader />
              </div>
            ) : (
              <>
                {this.state.errorMessages ? this.renderErrorModal() : null}
                <div className="main">
                  {this.state.sectores && this.state.sectores.length ? (
                    this.renderSections(
                      this.state.sectores,
                      this.state.acciones,
                    )
                  ) : (
                    <div className="container">
                      <div className="row">
                        <div className="col-12 text-center mt-5">
                          <div className="alert alert-warning">
                            <i className="fi fi-rs-triangle-warning mr-3" />
                            En este momento no es posible mostrar el componente.
                            Intente nuevamente más tarde.
                          </div>
                        </div>
                      </div>
                    </div>
                  )}
                </div>
              </>
            )}
          </div>
        </>
      );
    }
  }

  renderSections(sectores, actions) {
    const filtersSection = sectores.find((s) => s.tipologia === "FILTROS");
    const action = actions.find((a) => a.tipo !== "DESCARGA");
    const arregloAtributos = filtersSection.columnas;

    var arrayAtributos = [];
    
    arregloAtributos.map((item) => {
      if (item.filtros.length == 2) {
        arrayAtributos.push(item.filtros[0]);
        arrayAtributos.push(item.filtros[1]);
      } else if (item.filtros.length == 3) {
        arrayAtributos.push(item.filtros[0]);
        arrayAtributos.push(item.filtros[1]);
        arrayAtributos.push(item.filtros[2]);
      } else {
        arrayAtributos.push(item.filtros[0]);
      }
    });
    
    return (
      <div style={{ height: this.props.altura, maxHeight: this.props.altura }}>
        {this.props.componentes ? (
          
          <div className="filtros bg-white shadow-sm">
            <div className="container">
              <div className="row">
                <div className="d-flex justify-content-between align-items-center flex-wrap">
                  <div className="d-flex justify-content-start align-items-center flex-xl-row flex-lg-row flex-md-row flex-sm-row flex-column container-filter-search filter-header">
                    <h2 className="global-title-1 m-0" style={{width: "100%"}}>
                      {this.props.componentes.descripcion}
                    </h2>
                  </div>
                  <div className="d-flex  align-items-center buttons-filtros button-header">
                    <ul className="nav d-flex justify-content-between align-items-center">
                      {this.elementoDescargar(this.props.componentes.acciones)}
                      {this.state.filtros && (
                        <button
                          className="ml-2 btn-round btn-round-secondary"
                          onClick={(e) => this.handleShowPivotPanelModal()}
                        >
                          <i className="fi fi-rs-filter" />
                        </button>
                      )}
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        ) : (
          <Atributos
            atributos={arrayAtributos}
            acciones={this.state.acciones}
            sectores={this.state.sectores}
            componentId={this.state.componentId}
            id={this.state.id}
            filtersSection={filtersSection}
            handleDescargar={(e) => this.handleDescargar(e)}
            action={action}
            tipologia="REPORTES"
          />
        )}
        <div style={{ height: "90%", maxHeight: this.props.altura, padding: "10px 0" }}>
          {this.props.loadingData ? (
            <div className="col-12 mt-3">
              <Loader />
            </div>
          ) : (
            <div className="misas" style={{ height: "80%", maxHeight: this.props.altura }}>
              {this.props.componentes &&
              this.props.componentes.contenedor === "S" ? (
                <>
                  {this.props.tipologiaInfo
                    ? this.renderViews(this.props.sectores)
                    : 
                    !store.getState().ejecutado
                      ? null
                      :
                      <div className="row">
                        <div className="col-12 text-center mt-5">
                          <div className="alert alert-warning">
                            <i className="fi fi-rs-triangle-warning mr-3" />
                            No hay información para mostrar.
                          </div>
                        </div>
                      </div>
                    }
                </>
              ) : (
                <>
                  {this.state.sectores 
                    ? this.renderViews(this.state.sectores)
                    : 
                    !store.getState().ejecutado
                      ? null 
                      :
                      <div className="row">
                        <div className="col-12 text-center mt-5">
                          <div className="alert alert-warning">
                            <i className="fi fi-rs-triangle-warning mr-3" />
                            No hay información para mostrar.
                          </div>
                        </div>
                      </div>
                  }
                </>
              )}
            </div>
          )}
        </div>

        {this.renderPivotPanelModal()}
      </div>
    );
  }

  renderPivotPanelModal() {
    const viewsSections = this.state.sectores.filter(
      (s) => s.tipologia === "VISTA" || s.tipologia === "TEMPORAL",
    );

    return (
      this.state.showPivotPanelModal && (
        <div className="custom-modal custom-modal-big">
          <div className="custom-modal-content">
            <div className="custom-modal-header">
              <div className="row d-flex justify-content-between align-items-center">
                <div>
                  <h6> Filtros avanzados </h6>
                </div>
                <div>
                  <button
                    className="btn btn-close"
                    onClick={() => this.handleCancelPivotPanelModal()}
                  >
                    <i className="fi fi-rs-cross" />
                  </button>
                </div>
              </div>
            </div>
            <div className="right-body bg-white text-center">
              {viewsSections.map((section) => {
                return this.props.dataObInfo[section.id] ? (
                  <div key={section.id}>
                    <Olap.PivotPanel
                      itemsSource={this.state.engine[section.id]}
                    />
                  </div>
                ) : null;
              })}
            </div>
            <div className="modal-footer">
              <button
                className="ml-2 btn btn-secondary"
                onClick={() => this.handleCancelPivotPanelModal()}
              >
                Cancelar
              </button>
              <button
                className="ml-2 btn btn-primary"
                wj-part="btn-update"
                onClick={() => this.handleSubmitPivotPanelModal()}
              >
                Aplicar
              </button>
            </div>
          </div>
        </div>
      )
    );
  }

  renderPivotGridPanel() {
    return `<div class="root">  
                <div class="wrapper">
                    <div class="left-side">
                        <div class="field-list-label">
                            <label wj-part="g-flds"></label>  
                        </div>  
                        <div class="field-list">  
                            <div wj-part="d-fields"></div>  
                        </div>  
                    </div>
                    <div class="right-side">
                        <div class="drag-areas-label">  
                            <label wj-part="g-drag"></label>  
                        </div>  
                        <div class="sections">
                            <div class="section filter-list">  
                                <label>  
                                    <span class="wj-glyph wj-glyph-filter"></span>   
                                    <span wj-part="g-flt"></span>  
                                </label>  
                                <div wj-part="d-filters"></div>  
                            </div>  
                            <div class="section column-list">  
                                <label>  
                                    <span class="wj-glyph">?</span>   
                                    <span wj-part="g-cols"></span>  
                                </label>  
                                <div wj-part="d-cols"></div>  
                            </div>  
                            <div class="section row-list">  
                                <label>  
                                    <span class="wj-glyph">=</span>   
                                    <span wj-part="g-rows"></span>  
                                </label>  
                                <div wj-part="d-rows"></div>  
                            </div>  
                            <div class="section values-list">  
                                <label>  
                                    <span class="wj-glyph">S</span>   
                                    <span wj-part="g-vals"></span>  
                                </label>  
                                <div wj-part="d-vals"></div>  
                            </div>  
                        </div>
                    </div>
                </div>
                <div wj-part="d-prog"></div>  
                <div class="control-area">  
                    <label>  
                        <input wj-part="chk-defer" type="checkbox"/>   
                        <span wj-part="g-defer">Defer Updates</span>  
                    </label>  
                    <button wj-part="btn-update" class="wj-btn wj-state-disabled" type="button" disabled>
                        Aplicar  
                    </button>  
                </div>  
            </div>`;
  }

  armarGrillaPivot() {
    const data = this.props.dataObInfo;
    const section = this.state.sectores.find(
      (s) => s.id === this.state.acciones[0].refrescos[0].objeto,
    );

    const fields = section.visualizacion.atributos.filter(
      (attr) => attr.visible === "S",
    );
    const rowFields = [];
    const valueFields = [];
    const engine = this.state.engine;

    data[this.state.acciones[0].refrescos[0].objeto] =
      this.props.dataObInfo.map((item) => {
        const atributos = {};

        fields.forEach((attr) => {
          if (item[attr.id]) {
            atributos[attr.descripcion] = item[attr.id];
          } else {
            atributos[attr.descripcion] = "";
          }
        });

        return atributos;
      });

    fields.forEach((field) => {
      switch (field.analisis) {
        case "MEDIDA":
          valueFields.push(field.descripcion);
          break;
        case "DIMENSION":
          rowFields.push(field.descripcion);
          break;
        default:
          break;
      }
    });

    wjcOlap.PivotPanel.controlTemplate = this.renderPivotGridPanel();

    engine[this.state.acciones[0].refrescos[0].objeto] =
      new wjcOlap.PivotEngine({
        itemsSource: data[this.state.acciones[0].refrescos[0].objeto],
        valueFields: valueFields,
        rowFields: rowFields,
        showZeros: true,
      });

    fields.forEach((field) => {
      var agrupacion = "None";

      if (field.agrupacion !== "") {
        agrupacion = field.agrupacion;
      }

      engine[this.state.acciones[0].refrescos[0].objeto].fields.getField(
        field.descripcion,
      ).align = field.alineacion;
      engine[this.state.acciones[0].refrescos[0].objeto].fields.getField(
        field.descripcion,
      ).format = field.mascara;
      engine[this.state.acciones[0].refrescos[0].objeto].fields.getField(
        field.descripcion,
      ).aggregate = agrupacion;
    });
  }

  renderGrillaPivot(section) {
    return (
      <>
        <div key={section.id} className=" mb-4">
          <div className="d-flex justify-content-between">
            <h2 className="global-title-2 m-0">{section.descripcion}</h2>

            {!this.props.componentes ? (
              <div style={{ marginBottom: "0.5rem" }}>
                {Object.entries(this.props.tipologiaInfo.get(section.id)).length !== 0 ? (
                  <>
                    <Tooltip
                      content="Exportar PDF"
                      placement="bottom"
                      className="global-link-1 btn-acciones text-center"
                      onClick={this.exportPDF.bind(this)}
                      arrow={15}
                      background="#000"
                      color="#fff"
                      padding={13}
                      radius={8}
                    >
                      <i className="fi fi-rs-file-pdf"></i>
                    </Tooltip>
                  </>
                ) : null}
              </div>
            ) : null}
          </div>
          <Olap.PivotGrid
            itemsSource={this.state.engine[section.id]}
            initialized={this.initializePivotGrid.bind(this)}
          />
        </div>
      </>
    );
  }

  renderViews(sectores) {
    
    const viewsSections = sectores.filter(
      (s) => s.tipologia === "VISTA" || s.tipologia === "TEMPORAL",
    );
    return (
      <div style={{ height: "100%", maxHeight: "100%" }}>
        <div className="row" style={{height: "100%", maxHeight: "100%", width: "100%"}}>
          {this.props.loadingData ? (
            <div className="col-12" style={{ height: "100%", maxHeight: "100%" }}>
              <Loader />
            </div>
          ) : (
            <div className="col-12" style={{ height: "100%", maxHeight: "100%" }}>
              {viewsSections.map((section) => {
               
                switch (section.modalidad) {
                  case "PIVOT":
                    return (
                      <div key={section.id}>
                        {this.props.tipologiaInfo.get(section.id)  ? (
                          <>
                            {this.armarGrillaPivot()}
                            {this.renderGrillaPivot(section)}
                          </>
                        ) : null}
                      </div>
                    );
                  case "GRILLA":
                    return this.props.dataObInfo ? (
                      <div  style={{ height: "100%", maxHeight: "100%" }} key={section.id}>
                        {this.renderGrilla(section)}
                      </div>
                    ) : <div>No data</div>;
                  case "CAJA":
                    return this.props.dataObInfo ? (
                      <div  style={{ height: "100%", maxHeight: "100%" }} key={section.id}>
                        {this.renderCaja(section)}
                      </div>
                    ) : <div>No data</div>;
                  case "GRAFICO":
                    return this.renderGrafico(section.medidas[0]);
                  default:
                }
              })} 
            </div>
          )}
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  dataObInfo: state.dataObInfo,
  tipologiaInfo: state.tipologiaInfo,
  cabecerasGrilla: state.cabecerasGrilla,
  objetoReportes: state.objetoReportes,
  rerender: state.rerender,
  loadingData: state.loadingData,
  agrupadosReportes: state.agrupadosReportes
});

export default connect(mapStateToProps)(Reportes);
