// Generic
import React, { Component } from "react";
import Konva, { Stage, Layer, Circle, Label, Tag, Text, Star, Rect } from "react-konva";

import { TabPanel, Tab } from "wijmo/wijmo.react.nav";
import { FlexGrid, FlexGridColumn } from "wijmo/wijmo.react.grid";
import { CollectionView } from "wijmo/wijmo";
import Input from "../../GlobalComponents/Input";
import BootstrapTable from "react-bootstrap-table-next";




//Components
import Loader from "../../GlobalComponents/Loader";
import store from "../../../redux/store";
import URLS from "../../../urls";
import StringMask from "string-mask";


// Css
import "../Content.css";





class VisualizacionMapa extends Component {
  constructor(props) {
    super(props);
    
  
    
  
    this.state = {
      checkedStructure: [],
      expandedStructure: [],
      selectedStructure: null,
      selectedDefinicion: null,
      seleccionsAttributes: {},
      seleccionsData: {},
      selectedIndicator: "",
      selectedLegends: [],
      loadingSeleccion: true,
      escalaX: 1,
      escalaY: 1
    };
    
  }
  componentDidMount() {


    this.fetchDefinitionData(this.props.idDefinicion);
    

    

  }
 
  async fetchDefinitionData(id) {
    
    var myHeaders = new Headers();
    myHeaders.append(
      "Authorization",
      "Bearer " + sessionStorage.getItem("token"),
    );

    var requestOptions = {
      method: "GET",
      headers: myHeaders,
      redirect: "follow",
    };

    await fetch(
      store.getState().serverUrl + URLS.MAPAS_OBTENER_INFORMACION + id,
      requestOptions,
    )
      .then((response) => {
        if (response.status == 401) {
          sessionStorage.clear();
          window.location.reload(false);
        }else{
          return response.json();
        }
      })
      .then((result) => {
        const background = new Image();
        background.src = "data:image/jpeg;base64," + result.imagen;
        result.background = background;
        this.setState({
          selectedDefinicion: result,
        });
        this.fetchSeleccions();
      })
      .catch((error) => console.log("error", error));
  }
  
  fetchSeleccions() {
    const definition = this.state.selectedDefinicion;
    const seleccionsAttributes = this.state.seleccionsAttributes;
    const seleccionsData = this.state.seleccionsData;
    const myHeaders = new Headers();
    myHeaders.append(
      "Authorization",
      "Bearer " + sessionStorage.getItem("token"),
    );
    myHeaders.append("Content-Type", "application/json");

    var filters;
    var raw;
    var requestOptions;
   
    this.props.Selecciones.forEach((seleccion) => {
      
      filters = seleccion.visualizacion.relacionados.map((s) => {
        return {
          atributo: s.relacionado,
          contenido: definition[s.atributo],
        };
      });

      raw = JSON.stringify({
        idObjeto: parseInt(seleccion.visualizacion.id),
        idOpcion: parseInt(this.props.idOpcion),
        filtros: {
          busquedas: filters,
        },
      });

      requestOptions = {
        method: "POST",
        headers: myHeaders,
        body: raw,
        redirect: "follow",
      };

      fetch(
        store.getState().serverUrl + URLS.MAPAS_OBTENER_DISPONIBLES,
        requestOptions,
      )
        .then((response) => {
          if (response.status == 401) {
            sessionStorage.clear();
            window.location.reload(false);
          }else{
            return response.json();
          }
        })    
        .then((result) => {
          seleccionsAttributes[seleccion.descripcion] =
            seleccion.visualizacion.atributos;
          seleccionsData[seleccion.descripcion] = result;

          

          this.setState({
            loadingSeleccion: false,
            seleccionsAttributes: seleccionsAttributes,
            seleccionsData: seleccionsData,
          });

          if (
            seleccionsData &&
            seleccionsData.Indicadores &&
            seleccionsData.Leyendas
          ) {
            this.handleSelectIndicator(seleccionsData.Indicadores[0].id);
          }
        })
        .catch((error) => console.log("error", error));
    });
  }

  fetchIndicators(indicator) {
    const myHeaders = new Headers();
    myHeaders.append(
      "Authorization",
      "Bearer " + sessionStorage.getItem("token"),
    );
    myHeaders.append("Content-Type", "application/json");

    const raw = JSON.stringify({
      idObjeto: parseInt(this.props.Selecciones[1].id),
      idOpcion: parseInt(this.props.idOpcion),
      filtros: {
        generales: {
          asignacion: this.state.selectedDefinicion.asignacion,
          indicador: indicator,
          alternativo: "",
        },
      },
    });

    const requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: raw,
      redirect: "follow",
    };

    fetch(
      store.getState().serverUrl + URLS.MAPAS_OBTENER_INDICADORES,
      requestOptions,
    )
      .then((response) => {
        if (response.status == 401) {
          sessionStorage.clear();
          window.location.reload(false);
        }else{
          return response.json();
        }
      })
      .then((result) => {
        this.setState({
          legendsElements: result.componentes.leyendas,
        });

        this.handleLegendsElements(this.state.selectedLegends);
      })
      .catch((error) => console.log("error", error));
  }
  handleLegendsElements(selectedLegends) {
    const definition = this.state.selectedDefinicion;

    if (definition.elementos) {
      definition.elementos.forEach((element) => {
        element.variables.fill = "#ffffff90";
        element.variables.stroke = "#500000";
      });

      this.state.legendsElements.forEach((legend) => {
        if (selectedLegends.includes(parseInt(legend.id))) {
          legend.elementos.forEach((element) => {
            let elemento = definition.elementos.find(
              (e) => e.id === element.id,
            );
            if (elemento) {
              elemento.variables.fill = legend.relleno;
              elemento.variables.stroke = legend.contorno;
              elemento.hint = element.hint;
            }
          });
        }
      });

      this.setState({
        selectedDefinicion: definition,
      });
    }

    this.setState({
      loadingSeleccion: false,
    });
  }

  handleOpenLeyendsModal() {
    this.setState({
      showLegendsModal: true,
    });
  }

  handleCloseLeyendsModal() {
    this.setState({
      showLegendsModal: false,
    });
  }

  handleSubmitLeyendsModal(selectedLegends) {
    this.setState({
      showLegendsModal: false,
      selectedLegends: selectedLegends,
    });

    this.handleLegendsElements(selectedLegends);
  }
  handleSelectIndicator(indicator) {
    const legends = this.state.seleccionsData.Leyendas.filter(
      (legend) => legend["1007017002"] === parseInt(indicator),
    );
    const selectedLegends = legends.map((legend) => legend.id);

    this.setState({
      loadingSeleccion: true,
      legends: legends,
      selectedIndicator: indicator,
      selectedLegends: selectedLegends,
    });

    this.fetchIndicators(indicator);
  }

  handleClosePolygonInfoModal() {
    this.setState({
      showPolygonInfoModal: false,
      selectedPolygon: null,
    });
  }

  handlePolygonDblClick(e) {
    const definition = this.state.selectedDefinicion;
    const polygon = definition.elementos.find(
      (obj) => obj.id === e.currentTarget.attrs.id,
    );

    this.setState({
      showPolygonInfoModal: true,
      selectedPolygon: polygon,
    });
  }

  handlePolygonEnter(e) {
    const definition = this.state.selectedDefinicion;
    const polygon = definition.elementos.find(
      (obj) => obj.id === e.currentTarget.attrs.id,
    );

    const center = {};


    center.x = polygon.variables.x;
    center.y = polygon.variables.y;

    polygon.center = center;
    polygon.position = "down";

    if (center.y < 30) {
      polygon.position = "up";
    }
    if (center.x < 100) {
      polygon.position = "left";
    } else if (center.x > 1100) {
      polygon.position = "right";
    }

    this.setState({
      overPolygon: polygon,
      selectedDefinicion: definition,
    });
  }

  handlePolygonLeave() {
    this.setState({
      overPolygon: null,
    });
  }
  resizePanel(){
    var element = "container-stage-" + this.props.idDefinicion;
    var height = document.getElementById(element).clientHeight;
    var width = document.getElementById(element).clientWidth;
    var scalex = width / 1280;
    var scaley = height / 720;
    this.setState({
      escalaX: scalex,
      escalaY: scaley
    })
  }
  
  renderMaps() {
    
    const idStage = "container-stage-" + this.props.idDefinicion;
    return this.state.selectedDefinicion ? (

            <div className="container-stage" id={idStage} tabIndex={1}>
               <Stage
                  className="konvas-stage"
                  container={idStage}
                  width={1280}//1280 //991
                  height={720}//720
                  scaleX = {this.state.escalaX}
                  scaleY = {this.state.escalaY}
                >
                <Layer id="layer-background" className="konvas-layer background"> 
                  <Konva.Image
                    x={0} 
                    y={0}
                    image={this.state.selectedDefinicion.background}
                    width={1280}//1280 //991
                    height={720}//720
                  />
                </Layer>
                <Layer id="layer-polygons" className="konvas-layer polygons">
                  {this.state.selectedDefinicion.elementos &&
                  this.state.selectedDefinicion.elementos.map((item, key) => {
                    
                  return (
                    item.variables.forma == "C" ?
                    <Circle
                      key={key}
                      id={item.id}
                      name={item.variables.descripcion}
                      radius = {20}
                      x={parseInt(item.variables.x)}
                      y={parseInt(item.variables.y)}
                      stroke={item.variables.stroke}
                      strokeWidth={1}
                      fill={item.variables.fill}
                      
                      onDblClick={(e) => this.handlePolygonDblClick(e)}
                      onMouseEnter={(e) => this.handlePolygonEnter(e)}
                      onMouseLeave={() => this.handlePolygonLeave()}
                    />
                    : 
                    item.variables.forma == "E" ?
                    <Star
                      key={key}
                      id={item.id}
                      name={item.variables.descripcion}
                      innerRadius={10}
                      outerRadius={20}
                      x={parseInt(item.variables.x)}
                      y={parseInt(item.variables.y)}
                      closed
                      stroke={item.variables.stroke}
                      strokeWidth={1}
                      fill={item.variables.fill}
                      onDblClick={(e) => this.handlePolygonDblClick(e)}
                      onMouseEnter={(e) => this.handlePolygonEnter(e)}
                      onMouseLeave={() => this.handlePolygonLeave()}
                    /> 
                    : 
                    item.variables.forma == "R" ? 
                    <Rect
                      key={key}
                      id={item.id}
                      name={item.variables.descripcion}
                      width={30}
                      height={30}
                      x={parseInt(item.variables.x)}
                      y={parseInt(item.variables.y)}
                      closed
                      stroke={item.variables.stroke}
                      strokeWidth={1}
                      fill={item.variables.fill}
                      onDblClick={(e) => this.handlePolygonDblClick(e)}
                      onMouseEnter={(e) => this.handlePolygonEnter(e)}
                      onMouseLeave={() => this.handlePolygonLeave()}
                    />
                    :
                    <></>
                  );
                })}
                {this.state.overPolygon && (
                  <Label
                    x={parseInt(this.state.overPolygon.center.x)}
                    y={parseInt(this.state.overPolygon.center.y)}
                  >
                  <Tag
                    fill="DarkOrange"
                    pointerDirection={this.state.overPolygon.position}
                    pointerWidth={5}
                    pointerHeight={5}
                    lineJoin="round"
                  />
                  {this.state.overPolygon.hint ? (
                    <Text
                      text={this.state.overPolygon.hint}
                      fontFamily="Trebuchet MS"
                      fontStyle="bold"
                      fontSize={13}
                      padding={10}
                      fill="Black"
                    />
                ) : (
                  //Provisorio, en caso de error
                    <Text
                      text={this.state.overPolygon.descripcion}
                      fontFamily="Trebuchet MS"
                      fontStyle="bold"
                      fontSize={13}
                      padding={10}
                      fill="Black"
                    />
                )}
                </Label>
              )}
              </Layer>
             </Stage>
            </div>


    ) : (
      <Loader />
    );
}

  render() {
    return this.state.selectedDefinicion ? (
      <div className="box shadow overflow-auto mt-3 ">
       
           
        <div className="col-xl-12 col-lg-12 col-sm-12" >
          <div className="box shadow mt-3" >
            <div class="col-12 d-flex justify-content-between align-items-center flex-wrap">
              <div class="d-flex justify-content-start align-items-center flex-xl-row flex-lg-row flex-md-row flex-sm-row flex-column container-filter-search">
                <h2 className="global-title-1 m-0">
                  {this.state.selectedDefinicion.nombre}
                </h2>
                {this.state.seleccionsData && (
              <div className="navbar-menu-selectors">
                {this.state.seleccionsData.Indicadores &&
                  this.state.seleccionsData.Leyendas && (
                    <select
                      className="form-control selector"
                      name="Indicadores"
                      onChange={(e) =>
                        this.handleSelectIndicator(e.target.value)
                      }
                    >
                      {this.state.seleccionsData.Indicadores.map((i) => {
                        return (
                          <option key={i.id} value={i.id}>
                            {" "}
                            {i[1007001004]}{" "}
                          </option>
                        );
                      })}
                    </select>
                  )}

                <button
                  className="btn-round btn-round-secondary filter"
                  disabled={!this.state.selectedIndicator}
                  onClick={() => this.handleOpenLeyendsModal()}
                >
                  <i className="fi fi-rs-filter" />
                </button>
                <button onClick={ () => this.resizePanel()}>Resize</button>

                {this.state.seleccionsData.Leyendas &&
                  this.state.seleccionsAttributes.Leyendas &&
                  this.state.showLegendsModal && (
                    <LegendsModal
                      atributos={this.state.seleccionsAttributes.Leyendas}
                      data={this.state.legends}
                      handleClose={() => this.handleCloseLeyendsModal()}
                      handleSubmit={(selectedLegends) =>
                        this.handleSubmitLeyendsModal(selectedLegends)
                      }
                      selectedRowsId={this.state.selectedLegends}
                      title="Seleccionar leyendas"
                    />
                  )}
              </div>
            )}
              </div>
            </div>
            
              {this.renderMaps()}                
         </div>
       </div>
     

        {this.state.showPolygonInfoModal && (
          <PolygonInfoModal
            handleClose={() => this.handleClosePolygonInfoModal()}
            idObjeto={this.props.idOpcion}
            panels={this.props.Paneles}
            polygon={this.state.selectedPolygon}
          />
        )}
      </div>
    ) : null;
 }
       
}

export default VisualizacionMapa;


class PolygonInfoModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {},
      selectItem: {},
    };
   
  }

  componentDidMount() {
    const data = {};
    const selectItem = {};

    this.props.panels.forEach((panel) => {
      panel.sectores.forEach((sector) => {
        switch (sector.tipologia) {
          case "FILTROS":
            selectItem[JSON.stringify(sector.id)] = {};
            sector.columnas.forEach((column) => {
              column.filtros.forEach((item) => {
                selectItem[JSON.stringify(sector.id)][item.id] = "";
              });
            });
            break;

          case "VISTA":
            data[JSON.stringify(sector.id)] = null;
            selectItem[JSON.stringify(sector.id)] = {};
            break;
          default:
            break;
        }
      });
    });

    this.setState({
      data: data,
      selectItem: selectItem,
    });

    this.fetchPolygonInfo(this.props.panels[0].sectores[0]);
  }

  fetchPolygonInfo(sector) {
    const myHeaders = new Headers();
    myHeaders.append(
      "Authorization",
      "Bearer " + sessionStorage.getItem("token"),
    );
    myHeaders.append("Content-Type", "application/json");

    var selectedAllObjects = true;

    const filters = sector.visualizacion.relacionados.map((s) => {
      var contenido = this.props.polygon[s.atributo];

      if (s.objeto) {
        if (
          this.state.selectItem[JSON.stringify(s.objeto)] !== undefined &&
          this.state.selectItem[JSON.stringify(s.objeto)][s.atributo] !==
            undefined
        ) {
          contenido =
            this.state.selectItem[JSON.stringify(s.objeto)][s.atributo];
        } else {
          selectedAllObjects = false;
        }
      }

      return {
        atributo: s.relacionado,
        contenido: contenido,
      };
    });

    const raw = JSON.stringify({
      idObjeto: parseInt(sector.visualizacion.id),
      idOpcion: parseInt(this.props.idObjeto),
      filtros: {
        busquedas: filters,
      },
    });

    const requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: raw,
      redirect: "follow",
    };
    
    if (selectedAllObjects) {
      fetch(
        store.getState().serverUrl + URLS.MAPAS_OBTENER_DISPONIBLES,
        requestOptions,
      )
        .then((response) => {
          if (response.status == 401) {
            sessionStorage.clear();
            window.location.reload(false);
          }else{
            return response.json();
          }
        })    
        .then((result) => {
          const data = this.state.data;

          data[JSON.stringify(sector.id)] = new CollectionView(result);
          data[JSON.stringify(sector.id)].currentItem = null;

          this.setState({
            data: data,
          });

          if (sector.modalidad === "ATRIBUTOS") {
            const selectItem = this.state.selectItem;
            selectItem[JSON.stringify(sector.id)] = result[0];

            this.setState({
              selectItem: selectItem,
            });

            if (sector.acciones) {
              sector.acciones.forEach((action) => {
                if (
                  action.tipo === "SELECCION" &&
                  selectItem[JSON.stringify(sector.id)] !== undefined &&
                  selectItem[JSON.stringify(sector.id)][action.id] !== undefined
                ) {
                  this.handleSelectAction(action);
                }
              });
            }
          }
        })
        .catch((error) => console.log("error", error));
    } else {
      const data = this.state.data;
      data[JSON.stringify(sector.id)] = new CollectionView([]);
      data[JSON.stringify(sector.id)].currentItem = null;

      this.setState({
        data: data,
      });
    }
  }

  handleSelectAction(action) {
    action.refrescos.forEach((r) => {
      this.props.panels.forEach((panel) => {
        const sector = panel.sectores.find((s) => s.id === r.objeto);

        if (sector) {
          this.fetchPolygonInfo(sector);
        }
      });
    });
  }

  handleRowSelection(sector) {
    const sectorId = JSON.stringify(sector.id);

    if (
      sector.acciones &&
      sector.acciones.find((a) => a.tipo === "SELECCION")
    ) {
      sector.acciones.forEach((action) => {
        if (
          this.state.data[sectorId].currentItem &&
          action.tipo === "SELECCION"
        ) {
          const selectItem = this.state.selectItem;
          selectItem[sectorId] = this.state.data[sectorId].currentItem;

          this.setState({
            selectItem: selectItem,
          });

          this.handleSelectAction(action);
        }
      });
    }
  }

  handleChangeFilter(e, sectorId) {
    if ( e.target !== undefined ) {
    const { value, name } = e.target;
    const selectItem = this.state.selectItem;

    selectItem[sectorId][name] = value;

    this.setState({
      selectItem: selectItem,
    });
    }
  }

  handleSubmitFilter(sector) {
    sector.acciones.forEach((action) => {
      this.handleSelectAction(action);
    });
  }

  render() {
    return (
      <div className="right-overlay">
        <div className="right-content right-content-big bg-white shadow-lg">
          <div className="right-header">
            <div className="row d-flex justify-content-between align-items-center">
              <h6> Informacion - {this.props.polygon.descripcion} </h6>
              <div>
                <button
                  className="btn btn-close"
                  onClick={() => this.props.handleClose()}
                >
                  <i className="fi fi-rs-cross"></i>
                </button>
              </div>
            </div>
          </div>

          <div className="right-body bg-white">{this.renderTabPanel()}</div>

          <div className="right-footer d-flex justify-content-center align-items-stretch">
            <button
              disabled={this.state.buttonDisable}
              className="btn btn-primary"
              onClick={() => this.props.handleClose()}
            >
              Cerrar
            </button>
          </div>
        </div>
      </div>
    );
  }

  renderTabPanel() {
    return (
      <TabPanel>
        {this.props.panels
          ? this.props.panels.map((panel) => {
              return (
                <Tab key={panel.id}>
                  <span> {panel.nombre} </span>

                  {this.renderPanel(panel)}
                </Tab>
              );
            })
          : null}
      </TabPanel>
    );
  }

  renderPanel(panel) {
    return (
      <div>
        <h5 className="mt-2"> {panel.titulo} </h5>
        {panel.sectores.map((sector) => {
          switch (sector.modalidad) {
            case "ATRIBUTOS":
              return this.renderAtributos(sector);

            case "GRILLA":
              return this.renderGrilla(sector);

            case "GRAFICO":
              return this.renderGrafico(sector);

            default:
              return null;
          }
        })}
      </div>
    );
  }

  renderAtributos(sector) {
    switch (sector.tipologia) {
      case "VISTA":
        return (
          <div
            key={sector.id}
            className="d-flex flex-column justify-content-between w-100 mb-4 mt-4"
          >
            <h6> {sector.descripcion} </h6>

            {this.state.selectItem[JSON.stringify(sector.id)] !== undefined ? (
              sector.visualizacion.atributos.map((art) => {
                return (
                  <Input
                    arregloAtributos={art}
                    alineacion={art.alineacion}
                    ancho={art.ancho}
                    atr_valor={art.atr_valor}
                    atr_nombre={art.atr_nombre}
                    decimales={art.decimales}
                    descripcion={art.descripcion}
                    editable={"N"}
                    estilo={art.estilo}
                    id={art.id}
                    key={art.id}
                    longitud={art.longitud}
                    mascara={art.mascara}
                    nombre={art.nombre}
                    orden={art.orden}
                    origen={art.origen}
                    prefijo={art.prefijo}
                    sufijo={art.sufijo}
                    tipo={art.tipo}
                    tipologia={art.tipologia}
                    value={
                      this.state.selectItem[JSON.stringify(sector.id)][art.id]
                    }
                    visible={"S"}
                    onChange={() => {}}
                  />
                );
              })
            ) : (
              <Loader />
            )}
          </div>
        );

      case "FILTROS":
        return (
          <div
            key={sector.id}
            className="d-flex flex-column justify-content-between w-100 mb-4 mt-4"
          >
            <h6> {sector.descripcion} </h6>

            {sector.columnas.map((item) => {
              return (
                <div className="filter" key={item.id}>
                  {item.filtros.map((art) => {
                    var value = "";

                    if (
                      this.state.selectItem !== undefined &&
                      this.state.selectItem[JSON.stringify(sector.id)] !==
                        undefined &&
                      this.state.selectItem[JSON.stringify(sector.id)][art.id]
                    ) {
                      value =
                        this.state.selectItem[JSON.stringify(sector.id)][
                          art.id
                        ];
                    }

                    return (
                      <Input
                        alineacion={art.alineacion}
                        ancho={art.ancho}
                        atr_nombre={art.atr_nombre}
                        atr_valor={art.atr_valor}
                        atributo={art.id}
                        decimales={art.decimales}
                        descripcion={art.descripcion}
                        editable={art.editable}
                        estilo={art.estilo}
                        id={art.id}
                        key={art.id}
                        longitud={art.longitud}
                        mascara={art.mascara}
                        maximo={art.maximo}
                        minimo={art.minimo}
                        nombre={art.nombre}
                        obligatorio={art.obligatorio}
                        observaciones={art.observaciones}
                        onChange={(e) =>
                          this.handleChangeFilter(e, JSON.stringify(sector.id))
                        }
                        orden={art.orden}
                        origen={art.origen}
                        tipo={art.tipo}
                        tipologia={art.tipologia}
                        value={value}
                        visible={art.visible}
                      />
                    );
                  })}
                </div>
              );
            })}

            <button
              className="btn btn-primary"
              onClick={() => this.handleSubmitFilter(sector)}
            >
              {" "}
              Aplicar{" "}
            </button>
          </div>
        );
      default:
        return null;
    }
  }

  renderGrilla(sector) {
    const sectorId = JSON.stringify(sector.id);

    return (
      <div
        key={sector.id}
        className="d-flex flex-column justify-content-between w-100 mb-4 mt-4"
      >
        <h6> {sector.descripcion} </h6>

        {this.state.data !== undefined &&
        this.state.data[sectorId] !== undefined ? (
          <FlexGrid
            itemsSource={this.state.data[sectorId]}
            deferResizing={true}
            showMarquee={false}
            preserveSelectedState={true}
            selectionMode={"RowRange"}
            onSelectionChanged={() => this.handleRowSelection(sector)}
          >
            {sector.visualizacion.atributos
              .filter((attr) => attr.visible === "S")
              .map((attr) => {
                var ancho = attr.porcentaje;
                if (!ancho.includes("*")){
                  ancho = Number(ancho); 
                }
                return (
                  <FlexGridColumn
                    key={attr.orden}
                    binding={attr.id}
                    header={attr.descripcion}
                    isReadOnly={true}
                    format={attr.mascara}
                    width={ancho}
                    align={attr.alineacion}
                    //wordWrap={true}
                  />
                );
              })}
          </FlexGrid>
        ) : (
          <Loader />
        )}
      </div>
    );
  }

  renderGrafico(sector) {
    return (
      <div
        key={sector.id}
        className="d-flex flex-column justify-content-between w-100 mb-4 mt-4"
      >
        <h6> {sector.descripcion} </h6>
        GRAFICO
      </div>
    );
  }
}


class LegendsModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      columns: null,
      selectedRowsId: [],
    };
  }

  componentDidMount() {
    this.setState({
      selectedRowsId: this.props.selectedRowsId,
    });

    this.configColumns();
  }

  configColumns() {
    function columnFormatter(cell, row, rowIndex, formatExtraData) {
      var result = cell;

      if (formatExtraData.tipology === "COLOR") {
        return (
          <svg className="legends-colors">
            <rect className="legends-rectangle-colors" style={{ fill: cell }} />
          </svg>
        );
      } else {
        if (result === undefined) {
          return <span> {cell} </span>;
        } else {
          if (formatExtraData.type === "NUMERICO" && formatExtraData.mask) {
            const formatter = new StringMask(formatExtraData.mask, {
              reverse: true,
            });

            if (formatExtraData.decimals) {
              const number = JSON.stringify(result).split(".");

              if (number[1]) {
                number[1] =
                  number[1] +
                  new Array(formatExtraData.decimals - number[1].length)
                    .fill(0)
                    .join("");
              } else {
                number[1] = new Array(formatExtraData.decimals)
                  .fill(0)
                  .join("");
              }

              result = number.join("");
            }

            result = formatter.apply(result);
          }

          return <span> {result} </span>;
        }
      }
    }

    const columns = [];

    this.props.atributos.forEach((art) => {
      if (art.tipologia === "COLOR") {
        var formatData = {
          tipology: art.tipologia,
        };
      } else {
        if (art.mascara.split(".")[1] === undefined) {
          var decimales = 0;
        } else {
          var decimales = art.mascara.split(".")[1].length;
        }

        var formatData = {
          type: art.tipo,
          mask: art.mascara,
          decimals: parseInt(decimales),
        };
      }

      columns.push({
        id: art.id,
        dataField: art.id,
        formatter: columnFormatter,
        formatExtraData: formatData,
        text: art.descripcion,
        sort: true,
        align: art.alineacion,
        headerAlign: "center",
        hidden: art.visible === "N",
        headerStyle: { width: art.porcentaje + "%", wordBreak: "break-word" },
        style: { width: art.porcentaje + "%", wordBreak: "break-word" },
        tipologia: art.tipologia,
        visible: art.visible,
      });
    });
    this.setState({
      columns: columns,
    });
  }

  handleOnSelect = (row) => {
    const selectedRowsId = this.state.selectedRowsId;

    // Si ya estaba seleccionada, se elimina.
    if (selectedRowsId && selectedRowsId.includes(row.id)) {
      const rowsId = selectedRowsId.filter((id) => id !== row.id);
      this.setState({
        selectedRowsId: rowsId,
      });

      // Si no estaba seleccionada, se agrega.
    } else {
      selectedRowsId.push(row.id);

      this.setState({
        selectedRowsId: selectedRowsId,
      });
    }
  };

  handleOnSelectAll = () => {
    // Si hay alguna seleccionada, se eliminan todas.
    if (this.state.selectedRowsId && this.state.selectedRowsId.length) {
      this.setState({
        selectedRowsId: [],
      });

      // Si no hay alguna ninguna seleccionada, se agregan todas.
    } else {
      this.setState({
        selectedRowsId: this.props.data.map((legend) => legend.id),
      });
    }
  };

  handleClose() {
    this.props.handleClose();
  }

  handleSubmit() {
    this.props.handleSubmit(this.state.selectedRowsId);
  }

  renderButtons() {
    return (
      <div className="right-footer d-flex justify-content-center align-items-stretch">
        <button
          className="btn btn-secondary mr-2"
          onClick={() => this.handleClose()}
        >
          <span> Cancelar </span>
        </button>
        <button className="btn btn-primary" onClick={() => this.handleSubmit()}>
          <span> Aplicar </span>
        </button>
      </div>
    );
  }

  render() {
    const selectRow = {
      mode: "checkbox",
      clickToSelect: true,
      selected: this.state.selectedRowsId,
      onSelect: (row) => this.handleOnSelect(row),
      onSelectAll: () => this.handleOnSelectAll(),
    };

    return (
      <div className="right-overlay">
        <div className="right-content right-content-big bg-white shadow-lg">
          <div className="right-header">
            <div className="row d-flex justify-content-between align-items-center">
              <div>
                <h6>{this.props.title}</h6>
              </div>
              <div>
                <button
                  className="btn btn-close"
                  onClick={() => this.props.handleClose()}
                >
                  <i className="fi fi-rs-cross" />
                </button>
              </div>
            </div>
          </div>

          <div className="right-body bg-white">
            <div className="row">
              <div className="col-lg-12">
                {this.state.columns && this.props.data && (
                  <div className="col-12 mt-4">
                    <BootstrapTable
                      keyField="id"
                      data={this.props.data}
                      columns={this.state.columns}
                      striped
                      hover
                      headerClasses="header-class"
                      rowStyle={{ fontSize: "14px" }}
                      selectRow={selectRow}
                    />
                  </div>
                )}
              </div>
            </div>
          </div>
          {this.renderButtons()}
        </div>
      </div>
    );
  }
  
}
