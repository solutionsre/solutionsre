// Generic
import React, { Component } from "react";
import BootstrapTable from "react-bootstrap-table-next";
import ToolkitProvider from "react-bootstrap-table2-toolkit";
import paginationFactory from "react-bootstrap-table2-paginator";
import { connect } from "react-redux";
import StringMask from "string-mask";

// Components
import URLS from "../../../urls";
import TopNav from "../TopNav";
import Filtros from "../Filtros";
import Loader from "../../GlobalComponents/Loader";

// Css
import "../Content.css";
import store from "../../../redux/store";

// Images

class Mantenimientos extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      atributos: [],
      componentId: "",
      descripcion: "",
      habilitado: "",
      id: this.props.id,
      nombre: "",
      objeto: "",
      observaciones: "",
      tipologia: "",
      visualizacion: [],
      nombreMenu: "",
      rutaMenu: "",
      columns: [],
      rows: [],
      selectedRowsId: [],
      selectedRows: [],
      filtrosAvanzados: [],
      edicion: "",
    };
    this.fetchDataRows = this.fetchDataRows.bind(this);
    this.fetchDataFiltros = this.fetchDataFiltros.bind(this);
    this.handleChangeFiltroAvanzado =
      this.handleChangeFiltroAvanzado.bind(this);
    this.handleSendFiltrosAvanzados =
      this.handleSendFiltrosAvanzados.bind(this);
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props.id !== prevProps.id) {
      this.setState({
        loading: true,
        id: this.props.id,
        columns: [],
        rows: [],
      });
      this.fetchConfig();
    }
    if (
      prevState.selectedRows !== this.state.selectedRows &&
      !this.state.selectedRows
    ) {
      this.setState({
        selectedRowsId: [],
      });
    }
  }

  componentWillMount() {
    const urls = {
      agregar: URLS.MANTENIMIENTOS_AGREGAR,
      editar: URLS.MANTENIMIENTOS_MODIFICAR,
      eliminar: URLS.MANTENIMIENTOS_ELMINAR,
      importar: URLS.MANTENIMIENTOS_IMPORTAR,
      modelo: URLS.MANTENIMIENTOS_MODELO,
      visualizar: URLS.MANTENIMIENTOS_VISUALIZAR,
      descargar: URLS.MANTENIMIENTOS_DESCARGAR,
    };

    this.setState({
      urls: urls,
      id: this.props.id,
      selectedRows: null,
    });
    this.fetchConfig();
  }

  componentWillReceiveProps() {
    this.setState({
      selectedRows: null,
    });
    this.fetchDataRows();
  }

  fetchConfig() {
    var myHeaders = new Headers();
    myHeaders.append(
      "Authorization",
      "Bearer " + sessionStorage.getItem("token"),
    );
    myHeaders.append("Content-Type", "application/json");

    var requestOptions = {
      method: "GET",
      headers: myHeaders,
      redirect: "follow",
    };
    fetch(
      store.getState().serverUrl +
        URLS.MANTENIMIENTOS_CONFIGURACION +
        this.props.id,
      requestOptions,
    )
    .then((response) => {
      if (response.status == 401) {
        sessionStorage.clear();
        window.location.reload(false);
      }else{
        return response.json();
      }
    })
    .then((result) => {
      
        this.setState({
          atributos: result.componentes.atributos,
          acciones: result.componentes.acciones,
          componentId: result.componentes.id,
          descripcion: result.componentes.descripcion,
          habilitado: result.componentes.habilitado,
          nombre: result.componentes.nombre,
          objectSelectedId: result.componentes.atributos[0].id,
          opcion: result.componentes.opcion,
          observaciones: result.componentes.observaciones,
          tipologia: result.componentes.tipologia,
          visualizacion: result.componentes.visualizacion,
          nombreMenu: result.componentes.menu,
          rutaMenu: result.componentes.ruta,
          edicion: result.componentes.edicion,
        });

        this.fetchConfigColumns();
      })
      .catch((error) => console.log("error", error));
  }

  fetchConfigColumns() {
    function columnFormatter(cell, row, rowIndex, formatExtraData) {
      var result = cell;

      if (formatExtraData.type === "NUMERICO" && formatExtraData.mask) {
        var numeral = require("numeral");

        let decimales = "";

        for (var i = 0; i < formatExtraData.mask; i++) {
          decimales = decimales + "0";
        }
        
        if (decimales === "") {
          result = numeral(result).format("0");
        }else{
          result = numeral(result).format("0." + decimales);
        }
        /* result = formatter.apply(result); */
      }

      return <span> {result} </span>;
    }

    const columns = [];

    this.state.visualizacion.atributos.forEach((art) => {
      

      const formatData = {
        type: art.tipo,
        mask: art.mascara.substr(1, art.mascara.length),
        decimals: parseInt(art.longitud.split(",")[1]),
      };
      
      if (art.tipo != 'FECHA') {
        columns.push({
          align: art.alineacion,
          dataField: art.id,
          formatter: columnFormatter,
          formatExtraData: formatData,
          headerAlign: "center",
          headerStyle: { width: art.porcentaje + "%", wordBreak: "break-word" },
          hidden: art.visible !== "S",
          id: art.id,
          sort: true,
          style: { width: art.porcentaje + "%", wordBreak: "break-word" },
          text: art.descripcion,
          classes: art.truncado,
          uniqueData: art.id,        
        });
      }else{
        columns.push({
          align: art.alineacion,
          dataField: art.id,
          formatter: columnFormatter,
          formatExtraData: formatData,
          headerAlign: "center",
          headerStyle: { width: art.porcentaje + "%", wordBreak: "break-word" },
          hidden: art.visible !== "S",
          id: art.id,
          sort: true,
          style: { width: art.porcentaje + "%", wordBreak: "break-word" },
          text: art.descripcion,
          classes: art.truncado,
          uniqueData: art.id,        
          sortFunc: (a, b, order ) => {
            let primero = 0;
            let segundo = 0;
            if (a != null)
              primero = Date.parse(a.split("/").reverse().join("/"));
            if (b != null)
              segundo = Date.parse(b.split("/").reverse().join("/"));
            if (order === "asc")
              return  primero - segundo;
            else 
              return  segundo - primero;
          },
        });
      }
    });

    this.setState({
      columns: columns,
    });

    this.fetchDataRows();
  }

  fetchDataRows() {
    var myHeaders = new Headers();
    myHeaders.append(
      "Authorization",
      "Bearer " + sessionStorage.getItem("token"),
    );
    myHeaders.append("Content-Type", "application/json");

    var raw = JSON.stringify({ idObjeto: parseInt(this.state.componentId) });
    var requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: raw,
      redirect: "follow",
    };
    fetch(
      store.getState().serverUrl + URLS.OBTENER_INFORMACION,
      requestOptions,
    )
    .then((response) => {
      if (response.status == 401) {
        sessionStorage.clear();
        window.location.reload(false);
      }else{
        return response.json();
      }
    })
    .then((result) => {
        this.setState({
          rows: result,
          loading: false,
        });
      })
      .catch((error) => console.log("error", error));
  }

  fetchDataFiltros() {
    var myHeaders = new Headers();
    myHeaders.append("Authorization", "Bearer " + sessionStorage.getItem("token"));
    myHeaders.append("Content-Type", "application/json");

    var atributos = Object.entries(this.state.filtrosAvanzados).map((item) => {
      return {
        atributo: item[0],
        contenido: item[1],
      };
    });

    var raw = JSON.stringify({
      idObjeto: parseInt(this.state.componentId),
      filtros: {
        avanzados: atributos,
      },
    });

    var requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: raw,
      redirect: "follow",
    };

    fetch(
      store.getState().serverUrl + URLS.OBTENER_INFORMACION,
      requestOptions,
    )
    .then((response) => {
      if (response.status == 401) {
        sessionStorage.clear();
        window.location.reload(false);
      }else{
        return response.json();
      }
    })
    .then((result) => {
        this.setState({
          rows: result,
          loading: false,
        });
      })
      .catch((error) => console.log("error", error));
  }

  handleSendFiltrosAvanzados() {
    this.fetchDataFiltros();
  }

  handleOnSelect = (row) => {
    const rows = this.state.selectedRows || [];

    // Si ya estaba seleccionada, se elimina.
    if (rows && rows.includes(row)) {
      const r = rows.filter((r) => r !== row);
      this.setState({
        selectedRows: r,
        selectedRowsId: r.map((r) => r.id),
      });

      // Si no estaba seleccionada, se agrega.
    } else {
      rows.push(row);
      this.setState({
        selectedRows: rows,
        selectedRowsId: rows.map((r) => r.id),
      });
    }
  };

  handleOnSelectAll = (rows) => {
    // Si hay alguna seleccionada, se eliminan todas.
    if (this.state.selectedRows && this.state.selectedRows.length) {
      this.setState({
        selectedRows: [],
        selectedRowsId: [],
      });

      // Si no hay alguna ninguna seleccionada, se agregan todas.
    } else {
      this.setState({
        selectedRows: this.state.rows,
        selectedRowsId: this.state.rows.map((r) => r.id),
      });
    }
  };

  // Esta función pasa al componente Filtros para obtener los datos de los inputs del formulario de Filtros Avanzados
  handleChangeFiltroAvanzado(e) {
    const { value, name } = e.target;
    const prevState = this.state;

    this.setState({
      atributos: {
        ...prevState.atributos,
        [name]: value,
      },
    });
  }

  handleOnChangeFiltrosMultiple(e) {
    const { value, name } = e.target;
    const prevState = this.state;
    var checkboxs;

    if (prevState.filtrosAvanzados[name] === "") {
      checkboxs = prevState.filtrosAvanzados[name].split("");
    } else {
      checkboxs = prevState.filtrosAvanzados[name].split(",");
    }

    // Si ya estaba seleccionada, se elimina.
    if (checkboxs.includes(value)) {
      this.setState({
        filtrosAvanzados: {
          ...prevState.filtrosAvanzados,
          [name]: checkboxs.filter((v) => v !== value).toString(),
        },
      });

      // Si no estaba seleccionada, se agrega.
    } else {
      checkboxs.push(value);
      this.setState({
        filtrosAvanzados: {
          ...prevState.filtrosAvanzados,
          [name]: checkboxs.toString(),
        },
      });
    }
  }

  render() {
    return (
      <div id="sectores" className="content-container">
        <TopNav
          app={this.props.app}
          nombre={this.state.nombreMenu}
          ruta={this.state.rutaMenu}
        />
        {this.state.loading ? (
          <div className="container col-12 mt-4">
            <Loader />
          </div>
        ) : (
          this.renderData()
        )}
      </div>
    );
  }

  renderData() {
    const selectRow = {
      mode: "checkbox",
      clickToSelect: true,
      style: { backgroundColor: "#FF7F32", color: "#1F1006" },
      selected: this.state.selectedRowsId,
      onSelectAll: this.handleOnSelectAll,
      onSelect: this.handleOnSelect,
    };
    const MySearch = (props) => {
      let input;
      const handleClick = () => {
        props.onSearch(input.value);
      };
      return (
        <div className="search-group search-group-300 mr-2" key={4898794654654}>
          <input
            className="form-control input-search"
            ref={(n) => (input = n)}
            type="text"
            onChange={handleClick}
          />
          <button className="button-search">
            <i className="fi fi-rs-search"></i>
          </button>
        </div>
      );
    };

    const optionsPagination = {
      hideSizePerPage: true, // Hide the sizePerPage dropdown always
      showTotal: false,
      disablePageTitle: false,
      sizePerPageList: [{ value: 13 }],
    };

    const rowStyle = { fontSize: "14px" };
    return this.state.columns.length ? (
      <ToolkitProvider
        keyField="id"
        data={this.state.rows}
        columns={this.state.columns}
        search={<MySearch />}
      >
        {(props) => (
          <div className="main">
            <Filtros
              search={
                <MySearch
                  className="input-search"
                  placeholder="Buscar"
                  {...props.searchProps}
                />
              }
              acciones={this.state.acciones}
              atributos={this.state.atributos}
              handleChangeFiltroAvanzado={this.handleChangeFiltroAvanzado}
              handleSendFiltrosAvanzados={this.handleSendFiltrosAvanzados}
              onChangeMultiple={(e) => this.handleOnChangeFiltrosMultiple(e)}
              menu="mantenimientos"
              objectId={this.state.componentId}
              objectSelectedId={this.state.objectSelectedId}
              opcion={this.state.opcion}
              selectedRows={this.state.selectedRows}
              urls={this.state.urls}
              visualizacion={this.state.visualizacion}
              formatos={this.state.formatos}
              size="big"
              edicion={this.state.edicion}
              showButtonAdd={true}
            ></Filtros>

            <div className="container row" id="contenedorSearch">
              <div className="col-12 mt-4">
                <BootstrapTable
                  striped
                  hover
                  headerClasses="header-class"
                  rowStyle={rowStyle}
                  selectRow={selectRow}
                  pagination={paginationFactory(optionsPagination)}
                  {...props.baseProps}
                />
              </div>
            </div>
          </div>
        )}
      </ToolkitProvider>
    ) : null;
  }
}

const mapStateToProps = (state) => ({
  rerender: state.rerender,
});

export default connect(mapStateToProps)(Mantenimientos);
