// Generic
import React, { Component } from "react";
import "wijmo/wijmo.touch";
import { FlexGrid, FlexGridColumn } from "wijmo/wijmo.react.grid";
import { CollectionView } from "wijmo/wijmo";
import { Selector } from "wijmo/wijmo.grid.selector";

// Components
import store from "../../../redux/store";
import URLS from "../../../urls";
import TopNav from "../TopNav";
import Loader from "../../GlobalComponents/Loader";
import Filtros from "../Filtros";

// Images

// Css
import "../Content.css";
import "wijmo/styles/wijmo.css";

//set Spanish culture
import "wijmo/cultures/wijmo.culture.es-MX";

class Conversiones extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      acciones: [],
      atributos: [],
      componentId: "",
      descripcion: "",
      habilitado: "",
      id: this.props.id,
      nombre: "",
      observaciones: "",
      sectores: null,
      tipologia: "",
      nombreMenu: "",
      rutaMenu: "",
      toggle: true,
      loadingData: false,
      data: {},
      engine: {},
      showPivotPanelModal: false,
      filtros: false,
      descargarOpciones: false,
      descargarLoading: false,
      modalDescargarShow: false,
      descargarData: false,
      seleccion: false,
      editar: false,
      atributoPrincipal: false,
      dataModal: false,
      selectedRows: [],
      idCabecera: "",
      renderizar: false,
    };
    this.renderizar = this.renderizar.bind(this);
    this.renderEditarBoton = this.renderEditarBoton.bind(this);
    this.showModal = this.showModal.bind(this);
    this.editarModal = this.editarModal.bind(this);
    this.onSelectionChanged = this.onSelectionChanged.bind(this);
    this.renderGrilla = this.renderGrilla.bind(this);
    this.fetchDataFiltros = this.fetchDataFiltros.bind(this);
    this.handleChangeFiltroAvanzado =
      this.handleChangeFiltroAvanzado.bind(this);
    this.handleSendFiltrosAvanzados =
      this.handleSendFiltrosAvanzados.bind(this);
  }

  componentDidMount() {
    this.fetchConfig();
  }

  componentWillMount() {
    const urls = {
      agregar: URLS.CONVERSIONES_AGREGAR,
      editar: URLS.CONVERSIONES_MODIFICAR,
      eliminar: URLS.CONVERSIONES_ELIMINAR,
      importar: URLS.CONVERSIONES_IMPORTAR,
      modelo: URLS.CONVERSIONES_MODELO,
      visualizar: URLS.CONVERSIONES_VISUALIZAR,
      descargar: URLS.CONVERSIONES_DESCARGAR,
    };

    this.setState({
      urls: urls,
      id: this.props.id,
      selectedRows: null,
    });
    this.fetchConfig();
  }

  componentDidUpdate(prevProps) {
    if (this.props.id !== prevProps.id) {
      this.setState({
        data: {},
        loading: true,
      });
    }
  }

  fetchConfig() {
    var myHeaders = new Headers();
    myHeaders.append(
      "Authorization",
      "Bearer " + sessionStorage.getItem("token"),
    );
    myHeaders.append("Content-Type", "application/json");

    var requestOptions = {
      method: "GET",
      headers: myHeaders,
      redirect: "follow",
    };

    fetch(
      store.getState().serverUrl +
        URLS.CONVERSIONES_CONFIGURACION +
        this.props.id,
      requestOptions,
    )
      .then((response) => {
        if (response.status == 401) {
          sessionStorage.clear();
          window.location.reload(false);
        }else{
          return response.json();
        }
      })
      .then((result) => {
        /* if (result.componentes.sectores && result.componentes.sectores.length) {
          result.componentes.sectores
            .find((s) => s.tipologia === "FILTROS")
            .columnas.forEach((column) => {
              column.filtros.forEach((attr) => {
                atributos[attr.id] = "";
              });
            });
        } */
        
        this.setState({
          descripcion: result.componentes.descripcion,
          formatos: result.componentes.formatos,
          habilitado: result.componentes.habilitado,
          id: result.componentes.id,
          componentId: result.componentes.id,
          nombreMenu: result.componentes.menu,
          modalidad: result.componentes.modalidad,
          nombre: result.componentes.nombre,
          observaciones: result.componentes.observaciones,
          opcion: result.componentes.opcion,
          rutaMenu: result.componentes.ruta,
          sectores: result.componentes.sectores,
          tipologia: result.componentes.tipologia,
          objectSelectedId:
            result.componentes.sectores[1].visualizacion.atributos[0].id,
          loading: false,
        });

        this.obtenerResultadosFinal();
      })
      .catch((error) => console.log("error", error));
  }

  obtenerResultadosFinal() {
    var myHeaders = new Headers();
    myHeaders.append(
      "Authorization",
      "Bearer " + sessionStorage.getItem("token"),
    );
    myHeaders.append("Content-Type", "application/json");

    var raw = JSON.stringify({
      idObjeto: parseInt(this.state.sectores[0].id),
      idOpcion: parseInt(this.props.id),
    });

    var requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: raw,
      redirect: "follow",
    };

    fetch(store.getState().serverUrl + URLS.OBTENER_INFORMACION, requestOptions)
      .then((response) => {
        if (response.status == 401) {
          sessionStorage.clear();
          window.location.reload(false);
        }else{
          return response.json();
        }
      })
      .then((result) => {
        var data = [];
        data[JSON.stringify(this.state.sectores[0].id)] = new CollectionView(
          result,
        );

        this.setState({
          data: data,
        });
      })
      .catch((error) => console.log("error", error));
  }

  initGrid(flexGrid) {
    this.flexGrid = flexGrid;
    /* this.flexGrid.onSelectionChanged(null); */
    this.setState({
      grilla: flexGrid,
    });
  }

  renderGrilla(sector) {
    console.log("sector", sector);
    console.log("props", this.props);
    console.log("state", this.state);
    const sectorId = JSON.stringify(sector.id);

    return (
      <div
        key={sector.id}
        className="d-flex flex-column justify-content-between w-100 pb-4 pt-4"
      >
        <h6> {sector.descripcion} </h6>

        {this.state.sectores ? (
          <FlexGrid
            initialized={this.initGrid.bind(this)}
            itemsSource={this.state.data[sectorId]}
            deferResizing={true}
            showMarquee={true}
            preserveSelectedState={true}
            allowPinning="SingleColumn"
            selectionChanged={this.onSelectionChanged}
            allowSorting={true}
          >
            {sector.visualizacion.atributos
              .filter((attr) => attr.visible === "S")
              .map((attr) => {
                var ancho = attr.porcentaje;
                if (!ancho.includes("*")){
                  ancho = Number(ancho); 
                }
                return (
                  <FlexGridColumn
                    key={attr.orden}
                    binding={attr.id}
                    header={attr.descripcion}
                    isReadOnly={true}
                    width={ancho}
                    format={attr.mascara}
                    align={attr.alineacion}
                  ></FlexGridColumn>
                );
              })}
          </FlexGrid>
        ) : (
          <Loader />
        )}
      </div>
    );
  }

  onSelectionChanged(selected) {
    this.setState({
      seleccion: selected,
      idCabecera: selected.selectedRows[0].dataItem.id,
    });

    const sectorId = JSON.stringify(this.state.sectores[0].id);
    const atrValor = JSON.stringify(5001002003);
    const atrNombre = JSON.stringify(5001002002);

    this.setState({
      /* idCabecera: selected.selectedRows[0].dataItem.id, */
      rowSelected:
        this.state.data[sectorId]._view[this.flexGrid.selection._row],
      atrr_valorSelect:
        this.state.data[sectorId]._view[this.flexGrid.selection._row][atrValor],
      atrr_nombreSelect:
        this.state.data[sectorId]._view[this.flexGrid.selection._row][
          atrNombre
        ],
    });
  }

  editarModal() {
    if (this.state.seleccion) {
      return (
        <div className="d-flex justify-content-end mr-5">
          <button
            onClick={this.renderEditarBoton}
            className="btn btn-primary mr-4 mt-3"
          >
            <span>Configuracion de Conversion</span>
          </button>
        </div>
      );
    }
  }

  renderEditarBoton() {
    this.obtenerResultadosModal();
    this.setState({
      editar: true,
    });
  }

  obtenerResultadosModal() {
    var myHeaders = new Headers();
    myHeaders.append(
      "Authorization",
      "Bearer " + sessionStorage.getItem("token"),
    );
    myHeaders.append("Content-Type", "application/json");

    this.state.sectores[0].visualizacion.atributos.map((attr) => {
      if (attr.clave === "PRINCIPAL") {
        this.setState({
          atributoPrincipal: attr.id,
        });
      }
    });
    var raw = JSON.stringify({
      idObjeto: parseInt(this.state.sectores[1].id),
      idOpcion: parseInt(this.props.id),
      idCabecera: this.state.idCabecera,
      filtros: {},
    });

    var requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: raw,
      redirect: "follow",
    };

    fetch(
      store.getState().serverUrl + URLS.OBTENER_INFORMACION,
      requestOptions,
    )
      .then((response) => {
        if (response.status == 401) {
          sessionStorage.clear();
          window.location.reload(false);
        }else{
          return response.json();
        }
      })

      .then((result) => {
        var dataModal = [];
        dataModal[JSON.stringify(this.state.sectores[1].id)] =
          new CollectionView(result);

        this.setState({
          dataModal: dataModal,
        });
      })
      .catch((error) => console.log("error", error));
  }

  initGridModal(flexGrid) {
    this.selector = new Selector(flexGrid, {
      itemChecked: (s, e) => {
        this.setState({
          selectedItems: flexGrid.rows.filter((r) => r.isSelected),
        });
      },
    });

    this.flexGrid = flexGrid;
    /* this.flexGrid.onSelectionChanged(null); */
    this.setState({
      grilla: flexGrid,
    });
  }
  renderizar() {
    this.obtenerResultadosModal();
    this.setState((prevState) => ({
      renderizar: !prevState.renderizar,
    }));
  }

  showModal() {
    if (this.state.editar) {
      return (
        <div className="custom-modal custom-modal-big">
          <div className="custom-modal-content">
            <div className="custom-modal-header">
              <div className="row d-flex justify-content-between align-items-center">
                <div>
                  <h6> Conversiones </h6>
                </div>
                <div>
                  <button
                    className="btn btn-close"
                    onClick={() => this.handleOpcionesClose()}
                  >
                    <i className="fi fi-rs-cross" />
                  </button>
                </div>
              </div>
            </div>
            {this.state.loading ? (
              <div className="container col-12 mt-4">
                <Loader />
              </div>
            ) : (
              <div>
                <div>
                  <Filtros
                    acciones={this.state.sectores[1].acciones}
                    atributos={this.state.sectores[1].atributos}
                    handleChangeFiltroAvanzado={this.handleChangeFiltroAvanzado}
                    handleSendFiltrosAvanzados={this.handleSendFiltrosAvanzados}
                    menu="mantenimientos"
                    objectId={this.state.sectores[1].id}
                    idCabecera={this.state.idCabecera}
                    objectSelectedId={this.state.objectSelectedId}
                    opcionId={this.state.opcion}
                    opcion={this.state.opcion}
                    selectedRows={this.state.selectedItems}
                    urls={this.state.urls}
                    visualizacion={this.state.sectores[1].visualizacion}
                    formatos={this.state.sectores[1].formatos}
                    size="big"
                    grillaTipo="wijmo"
                    renderizar={this.renderizar}
                    showButtonAdd={true}
                  ></Filtros>
                  <div
                    className="col-12 container-grilla main"
                    key={this.state.sectores.id}
                  >
                    {this.renderGrillaModal(this.state.sectores[1])}
                  </div>
                </div>
              </div>
            )}
          </div>
        </div>
      );
    }
  }

  renderGrillaModal(sector) {
    const sectorId = JSON.stringify(sector.id);
    if (this.state.dataModal !== false) {
      return (
        <>
          {this.state.loading ? (
            <div className="container col-12 mt-4">
              <Loader />
            </div>
          ) : (
            <div
              key={sector.id}
              className="d-flex flex-column justify-content-between w-100 mb-4 mt-4"
            >
              <h6> {sector.descripcion} </h6>

              {this.state.sectores ? (
                <FlexGrid
                  initialized={this.initGridModal.bind(this)}
                  itemsSource={this.state.dataModal[sectorId]}
                  deferResizing={true}
                  showMarquee={true}
                  preserveSelectedState={true}
                  allowPinning="SingleColumn"
                  allowSorting={true}
                >
                  {sector.visualizacion.atributos
                    .filter((attr) => attr.visible === "S")
                    .map((attr) => {
                      var ancho = attr.porcentaje;
                      if (!ancho.includes("*")){
                        ancho = Number(ancho); 
                      }    
                      return (
                        <FlexGridColumn
                          key={attr.orden}
                          binding={attr.id}
                          header={attr.descripcion}
                          isReadOnly={true}
                          width={ancho}
                          format={attr.mascara}
                          align={attr.alineacion}
                        ></FlexGridColumn>
                      );
                    })}
                </FlexGrid>
              ) : (
                <Loader />
              )}
            </div>
          )}
        </>
      );
    }
  }

  handleOpcionesClose() {
    this.setState((prevState) => ({
      editar: !prevState.editar,
      /* seleccion: !prevState.seleccion, */
    }));
  }

  handleShowPivotPanelModal() {
    this.setState({
      showPivotPanelModal: true,
    });
  }

  fetchDataFiltros() {
    var myHeaders = new Headers();
    myHeaders.append("Authorization", "Bearer " + sessionStorage.getItem("token"));
    myHeaders.append("Content-Type", "application/json");

    var atributos = Object.entries(this.state.filtrosAvanzados).map((item) => {
      return {
        atributo: item[0],
        contenido: item[1],
      };
    });

    var raw = JSON.stringify({
      idObjeto: parseInt(this.state.componentId),
      filtros: {
        avanzados: atributos,
      },
    });

    var requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: raw,
      redirect: "follow",
    };

    fetch(
      store.getState().serverUrl + URLS.OBTENER_INFORMACION,
      requestOptions,
    )
      .then((response) => {
        if (response.status == 401) {
          sessionStorage.clear();
          window.location.reload(false);
        }else{
          return response.json();
        }
      })
      .then((result) => {
        this.setState({
          rows: result,
          loading: false,
        });
      })
      .catch((error) => console.log("error", error));
  }

  handleSendFiltrosAvanzados() {
    this.fetchDataFiltros();
  }

  handleOnSelect = (row) => {
    const rows = this.state.selectedRows || [];

    // Si ya estaba seleccionada, se elimina.
    if (rows && rows.includes(row)) {
      const r = rows.filter((r) => r !== row);
      this.setState({
        selectedRows: r,
        selectedRowsId: r.map((r) => r.id),
      });

      // Si no estaba seleccionada, se agrega.
    } else {
      rows.push(row);
      this.setState({
        selectedRows: rows,
        selectedRowsId: rows.map((r) => r.id),
      });
    }
  };

  handleOnSelectAll = () => {
    // Si hay alguna seleccionada, se eliminan todas.
    if (this.state.selectedRows && this.state.selectedRows.length) {
      this.setState({
        selectedRows: [],
        selectedRowsId: [],
      });

      // Si no hay alguna ninguna seleccionada, se agregan todas.
    } else {
      this.setState({
        selectedRows: this.state.rows,
        selectedRowsId: this.state.rows.map((r) => r.id),
      });
    }
  };

  // Esta función pasa al componente Filtros para obtener los datos de los inputs del formulario de Filtros Avanzados
  handleChangeFiltroAvanzado(e) {
    const { value, name } = e.target;
    const prevState = this.state;

    // Manejo de tipos de divisas
    if (prevState.filtrosAvanzados[name] && name === "1006001006") {
      const checkboxs = prevState.filtrosAvanzados[name].split(",");

      // Si ya estaba seleccionada, se elimina.
      if (checkboxs.includes(value)) {
        this.setState({
          filtrosAvanzados: {
            ...prevState.filtrosAvanzados,
            [name]: checkboxs.filter((v) => v !== value).toString(),
          },
        });

        // Si no estaba seleccionada, se agrega.
      } else {
        checkboxs.push(value);
        this.setState({
          filtrosAvanzados: {
            ...prevState.filtrosAvanzados,
            [name]: checkboxs.toString(),
          },
        });
      }

      // El resto de los filtros se sobre escriben
    } else {
      this.setState({
        filtrosAvanzados: {
          ...prevState.filtrosAvanzados,
          [name]: value,
        },
      });
    }
  }

  render() {
    return (
      <>
        {this.showModal()}

        <div className="content-container" id="sectores">
          <TopNav
            app={this.props.app}
            nombre={this.state.nombreMenu}
            ruta={this.state.rutaMenu}
          />
          {this.state.loading ? (
            <div className="container col-12 mt-4">
              <Loader />
            </div>
          ) : (
            <>
              {this.state.sectores && (
                <>
                  {this.editarModal()}
                  <div
                    className="col-12 container-grilla main"
                    key={this.state.sectores.id}
                  >
                    {this.renderGrilla(this.state.sectores[0])}
                  </div>
                </>
              )}
            </>
          )}
        </div>
      </>
    );
  }
}

export default Conversiones;
