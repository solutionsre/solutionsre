// Generic
import React, { Component, Fragment } from "react";
import store from "../../../redux/store";
import CheckboxTree from "react-checkbox-tree";
import Loader from "../../GlobalComponents/Loader";
import 'flexlayout-react/style/light.css';

// Components
import URLS from "../../../urls";
import TopNav from "../TopNav";

// Css
import "../Content.css";
import LayOutFlex from "./LayOutFlex";

// Images

class Visualizaciones extends Component {
  constructor(props) {
    super(props);
    this.state = {
      checkedStructure: [],
      expandedStructure: [],
      selectedStructure: null,
      selectedDefinicion: null,
      seleccionsAttributes: {},
      seleccionsData: {},
      selectedIndicator: "",
      selectedLegends: [],
      loadingSeleccion: true,
    };
  }

  componentDidMount() {
    this.fetchConfig();
  }

  async fetchConfig() {
    var myHeaders = new Headers();
    myHeaders.append(
      "Authorization",
      "Bearer " + sessionStorage.getItem("token"),
    );
    myHeaders.append("Content-Type", "application/json");

    var requestOptions = {
      method: "GET",
      headers: myHeaders,
      redirect: "follow",
    };

    await fetch(
      store.getState().serverUrl + URLS.MAPAS_ESTRUCTURA + this.props.id,
      requestOptions,
    )
      .then((response) => {
        if (response.status == 401) {
          sessionStorage.clear();
          window.location.reload(false);
        }else{
          return response.json();
        }
      })
      .then((result) => {
        this.setState({
          atributos: result.componentes.atributos,
          acciones: result.componentes.acciones,
          componentId: result.componentes.id,
          descripcion: result.componentes.descripcion,
          estructura: result.componentes.estructura,
          habilitado: result.componentes.habilitado,
          nombre: result.componentes.nombre,
          observaciones: result.componentes.observaciones,
          paneles: result.componentes.paneles,
          selecciones: result.componentes.selecciones,
          tipologia: result.componentes.tipologia,
          nombreMenu: result.componentes.menu,
          rutaMenu: result.componentes.ruta,
          editDefinitionAtributos: [
            result.componentes.atributos.find(
              (attr) => attr.nombre === "NOMBRE",
            ),
            result.componentes.atributos.find(
              (attr) => attr.nombre === "OBSERVACIONES",
            ),
          ],
        });

        this.handleStructuresTreeNodes();
      })
      .catch((error) => console.log("error", error));
  }

  async fetchDefinitionData(id) {
    var myHeaders = new Headers();
    myHeaders.append(
      "Authorization",
      "Bearer " + sessionStorage.getItem("token"),
    );

    var requestOptions = {
      method: "GET",
      headers: myHeaders,
      redirect: "follow",
    };

    await fetch(
      store.getState().serverUrl + URLS.MAPAS_OBTENER_INFORMACION + id,
      requestOptions,
    )
      .then((response) => {
        if (response.status == 401) {
          sessionStorage.clear();
          window.location.reload(false);
        }else{
          return response.json();
        }
      })
      .then((result) => {
        const background = new Image();
        background.src = "data:image/jpeg;base64," + result.imagen;
        result.background = background;

        this.setState({
          selectedDefinicion: result,
        });

        this.fetchSeleccions();
      })
      .catch((error) => console.log("error", error));
  }


  handleStructuresTreeNodes() {
    var nodes = [];

    if (this.state.estructura) {
      nodes = this.state.estructura.map((pais) => {
        if (pais.estructura) {
          const childrens = pais.estructura.map((componente) => {
            if (componente.estructura) {
              const childrens = componente.estructura.map((planta) => {
                return {
                  label: planta.descripcion,
                  value: [
                    pais.referencia,
                    componente.referencia,
                    planta.referencia,
                  ].join("-"),
                };
              });

              return {
                label: componente.descripcion,
                value: [pais.referencia, componente.referencia].join("-"),
                children: childrens,
              };
            } else {
              return {
                label: componente.descripcion,
                value: [pais.referencia, componente.referencia].join("-"),
              };
            }
          });

          return {
            label: pais.descripcion,
            value: pais.referencia,
            children: childrens,
          };
        } else {
          return {
            label: pais.descripcion,
            value: pais.referencia,
          };
        }
      });
    }

    this.setState({
      structuresTreeNodes: nodes,
    });
  }

  handleSelectStructureNode(e) {
    const value = e.value;

    if (value !== this.state.checkedStructure[0]) {
      const originsArray = value.split("-");
      const structure = this.handleFindStructure(originsArray);
      structure.originsArray = originsArray;

      this.setState({
        checkedStructure: [value],
        selectedStructure: structure,
        selectedDefinicion: null,
      });
      //{structure.definiciones.map((definition) => {
      //  this.handleSelectDefinition(definition.asignacion)
     // })}
    }
  }

  handleSelectCheckboxStructureNode(e) {
    if (e.length) {
      const value = e.find(
        (item) => !this.state.checkedStructure.includes(item),
      );

      if (value !== this.state.checkedStructure[0]) {
        const originsArray = value.split("-");
        const structure = this.handleFindStructure(originsArray);
        structure.originsArray = originsArray;

        this.setState({
          checkedStructure: [value],
          selectedStructure: structure,
          selectedDefinicion: null,
        });
       
      }
    }
  }

  handleFindStructure(originsArray) {
    var structure = this.state.estructura;

    originsArray.forEach((referencia) => {
      if (structure.estructura) {
        structure = structure.estructura.find(
          (s) => s.referencia === referencia,
        );
      } else {
        structure = structure.find((s) => s.referencia === referencia);
      }
    });
   
    return structure;
  }

  handleSelectDefinition(id) {
    this.fetchDefinitionData(id);
  }

  handleCloseMenu() {
    this.setState({
      selectedDefinicion: null,
      selectedStructure: null
    });
  }


  render() {
    return (
      <div className="content-container">
        <TopNav
          app={this.props.app}
          nombre={this.state.nombreMenu}
          ruta={this.state.rutaMenu}
        />


                {this.state.selectedStructure && this.state.selectedStructure.definiciones  ? (
                  this.renderMenu()
                ) : (
                  <div className="content-paneles">
                    {this.state.estructura && this.state.structuresTreeNodes ? (
                      <Fragment>
                        {this.renderEstructuras()}


                      
                      </Fragment>
                    ) : (
                      <Loader />
                    )}
                  </div>
                )}
             

      </div>
    );
  }

  renderEstructuras() {
    return this.state.structuresTreeNodes ? (
      <div className="structures-content">
        <h2 className="global-title-2 mb-4">Estructuras</h2>
        <div className="structures-list">
          <CheckboxTree
            nodes={this.state.structuresTreeNodes}
            iconsClass="fa5"
            checked={this.state.checkedStructure}
            checkModel={false}
            expanded={this.state.expandedStructure}
            noCascade={true}
            onClick={(e) => this.handleSelectStructureNode(e)}
            onCheck={(e) => this.handleSelectCheckboxStructureNode(e)}
            onExpand={(expanded) =>
              this.setState({ expandedStructure: expanded })
            }
            showNodeIcon={false}
            showExpandAll={true}
          />
        </div>
      </div>
    ) : null;
  }


  renderDefiniciones() {
    return this.state.selectedStructure ? (
      <div className="definitions-content">
        <div className="d-flex justify-content-between align-items-center w-100 mb-4 mt-4">
          <h2 className="global-title-2">Definiciones</h2>
        </div>
        <div className="definitions-list mt-6">
          {this.state.selectedStructure.definiciones ? (
            <ul className="generic-u">
              {this.state.selectedStructure.definiciones.map((definition) => {
                return (
                  <li className="navigation-li" key={definition.definicion}>
                    <span
                      className="rct-node-clickable"
                      onClick={() =>
                        this.handleSelectDefinition(definition.asignacion)
                      }
                    >
                      {" "}
                      {definition.nombre}{" "}
                    </span>
                  </li>
                );
              })}
            </ul>
          ) : null}
        </div>
      </div>
    ) : null;
  }

  renderMenu() {
    return this.state.selectedStructure && this.state.selectedStructure.definiciones ? (

      <div className="content-paneles">
        <LayOutFlex
          Definiciones = {this.state.selectedStructure.definiciones}
          Selecciones = {this.state.selecciones}
          idOpcion = {this.props.id}
          Paneles = {this.state.paneles}
        />
       </div>
    
      
    ) : null;
  }

 
}

export default Visualizaciones;