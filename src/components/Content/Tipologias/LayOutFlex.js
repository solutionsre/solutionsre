import React from 'react';
import { Layout, Model } from 'flexlayout-react';
import VisualizacionMapa from './VisulizacionMapa';

import 'flexlayout-react/style/dark.css';
import '../Content.css'



export default function LayOutFlex(props) {

 var Definiciones = props.Definiciones;
 var Selecciones = props.Selecciones;
 var idOpcion = props.idOpcion;
 var Paneles = props.Paneles;
 const paneles = [];

 Definiciones.map((definition) => {

      paneles.push({
              id: definition.asignacion ,
              type: 'tab',
              name: definition.nombre,
              component: 'panel',
      });
 });

  var json = {
    global: { 
      tabEnableClose: false
    },
    borders: [],
    layout: {
      type: 'row',
      height: null,
      weight: null,
      id: '#1',
      children: [
        {
          type: 'tabset',
          weight: 100,
          selected: 0,
          children:paneles 
        }
      ]
    }
  };
  const model = Model.fromJson(json);
  function factory(node) {
    const component = node.getComponent();

    if (component === 'panel') {
      return(
       
        <VisualizacionMapa
          idDefinicion = {node.getId()}
          Selecciones = {Selecciones}
          idOpcion = {idOpcion}
          Paneles = {Paneles}
        />

      ); 
    }
  }
  return (
   
      <Layout model={model} factory={factory}/>
  
  );
}

