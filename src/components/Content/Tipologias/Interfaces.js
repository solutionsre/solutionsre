// Generic
import React, { Component } from "react";
import { connect } from "react-redux";
import "wijmo/wijmo.touch";
import {
  FlexGrid,
  FlexGridColumn,
  FlexGridCellTemplate,
} from "wijmo/wijmo.react.grid";
import { Selector } from "wijmo/wijmo.grid.selector";
import { CollectionView } from "wijmo/wijmo";
import { scroller } from "react-scroll";
import $ from "jquery";

// Components
import store from "../../../redux/store";
import URLS from "../../../urls";
import TopNav from "../TopNav";
import Loader from "../../GlobalComponents/Loader";
import Input from "../../GlobalComponents/Input";
import Atributos from "../Atributos";

// Css
import "../Content.css";
import "wijmo/styles/wijmo.css";

//set Spanish culture
import "wijmo/cultures/wijmo.culture.es-MX";

class Interfaces extends Component {
  constructor(props) {
    super(props);
    this.selector = null;

    this.state = {
      loading: true,
      acciones: [],
      atributos: [],
      componentId: "",
      descripcion: "",
      habilitado: "",
      id: this.props.id,
      nombre: "",
      observaciones: "",
      sectores: [],
      tipologia: "",
      nombreMenu: "",
      rutaMenu: "",
      toggle: true,
      loadingData: false,
      data: false,
      engine: {},
      showPivotPanelModal: false,
      filtros: false,
      filtrosAplicados: false,
      grouped: true,
      headers: true,
      selectedItems: [],
      dataCruda: [],
      confirmacion: false,
      showSpinner: false,
      showSpinner2: true
    };

    this.renderGrilla = this.renderGrilla.bind(this);
    this.obtenerResultadosFinal = this.obtenerResultadosFinal.bind(this);
    this.renderGridProcesada = this.renderGridProcesada.bind(this);
    this.templateEstado = this.templateEstado.bind(this);
    this.renderStates = this.renderStates.bind(this);
    this.renderErrorModal = this.renderErrorModal.bind(this);
  }

  componentDidUpdate(prevProps) {
    if (this.props.id !== prevProps.id) {
      this.setState({
        loading: true,
      });
      this.fetchConfig();
    }
  }

  componentWillMount() {
    this.setState({
      buttonDisable: false,
      loading: true,
      acciones: [],
      atributos: [],
      componentId: "",
      descripcion: "",
      habilitado: "",
      id: this.props.id,
      nombre: "",
      observaciones: "",
      sectores: [],
      tipologia: "",
      nombreMenu: "",
      rutaMenu: "",
      toggle: true,
      loadingData: false,
      data: [],
      filtros: false,
    });
    this.fetchConfig();

    store.dispatch({
      type: "SET_FILTROS_APLICADOS",
      payload: false,
    });
    
    store.dispatch({
      type: "MISA",
      payload: false,
    });
  }

  fetchConfig() {
    var myHeaders = new Headers();
    myHeaders.append(
      "Authorization",
      "Bearer " + sessionStorage.getItem("token"),
    );
    myHeaders.append("Content-Type", "application/json");

    var requestOptions = {
      method: "GET",
      headers: myHeaders,
      redirect: "follow",
    };

    fetch(
      store.getState().serverUrl +
        URLS.INTERFACES_CONFIGURACION +
        this.props.id,
      requestOptions,
    )
      .then((response) => {
        if (response.status == 401) {
          sessionStorage.clear();
          window.location.reload(false);
        }else{
          return response.json();
        }
      })
      .then((result) => {
        const atributos = {};

        if (result.componentes.sectores && result.componentes.sectores.length) {
          result.componentes.sectores
            .find((s) => s.tipologia === "FILTROS")
            .columnas.forEach((column) => {
              column.filtros.forEach((attr) => {
                atributos[attr.id] = "";
              });
            });
        }

        this.setState({
          acciones: result.componentes.acciones,
          atributos: atributos,
          componentId: result.componentes.id,
          descripcion: result.componentes.descripcion,
          habilitado: result.componentes.habilitado,
          nombre: result.componentes.nombre,
          observaciones: result.componentes.observaciones,
          sectores: result.componentes.sectores,
          tipologia: result.componentes.tipologia,
          nombreMenu: result.componentes.menu,
          rutaMenu: result.componentes.ruta,
          loading: false,
          dataProcesada: false,
          dataCruda: [],
        });
      })
      .catch((error) => console.log("error", error));
  }

  fetchData(action) {

    var resultado;
    var idProceso;

    if (action.tipo === "ACCION"){
      resultado = true;
    }else{
      resultado = this.handleValidation();
    }
    
    if (resultado) {
      $("#filterWrapper").toggle(false);
      $("#filter-toggle").find("i").addClass("fi fi-rs-arrow-small-down");
      $("#filter-toggle").find("i").removeClass("fi fi-rs-arrow-small-up");
     
      this.setState({
        toggle: false,
        loadingData: true,
        buttonDisable: true,
      });
      var actionUrl;

      switch (action.tipo) {
        case "PROCESO":
          actionUrl = store.getState().serverUrl + URLS.INTERFACES_PROCESAR;
          break;
        case "FILTRAR":
          actionUrl =
            store.getState().serverUrl + URLS.INTERFACES_APLICAR_FILTROS;
          break;
        case "IMPORTAR":
          actionUrl = store.getState().serverUrl + URLS.INTERFACES_IMPORTAR;
          break;
        case "ACCION":
          actionUrl = store.getState().serverUrl + URLS.INTERFACES_MODELO;
          break;
        default:
          console.log("Tipo de Accion '" + action.id + "' desconocida");
          break;
      }

      if (actionUrl) {

        $("button[id = " + action.id + "]").prop('disabled', true);

        var myHeaders;
        var raw;
        var requestOptions;

        myHeaders = new Headers();
        myHeaders.append("Authorization", "Bearer " + sessionStorage.getItem("token"));
        myHeaders.append("Content-Type", "application/json");
        var procesos = [];
        const atributos = Object.entries(this.state.atributos).map((item) => {
          const id = item[0];
          var value = item[1];
          if (item[0] === "6003002004") {
            var name = document.getElementById(
              "adjuntoSpan6003002004",
            ).textContent;
            var value = {
              interno: "",
              archivo: name,
              adjunto: item[1],
            };
          }
          if (id === "6003002003")
            idProceso = value;

          procesos.push({
            atributo: id,
            contenido: value,
          });
        });

        if (action.tipo === "PROCESO") {
          var arrayFinal = [];
          arrayFinal = [];

          // Leer data de columnaas seleccionadas. a ese objeto lo comparo con la data original y le cambio el atributo seleccionado cuando la secuencia sea igual
          let dataFinalSelect = [];
          let dataFinalCruda = [];
          this.state.selectedItems.map((item) => {
            var atributos = Object.entries(item._data).map((itema) => {
              if (itema[0] === "SELECCIONADO") {
               
                return {
                  atributo: itema[0],
                  contenido: "S",
                };
              } else {
                return {
                  atributo: itema[0],
                  contenido: itema[1],
                };
              }
            });
            var objetoFinal = new Object();
            objetoFinal = {
              posicion: item._data.SECUENCIA,
              atributos: atributos,
            };
            dataFinalSelect.push(objetoFinal);
          });
          
          this.state.dataCruda.map((item) => {
            var atributos = Object.entries(item).map((itema) => {
              if (itema[0] === "SELECCIONADO") {
               
                return {
                  atributo: itema[0],
                  contenido: "N",
                };
              } else {
                return {
                  atributo: itema[0],
                  contenido: itema[1],
                };
              }
            });
            var objetoFinal = new Object();

            objetoFinal = {
              posicion: item.SECUENCIA,
              atributos: atributos,
            };
            dataFinalCruda.push(objetoFinal);
          });

          arrayFinal = [];

          Object.entries(dataFinalCruda).map((crudo) => {
            if (Object.entries(dataFinalSelect).length === 0) {
              arrayFinal.push(crudo[1]);
            } else {
              Object.entries(dataFinalSelect).map((select) => {
                if (crudo[1].posicion === select[1].posicion) {
                  delete dataFinalCruda[select[1].posicion - 1];
                  dataFinalCruda.push(select[1]);
                }
              });
            }
          });

          var movimientos = dataFinalCruda.filter(function (el) {
            return el != null;
          });

          var raw = {
            idObjeto: parseInt(this.state.componentId),
            idOpcion: parseInt(this.props.id),
            idAccion: parseInt(action.id),
            idFormato: "",
            filtros: {
              procesos,
            },
            movimientos: movimientos,
          };
        } else {
          raw = {
            idObjeto: parseInt(this.state.componentId),
            idOpcion: parseInt(this.props.id),
            idAccion: parseInt(action.id),
            idFormato: "",
            filtros: {
              procesos,
            },
          };
        }
        if (action.tipo === "IMPORTAR") {
          store.dispatch({
            type: "SET_LOADING_DATA",
            payload: true,
          });
          requestOptions = {
            method: "POST",
            headers: myHeaders,
            body: JSON.stringify(raw),
            redirect: "follow",
          };
        } else {
          requestOptions = {
            method: "POST",
            headers: myHeaders,
            body: JSON.stringify(raw),
            redirect: "follow",
          };
        }
        
        if (action.tipo === "ACCION") {
          var requestOptions = {
            method: "GET",
            headers: myHeaders,
            redirect: "follow",
          };
          actionUrl = actionUrl + idProceso;
          fetch(actionUrl, requestOptions,)
              .then((response) => {
                if (response.status == 401) {
                  sessionStorage.clear();
                  window.location.reload(false);
                }else{
                  return response.json();
                }
              })
              .then((result) => {
            
            $("button[id = " + action.id + "]").prop('disabled', false);

            this.setState({
              loadingData: false,
            });
            var contenido = result.contenido;
            var nombreArchivo = result.nombreArchivo;
            this.downloadFile(contenido, "xlsx", nombreArchivo);
          })
          .catch((error) => {
            $("button[id = " + action.id + "]").prop('disabled', false);
            console.log("error", error)
          });
        }else{
          
          fetch(actionUrl, requestOptions)
            .then(async (response) => {
              const isJson = response.headers
                .get("content-type")
                ?.includes("application/json");
              
              $("button[id = " + action.id + "]").prop('disabled', false);
              if (!response.ok) {
                const error = response.json();
                store.dispatch({
                  type: "SET_LOADING_DATA",
                  payload: false,
                });
                this.setState({
                  state: 400,
                  buttonDisable: false,
                  errorMessages: response.json().Mensajes,
                  showModal: true,
                });
                this.renderStates();
                return Promise.reject(error);
              }
            })
            .then((result) => {
              
              $("button[id = " + action.id + "]").prop('disabled', false);

              if (action.tipo === "PROCESO") {
                this.setState({
                  loadingData: false,
                });
                this.obtenerResultadosFinal(action.id);
              } else if (action.tipo === "FILTRAR") {
                this.setState({
                  confirmacion: false,
                  filtrosAplicados: true,
                });
                
                store.dispatch({
                  type: "SET_FILTROS_APLICADOS",
                  payload: true,
                });
                
                         
                this.aplicarFiltros(action.id);
              } else if (action.tipo === "IMPORTAR") {
                store.dispatch({
                  type: "SET_FILTROS_APLICADOS",
                  payload: true,
                });
                this.setState({
                  confirmacion: false,
                  filtrosAplicados: true,
                });
                
                this.aplicarFiltros(action.id);
              }
            })
            .catch((error) => {
              $("button[id = " + action.id + "]").prop('disabled', false);
              console.log("error", error)
          });
          
        }
      }
    }
  }

  renderStates() {
    switch (this.state.state) {
      case 400:
        this.setState({
          showModal: true,
        });
        return this.renderErrorModal();

      default:
    }
  }

  handleCloseModal() {
    this.setState((prevState) => ({
      showModal: !prevState.showModal,
    }));
  }

  renderErrorModal() {
    return (
      <>
        <div className="custom-modal">
          <div className="custom-modal-content">
            <div className="custom-modal-header">
              <div className="row d-flex justify-content-between align-items-center">
                <div>
                  <h6> {this.props.tipologia} </h6>
                </div>
                <div>
                  <button
                    className="btn btn-close"
                    onClick={() => this.handleCloseModal()}
                  >
                    <i className="fi fi-rs-cross" />
                  </button>
                </div>
              </div>
            </div>
            <div className="mt-5">
              <div className="d-flex justify-content-center">
                <i className="right-icon-big-no fi fi-rs-cross" />
              </div>
              <div className="d-flex flex-column justify-content-center mt text-center">
                <p>
                  <b>Ocurrió un error</b>
                </p>

                {this.state.errorMessages
                  ? this.state.errorMessages.map((error, i) => {
                      return (
                        <div
                          className="alert alert-danger mx-5"
                          role="alert"
                          key={i}
                        >
                          <div style={{ wordBreak: "break-word" }}>
                            {error.mensaje}
                          </div>
                        </div>
                      );
                    })
                  : null}
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }

  aplicarFiltros(accion) {

    $("button[id = " + accion + "]").prop('disabled', true);

    this.setState({
      dataProcesada: false,
      showSpinner2: true
    });
    var myHeaders = new Headers();
    myHeaders.append(
      "Authorization",
      "Bearer " + sessionStorage.getItem("token"),
    );
    myHeaders.append("Content-Type", "application/json");

    var objeto;

    this.state.acciones.map((item) => {
      if (item.id === accion) {
        objeto = item.refrescos[0].objeto;
      }
    });

    var raw = JSON.stringify({ idObjeto: parseInt(objeto) });
    var requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: raw,
      redirect: "follow",
    };

    fetch(store.getState().serverUrl + URLS.OBTENER_INFORMACION, requestOptions)
      .then((response) => {
        if (response.status == 401) {
          sessionStorage.clear();
          window.location.reload(false);
        }else{
          return response.json();
        }
      })
      .then((result) => {

        $("button[id = " + accion + "]").prop('disabled', false);


        const data = this.state.data;

        data[JSON.stringify(objeto)] = new CollectionView(result);
        data[JSON.stringify(objeto)].currentItem = null;

        this.setState({
          loadingData: false,
          data: data,
          dataCruda: result,
          showSpinner: true,
          showSpinner2: false
        });
        store.dispatch({
          type: "SET_LOADING_DATA",
          payload: false,
        });
      })
      .catch((error) => {
        $("button[id = " + accion + "]").prop('disabled', false);
        console.log("error", error)
      });
  }

  obtenerInformacion(rawData) {

    var actionUrl;

    actionUrl = store.getState().serverUrl + URLS.OBTENER_INFORMACION;

    if (actionUrl) {
      var myHeaders;
      var raw;
      var requestOptions;

      myHeaders = new Headers();
      myHeaders.append("Authorization", "Bearer " + sessionStorage.getItem("token"));
      myHeaders.append("Content-Type", "application/json");

      raw = JSON.stringify(rawData);

      requestOptions = {
        method: "POST",
        headers: myHeaders,
        body: raw,
        redirect: "follow",
      };

      fetch(actionUrl, requestOptions)
        .then((response) => {
          if (response.status == 401) {
            sessionStorage.clear();
            window.location.reload(false);
          }else{
            return response.json();
          }
        })
        .then((result) => {
          var data = [];
          data = new CollectionView(result.Resultados);

          this.setState({
            dataCruda: data,
            dataProcesada: data,
            data: this.state.data,
          });
          store.dispatch({
            type: "SET_FILTROS_APLICADOS",
            payload: false,
          });
        });
    }
  }

  obtenerResultadosFinal(accion) {

    $("button[id = " + accion + "]").prop('disabled', true);

    var myHeaders = new Headers();
    myHeaders.append(
      "Authorization",
      "Bearer " + sessionStorage.getItem("token"),
    );
    myHeaders.append("Content-Type", "application/json");

    var requestOptions = {
      method: "GET",
      headers: myHeaders,
      redirect: "follow",
    };
    fetch(store.getState().serverUrl + URLS.OBTENER_RESULTADOS, requestOptions)
      .then((response) => {
        if (response.status == 401) {
          sessionStorage.clear();
          window.location.reload(false);
        }else{
          return response.json();
        }
      })
      .then((result) => {

        $("button[id = " + accion + "]").prop('disabled', false);

        var data = [];
        data = new CollectionView(result.Resultados);

        this.setState({
          confirmacion: true,
          loadingDataProcesada: false,
          dataProcesada: data,
          filtrosAplicados: false,
          data: {},
        });
        store.dispatch({
          type: "SET_FILTROS_APLICADOS",
          payload: false,
        });

        scroller.scrollTo("sector2", {
          containerId: "sectores",
          smooth: true,
          offset: 100,
          duration: 1000,
        });
      })
      .catch((error) => {
        $("button[id = " + accion + "]").prop('disabled', false);
        console.log("error", error)
      });
  }

  hangleOnChangeSeleccion(e) {
    if (e.target) {
      var { value, name } = e.target;
    } else if (e.inputType === "text") {
      var value = e.selectedValue;
      var name = e._tbx.name;
    } else if (e.inputType === "tel") {
      var value = e._tbx.value;
      var name = e._tbx.name;
    }
    const prevState = this.state;

    this.setState({
      atributosSeleccion: {
        ...prevState.atributosSeleccion,
        [name]: value,
      },
    });
  }

  async handleOnChangeFiltros(e) {

    if (e.target) {
      var { value, name, type } = e.target;
    } else if (e.inputType === "text") {
      var value = e.selectedValue;
      var name = e._tbx.name;
      var type = e._tbx.type;
    } else if (e.inputType === "tel") {
      var value = e._tbx.value;
      var name = e._tbx.name;
      var type = e._tbx.type;
    }
    if (name != undefined) {
      if (type === "file") {
        var file = e.target.files[0];
        const fileToBase64 = (file) =>
          new Promise((resolve, reject) => {
            const reader = new FileReader();
            reader.readAsDataURL(file);
  
            reader.onload = () =>
              resolve(reader.result.replace(/^data:.+;base64,/, ""));
  
            reader.onerror = (error) => reject(error);
          });
        var value = await fileToBase64(file);
  
        this.setState({ fileValor: e });
      }
      const prevState = this.state;
      this.setState({
        atributos: {
          ...prevState.atributos,
          [name]: value,
        },
      });
    }
  }

  handleOnChangeFiltrosMultiple(e) {
    if (e.target) {
      var { value, name } = e.target;
    } else if (e.inputType === "text") {
      var value = e.selectedValue;
      var name = e._tbx.name;
    } else if (e.inputType === "tel") {
      var value = e._tbx.value;
      var name = e._tbx.name;
    }
    const prevState = this.state;
    var checkboxs;

    if (prevState.atributos[name] === "") {
      checkboxs = prevState.atributos[name].split("");
    } else {
      checkboxs = prevState.atributos[name].split(",");
    }

    // Si ya estaba seleccionada, se elimina.
    if (checkboxs.includes(value)) {
      this.setState({
        atributos: {
          ...prevState.atributos,
          [name]: checkboxs.filter((v) => v !== value).toString(),
        },
      });

      // Si no estaba seleccionada, se agrega.
    } else {
      checkboxs.push(value);
      this.setState({
        atributos: {
          ...prevState.atributos,
          [name]: checkboxs.toString(),
        },
      });
    }
  }

  handleValidation() {
    var validacion = true;
    const filtersSection = this.state.sectores.find(
      (s) => s.tipologia === "FILTROS",
    );
    const filtersErrors = [];

    filtersSection.columnas.forEach((column) => {
      column.filtros.forEach((art) => {
        if (this.state.atributos[art.id]) {
          if (art.tipologia === "TEXTO") {
            if (art.tipo === "CARACTER" && art.estilo === "MAYUSCULAS") {
              var input = $("label[id = label" + art.id + "]");
              input.removeClass("is-invalid");
              const newState = this.state.atributos;

              newState[art.id] = newState[art.id].toUpperCase();
            }
            if (art.tipo === "NUMERICO") {
              var input = $("label[id = label" + art.id + "]");
              input.removeClass("is-invalid");
              if (
                (art.maximo &&
                  parseFloat(this.state.atributos[art.id]) >
                    parseFloat(art.maximo)) ||
                (art.minimo &&
                  parseFloat(this.state.atributos[art.id]) <
                    parseFloat(art.minimo))
              ) {
                const newState = this.state.atributos;
                newState[art.id] = "";
                validacion = false;
                filtersErrors.push(
                  "El numero ingresado en " +
                    art.descripcion +
                    " supera los limites",
                );
              }
            }
          } else if (art.tipologia === "ADJUNTO") {
            if (this.state.fileValor.adjunto === "") {
              var input = $("span[id = label-adjunto]");
              input.addClass("label-adjunto-invalido");
            } else {
              var input = $("span[id = label-adjunto]");
              input.removeClass("label-adjunto-invalido");
            }
          } else if (art.tipologia === "FECHA") {
            var input = $("label[id = label" + art.id + "]");
            input.removeClass("is-invalid");
          }
        } else {
          if (art.obligatorio === "S") {
            if (art.tipo === "ADJUNTO") {
              var input = $("span[id = label-adjunto]");

              input.addClass("label-adjunto-invalido");
            } else {
              var input = $("label[id = label" + art.id + "]");
              input.addClass("is-invalid");
            }
            validacion = false;
            filtersErrors.push(
              "El campo " + art.descripcion + " es obligatorio",
            );
          }
        }
      });
    });

    store.dispatch({
      type: "SET_FILTERS_ERRORS",
      payload: filtersErrors,
    });

    return validacion;
  }


  marcarFilasExistentes(grid) {
    
    if (grid.rows != undefined) {
      grid.rows.forEach((fila) => {
        if (fila.dataItem["SELECCIONADO"] === "S"){
          fila.isSelected = true;
        }
      });
    }
  }

  initGrid(grid) {
    
    
    this.marcarFilasExistentes(grid);
    
    this.selector = new Selector(grid, {
      itemChecked: (s, e) => {
        this.selectItems(grid);
      },
    });

    this.setState({
      grilla: grid,
    });
  }

  selectItems(grid) {
    this.setState({
      selectedItems: grid.rows.filter((r) => r.isSelected),
    });
    
    store.dispatch({
      type: "SET_ITEM_SELECCIONADO_INTERFACES",
      payload: grid.rows.filter((r) => r.isSelected),
    });
  }

  renderGrilla(sector) {
    const sectorId = JSON.stringify(sector.id);
    if (this.state.loadingData === true) {
      return <Loader />;
    } else {
      if (this.state.confirmacion === false) {
        return (
          <>
            <div
              key={sector.id}
              className="d-flex flex-column justify-content-between w-100 mb-4 mt-4"
            >
              <h6> {sector.descripcion} </h6>

              {this.state.data[sectorId].itemCount ? 
              <FlexGrid
                initialized={this.initGrid.bind(this)}
                itemsSource={this.state.data[sectorId]}
                deferResizing={true}
                showMarquee={true}
                preserveSelectedState={true}
                allowPinning="SingleColumn"
                allowSorting={true}
                
              >
                {sector.visualizacion.atributos
                  .filter((attr) => attr.visible === "S")
                  .map((attr) => {
                    var ancho = attr.porcentaje;
                    if (!ancho.includes("*")){
                      ancho = Number(ancho); 
                    }  
                    return (
                      <FlexGridColumn
                        key={attr.id}
                        binding={attr.id}
                        header={attr.descripcion}
                        isReadOnly={true}
                        width={ancho}
                        format={attr.mascara}
                        align={attr.alineacion}
                        visible={(attr.visible === "S")}
                      ></FlexGridColumn>
                    );
                  })}
              </FlexGrid>
                : (
                  <div className="row">
                    <div className="col-12 text-center mt-5">
                      <div className="alert alert-warning">
                        <i className="fi fi-rs-triangle-warning mr-3" />
                        No hay información para mostrar.
                      </div>
                    </div>
                  </div>
                  )}
            </div>
          </>
        );
      }
    }
  }

  templateEstado = (ctx) => {
    if (ctx.item.tipo === "ERROR") {
      return (
        <React.Fragment>
          <span className="badge badge-danger  pt-2 pb-2 pr-3 pl-3">
            <i className="fi fi-rs-cross mr-2"></i>
            {ctx.item.tipo}
          </span>
        </React.Fragment>
      );
    } else {
      return (
        <React.Fragment>
          <span className="badge badge-success pt-2 pb-2 pr-3 pl-3">
            <i className="fi fi-rs-check mr-2"></i> {ctx.item.tipo}
          </span>
        </React.Fragment>
      );
    }
  };

  renderGridProcesada(sector) {
    if (this.state.loadingData === true) {
      return <Loader />;
    } else {
      return Object.entries(this.state.dataProcesada).length !== 0 ? (
        <FlexGrid
          itemsSource={this.state.dataProcesada}
          deferResizing={true}
          showMarquee={true}
          preserveSelectedState={true}
          allowPinning="SingleColumn"
          allowSorting={true}
        >
          {sector.visualizacion.atributos
            .filter((attr) => attr.visible === "S")
            .map((attr) => {
              var ancho = attr.porcentaje;
              if (!ancho.includes("*")){
                ancho = Number(ancho); 
              }
              return (
                <FlexGridColumn
                  key={attr.orden}
                  binding={attr.id}
                  header={attr.descripcion}
                  isReadOnly={true}
                  width={ancho}
                  format={attr.mascara}
                  align={attr.alineacion}
                >
                  {attr.descripcion === "Tipo" ? (
                    <FlexGridCellTemplate
                      cellType="Cell"
                      template={this.templateEstado}
                      dataMap={attr.descripcion}
                    />
                  ) : (
                    false
                  )}
                </FlexGridColumn>
              );
            })}
        </FlexGrid>
      ) : (
        <Loader />
      );
    }
  }

  render() {
    return (
      <div className="content-container" id="sectores">
        <TopNav
          app={this.props.app}
          nombre={this.state.nombreMenu}
          ruta={this.state.rutaMenu}
        />
        {this.state.loading ? (
          <div className="container col-12 mt-4">
            <Loader />
          </div>
        ) : (
          <div className="main">
            {this.state.sectores && this.state.sectores.length ? (
              this.renderSections(this.state.sectores, this.state.acciones)
            ) : (
              <div className="container">
                <div className="row">
                  <div className="col-12 text-center mt-5">
                    <div className="alert alert-warning">
                      <i className="fi fi-rs-triangle-warning mr-3" />
                      En este momento no es posible mostrar el componente.
                      Intente nuevamente más tarde.
                    </div>
                  </div>
                </div>
              </div>
            )}
          </div>
        )}
        {this.state.showModal ? this.renderErrorModal() : null}
      </div>
    );
  }

  renderSections(sectores, actions) {
    const filtersSection = this.state.sectores.find(
      (s) => s.tipologia === "FILTROS",
    );
    const action = actions.find((a) => a.tipo !== "DESCARGA");

    const arregloAtributos = filtersSection.columnas;
    var arrayAtributos = [];

    arregloAtributos.map((item) => {
      if (item.filtros.length == 2) {
        arrayAtributos.push(item.filtros[0]);
        arrayAtributos.push(item.filtros[1]);
      } else if (item.filtros.length == 3) {
        arrayAtributos.push(item.filtros[0]);
        arrayAtributos.push(item.filtros[1]);
        arrayAtributos.push(item.filtros[2]);
      } else {
        arrayAtributos.push(item.filtros[0]);
      }
    });

    return (
      <div>
        <Atributos
          atributos={arrayAtributos}
          acciones={this.state.acciones}
          sectores={this.state.sectores}
          componentId={this.state.componentId}
          id={this.state.id}
          filtersSection={filtersSection}
          action={action}
          tipologia="INTERFACES"
          fetchData={(e) => this.fetchData(e)}
          handleOnChangeFiltros={(e) => this.handleOnChangeFiltros(e)}
        />

        <div className="container">
          {this.props.loadingData ? (
            <div className="col-12 mt-3">
              <Loader />
            </div>
          ) : (
            this.renderViews()
          )}
        </div>
      </div>
    );
  }

  downloadFile(file, extention, filename) {
    const linkSource = `data:application/${extention};base64,${file}`;
    const downloadLink = document.createElement("a");
    const fileName = filename;
  
    downloadLink.href = linkSource;
    downloadLink.download = fileName;
    downloadLink.click();
  }
  
  renderViews() {
    return (
      <div className="container-fluid">
        <div className="row mt-4">
          {this.state.loadingData ? (
            <div className="container col-12 mt-4">
              <Loader />
            </div>
          ) : (
            <>
              <div className="col-12">
                {this.state.data &&
                Object.keys(this.state.data).length !== 0 ? (
                  <div>
                    <h1 className="global-title">
                      {this.state.sectores[1].descripcion}
                    </h1>
                    <div
                      className="container-grilla"
                      key={this.state.sectores[1].id}
                    >
                      {this.renderGrilla(this.state.sectores[1])}
                    </div>
                  </div>
                ) : null}
              </div>

              <div className="col-12 mb-5" id="sector2">
                {this.state.dataProcesada ? (
                  <div>
                    <h1 className="global-title mb-4 mt-4">
                      {this.state.sectores[2].descripcion}
                    </h1>
                    <div
                      className="container-grilla"
                      key={this.state.sectores[2].id}
                    >
                      {this.renderGridProcesada(this.state.sectores[2])}
                    </div>
                  </div>
                ) : null}
              </div>
            </>
          )}
        </div>
      </div>
    );
  }
}



const mapStateToProps = (state) => ({
  rerender: state.rerender,
  loadingData: state.loadingData,
  dataObInfo: state.dataObInfo,
  atributosFiltros: state.atributosFiltros,
});

export default connect(mapStateToProps)(Interfaces);
