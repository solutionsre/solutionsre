// Generic
import React, { Component } from "react";
import Tooltip from "react-simple-tooltip";

// Css
import "./DashboardComponents.css";

//Components
import Filtros from "../Filtros";
import Loader from "../../GlobalComponents/Loader";
import store from "../../../redux/store";
import URLS from "../../../urls";

import { FlexGrid, FlexGridColumn } from "wijmo/wijmo.react.grid";
import { CollectionView } from "wijmo/wijmo";

class Box extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {},
    };
    this.obtenerResultadosFinal = this.obtenerResultadosFinal.bind(this);
  }

  componentDidMount() {
    this.obtenerResultadosFinal();
  }

  obtenerResultadosFinal() {
    var myHeaders = new Headers();
    myHeaders.append(
      "Authorization",
      "Bearer " + sessionStorage.getItem("token"),
    );
    myHeaders.append("Content-Type", "application/json");

    if (this.props.sectores && this.props.sectores.length > 0) {
      var raw = JSON.stringify({
        idObjeto: parseInt(this.props.sectores[0].id),
        idOpcion: parseInt(this.props.id),
      });
    }

    var requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: raw,
      redirect: "follow",
    };

    fetch(store.getState().serverUrl + URLS.OBTENER_INFORMACION, requestOptions)
      .then((response) => {
        if (response.status == 401) {
          sessionStorage.clear();
          window.location.reload(false);
        }else{
          return response.json();
        }
      })
      .then((result) => {
        var data = [];
        data[JSON.stringify(this.props.sectores[0].id)] = new CollectionView(
          result,
        );

        this.setState({
          data: data,
        });
      })
      .catch((error) => console.log("error", error));
  }

  initGrid(flexGrid) {
    this.flexGrid = flexGrid;
    /* this.flexGrid.onSelectionChanged(null); */
    this.setState({
      grilla: flexGrid,
    });
  }

  renderGrilla(sector) {
    const sectorId = JSON.stringify(sector.id);
    const atributosVisibles = sector.visualizacion.atributos.filter(
      (atributo) => atributo.visible === "S",
    );

    return (
      <div
        key={sector.id}
        className="d-flex flex-column justify-content-between w-100 mb-4 mt-4"
      >
        <h6> {sector.descripcion} </h6>

        {this.props.sectores && this.props.sectores.length > 0 ? (
          <FlexGrid
            initialized={this.initGrid.bind(this)}
            itemsSource={this.state.data[sectorId]}
            deferResizing={true}
            showMarquee={true}
            preserveSelectedState={true}
            allowPinning="SingleColumn"
            selectionChanged={this.onSelectionChanged}
            allowSorting={true}
          >
            {atributosVisibles.map((attr) => {
              var ancho = attr.porcentaje;
              if (!ancho.includes("*")){
                ancho = Number(ancho); 
              }              
              return (
                <FlexGridColumn
                  key={attr.orden}
                  binding={attr.id}
                  header={attr.descripcion}
                  isReadOnly={true}
                  width={ancho}
                  format={attr.mascara}
                  align={attr.alineacion}
                ></FlexGridColumn>
              );
            })}
          </FlexGrid>
        ) : (
          <Loader />
        )}
      </div>
    );
  }

  render() {
    console.log("Box", this.props);

    const renderIcon = (action) => {
      switch (action) {
        case "Procesar Informacion":
          return <i className="fi fi-rs-play-circle" />;
        case "Agregar":
          return <i className="fi fi-rs-add" />;
        case "Modificar":
          return <i className="fi fi-rs-pen-clip" />;
        case "Eliminar":
          return <i className="fi fi-rs-trash" />;
        case "Visualizar":
          return <i className="fi fi-rs-file-pdf" />;
        case "Descargar Informacion":
        case "Descargar":
          return <i className="fi fi-rs-download" />;
        case "Modelo":
          return <i className="fi fi-rs-file-download" />;
        case "Importar":
          return <i className="fi fi-rs-file-upload" />;
        case "Filtrar":
          return <i className="fi fi-rs-filter"></i>;
        default:
          return null;
      }
    };

    return (
      <div>
        <div
          className="box shadow overflow-auto"
          style={{ height: this.props.altura }}
        >
          <div className="d-flex justify-content-between">
            <h2 className="global-title-2 mt-2">
              {this.props.componentes.descripcion}
            </h2>
            <hr className="line mb-2" />
            <div className="">
              {this.props.componentes.acciones.map((action) => {
                return (
                  <Tooltip
                    content={action.observaciones}
                    placement="bottom"
                    className="global-link-1 btn-acciones"
                  >
                    {renderIcon(action.descripcion)}
                  </Tooltip>
                );
              })}
            </div>
          </div>
          {this.props.sectores && this.props.sectores.length > 0 ? (
            <>{this.renderGrilla(this.props.sectores[1])}</>
          ) : null}
        </div>
      </div>
    );
  }
}

export default Box;
