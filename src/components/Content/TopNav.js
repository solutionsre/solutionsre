import React, { Component } from "react";
import BreadCrumb from "../GlobalComponents/BreadCrumb";
import { Link, Redirect, NavLink } from "react-router-dom";
import Tooltip from "react-simple-tooltip";
import Favoritos from "../GlobalComponents/Favoritos";
import "./TopNav.css";
// import { NavLink } from "react-router-dom";

//Azure
import { PublicClientApplication } from "@azure/msal-browser";

// Images
import Flowmanager from "../../assets/images/flowmanager.png";
import Layout from "../../assets/images/layout.png";
import Monitoring from "../../assets/images/monitoring.png";
import Ecommerce from "../../assets/images/ecommerce.png";
import Consultoria from "../../assets/images/consultoria.png";
import Bi from "../../assets/images/bi.png";
import Solutions from "../../assets/images/solutions.png";
import Seguridad from "../../assets/images/seguridad.png";
import ChangePasswordForm from "../Login/ChangePasswordForm";
import $ from "jquery";
import Portal from "../../assets/images/portal.png";
import Ecustomer from "../../assets/images/ecustomer.png";
import URLS from "../../urls";
import store from "../../redux/store";
import logo from "../../assets/images/favicon.png";
import Ingresos from "../../assets/images/ingresos.png";

import BtnCAMPLogout from "../Login/BtnCAMPLogout";

var images = {
  Flowmanager: Flowmanager,
  Layout: Layout,
  Monitoring: Monitoring,
  Ecommerce: Ecommerce,
  Consultoria: Consultoria,
  Analytics: Bi,
  Solutions: Solutions,
  Seguridad: Seguridad,
  Portal: Portal,
  Ecustomer: Ecustomer,
  Ingresos: Ingresos,
};

class TopNav extends Component {
  constructor(props) {
    super(props);
    this.state = {
      nombre: this.props.nombre,
      ruta: this.props.ruta,
      menuOpen: false,
      menuSeguridad: false,
      menuContra: false,
      logout: false,
      userMenu: JSON.parse(sessionStorage.getItem("userMenu")),
      user: JSON.parse(sessionStorage.getItem("user")),
      showPopover: false,
      LogoutCamp: false,
    };
    this.handleCerrarSesion = this.handleCerrarSesion.bind(this);
    this.handleOpenMenu = this.handleOpenMenu.bind(this);
    this.handleOpenMenuSeguridad = this.handleOpenMenuSeguridad.bind(this);
    this.handleOpenMenuCambioContra =
      this.handleOpenMenuCambioContra.bind(this);
    this.handleClickCloseMenu = this.handleClickCloseMenu.bind(this);
  }

  componentDidMount() {
    this.setState({
      userMenuSeguridad: this.state.userMenu.principal[1].seguridad[0],
    });

    // Session de Azure y de Camp
    fetch("./url.json")
      .then((res) => res.json())
      .then(async (data) => {
        //console.log(data)
        /////////////////////////////////////////////////////////////////
        var Azures = new PublicClientApplication({
          auth: {
            clientId: data.appId,
            redirectUri: data.redirectUri,
            authority: data.authority,
          },
          cache: {
            cacheLocation: "sessionStorage",
            storeAuthStateInCookie: true,
          },
        });
        //console.log(Azures)
        this.PublicClientApplication = Azures;
      })
      .catch((err) => console.log(err, " error"));
  }

  handleCerrarSesion() {
    // Cerramos la sesion en el server y caducamos el token
    var actionUrl;

    actionUrl = store.getState().serverUrl + URLS.SEGURIDAD_DESCONECTAR;

    if (actionUrl) {
      var myHeaders;
      var requestOptions;

      myHeaders = new Headers();
      myHeaders.append(
        "Authorization",
        "Bearer " + sessionStorage.getItem("token")
      );
      myHeaders.append("Content-Type", "application/json");

      requestOptions = {
        method: "POST",
        headers: myHeaders,
        redirect: "follow",
      };

      fetch(actionUrl, requestOptions)
        .then((response) => {
          if (response.status == 401) {
            sessionStorage.clear();
            window.location.reload(false);
          } else {
            return response.text();
          }
        })
        .then((result) => {
          let loginAzure = sessionStorage.getItem("loginAzure");
          let loginCamp = sessionStorage.getItem("loginCamp");

          // Cerramos sesion de Azure
          if (loginAzure == "true") {
            this.PublicClientApplication.logoutPopup();
          }

          // Cerramos sesion de Camp

          if (loginCamp == "true") {
            this.setState({
              LogoutCamp: true,
            });
          } else {
            sessionStorage.clear();
            this.setState({
              logout: true,
            });
          }
        });
    }
  }

  handleOpenMenu() {
    $("#btnMenu").toggleClass("active");
    $("#navigationPrimary").toggleClass("menu-open");

    this.setState((prevState) => ({
      menuOpen: !prevState.menuOpen,
    }));
  }

  handleOpenMenuSeguridad() {
    this.setState((prevState) => ({
      menuSeguridad: !prevState.menuSeguridad,
    }));
  }

  handleOpenMenuCambioContra() {
    this.setState((prevState) => ({
      menuContra: !prevState.menuContra,
    }));
  }

  handleClickCloseMenu() {
    $("#btnMenu").toggleClass("active");
    $("#navigationPrimary").toggleClass("menu-open");
  }

  render() {
    var app = null;

    if (this.props.app) {
      if (this.props.app === "Seguridad") {
        app = this.state.userMenu.principal[1].seguridad[0];
      } else {
        const apps = this.state.userMenu.principal[0].aplicativos;
        app = apps.find((app) => app.descripcion === this.props.app);
      }
    } else {
      app = JSON.parse(sessionStorage.getItem("defaultApp"));
    }

    const menu = app.menu;
    var favoritos = [];

    if (menu) {
      for (let i = 0; i < menu.length; i++) {
        if (menu[i].menu !== undefined) {
          for (let z = 0; z < menu[i].menu.length; z++) {
            if (menu[i].menu[z].favorito === "1") {
              favoritos.push([
                menu[i].menu[z].texto,
                menu[i].menu[z].id,
                menu[i].menu[z].tipologia,
              ]);
            }
          }
        }
      }
    }
    
    const userMenu = JSON.parse(sessionStorage.getItem("userMenu"));
    const defaultApp = JSON.parse(sessionStorage.getItem("defaultApp"));
    const defaultId = JSON.parse(sessionStorage.getItem("defaultId"));
    const currentApp = JSON.parse(sessionStorage.getItem("currentApp"));

    return this.state.logout ? (
      <Redirect to="/login" />
    ) : (
      <div className="top-nav">
        {this.renderMenuSeguridad()}
        {this.renderMenuCambioContra()}
        <div className="container">
          <nav className="navbar navbar-expand-lg navbar-light navbar-inverse">
            <div className="container-fluid">
              <div className="left-navbar-header">
                {/* Logo */}
                <div className="flogo">
                  <NavLink
                    to={"/" + defaultApp + "/Dashboard/" + defaultId}
                    className="img-brand"
                  >
                    <img src={logo} alt="" />
                  </NavLink>
                  <div className="divider"></div>
                </div>

                {/* Logo */}

                <div className="navbar-header">
                  <div className="d-flex flex-xl-row flex-lg-row flex-md-row flex-sm-row flex-column">
                    <button
                      id="btnMenu"
                      className="btn-menu mb-xl-0 mb-lg-0 mb-md-0 mb-sm-0 mb-2"
                      onClick={() => this.handleOpenMenu()}
                    >
                      {this.state.menuOpen ? (
                        <i className="fi fi-rs-cross" />
                      ) : (
                        <i className="fi fi-rs-burger-menu" />
                      )}
                    </button>
                    <div className="d-flex align-items-center justify-content-between">
                      <img
                        className="icon-app"
                        src={images[this.props.app]}
                        alt={this.props.app}
                        width={30}
                        height={30}
                      />
                      <div>
                        <h1 className="global-title-2 text-white">
                          {this.props.nombre}
                        </h1>
                        <BreadCrumb data={this.props.ruta} />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <ul className="nav navbar-nav navbar-right">
                <li className="nav-item dropdown text-white">
                  <div
                    className="nav-link text-white btn-transparent"
                    aria-haspopup="true"
                    aria-expanded="false"
                  >
                    <p>
                      {this.state.user.mostrar}
                    </p>
                  </div>
                </li>
                {userMenu.principal[1].seguridad[0].habilitado === "1" && (
                  <li className="nav-item dropdown text-white">
                    <button
                      className="nav-link text-white btn-transparent"
                      onClick={() => this.handleOpenMenuSeguridad()}
                    >
                      <i className="fi fi-rs-user-gear" />
                    </button>
                  </li>
                )}
                <li className="nav-item dropdown text-white">
                  <button
                    className="nav-link text-white btn-transparent"
                    data-toggle="dropdown"
                    aria-haspopup="true"
                    aria-expanded="false"
                  >
                    <i className="fi fi-rs-star"></i>
                  </button>
                  <div
                    className="dropdown-menu dropdown-menu-right"
                    aria-labelledby="navbarDropdown"
                  >
                    {favoritos.length !== undefined && (
                      <h2 className="global-title-2 ml-3 my-2">Favoritos</h2>
                    )}
                    
                    {favoritos
                      ? favoritos.map((data, key) => {
                          return (
                            <p className="dashboard-fav ml-3 my-1" key={key}>
                              <Link
                                to={
                                  "/" +
                                  this.props.app +
                                  "/" +
                                  data[2] +
                                  "/" +
                                  data[1]
                                }
                              >
                                <i className="fi fi-rs-star mr-2"></i>
                                {data[0]}
                              </Link>
                            </p>
                          );
                        })
                      : null}
                  </div>
                </li>
                <li className="nav-item dropdown text-white">
                  <Link
                    className="nav-link text-white"
                    to={`/${currentApp.descripcion}/Dashboard/` + currentApp.id}
                  >
                    <i className="fi fi-rs-screen" />
                  </Link>
                </li>
                {window.innerWidth > 600 ? (
                  this.state.showPopover ? (
                    <div className="user-data-hover">
                      {/* <p>Cuenta</p> */}
                      <p>{this.state.user.mostrar}</p>
                      <p className="mail">{this.state.user.correo}</p>
                    </div>
                  ) : (
                    <></>
                  )
                ) : (
                  <></>
                )}
                <li
                  className="nav-item dropdown text-white user-data-dropdown"
                  onMouseEnter={() =>
                    this.setState({
                      ...this.state,
                      showPopover: true,
                    })
                  }
                  onMouseLeave={() =>
                    this.setState({
                      ...this.state,
                      showPopover: false,
                    })
                  }
                >
                  <button
                    className="nav-link text-white btn-transparent"
                    data-toggle="dropdown"
                    aria-haspopup="true"
                    aria-expanded="false"
                  >
                    <i className="fi fi-rs-user" />
                  </button>
                  <div
                    className="dropdown-menu dropdown-menu-right"
                    aria-labelledby="navbarDropdown"
                  >
                    {this.state.user.opciones.map((opcion) => {
                      switch (opcion.id) {
                        case "Cuenta":
                          return (
                            <div className="dropdown-item user-data">
                              <i class={opcion.icono + " mr-2 contact-icon"} />
                              <span>
                                {this.state.user.mostrar}
                                <p className="mail ">
                                  {this.state.user.correo}
                                </p>
                              </span>
                            </div>
                          );
                        case "Cambiar":
                          return (
                            <button
                              className="dropdown-item"
                              onClick={() => this.handleOpenMenuCambioContra()}
                            >
                              <i className={opcion.icono + " mr-2"} />
                              {opcion.texto}
                            </button>
                          );
                        case "Cerrar":
                          return (
                            <button
                              className="dropdown-item"
                              onClick={() => this.handleCerrarSesion()}
                            >
                              <i
                                className={
                                  opcion.icono + " mr-2"
                                }
                              />
                              {opcion.texto}
                            </button>
                          );
                      }
                    })}
                  </div>
                </li>
              </ul>
            </div>
          </nav>
        </div>
        <BtnCAMPLogout logout={this} realizarLogout={this.state.LogoutCamp} />
      </div>
    );
  }

  renderMenuSeguridad() {
    var favoritos = [];
    var favoritosMenu = [];

    if (this.state.menuSeguridad) {
      return (
        <div className="menu-seguridad">
          <div className="header">
            <div className="d-flex justify-content-between align-items-center">
              <h4 className="m-0">
                {this.state.userMenuSeguridad &&
                  this.state.userMenuSeguridad.descripcion}
              </h4>
              <div
                className="close"
                onClick={() => this.handleOpenMenuSeguridad()}
              >
                <i className="fi fi-rs-cross" />
              </div>
            </div>
          </div>

          <div className="divider" />

          <div className="search-group">
            <input className="input-search" type="text" placeholder="Buscar" />
            <span className="button-search">
              <i className="fi fi-rs-search" />
            </span>
          </div>

          {this.state.userMenuSeguridad &&
            this.state.userMenuSeguridad.menu.forEach((item) => {
              item.menu.map((itemm) => {
                return favoritos.push(itemm.favorito);
              });
            })}

          {favoritos.includes("1") && (
            <div>
              <div className="divider" />
              <h3>Favoritos</h3>
            </div>
          )}

          <ul className="primary-menu">
            {this.state.userMenuSeguridad &&
              this.state.userMenuSeguridad.menu.map((itemm) => {
                return (
                  <div key={itemm.id}>
                    {itemm.menu.map((item) => {
                      if (item.favorito === "1")
                        return (
                          <li
                            className="primary-menu-li favorite"
                            key={item.id}
                          >
                            <Favoritos id={item.id} operacion="ELIMINAR" />
                            <Link
                              onClick={() => this.handleClickCloseMenu()}
                              to={
                                "/Seguridad/" + item.tipologia + "/" + item.id
                              }
                            >
                              <span>
                                <i className={`${item.icono} mr-2`} />
                                {item.texto}
                              </span>
                            </Link>
                          </li>
                        );
                      return false;
                    })}
                  </div>
                );
              })}
          </ul>

          {this.state.userMenuSeguridad &&
            this.state.userMenuSeguridad.menu.forEach((item) => {
              item.menu.map((itemm) => {
                return favoritosMenu.push(itemm.favorito);
              });
            })}

          {this.state.userMenuSeguridad &&
            this.state.userMenuSeguridad.menu.map((item) => {
              if (item.habilitado === "1") {
                return (
                  <div key={item.id}>
                    <div className="divider" />
                    <h3>{item.texto}</h3>

                    {
                      <ul className="primary-menu">
                        {item.menu.map((item) => {
                          if (item.favorito !== "1" && item.habilitado === "1")
                            return (
                              <li
                                className="primary-menu-li flex"
                                key={item.id}
                              >
                                <Link
                                  onClick={() => this.handleClickCloseMenu()}
                                  to={
                                    "/Seguridad/" +
                                    item.tipologia +
                                    "/" +
                                    item.id
                                  }
                                >
                                  <span>
                                    <i className={`${item.icono} mr-2`} />
                                    {item.texto}
                                  </span>
                                </Link>
                                <span className="icons">
                                  <Tooltip content={item.observaciones}>
                                    <i className="fi fi-rs-interrogation mr-2" />
                                  </Tooltip>
                                  <Favoritos
                                    className="ml-2"
                                    id={item.id}
                                    operacion="AGREGAR"
                                  />
                                </span>
                              </li>
                            );
                          return false;
                        })}
                      </ul>
                    }
                  </div>
                );
              } else {
                return false;
              }
            })}
        </div>
      );
    }
  }

  renderMenuCambioContra() {
    if (this.state.menuContra) {
      return (
        <div className="menu-seguridad">
          <div className="header">
            <div className="d-flex justify-content-between align-items-center">
              <h4 className="m-0">Cambiar Contraseña</h4>
              <div
                className="close"
                onClick={() => this.handleOpenMenuCambioContra()}
              >
                <i className="fi fi-rs-cross" />
              </div>
            </div>
          </div>

          <div className="divider" />

          <ChangePasswordForm
            onSuccess={() => this.handleOpenMenuCambioContra()}
          />
        </div>
      );
    }
  }
}

const mapStateToProps = (state) => ({
  logout: state.logout,
});

export default TopNav;
