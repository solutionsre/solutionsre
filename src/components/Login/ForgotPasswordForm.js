// Generic
import React, { Component } from "react";
import store from "../../redux/store";

// Components
import URLS from "../../urls";

// Images

// Css
import "./LoginComponent.css";

const palabraSecreta = "o9szYINF1qBMihWNhNvaq4ilqUvCekxR";

class ForgotPasswordForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {},
      validation: "needs-validation",
      buttonDisable: false,
      state: 0,
    };
  }

  componentDidMount() {
    this.userInput.focus();
  }

  fetchForgotPassword() {
    this.setState({
      buttonDisable: true,
    });

    const myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");

    var CryptoJS = require("crypto-js");

    var llave = CryptoJS.enc.Base64.parse(palabraSecreta);
    var emailCyper = CryptoJS.AES.encrypt(this.state.data.email, llave, {
      mode: CryptoJS.mode.ECB,
      padding: CryptoJS.pad.Pkcs7,
    });

    const raw = JSON.stringify({
      email: emailCyper.toString(),
    });

    // console.log(raw)

    const requestOptions = {
      method: "POST",
      headers: myHeaders,
      redirect: "follow",
      body: raw,
    };

    fetch(
      store.getState().serverUrl + URLS.SEGURIDAD_RECUPERAR_CLAVE,
      requestOptions
    )
      .then((response) => response.text())
      .then((result) => {
        if (result === "") {
          this.setState({
            data: {},
            validation: "needs-validation",
            buttonDisable: false,
            state: 200,
          });
        } else {
          this.setState({
            buttonDisable: false,
            state: 400,
            errorMessages: JSON.parse(result).Mensajes,
          });
        }
      })
      .catch((error) => console.log("error", error));
  }

  handleInput(e) {
    const { value, name } = e.target;
    this.setState({
      data: {
        ...this.state.data,
        [name]: value,
      },
    });
  }

  handleCloseErroModal() {
    this.setState({
      state: 0,
      buttonDisable: false,
    });
  }

  handleCloseSuccessModal() {
    this.setState({
      state: 0,
      buttonDisable: false,
    });
    this.props.handleForgotPassword(false);
  }

  handleValidation() {
    var validation = true;
    // var emailRegex = /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;
    // if (!emailRegex.test(this.state.data.email)) {
    //     return false;
    // }
    if (!this.state.data.email) {
      this.setState({
        validation: "needs-validation was-validated",
      });
      validation = false;
    }

    return validation;
  }

  handleSubmit() {
    this.setState({
      validation: "needs-validation was-validated",
    });
    if (this.handleValidation()) {
      this.fetchForgotPassword();
    }
  }

  handleForgotPassword() {
    this.props.handleForgotPassword(false);
  }

  render() {
    switch (this.state.state) {
      case 400:
        return this.renderErrorModal();
      case 200:
        return this.renderSuccessModal();
      default:
        return this.renderForgotPasswordModal();
    }
  }

  renderErrorModal() {
    return (
      <div className="h-100 d-flex flex-column justify-content-around">
        <div className="error d-flex flex-column justify-content-center align-items-center">
          <div className="iconContainer">
            <i className="fi fi-rs-cross" />
          </div>
          <p>Ocurrió un error</p>

          {this.state.errorMessages
            ? this.state.errorMessages.map((error, i) => {
                return (
                  <div className="alert alert-danger" role="alert" key={i}>
                    <div className="wordBreak">
                      {error.mensaje}
                    </div>
                  </div>
                );
              })
            : null}
        </div>

        <div className="d-flex justify-content-center align-items-stretch">
          <button
            disabled={this.state.buttonDisable}
            className="btn btn-primary"
            onClick={() => this.handleCloseErroModal()}
          >
            Volver
          </button>
        </div>
      </div>
    );
  }

  renderSuccessModal() {
    return (
      <div className="h-100 d-flex flex-column justify-content-around">
        <div className="success d-flex flex-column justify-content-center align-items-center">
          <div className="iconContainer">
            <i className="fi fi-rs-social-network" />
          </div>
          <p>
            Se enviará un correo electrónico a la dirección especificada si se
            registra una cuenta en la misma.
          </p>
        </div>

        <div className="d-flex justify-content-center align-items-stretch">
          <button
            disabled={this.state.buttonDisable}
            className="btn btn-primary"
            onClick={() => this.handleCloseSuccessModal()}
          >
            Continuar
          </button>
        </div>
      </div>
    );
  }

  renderForgotPasswordModal() {
    return (
      <div>
        <h1 className="global-title-1"> Ayuda con la contraseña </h1>
        <p className="global-text-1">
          {" "}
          Introduce tu email para recuperar tu contraseña. Recibirás un correo
          con instrucciones.{" "}
        </p>

        <form
          onSubmit={() => this.fetchForgotPassword()}
          className={this.state.validation}
          noValidate
          autoComplete="off"
        >
          <div className="form-group">
            <label htmlFor="email"> Email </label>
            <input
              ref={(input) => {
                this.userInput = input;
              }}
              type="text"
              autoComplete="off"
              className="form-control"
              id="email"
              name="email"
              placeholder="Email"
              onChange={(e) => this.handleInput(e)}
              value={this.state.data.email}
              required
            />
            <div className="invalid-feedback"> Ingresa tu Email </div>
          </div>

          {this.renderButtonsAction()}

          <div
            className="btn-secondary mt-5 mb-5 d-inline-block text-right d-flex justify-content-between align-items-center"
            onClick={() => this.handleForgotPassword()}
          >
            Volver al inicio de sesión <i className="fi fi-rs-arrow-small-left mr-1" />
          </div>
        </form>
      </div>
    );
  }

  renderButtonsAction() {
    return this.state.buttonDisable ? (
      <button
        disabled={this.state.buttonDisable}
        className="btn btn-primary mt-3"
      >
        <span
          className="spinner-grow spinner-grow-sm"
          role="status"
          aria-hidden="true"
        ></span>
        <span className="sr-only">Loading...</span>
        <span
          className="spinner-grow spinner-grow-sm"
          role="status"
          aria-hidden="true"
        ></span>
        <span className="sr-only">Loading...</span>
        <span
          className="spinner-grow spinner-grow-sm"
          role="status"
          aria-hidden="true"
        ></span>
        <span className="sr-only">Loading...</span>
      </button>
    ) : (
      <button
        disabled={this.state.buttonDisable}
        className="btn btn-primary mt-3"
        onClick={() => this.fetchForgotPassword()}
      >
        Enviar
      </button>
    );
  }
}
export default ForgotPasswordForm;
