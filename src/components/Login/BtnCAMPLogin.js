import React, { useState } from "react";
import { useKeycloak } from "@react-keycloak/web";
import Loader from "../GlobalComponents/Loader";

import logoWindows from "../../assets/images/logo-windows.png";

function BtnCAMPLogin(props) {
  const { keycloak } = useKeycloak();

  if (keycloak.authenticated) {
    let data = {
      token: keycloak.token,
    };
    props.login.fetchLoginCamp(data);
  }

  const loginCAMP = async (event) => {
    event.preventDefault();
    try {
      await keycloak.login();
    } catch (error) {
      console.error("Fallo de login", error);
    }
  };

  return (
    <div>
      {keycloak.authenticated ? (
        <Loader />
      ) : (
        <>
          <button onClick={loginCAMP} className="btn btn-azure-login">
            Login con CAMP
          </button>
        </>
      )}
    </div>
  );
}

export default BtnCAMPLogin;
