// Generic
import React, { Component } from "react";
import store from "../../redux/store";

// Components
import URLS from "../../urls";

// Images

// Css
import "./LoginComponent.css";

const palabraSecreta = "o9szYINF1qBMihWNhNvaq4ilqUvCekxR";

class ChangePasswordForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {},
      validation: "needs-validation",
      buttonDisable: false,
      state: 0,
      validationsMessages: {},
      showPassword: false,
      showPasswordCH: false,
      showPasswordCF: false,
    };
  }

  componentDidMount() {
    this.currentPasswordInput.focus();
  }

  async fetchChangePassword() {
    const myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    myHeaders.append(
      "Authorization",
      "Bearer " + sessionStorage.getItem("token")
    );

    var CryptoJS = require("crypto-js");

    var llave = CryptoJS.enc.Base64.parse(palabraSecreta);
    var claveActualCyper = CryptoJS.AES.encrypt(
      this.state.data.currentPassword,
      llave,
      { mode: CryptoJS.mode.ECB, padding: CryptoJS.pad.Pkcs7 }
    );
    var claveNuevaCyper = CryptoJS.AES.encrypt(
      this.state.data.newPassword1,
      llave,
      { mode: CryptoJS.mode.ECB, padding: CryptoJS.pad.Pkcs7 }
    );
    var claveConfirmacionCyper = CryptoJS.AES.encrypt(
      this.state.data.newPassword2,
      llave,
      { mode: CryptoJS.mode.ECB, padding: CryptoJS.pad.Pkcs7 }
    );

    const userId = JSON.parse(sessionStorage.getItem("user")).interno;
    const cambiar = JSON.parse(sessionStorage.getItem("user")).cambiarclave;

    const raw = JSON.stringify({
      usuario: parseInt(userId),
      claveActual: claveActualCyper.toString(),
      claveNueva: claveNuevaCyper.toString(),
      claveConfirmacion: claveConfirmacionCyper.toString(),
      cambiar: cambiar,
    });

    const requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: raw,
      redirect: "follow",
    };

    fetch(
      store.getState().serverUrl + URLS.SEGURIDAD_CAMBIAR_CLAVE,
      requestOptions
    )
      .then((response) => response.text())
      .then((result) => {
        var token;
        var mensajes;
        var salida;

        if (result === "") {
          mensajes = "";
          token = "";
        } else {
          salida = JSON.parse(result);
          console.log(salida);
          token = salida.token;
          if (salida.Mensajes) mensajes = salida.Mensajes;
          else mensajes = "";
        }
        if (mensajes === "") {
          const user = JSON.parse(sessionStorage.getItem("user"));
          user.cambiarclave = "N";
          sessionStorage.setItem("user", JSON.stringify(user));

          if (token !== "") {
            console.log("de paso cañazo");
            sessionStorage.setItem("token", token);
          } else {
            console.log("cambio normal");
          }
          this.setState({
            data: {},
            buttonDisable: false,
            state: 200,
          });
        } else {
          this.setState({
            buttonDisable: false,
            state: 400,
            errorMessages: mensajes,
          });
        }
      })
      .catch((error) => console.log("error", error));
  }

  handleInput(e) {
    const { value, name } = e.target;
    this.currentPasswordInput.classList.remove("is-invalid");
    this.newPassword1Input.classList.remove("is-invalid");
    this.newPassword2Input.classList.remove("is-invalid");
    this.setState({
      data: {
        ...this.state.data,
        [name]: value,
      },
    });
  }

  handleCloseErroModal() {
    this.setState({
      state: 0,
      buttonDisable: false,
    });
  }

  handleValidation() {
    const validationsMessages = {};
    var validations = {
      currentPassword: true,
      newPassword1: true,
      newPassword2: true,
    };

    if (!this.state.data.currentPassword) {
      validations.currentPassword = false;
    }

    if (this.state.data.newPassword1 && this.state.data.newPassword2) {
      if (this.state.data.currentPassword === this.state.data.newPassword1) {
        validations.newPassword1 = false;
        validations.newPassword2 = false;
        validationsMessages.newPassword1 =
          "La contraseña nueva debe ser distinta a la anterior";
        validationsMessages.newPassword2 =
          "La contraseña nueva debe ser distinta a la anterior";
      } else {
        const requirements = JSON.parse(
          sessionStorage.getItem("passwordRequirements")
        );
        if (
          this.state.data.newPassword1.length >= requirements.largominimo &&
          this.state.data.newPassword1.length <= requirements.largomaximo
        ) {
          var regex = "";

          const value = this.state.data.newPassword1;

          if (parseInt(requirements.numeros)) {
            regex = /^(?=.*\d)/;
            if (!value.match(regex)) {
              validations.newPassword1 = false;
              validations.newPassword2 = false;
              validationsMessages.newPassword1 =
                "La contraseña debe tener numeros";
              validationsMessages.newPassword2 =
                "La contraseña debe tener numeros";
            }
          }

          if (parseInt(requirements.letras)) {
            regex = /^(?=.*[A-z])/;
            if (!value.match(regex)) {
              validations.newPassword1 = false;
              validations.newPassword2 = false;
              validationsMessages.newPassword1 =
                "La contraseña debe tener letras";
              validationsMessages.newPassword2 =
                "La contraseña debe tener letras";
            }
          }

          if (parseInt(requirements.simbolos)) {
            regex = /^(?=.*?[!@#$%^&*+`~=?|<>/])/;
            if (!value.match(regex)) {
              validations.newPassword1 = false;
              validations.newPassword2 = false;
              validationsMessages.newPassword1 =
                "La contraseña debe tener simbolos";
              validationsMessages.newPassword2 =
                "La contraseña debe tener simbolos";
            }
          }

          if (requirements.espacios === "N") {
            regex = /^(?!.* )/;
            if (!value.match(regex)) {
              validations.newPassword1 = false;
              validations.newPassword2 = false;
              validationsMessages.newPassword1 =
                "La contraseña no debe tener espacios";
              validationsMessages.newPassword2 =
                "La contraseña no debe tener espacios";
            }
          }

          if (this.state.data.newPassword1 !== this.state.data.newPassword2) {
            validations.newPassword2 = false;
            validationsMessages.newPassword2 = "Las contraseñas no coinciden";
          }
        } else {
          validations.newPassword1 = false;
          validations.newPassword2 = false;
          validationsMessages.newPassword1 =
            "La contraseña debe tener longitud entre " +
            requirements.largominimo +
            " y " +
            requirements.largomaximo +
            " caracteres.";
          validationsMessages.newPassword2 =
            "La contraseña debe tener longitud entre " +
            requirements.largominimo +
            " y " +
            requirements.largomaximo +
            " caracteres.";
        }
      }
    } else {
      if (!this.state.data.newPassword1) {
        validations.newPassword1 = false;
        validationsMessages.newPassword1 = "Ingresa tu contraseña";
      }
      if (!this.state.data.newPassword2) {
        validations.newPassword2 = false;
        validationsMessages.newPassword2 = "Ingresa tu contraseña nuevamente";
      }
    }

    if (validations.currentPassword) {
      this.currentPasswordInput.classList.add("is-valid");
    } else {
      this.currentPasswordInput.classList.add("is-invalid");
    }
    if (validations.newPassword1) {
      this.newPassword1Input.classList.add("is-valid");
    } else {
      this.newPassword1Input.classList.add("is-invalid");
    }
    if (validations.newPassword2) {
      this.newPassword2Input.classList.add("is-valid");
    } else {
      this.newPassword2Input.classList.add("is-invalid");
    }

    this.setState({
      validationsMessages: validationsMessages,
    });

    return (
      validations.currentPassword &&
      validations.newPassword1 &&
      validations.newPassword2
    );
  }

  handleSubmit() {
    this.setState({
      buttonDisable: true,
      validation: "needs-validation was-validated",
    });

    if (this.handleValidation()) {
      this.fetchChangePassword();
    }

    this.setState({
      buttonDisable: false,
    });
  }

  handleClickShowPassword = () => {
    if (this.state.showPassword) {
      this.setState({
        showPassword: false,
      });
    } else {
      this.setState({
        showPassword: true,
      });
    }
  };

  handleClickShowPasswordCH = () => {
    if (this.state.showPasswordCH) {
      this.setState({
        showPasswordCH: false,
      });
    } else {
      this.setState({
        showPasswordCH: true,
      });
    }
  };

  handleClickShowPasswordCF = () => {
    if (this.state.showPasswordCF) {
      this.setState({
        showPasswordCF: false,
      });
    } else {
      this.setState({
        showPasswordCF: true,
      });
    }
  };

  render() {
    switch (this.state.state) {
      case 400:
        return this.renderErrorModal();
      case 200:
        return this.renderSuccessModal();
      default:
        return this.renderChangePasswordModal();
    }
  }

  renderErrorModal() {
    return (
      <div className="h-100 d-flex flex-column justify-content-around">
        <div className=" error d-flex flex-column justify-content-center align-items-center">
          <div className="iconContainer">
            <i className="fi fi-rs-cross" />
          </div>
          <p>Ocurrió un error</p>

          {this.state.errorMessages
            ? this.state.errorMessages.map((error, i) => {
                return (
                  <div className="alert alert-danger" role="alert" key={i}>
                    <div className="wordBreak">
                      {error.mensaje}
                    </div>
                  </div>
                );
              })
            : null}
        </div>

        <div className="d-flex justify-content-center align-items-stretch">
          <button
            disabled={this.state.buttonDisable}
            className="btn btn-primary"
            onClick={() => this.handleCloseErroModal()}
          >
            Volver
          </button>
        </div>
      </div>
    );
  }

  renderSuccessModal() {
    return (
      <div className="h-100 d-flex flex-column justify-content-around">
        <div className="success d-flex flex-column justify-content-center align-items-center">
          <div className="iconContainer">
            <i className="fi fi-rs-social-network" />
          </div>
          <p>Se actualizó la contraseña con éxito!</p>
        </div>

        <div className="d-flex justify-content-center align-items-stretch">
          <button
            disabled={this.state.buttonDisable}
            className="btn btn-primary"
            onClick={() => this.props.onSuccess()}
          >
            Continuar
          </button>
        </div>
      </div>
    );
  }

  renderChangePasswordModal() {
    return (
      <div>
        <h1 className="global-title-1 mb-2"> {this.props.title} </h1>
        <p className="global-text-1">
          {" "}
          Ingresa tu contraseña actual y luego ingresa tu nueva contraseña{" "}
        </p>
        <div>
          <div className="form-group">
            <label htmlFor="currentPassword">Contraseña actual</label>
            <div className="password-contenedor">
              <input
                ref={(input) => {
                  this.currentPasswordInput = input;
                }}
                type={this.state.showPassword ? "text" : "password"}
                autoComplete="off"
                className="form-control"
                id="currentPassword"
                name="currentPassword"
                placeholder="******"
                onChange={(e) => this.handleInput(e)}
                value={this.state.data.currentPassword}
                required
              />
              <i
                onClick={this.handleClickShowPassword}
                class={
                  this.state.showPassword ? "fi fi-rs-crossed-eye" : "fi fi-rs-eye"
                }
              ></i>
            </div>
            <div className="invalid-feedback">
              {" "}
              Ingresa tu contraseña actual{" "}
            </div>
          </div>
          <div className="form-group">
            <label htmlFor="newPassword1"> Contraseña nueva </label>
            <div className="password-contenedor">
              <input
                ref={(input) => {
                  this.newPassword1Input = input;
                }}
                type={this.state.showPasswordCH ? "text" : "password"}
                autoComplete="off"
                className="form-control"
                id="newPassword1"
                name="newPassword1"
                placeholder="******"
                onChange={(e) => this.handleInput(e)}
                value={this.state.data.newPassword1}
                required
              />
              <i
                onClick={this.handleClickShowPasswordCH}
                class={
                  this.state.showPasswordCH ? "fi fi-rs-crossed-eye" : "fi fi-rs-eye"
                }
              ></i>
            </div>
            <div className="invalid-feedback">
              {" "}
              {this.state.validationsMessages.newPassword1}{" "}
            </div>
          </div>
          <div className="form-group">
            <label htmlFor="newPassword2"> Repite contraseña nueva </label>
            <div className="password-contenedor">
              <input
                ref={(input) => {
                  this.newPassword2Input = input;
                }}
                type={this.state.showPasswordCF ? "text" : "password"}
                autoComplete="off"
                className="form-control"
                id="newPassword2"
                name="newPassword2"
                placeholder="******"
                onChange={(e) => this.handleInput(e)}
                value={this.state.data.newPassword2}
                required
              />
              <i
                onClick={this.handleClickShowPasswordCF}
                class={
                  this.state.showPasswordCF ? "fi fi-rs-crossed-eye" : "fi fi-rs-eye"
                }
              ></i>
            </div>
            <div className="invalid-feedback">
              {" "}
              {this.state.validationsMessages.newPassword2}{" "}
            </div>
          </div>
          {this.renderButtonsAction()}
        </div>
      </div>
    );
  }

  renderButtonsAction() {
    return this.state.buttonDisable ? (
      <button
        disabled={this.state.buttonDisable}
        className="btn btn-primary mt-3"
      >
        <span
          className="spinner-grow spinner-grow-sm"
          role="status"
          aria-hidden="true"
        ></span>
        <span className="sr-only"> Loading... </span>
        <span
          className="spinner-grow spinner-grow-sm"
          role="status"
          aria-hidden="true"
        ></span>
        <span className="sr-only"> Loading... </span>
        <span
          className="spinner-grow spinner-grow-sm"
          role="status"
          aria-hidden="true"
        ></span>
        <span className="sr-only"> Loading... </span>
      </button>
    ) : (
      <button
        disabled={this.state.buttonDisable}
        className="btn btn-primary mt-3"
        onClick={() => this.handleSubmit()}
      >
        Modificar contraseña
      </button>
    );
  }
}

export default ChangePasswordForm;
