// Generic
import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import $ from "jquery";

// Components
import ChangePasswordForm from "./ChangePasswordForm";

// Images
import bg1 from "../../assets/images/login-bg-1.jpg";
import bg2 from "../../assets/images/login-bg-2.jpg";
import bg3 from "../../assets/images/login-bg-3.jpg";
import bg4 from "../../assets/images/login-bg-4.jpg";
import logobig from "../../assets/images/logo-big.png";

// Css
import "./LoginComponent.css";

class ChangePasswordComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      redirect: false,
    };
  }

  componentDidMount() {
    var count = 0;
    var images = [bg1, bg2, bg3, bg4];
    var image = $(".fader");

    image.css("background-image", "url(" + images[count] + ")");

    setInterval(function () {
      image.fadeOut(500, function () {
        image.css("background-image", "url(" + images[count++] + ")");
        image.fadeIn(500);
      });
      if (count === images.length) {
        count = 0;
      }
    }, 5000);
  }

  handleRedirect() {
    this.setState({
      redirect: true,
    });
  }

  render() {
    const defaultId = JSON.parse(sessionStorage.getItem("defaultId"));
    return this.state.redirect ? (
      <Redirect
        to={
          "/" +
          JSON.parse(sessionStorage.getItem("defaultApp")).descripcion +
          "/Dashboard/" +
          defaultId
        }
      />
    ) : (
      <section className="login">
        <div className="fader">
          <img className="img-login-logo" src={logobig} alt="Solutions Malls" />
        </div>

        <div className="box-login">
          <img
            className="d-xl-none d-lg-none d-sm-block d-md-none d-block ml-auto mr-auto mb-4"
            src={logobig}
            alt="Solutions Malls"
          />
          <ChangePasswordForm
            title="Cambiar Contraseña"
            onSuccess={() => this.handleRedirect()}
          />
        </div>
      </section>
    );
  }
}
export default ChangePasswordComponent;
