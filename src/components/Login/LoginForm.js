// Generic
import React, { Component } from "react";
import store from "../../redux/store";

// Components
import URLS from "../../urls";
import BtnAzureLogin from "./BtnAzureLogin";
import BtnCAMPLogin from "./BtnCAMPLogin";

// Css
import "./LoginComponent.css";
import { Image } from "react-konva";

const palabraSecreta = "o9szYINF1qBMihWNhNvaq4ilqUvCekxR";

class LoginForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {},
      state: 0,
      validation: "needs-validation",
      buttonDisable: false,
      LogoPrincipal: null,
      showPassword: false,
      botonCampEnable: false,
    };
    if (document.getElementById("OmuniWebChat")!= undefined)
      document.getElementById("OmuniWebChat").style.visibility = "hidden";
  }

  componentDidMount() {
    this.fetchIniciales();

    this.userInput.focus();
  }

  fetchIniciales() {
    fetch("./url.json")
      .then((res) => res.json())
      .then(async (data) => {
        this.setState({
          botonCampEnable: data.Camp_LoginEnabled,
        });
      });

    const myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");

    const requestOptions = {
      method: "POST",
      headers: myHeaders,
      redirect: "follow",
    };

    fetch(
      store.getState().serverUrl + URLS.CONFIGURACION_INICIAL,
      requestOptions
    )
      .then((response) => response.json())
      .then((result) => {
        this.setState({
          LogoPrincipal: result.configuracion.logoprincipal,
        });
        sessionStorage.setItem(
          "passwordRequirements",
          JSON.stringify(result.contrasenias)
        );
        sessionStorage.setItem(
          "applicationInfo",
          JSON.stringify(result.informacion)
        );
        sessionStorage.setItem(
          "ticketera",
          result.configuracion.webchat_ticketera
        );

        if (result.Mensajes) {
          this.setState({
            state: 400,
            buttonDisable: false,
            errorMessages: result.Mensajes,
          });
        }
      })
      .catch((error) => console.log("error", error));
  }

  async fetchLogin() {
    this.setState({
      buttonDisable: true,
    });

    const myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");

    var CryptoJS = require("crypto-js");

    var llave = CryptoJS.enc.Base64.parse(palabraSecreta);
    var usuarioCyper = CryptoJS.AES.encrypt(this.state.data.user, llave, {
      mode: CryptoJS.mode.ECB,
      padding: CryptoJS.pad.Pkcs7,
    });
    var claveCyper = CryptoJS.AES.encrypt(this.state.data.password, llave, {
      mode: CryptoJS.mode.ECB,
      padding: CryptoJS.pad.Pkcs7,
    });

    const raw = {
      usuario: usuarioCyper.toString(),
      clave: claveCyper.toString(),
    };

    const requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: JSON.stringify(raw),
      redirect: "follow",
    };

    fetch(
      store.getState().serverUrl + URLS.AUTENTICACION_VALIDAR,
      requestOptions
    )
      .then((response) => {
        return response.json();
      })
      .then((result) => {
        /*
        if (result.ingresoEstado) {
          if (result.ingresoEstado === "CAMBIAR"){
            sessionStorage.setItem("isAuth", true);
            sessionStorage.setItem("user", JSON.stringify(result.usuario));
            
            this.props.onSuccess(true);
          }
        }else*/
        if (result.token) {
          const defaultLanguage = result.usuario.idiomas.find(
            (lang) => lang.defecto === "S"
          );

          sessionStorage.setItem("isAuth", true);
          sessionStorage.setItem("user", JSON.stringify(result.usuario));
          sessionStorage.setItem("token", result.token);
          sessionStorage.setItem(
            "languages",
            JSON.stringify(result.usuario.idiomas)
          );

          sessionStorage.setItem(
            "defaultLanguage",
            JSON.stringify(defaultLanguage)
          );
          this.fetchMenu(result.usuario.alertar);
        }

        if (result.Mensajes) {
          this.setState({
            state: 400,
            buttonDisable: false,
            errorMessages: result.Mensajes,
          });
        }
      })
      .catch((error) => console.log("error", error));
  }

  // Funcion que loguea con los datos de Azure
  async fetchLoginAzure(props) {
    this.setState({
      isAuthenticated: true,
      data: {
        user: props.username,
        token: props.token,
      },
    });

    const myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");

    var CryptoJS = require("crypto-js");

    var llave = CryptoJS.enc.Base64.parse(palabraSecreta);
    var usuarioCyper = CryptoJS.AES.encrypt(this.state.data.user, llave, {
      mode: CryptoJS.mode.ECB,
      padding: CryptoJS.pad.Pkcs7,
    });
    var tokenCyper = CryptoJS.AES.encrypt(this.state.data.token, llave, {
      mode: CryptoJS.mode.ECB,
      padding: CryptoJS.pad.Pkcs7,
    });

    const raw = {
      usuario: usuarioCyper.toString(),
      token: tokenCyper.toString(),
    };

    const requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: JSON.stringify(raw),
      redirect: "follow",
    };

    fetch(
      store.getState().serverUrl + URLS.AUTENTICACION_VALIDAR_AZURE,
      requestOptions
    )
      .then((response) => response.json())
      .then((result) => {
        if (result.token) {
          const defaultLanguage = result.usuario.idiomas.find(
            (lang) => lang.defecto === "S"
          );

          // cambiamos el cambiarclave
          result.usuario.cambiarclave = "N";

          sessionStorage.setItem("isAuth", true);
          sessionStorage.setItem("user", JSON.stringify(result.usuario));
          sessionStorage.setItem("token", result.token);
          sessionStorage.setItem(
            "languages",
            JSON.stringify(result.usuario.idiomas)
          );
          sessionStorage.setItem(
            "defaultLanguage",
            JSON.stringify(defaultLanguage)
          );
          sessionStorage.setItem("loginAzure", true);

          this.fetchMenu(result.usuario.alertar);
        }

        if (result.Mensajes) {
          this.setState({
            state: 400,
            buttonDisable: false,
            errorMessages: result.Mensajes,
          });
        }
      })
      .catch((error) => console.log("error", error));
  }

  // Funcion que loguea con los datos de CAMP
  async fetchLoginCamp(props) {
    const myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");

    var CryptoJS = require("crypto-js");

    var llave = CryptoJS.enc.Base64.parse(palabraSecreta);
    var tokenCyper = CryptoJS.AES.encrypt(props.token, llave, {
      mode: CryptoJS.mode.ECB,
      padding: CryptoJS.pad.Pkcs7,
    });

    const raw = {
      token: tokenCyper.toString(),
    };

    const requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: JSON.stringify(raw),
      redirect: "follow",
    };

    fetch(
      store.getState().serverUrl + URLS.AUTENTICACION_VALIDAR_CAMP,
      requestOptions
    )
      .then((response) => response.json())
      .then((result) => {
        if (result.token) {
          const defaultLanguage = result.usuario.idiomas.find(
            (lang) => lang.defecto === "S"
          );

          // cambiamos el cambiarclave
          result.usuario.cambiarclave = "N";

          sessionStorage.setItem("isAuth", true);
          sessionStorage.setItem("user", JSON.stringify(result.usuario));
          sessionStorage.setItem("token", result.token);
          sessionStorage.setItem(
            "languages",
            JSON.stringify(result.usuario.idiomas)
          );
          sessionStorage.setItem(
            "defaultLanguage",
            JSON.stringify(defaultLanguage)
          );
          sessionStorage.setItem("loginCamp", true);

          this.fetchMenu(result.usuario.alertar);
        }

        if (result.Mensajes) {
          this.setState({
            state: 400,
            buttonDisable: false,
            errorMessages: result.Mensajes,
          });
        }
      })
      .catch((error) => console.log("error", error));
  }

  fetchMenu(alertar) {
    if (
      sessionStorage.getItem("ticketera") !== undefined &&
      sessionStorage.getItem("ticketera") === "SI"
    ) {
      if (document.getElementById("OmuniWebChat")!= undefined)
        document.getElementById("OmuniWebChat").style.visibility = "visible";
    } else {
      if (document.getElementById("OmuniWebChat")!= undefined)
        document.getElementById("OmuniWebChat").style.visibility = "hidden";
    }

    var myHeaders = new Headers();
    myHeaders.append(
      "Authorization",
      "Bearer " + sessionStorage.getItem("token")
    );
    myHeaders.append("Content-Type", "application/json");

    var requestOptions = {
      method: "GET",
      headers: myHeaders,
      redirect: "follow",
    };

    fetch(
      store.getState().serverUrl + URLS.AUTENTICACION_OBTENER_MENU,
      requestOptions
    )
      .then((response) => response.json())
      .then((result) => {
        const defaultApp = result.menu.principal[0].aplicativos.find(
          (app) => app.defecto === "S"
        );

        const defaultSeg = result.menu.principal[1].seguridad.find(
          (app) => app.defecto === "S"
        );

        sessionStorage.setItem("userMenu", JSON.stringify(result.menu));

        if (defaultApp !== undefined) {
          sessionStorage.setItem("defaultId", defaultApp.id);
          sessionStorage.setItem("defaultApp", JSON.stringify(defaultApp));
        } else {
          sessionStorage.setItem("defaultId", defaultSeg.id);
          sessionStorage.setItem("defaultApp", JSON.stringify(defaultSeg));
        }

        if (alertar) {
          this.setState({
            state: 401,
            buttonDisable: false,
            errorMessages: alertar,
          });
        } else {
          this.props.onSuccess(false);
        }
      })
      .catch((error) => console.log("error", error));
  }

  handleInput(e) {
    const { value, name } = e.target;
    this.setState({
      data: {
        ...this.state.data,
        [name]: value,
      },
    });
  }

  handleCloseErroModal() {
    this.setState({
      state: 0,
      buttonDisable: false,
    });
  }

  handleIngresarModal() {
    this.setState({
      state: 0,
    });
    this.props.onSuccess(false);
  }

  handleCambiarModal() {
    this.setState({
      state: 0,
    });
    this.props.onSuccess(true);
  }

  handleValidation() {
    var validation = true;
    // var emailRegex = /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;
    // if (!emailRegex.test(this.state.data.user)) {
    //     return false;
    // }
    if (!this.state.data.user || !this.state.data.password) {
      validation = false;
    }

    return validation;
  }

  handleSubmit() {
    this.setState({
      validation: "needs-validation was-validated",
    });
    if (this.handleValidation()) {
      this.fetchLogin();
    }
  }

  handleForgotPassword() {
    this.props.handleForgotPassword(true);
  }

  render() {
    switch (this.state.state) {
      case 401:
        return this.renderAlertarModal();
      case 400:
        return this.renderErrorModal();
      default:
        return this.renderLoginModal();
    }
  }

  renderErrorModal() {
    return (
      <div className="h-100 d-flex flex-column justify-content-around">
        <div className="error d-flex flex-column justify-content-center align-items-center">
          <div className="iconContainer">
            <i className="fi fi-rs-cross" />
          </div>
          <p>Ocurrió un error</p>

          {this.state.errorMessages
            ? this.state.errorMessages.map((error, i) => {
                return (
                  <div className="alert alert-danger" role="alert" key={i}>
                    <div className="wordBreak">
                      {error.mensaje}
                    </div>
                  </div>
                );
              })
            : null}
        </div>

        <div className="d-flex justify-content-center align-items-stretch">
          <button
            disabled={this.state.buttonDisable}
            className="btn btn-primary"
            onClick={() => this.handleCloseErroModal()}
          >
            Volver
          </button>
        </div>
      </div>
    );
  }

  renderAlertarModal() {
    return (
      <div className="h-100 d-flex flex-column justify-content-around">
        <div className="success d-flex flex-column justify-content-center align-items-center">
          {/* succes? */}
          <div className="iconContainer">
            <i className="fi fi-rs-social-network"></i>
          </div>

          <div className="alert alert-warning" role="alert">
            <div style={{ wordBreak: "break-word" }}>
              {this.state.errorMessages}
            </div>
          </div>
        </div>

        <div className="d-flex justify-content-center align-items-stretch">
          <button
            className="btn btn-primary"
            onClick={() => this.handleCambiarModal()}
          >
            Cambiar
          </button>
          &nbsp;
          <button
            className="btn btn-primary"
            onClick={() => this.handleIngresarModal()}
          >
            Ingresar
          </button>
        </div>
      </div>
    );
  }

  handleClickShowPassword = () => {
    if (this.state.showPassword) {
      this.setState({
        showPassword: false,
      });
    } else {
      this.setState({
        showPassword: true,
      });
    }
  };

  handleMouseDownPassword = (event) => {
    event.preventDefault();
  };
  onKeyPress = (e) => {
    if (e.which === 13) {
      this.handleSubmit();
    }
  };
  renderLoginModal() {
    return (
      <>
        <div className="title">
          <h1 className="global-title-1">Inicia Sesión</h1>
          <p className="global-text-1">
            Completa el formulario con tus datos para iniciar sesión
          </p>
        </div>

        <div className="form-group">
          <label htmlFor="user"> Usuario </label>
          <input
            ref={(input) => {
              this.userInput = input;
            }}
            type="text"
            autoComplete="off"
            className="form-control"
            id="user"
            name="user"
            placeholder="Nombre de usuario / email"
            onChange={(e) => this.handleInput(e)}
            value={this.state.data.user}
            required
            on
            tabIndex="0"
          />
          <div className="invalid-feedback"> Ingresa tu usuario </div>
        </div>

        <div className="form-group">
          <label htmlFor="password"> Contraseña </label>

          <div className="password-contenedor">
            <input
              type={this.state.showPassword ? "text" : "password"}
              autoComplete="off"
              className="form-control"
              id="password"
              name="password"
              placeholder="******"
              onChange={(e) => this.handleInput(e)}
              onKeyDown={this.onKeyPress}
              value={this.state.data.password}
              required
            />
            <i
              onClick={this.handleClickShowPassword}
              class={
                this.state.showPassword
                  ? "fi fi-rs-crossed-eye"
                  : "fi fi-rs-eye"
              }
            ></i>
          </div>
          <div className="invalid-feedback"> Ingresa tu contraseña </div>
        </div>
        {/* <div className="form-group form-check">
                      <input type="checkbox" className="form-check-input" id="exampleCheck1" />
                      <label className="form-check-label" htmlFor="exampleCheck1"> Mantenerme conectado </label>
                  </div> */}

        {this.renderButtonsAction()}

        <div
          className="btn-secondary d-inline-block text-right d-flex justify-content-between align-items-center"
          onClick={() => this.handleForgotPassword()}
        >
          Olvidé mi contraseña <i className="fi fi-rs-arrow-small-right mr-1" />
        </div>
        <div className="alternativeLogin">
          <BtnAzureLogin login={this} />
          {this.state.botonCampEnable ? <BtnCAMPLogin login={this} /> : null}
        </div>

        {this.state.LogoPrincipal ? (
          <div className=" logoPrincipal">
            <img src={`data:image/jpeg;base64,${this.state.LogoPrincipal}`} />
          </div>
        ) : null}
      </>
    );
  }

  renderButtonsAction() {
    return this.state.buttonDisable ? (
      <button
        disabled={this.state.buttonDisable}
        className="btn btn-primary mt-3"
      >
        <span
          className="spinner-grow spinner-grow-sm"
          role="status"
          aria-hidden="true"
        ></span>
        <span className="sr-only">Loading...</span>
        <span
          className="spinner-grow spinner-grow-sm"
          role="status"
          aria-hidden="true"
        ></span>
        <span className="sr-only">Loading...</span>
        <span
          className="spinner-grow spinner-grow-sm"
          role="status"
          aria-hidden="true"
        ></span>
        <span className="sr-only">Loading...</span>
      </button>
    ) : (
      <button
        disabled={this.state.buttonDisable}
        className="btn btn-primary"
        onClick={() => this.handleSubmit()}
      >
        Iniciar Sesión
      </button>
    );
  }
}
export default LoginForm;
