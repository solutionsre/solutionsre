import { PublicClientApplication } from "@azure/msal-browser";
import React, { Component } from "react";
import Loader from "../GlobalComponents/Loader";

import logoWindows from "../../assets/images/logo-windows.png";

class BtnAzureLogin extends Component {
  constructor(props) {
    super(props);
    this.state = {
      errr: null,
      isAuthenticated: false,
      user: {},
      azureLoginEnabled: true,
      dataAzure: [],
      listo: false,
    };
    this.loginAzure = this.loginAzure.bind(this);
  }

  componentDidMount() {
    //console.log('Cargado!')
    fetch("./url.json")
      .then((res) => res.json())
      .then(async (data) => {
        //console.log(data)
        /////////////////////////////////////////////////////////////////
        var Azures = new PublicClientApplication({
          auth: {
            clientId: data.Azure_appId,
            redirectUri: data.Azure_redirectUri,
            authority: data.Azure_authority,
          },
          cache: {
            cacheLocation: "sessionStorage",
            storeAuthStateInCookie: true,
          },
        });
        //console.log(Azures)
        this.PublicClientApplication = Azures;
        ////////////////////////////////////////////////////////////////

        this.setState({
          listo: true,
          dataAzure: data,
        });
      })
      .catch((err) => console.log(err, " error"));
  }

  async loginAzure() {
    try {
      // pop up login
      await this.PublicClientApplication.loginPopup({
        scopes: this.state.dataAzure.Azure_scopes,
        prompt: "select_account",
      });
      this.setState({
        isAuthenticated: true,
      });

      let acount = this.PublicClientApplication.getAllAccounts();
      let username = acount[0].username;
      let token = acount[0].idTokenClaims.aio;

      let data = {
        username: username,
        token: token,
      };

      this.props.login.fetchLoginAzure(data);
    } catch (err) {
      this.setState({
        isAuthenticated: false,
        user: {},
        error: err,
      });
    }
  }

  render() {
    if (this.state.dataAzure.Azure_LoginEnabled) {
      return (
        <div>
          {this.state.isAuthenticated ? (
            <Loader />
          ) : this.state.listo ? (
            <>
              <button
                onClick={() => this.loginAzure()}
                className="btn btn-azure-login"
              >
                <i class="fab fa-windows"></i>
                Login con Azure AD
              </button>
            </>
          ) : (
            <Loader />
          )}
        </div>
      );
    } else {
      return <div></div>;
    }
  }
}

export default BtnAzureLogin;
