import React, { useState } from "react";
import { useKeycloak } from "@react-keycloak/web";
import { Redirect } from "react-router-dom";

function BtnCAMPLogout(props) {
    
    const { keycloak } = useKeycloak();

    const salida = false;

    if (keycloak.authenticated && props.realizarLogout) {
      try {
        keycloak.logout();
        sessionStorage.clear();
        salida = true;
      } catch (error) {
        console.error("Fallo de logout", error);
      }
      
    }
  
    return salida ? (
      <Redirect to="/login" />
    ) : (
      <div>      
      </div>
    )


  }
  
  export default BtnCAMPLogout;
  