//import { CollectionView } from "wijmo/wijmo";
import store from "../redux/store";
import URLS from "../urls";

export const cabeceras = (rawData, cabecera) => {
  store.dispatch({
    type: "SET_LOADING_DATA",
    payload: true,
  });
  
  var actionUrl;
  if (cabecera === "S") {
    actionUrl = store.getState().serverUrl + URLS.CABECERAS;

    if (actionUrl) {
      var myHeaders;
      var raw;
      var requestOptions;
  
      myHeaders = new Headers();
      myHeaders.append("Authorization", "Bearer " + sessionStorage.getItem("token"));
      myHeaders.append("Content-Type", "application/json");
  
      raw = rawData;
      requestOptions = {
        method: "POST",
        headers: myHeaders,
        body: raw,
        redirect: "follow",
      };
  
      fetch(actionUrl, requestOptions)
        .then((response) => {
          if (response.status == 401) {
            sessionStorage.clear();
            window.location.reload(false);
          }else{
            return response.json();
          }
        })
        .then((result) => {

          store.dispatch({
            type: "SET_CABECERA_GRILLA",
            payload: result,
          });
          
         obtenerInformacion(rawData);
        });
    }
  }else{
    obtenerInformacion(rawData);
  }
};


export const obtenerInformacion = (rawData) => {
  store.dispatch({
    type: "SET_LOADING_DATA",
    payload: true,
  });

  var actionUrl;
  
  actionUrl = store.getState().serverUrl + URLS.OBTENER_INFORMACION;

  if (actionUrl) {
    var myHeaders;
    var raw;
    var requestOptions;

    myHeaders = new Headers();
    myHeaders.append("Authorization", "Bearer " + sessionStorage.getItem("token"));
    myHeaders.append("Content-Type", "application/json");

    raw = rawData;
    

    requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: raw,
      redirect: "follow",
    };
    var status;
    fetch(actionUrl, requestOptions)
      .then((response) => {
        if (response.status == 401) {
          status = response.status;
          sessionStorage.clear();
          window.location.reload(false);
        }else{
          status = response.status;
          return response.json();
        }
      })
      .then((result) => {

        if (status === 200) {
          store.dispatch({
            type: "SET_DATA",
            payload: result,
          });
        }else{
          console.log("Error: " + JSON.stringify(result));
        }
        store.dispatch({
          type: "SET_LOADING_DATA",
          payload: false,
        });
      })
      .catch((error) => console.log("error", error));
  }
};
