import { createStore } from "redux";

const initialState = {
  /* ----------------------------------------GLOBALES */
  serverUrl: "",
  rerender: [],
  loadingData: false,
  dataObInfo: [],
  dataObInfo01: [],
  dataObInfo02: [],
  cabecerasGrilla: null,
  ejecutado: false,
  refrescar: null,
  valorFiltro: null,

  /* FIN GLOBALES */

  /* ----------------------------------------INPUT */
  tipologiaInfo: new Map(),
  /* FIN INPUT */

  /* ----------------------------------------TIPOLOGIA REPORTES */
  agrupadosReportes: "",
  objetoReportes: "",
  /* FIN REPORTES */

  /* ----------------------------------------TIPOLOGIA INTERFACES */
  filtrosAplicados: false,
  itemSeleccionadoInterfaces: [],
  filtersErrors: [],
  /* FIN INTERFACES */

  /* ----------------------------------------TIPOLOGIA PROCESOS */
  itemSeleccionadoProcesos: [],
  /* FIN PROCESOS */

};

function reducer(state = initialState, action) {
  switch (action.type) {
    /* GLOBALES */
    case "SERVER_URL":
      return {
        ...state,
        serverUrl: action.serverUrl,
      };
      break;
    case "RE_RENDER":
      return {
        ...state,
        rerender: state.rerender.concat(action.rerender),
      };
      break;
    case "SET_LOADING_DATA":
      return {
        ...state,
        loadingData: action.payload,
      };
      break;
    case "SET_DATA":
      return {
        ...state,
        dataObInfo: action.payload,
      };
      case "SET_CABECERA_GRILLA":
        return {
          ...state,
          cabecerasGrilla: action.payload,
        };
    case "MISA":
      return {
        ...state,
        ejecutado: action.payload,
      };
      break;

    /* FIN GLOBALES */

    /* INPUT */
    case "SET_TIPOLOGIA_INFO":
      let tipologiaMap = state.tipologiaInfo;
      tipologiaMap.set(action.name, action.data);
      return {
        ...state,
        tipologiaInfo: tipologiaMap,    
      };
      break;

      case "SET_DEPENDENCIA_COMBO":
        return {
          ...state,
          refrescar: action.name,
          valorFiltro: action.data,    
        };
        break;
  
      /* FIN INPUT */

    /* TIPOLOGIA REPORTES */

    case "SET_AGRUPADOS_REPORTES":
      return {
        ...state,
        agrupadosReportes: action.payload,
      };
      break;
    case "SET_OBJETO_REPORTES":
      return {
        ...state,
        objetoReportes: action.payload,
      };
      break;
    /* FIN REPORTES */

    /* TIPOLOGIA INTERFACES */

    case "SET_FILTROS_APLICADOS":
      return {
        ...state,
        filtrosAplicados: action.payload,
      };
      break;
    case "SET_ITEM_SELECCIONADO_INTERFACES":
      return {
        ...state,
        itemSeleccionadoInterfaces: action.payload,
      };
      break;
      case "SET_FILTERS_ERRORS":
      return {
        ...state,
        filtersErrors: action.payload,
      };
      break;

    /* FIN INTERFACES */


    /* TIPOLOGIA PROCESOS */

    case "SET_ITEM_SELECCIONADO_PROCESOS":
      return {
        ...state,
        itemSeleccionadoProcesos: action.payload,
      };
      break;

    /* FIN PROCESOS */

    default:
      return state;
  }
}

export default createStore(
  reducer,
  initialState,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
);
