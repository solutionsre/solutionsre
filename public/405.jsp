<!DOCTYPE html>
<html lang="es" dir="ltr">
<head>
<meta charset="utf-8">
<title>405 Metodo no disponible | SolutionsMalls</title>
<link href="css/error.css" rel="stylesheet" type="text/css">
</head>
<body>
	<div id="error-page">
		<div class="content">
			<h2 class="header" data-text="405">405</h2>
			<h4 data-text="Prohibido">Metodo no disponible</h4>
			<p>Lo sentimos, pero la operacion que intenta realizar no se encuentra disponible. Contacte con su administrador para resolver el problema.</p>
			<div class="btns">
				<a href="index.html">Volver al inicio</a>
			</div>
		</div>
	</div>
</body>
</html>