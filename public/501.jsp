<!DOCTYPE html>
<html lang="es" dir="ltr">
<head>
<meta charset="utf-8">
<title>501 Metodo desconocido | SolutionsMalls</title>
<link href="css/error.css" rel="stylesheet" type="text/css">
</head>
<body>
	<div id="error-page">
		<div class="content">
			<h2 class="header" data-text="404">404</h2>
			<h4 data-text="Metodo desconocido">Metodo desconocido</h4>
			<p>Lo sentimos, el metodo que desea invocar no existe. Contacte con su administrador para resolver el problema.</p>
			<div class="btns">
				<a href="index.html">Volver al inicio</a>
			</div>
		</div>
	</div>
</body>
</html>