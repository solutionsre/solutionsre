<!DOCTYPE html>
<html lang="es" dir="ltr">
<head>
<meta charset="utf-8">
<title>403 Prohibido | SolutionsMalls</title>
<link href="css/error.css" rel="stylesheet" type="text/css">
</head>
<body>
	<div id="error-page">
		<div class="content">
			<h2 class="header" data-text="403">403</h2>
			<h4 data-text="Prohibido">Prohibido</h4>
			<p>Lo sentimos, pero este recurso no se encuentra disponible. Contacte con su administrador para resolver el problema.</p>
			<div class="btns">
				<a href="index.html">Volver al inicio</a>
			</div>
		</div>
	</div>
</body>
</html>